/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para crear los triggers
  ------------------------------------------------------------------------------------------------------------------------------------
*/

/* Triggers de para mayusculas */

/* Institucion */
create trigger upper_institucion
    on cp_institucion
    for insert,update
    as
    update cp_institucion set idinstituto = upper(idinstituto), descripcion = upper(descripcion)
;

/* Escolaridad del personal */
create trigger upper_escolaridad_p
    on cp_escolaridad_p
    for insert,update
    as
    update cp_escolaridad_p set descripcion = upper(descripcion)
;
create trigger upper_personal_esc
    on cp_personal_esc
    for insert,update
    as
    update cp_personal_esc set rfc = upper(rfc), estado = upper(estado)
;

/* Subdireccion - Departamento - Puesto */
create trigger upper_subdireccion
    on cp_subdireccion
    for insert,update
    as
    update cp_subdireccion set idsub = upper(idsub), descripcion = upper(descripcion), observaciones = upper(observaciones)
;
create trigger upper_departamento
    on cp_departamento
    for insert,update
    as
    update cp_departamento set iddepto = upper(iddepto), descripcion = upper(descripcion), observaciones = upper(observaciones)
;
create trigger upper_puesto
    on cp_puesto
    for insert,update
    as
    update cp_puesto set idpuesto = upper(idpuesto), descripcion = upper(descripcion), observaciones = upper(observaciones)
;

/* Detalle del Personal */
create trigger upper_personal_det
    on cp_personal_det
    for insert,update
    as
    update cp_personal_det set rfc = upper(rfc), idinstituto = upper(idinstituto), idpuesto = upper(idpuesto), actividades = upper(actividades)
;

/* Capacitaciones */
create trigger upper_grupos_cap
    on cp_grupos_cap
    for insert,update
    as
    update cp_grupos_cap set descripcion = upper(descripcion)
;
create trigger upper_subgrupo_cap
    on cp_subgrupo_cap
    for insert,update
    as
    update cp_subgrupo_cap set descripcion = upper(descripcion)
;
create trigger upper_capacidades
    on cp_capacidades
    for insert,update
    as
    update cp_capacidades set idcapacidad = upper(idcapacidad), descripcion = upper(descripcion)
;
create trigger upper_otras_capacidades
    on cp_otras_capacidades
    for insert,update
    as
    update cp_otras_capacidades set idotracap = upper(idotracap), descripcion = upper(descripcion)
;
create trigger upper_tipo_cap
    on cp_tipo_cap
    for insert,update
    as
    update cp_tipo_cap set descripcion = upper(descripcion), observaciones = upper(observaciones)
;

/* Horarios */
create trigger upper_horario_gral
    on cp_horario_gral
    for insert,update
    as
    update cp_horario_gral set observaciones = upper(observaciones)
;

/* Barreras */
create trigger upper_barreras_personal
    on cp_barreras_personal
    for insert,update
    as
    update cp_barreras_personal set descripcion = upper(descripcion), observaciones = upper(observaciones)
;

/* Instructor */
create trigger upper_instructor_emp
    on cp_instructor_emp
    for insert,update
    as
    update cp_instructor_emp set idempresa = upper(idempresa), descripcion = upper(descripcion), pais = upper(pais), estadoe = upper(estadoe), ciudad = upper(ciudad), direccion = upper(direccion), telefonos = upper(telefonos)
;
create trigger upper_instructor_cap
    on cp_instructor_cap
    for insert,update
    as
    update cp_instructor_cap set idinstructor = upper(idinstructor), titulo = upper(titulo), nombre = upper(nombre), idempresa = upper(idempresa)
;

/* Horarios */
create trigger upper_horario_gral
    on cp_horario_gral
    for insert,update
    as
    update cp_horario_gral set idhorario = upper(idhorario)
;
create trigger upper_horario_det
    on cp_horario_det
    for insert,update
    as
    update cp_horario_det set idhd = upper(idhd)
;

/* Capacidad autorizada */
create trigger upper_capacidad_autorizada
    on cp_capacidad_autorizada
    for insert,update
    as
    update cp_capacidad_autorizada set clavecurso = upper(clavecurso), idcapacidad = upper(idcapacidad), idinstructor = upper(idinstructor), idhorario = upper(idhorario), idinstituto = upper(idinstituto)
;

/* Criterios de evaluacion */
create trigger upper_evaluacion_criterios
    on cp_evaluacion_criterios
    for insert,update
    as
    update cp_evaluacion_criterios set descripcion = upper(descripcion), observaciones = upper(observaciones)
;

/* Eventos */
create trigger upper_eventos_gral
    on cp_eventos_gral
    for insert,update
    as
    update cp_eventos_gral set semestre = upper(semestre)
;

/* Encuesta */
create trigger upper_criterios_grupos
    on cp_criterios_grupos
    for insert,update
    as
    update cp_criterios_grupos set descripcion = upper(descripcion)
;
create trigger upper_criterios_enc
    on cp_criterios_enc
    for insert,update
    as
    update cp_criterios_enc set descripcion = upper(descripcion), observaciones = upper(observaciones)
;
create trigger upper_encuesta_instructor
    on cp_encuesta_instructor
    for insert,update
    as
    update cp_encuesta_instructor set idencuesta = upper(idencuesta), clavecurso = upper(clavecurso), observaciones = upper(observaciones)
;
create trigger upper_enc_cri_ins
    on cp_enc_cri_ins
    for insert,update
    as
    update cp_enc_cri_ins set idencuesta = upper(idencuesta)
;

/* Seguimiento */
create trigger upper_seg
	on cp_seguimiento
	for insert,update
	as
	update cp_seguimiento set descripcion = upper(descripcion)
;





