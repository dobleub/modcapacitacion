/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Scripts generales
  ------------------------------------------------------------------------------------------------------------------------------------
*/

/*  */
/* sp_helpsort; */

/*select s.descripcion, g.descripcion 
    from subgrupo_cap as s inner join grupos_cap as g 
    on s.idgrupo = g.idgrupo and g.estado='1'*/

/* select * from barreras_personal */

-- execute i_escolaridad_p @des = "SECUNDARIA"
-- execute i_escolaridad_p SECUNDARIA
-- execute s_institucion
-- execute s_personal

-- select c.idcapacidad,c.descripcion,s.descripcion,g.descripcion from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g
-- where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo

-- declare @guess char(1)
-- select @guess = "1" 
-- exec i_subdireccion @des = "GESTIÓN Y VINCULACIÓN", @echo = @guess output 
-- select @guess

-- drop procedure cp_calcular_semestre

--declare @dia datetime, @otro datetime, @idp varchar(12)
--set @dia = (select getdate())
--      exec cp_calcular_dia '06/25/2013', @idp output
--select @idp


-- declare @dia datetime, @otro datetime, @idp integer
--        exec cp_calcular_daysdiff '02/01/2012', '03/01/2012', @idp output
-- select @idp as dias


-- declare @idp varchar(50), @dia datetime
--       set @dia = (select getdate())
--       exec cp_calcular_semestre @dia, @idp output
-- select @idp


-- select c.idcapacidad, c.descripcion, g.descripcion, s.descripcion 
-- from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g 
-- where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' and c.descripcion like ADMIN order by c.idcapacidad

-- declare @guess char(1)
-- select @guess = "1"
--     exec i_instructor_emp "instituto tecnológico de tlaxiaco","méxico","oaxaca","tlaxiaco","Conocido S/N","ejemplo@email.com","NA", @echo = @guess output 
-- select @guess
 
-- select * from cp_instructor_emp
-- DELETE FROM dbo.cp_instructor_emp WHERE idempresa='EPR-ISNT30'

-- declare @guess char(1)
-- select @guess = "1"
--     exec i_instructor_cap "C.","Nombre de la persona", "Tlaxiaco, Oaxaca, Col. Adolfo Lopez Mateos, Conocido S/N","ejemplo@email.com","NA","EPR-ISNT30","N", @echo = @guess output
-- select @guess

-- en las incersiones si no se hizo correctamente nos retorna un valor '1' de lo contrario muestra el identificador creado

-- declare @echo char(1) exec i_grupos_cap 'fuera de lista', @echo output select @echo as echo

/*
create procedure i_evaluacion_ins @idins varchar(10), @fecha datetime, @capa varchar(10), @ace char(1), @ins varchar(20) = '20DIT0004L', @idcrit integer, @idesc integer,
	@echo char(1) output
	as
    declare @semact varchar(50), @sem varchar(50), @semi datetime, @criterio integer
	
		set @semact = (select semestre from cp_eventos_gral where estado = '2')
        set @sem = (select getdate())
		exec cp_calcular_semestre @sem, @sem output
        
        exec i_evaluacion_crit_tmp @idcrit, @idesc, @criterio output
        
    if (@fecha = "")
    begin
        set @semi = (select getdate())
    end
    else
    begin
       set @semi = @fecha
    end

	if ( (select count(*) from cp_evaluacion_ins where idinstructor = @idins and idcet = @criterio and estado = '1' ) = 0 and (@semact != @sem) )
    begin
		insert into cp_evaluacion_ins(idinstructor,idcet,fecha,idcapacidad,aceptado,idinstituto) values(@idins, @criterio, @semi, @capa, @ace, @ins)
        print 'Evaluacion del Instructor dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Evaluacion del Instructor dado de alta previamente'
		select @echo = cast(0 as char)
    end
;*/

--declare @echo char(1), @dt datetime
--    set @dt = (select getdate())
--    exec i_horario_det @dt, '10:50-11:40', NULL, NULL, @echo output
--select @echo


-- select ca.clavecurso, c.descripcion, i.nombre, ca.limite_personal, h.fechainicio,h.fechafin,h.totalhoras, hd.dia, hd.horainicio, hd.horamedio
-- from cp_capacidad_autorizada as ca, cp_capacidades as c, cp_instructor_cap as i, cp_horario_gral as h, cp_horario_det as hd, cp_horario_gral_det as hgd
-- where ca.idcapacidad = c.idcapacidad and ca.idinstructor = i.idinstructor and ca.idhorario = h.idhorario and hgd.idhorario = ca.idhorario and hgd.idhd = hd.idhd

-- select ca.clavecurso, c.descripcion, i.nombre, ca.limite_personal, h.fechainicio,h.fechafin,h.totalhoras
-- from cp_capacidad_autorizada as ca, cp_capacidades as c, cp_instructor_cap as i, cp_horario_gral as h
-- where ca.idcapacidad = c.idcapacidad and ca.idinstructor = i.idinstructor and ca.idhorario = h.idhorario 


select ca.clavecurso, c.descripcion, i.nombre, ca.limite_personal, h.fechainicio,h.fechafin,h.totalhoras
from cp_capacidad_autorizada as ca, cp_otras_capacidades as c, cp_instructor_cap as i, cp_horario_gral as h
where ca.idotracap = c.idotracap and ca.idinstructor = i.idinstructor and ca.idhorario = h.idhorario 

