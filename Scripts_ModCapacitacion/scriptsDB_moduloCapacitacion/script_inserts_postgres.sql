/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para las inserciones b[asicas
  ------------------------------------------------------------------------------------------------------------------------------------
*/

-- alter table criterio_grp add estado char(1) default '1';


-- Insercion para Institucion 
insert into cp_institucion values('20DIT0004L','INSTITUTO TECNOLÓGICO DE TLAXIACO');

-- Insercion de grados de escolaridad 
insert into cp_escolaridad_p(descripcion) values(upper('primaria'));
insert into cp_escolaridad_p(descripcion) values(upper('secundaria'));
insert into cp_escolaridad_p(descripcion) values(upper('carrera técnica'));
insert into cp_escolaridad_p(descripcion) values(upper('preparatoria/bachillerato'));
insert into cp_escolaridad_p(descripcion) values(upper('licenciatura'));
insert into cp_escolaridad_p(descripcion) values(upper('maestría'));
insert into cp_escolaridad_p(descripcion) values(upper('doctorado'));


-- Insercion para Capacitaciones disponibles 
insert into cp_grupos_cap(descripcion) values(upper('administrativo'));
insert into cp_grupos_cap(descripcion) values(upper('profesional'));
insert into cp_grupos_cap(descripcion) values(upper('servicios'));
insert into cp_grupos_cap(descripcion) values(upper('técnico'));
insert into cp_grupos_cap(descripcion) values(upper('educación')); 

declare @var integer
set @var = (select idgrupo from cp_grupos_cap where descripcion=upper('administrativo'))
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: administración',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: calidad',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: secretarial',@var)

set @var = (select idgrupo from cp_grupos_cap where descripcion=upper('profesional'))
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: normatividad',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: medicina',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: contaduría',@var)

set @var = (select idgrupo from cp_grupos_cap where descripcion=upper('servicios'))
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: servicios',@var)

set @var = (select idgrupo from cp_grupos_cap where descripcion=upper('técnico'))
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: investigación',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: computación',@var)

set @var = (select idgrupo from cp_grupos_cap where descripcion=upper('educación'))
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: educación y capacitación',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidades de desarrollo integral e institucional',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: idiomas',@var)
insert into cp_subgrupo_cap(descripcion,idgrupo) values('capacidad técnica: protección civil',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: administración'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm02','administración de proyectos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm04','administración de lo urgente y lo importante',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm05','administración del tiempo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm06','administración del tiempo y manejo de estrés',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm08','administración estratégia',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm09','administración por objetivos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm10','administración pública',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm11','análisis de problemas administrativos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm12','clasificación de material de biblioteca',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm14','elaboración y presentación de programas administrativos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm15','introducción a la administración',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm17','negociación',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm18','negociación y toma de decisiones',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm19','organización de oficinas',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm20','orientación a resultados',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm22','principios básicos de estadística',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm23','técnicas de supervisión',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm25','técnicas e instrumentos de planeación y evaluación',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm26','toma de decisiones',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scadm27','trabajo en equipo',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: calidad'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal01','actitud de servicio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal02','actualización de auditores/as internos/as con base en la norma 19011:2005',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal06','formación de auditor líder',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal08','conocimiento e interpretación de la norma iso14000:2004',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal09','conocimiento e interpretación de la norma iso9001:2008',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal11','éxito en la atención al cliente',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal13','formación de auditor/a interno/a con base a la norma iso9001:2008',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal14','formación de auditoras/es internas/os del modelo de equidad de género',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal15','gestión de actitud',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal17','herramientas para la mejora continua',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal19',"las 5's",@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal20','logrando la excelencia a través del servicio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal23','mejoramiento del ambiente de trabajo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal24','sensibilización en equidad de género',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccal25','sistema de gestión de la calidad',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: secretarial'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scsec02','actualización secretarial',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scsec03','clasificación y control de archivo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scsec09','ortografía',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scsec10','redacción',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: normatividad'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scjur08','normatividad gubernamental',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scjur11','leyes y reglamentos de la administración pública',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: medicina'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scmed02','primeros auxilios',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: contaduría'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccont01','administración y control de almacenes',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sccont09','técnicas de control y administración de inventarios',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: servicios'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser01','carpintería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser02','cerrajería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser03','electricidad',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser05','fontanería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser06','herrería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser08','jardinería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser11','plomería',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scser14','soldadura eléctrica',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: investigación'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinv02','biblioteconomía',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinv03','conocimiento y uso de equipos de laboratorio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinv04','control y registro de material de laboratorio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinv06','mantenimiento de material de laboratorio',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: computación'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf16','ofimática básica',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf17','ofimática intermedia',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf18','ofimática avanzada',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf23','sistema operativo windows vista',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf24','sistema operativo windows 7',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf25','internet y correo electrónico (outlook)',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf29','multimedia',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf32','autocad',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf33','corel draw',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf45','biblioteca virtual',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf51','diseño de páginas web',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf53','herramientas informáticas para la toma de decisiones',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf55','manejo de base de datos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf57','mantenimiento a equipo de cómputo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf58','microsoft project',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scinf60','presentaciones ejecutivas con power point',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: educación y capacitación'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sceyc06','formación de facultadores',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('sceyc07','métodos de evaluación',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidades de desarrollo integral e institucional'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei03','código de ética del gobierno federal',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei10','aprendiendo a razonar',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei11','asertividad',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei13','autoestima',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei14','coaching y liderazgo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei15','como hablar en público',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei16','comportamiento humano',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei17','construyendo instituciones inteligentes',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei18','comunicación asertiva',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei20','comunicación eficaz',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei22','creatividad',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei23','desarrollo de la actitud humana para la calidad en el servicio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei24','desarrollo humano y valores',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei25','efectividad en el trabajo nivel 2: técnicas de efectividad avanzadas',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei26','efectividad en el trabajo nivel 1: técnicas de efectividad básica',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei27','equipos de alto desempeño',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei28','estratégias para manejar personas difíciles',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei29','gestión del estrés',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei30','gimnasia cerebral',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei31','habilidad para dirigir',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei32','imagen personal',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei33','inducción al ejercicio de los recursos en la administración pública federal y sus responsabilidades',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei34','interacción humana',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei35','integración de equipos de trabajo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei36','integración y desarrollo de equipos de alto desempeño',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei37','inteligencia emocional',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei38','inteligencia emocional aplicada al ámbito laboral',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei39','introducción a la administración pública y al sist nac educ sup tec',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei40','introducción al módelo educativo y gestión por procesos',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei41','la comunicación en los equipos eficientes',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei42','liderazgo creativo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei43','liderazgo transformacional',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei44','los 7 habitos de la gente altamente efectiva',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei48','motivación en el trabajo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei49','motivación y superación personal',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei50','nuevas tendencias de la educación superior',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei51','nuevos horizontes',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei53','plan de vida y carrera',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei54','pensamiento creativo',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei55','prevensión del conflicto',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei57','prospectiva estratégica',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei59','red de relaciones efectivas',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei60','relaciones humanas',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei62','sensibilización',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei66','tendencias de comunicación',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scdiei67','trabajo conversacional en la gestión institucional',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: idiomas'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scid01','inglés básico',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scid02','inglés intermedio',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scid03','inglés avanzado',@var)

set @var = (select idsubgrupo from cp_subgrupo_cap where descripcion=upper('capacidad técnica: protección civil'))
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scpc03','higiene escolar',@var)
insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values('scpc06','seguridad e higiene',@var)


-- Insercion de barreras
insert into cp_barreras_personal(descripcion) values('reconocimiento al desempeño');
insert into cp_barreras_personal(descripcion) values('distribución de tareas');
insert into cp_barreras_personal(descripcion) values('trabajo en equipo');
insert into cp_barreras_personal(descripcion) values('confianza');
insert into cp_barreras_personal(descripcion) values('infraestructura');
insert into cp_barreras_personal(descripcion) values('imparcialidad');
insert into cp_barreras_personal(descripcion) values('iniciativa');
insert into cp_barreras_personal(descripcion) values('liderazgo');
insert into cp_barreras_personal(descripcion) values('comunicación organizacional');
insert into cp_barreras_personal(descripcion) values('comunicación interpersonal');
insert into cp_barreras_personal(descripcion) values('estrés laboral');
insert into cp_barreras_personal(descripcion) values('colaboración');
insert into cp_barreras_personal(descripcion) values('infraestructura');
insert into cp_barreras_personal(descripcion) values('compromiso');
insert into cp_barreras_personal(descripcion) values('respeto'); 


-- Inserciones de criterio de evaluacion para instructor 
insert into cp_evaluacion_criterios(descripcion) values('formación profesional relacionada a la capacitación a impartir'); 
insert into cp_evaluacion_criterios(descripcion) values('experiencia en capacitación y en la temática a impartir'); 
insert into cp_evaluacion_criterios(descripcion) values('materiales didácticos a utilizar'); 
insert into cp_evaluacion_criterios(descripcion) values('disponibilidad de tiempo'); 
insert into cp_evaluacion_criterios(descripcion) values('certificaciones y acreditaciones relacionadas al área de capacitación');


-- Inserciones de criterio de encuesta de opinion
insert into cp_criterios_grupos(descripcion) values('instructor'); 
insert into cp_criterios_grupos(descripcion) values('material didáctico'); 
insert into cp_criterios_grupos(descripcion) values('curso'); 
insert into cp_criterios_grupos(descripcion) values('infraestructura');

declare @var2 integer
set @var2 = (select idgrupo from cp_criterios_grupos where descripcion=upper('instructor'))
insert into cp_criterios_enc(descripcion,idgrupo) values('expuso el objetivo y temario del curso',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('mostró dominio del contenido abordado',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('fomentó la participación del grupo',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('aclaró las dudas que se presentaron',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('dio retroalimentación a los ejercicios realizados',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('aplicó una evaluación final relacionada con los contenidos del curso',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('inició y concluyó puntualmente las clases',@var2)

set @var2 = (select idgrupo from cp_criterios_grupos where descripcion=upper('material didáctico'))
insert into cp_criterios_enc(descripcion,idgrupo) values('el material didáctico fue útil a lo largo del curso',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('la impresión del material didáctico fue legible',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('la variedad del material didáctico fue suficiente para apoyar su aprendizaje',@var2)

set @var2 = (select idgrupo from cp_criterios_grupos where descripcion=upper('curso'))
insert into cp_criterios_enc(descripcion,idgrupo) values('la distribución del tiempo fue adecuada para cubrir el contenido',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('los temas fueron suficientes para alcanzar el objetivo del curso',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('el curso comprendió ejercicios de práctica relacionados con el contenido',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('el curso cubrió todas las espectativas',@var2)

set @var2 = (select idgrupo from cp_criterios_grupos where descripcion=upper('infraestructura'))
insert into cp_criterios_enc(descripcion,idgrupo) values('la iluminación del aula fue adecuada',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('la ventilacion del aula fue adecuada',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('el aseo del aula fue adecuado',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('el servicio de los sanitarios fue adecuado',@var2)
insert into cp_criterios_enc(descripcion,idgrupo) values('recibió apoyo del personal que coordinó el curso',@var2) 

insert into cp_tipo_cap(descripcion) values('competencias generales');
insert into cp_tipo_cap(descripcion) values('competencias directivas');
insert into cp_tipo_cap(descripcion) values('otros (cursos fuera del catálogo)');


insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Enero'),upper('January'),0,0);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Febrero'),upper('February'),3,3);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Marzo'),upper('March'),3,4);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Abril'),upper('April'),6,0);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Mayo'),upper('May'),1,2);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Junio'),upper('June'),4,5);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Julio'),upper('July'),6,0);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Agosto'),upper('August'),2,3);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Septiembre'),upper('September'),5,6);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Octubre'),upper('October'),0,1);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Noviembre'),upper('November'),3,4);
insert into cp_doomsday_mod(mes,mesingles,anioreg,aniobis) values(upper('Diciembre'),upper('December'),5,6);

insert into cp_doomsday_days(dia) values(upper('domingo'));
insert into cp_doomsday_days(dia) values(upper('lunes'));
insert into cp_doomsday_days(dia) values(upper('martes'));
insert into cp_doomsday_days(dia) values(upper('miércoles'));
insert into cp_doomsday_days(dia) values(upper('jueves'));
insert into cp_doomsday_days(dia) values(upper('viernes'));
insert into cp_doomsday_days(dia) values(upper('sábado'));

insert into cp_seguimiento(descripcion) values('Los conocimientos que adquirió su colaborador en el curso tienen aplicación el ámbito laboral a corto y mediano plazo');
insert into cp_seguimiento(descripcion) values('el curso ayudó a su colaborador a mejorar el desempeño de sus funciones');
insert into cp_seguimiento(descripcion) values('el curso le ayudó a su colaborador a considerar nuevas formas de trabajo');
insert into cp_seguimiento(descripcion,estado) values('el curso que tomó su colaborador le:','0');
insert into cp_seguimiento(descripcion) values('produjo un incremento en su motivación');
insert into cp_seguimiento(descripcion) values('ha servido para su desarrollo personal');
insert into cp_seguimiento(descripcion) values('sirvió para integrarse mejor con sus compañeros de trabajo');
insert into cp_seguimiento(descripcion) values('produjo una mayor comprensión del servicio que presta al snest');
insert into cp_seguimiento(descripcion) values('facilitó una mejoría en su actitud hacia la dgest, el snest o sus compañeros de trabajo');
insert into cp_seguimiento(descripcion) values('permitió desarrollar algunas habilidades adicionales');
insert into cp_seguimiento(descripcion) values('generó una mejor comprensión de los conceptos generales del curso aplicables a su trabajo');
insert into cp_seguimiento(descripcion) values('ofrecieron algunos valores compatibles con los suyos (del participante)');


-- insert personal
--    select * from siides.dbo.personal
