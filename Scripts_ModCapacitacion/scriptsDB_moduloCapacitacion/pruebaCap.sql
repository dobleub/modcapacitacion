USE master
go


PRINT "<<<< CREATE DATABASE pruebaCap >>>>"
go


IF EXISTS (SELECT 1 FROM master.dbo.sysdatabases
	   WHERE name = 'pruebaCap')
	DROP DATABASE pruebaCap
go


IF (@@error != 0)
BEGIN
	PRINT "Error dropping database 'pruebaCap'"
	SELECT syb_quit()
END
go


CREATE DATABASE pruebaCap
	    ON pruebaCap_data = '256M' -- 131072 pages
	LOG ON pruebaCap_log = '128M' -- 65536 pages
go

use pruebaCap
go

exec sp_changedbowner 'sa', true 
go

exec master.dbo.sp_dboption pruebaCap, 'select into/bulkcopy', true
go

exec master.dbo.sp_dboption pruebaCap, 'trunc log on chkpt', true
go

exec master.dbo.sp_dboption pruebaCap, 'abort tran on log full', true
go

checkpoint
go


USE master
go


PRINT "<<<< CREATE DATABASE pruebaCap >>>>"
go


IF EXISTS (SELECT 1 FROM master.dbo.sysdatabases
	   WHERE name = 'pruebaCap')
	DROP DATABASE pruebaCap
go


IF (@@error != 0)
BEGIN
	PRINT "Error dropping database 'pruebaCap'"
	SELECT syb_quit()
END
go


CREATE DATABASE pruebaCap
	    ON pruebaCap_data = '256M' -- 131072 pages
	LOG ON pruebaCap_log = '128M' -- 65536 pages
go

use pruebaCap
go

exec sp_changedbowner 'sa', true 
go

exec master.dbo.sp_dboption pruebaCap, 'select into/bulkcopy', true
go

exec master.dbo.sp_dboption pruebaCap, 'trunc log on chkpt', true
go

exec master.dbo.sp_dboption pruebaCap, 'abort tran on log full', true
go

checkpoint
go


-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.institucion'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.institucion" >>>>>'
go

use pruebaCap
go 

setuser 'dbo'
go 

create table institucion (
	idinstituto                     varchar(20)                      not null  ,
	descripcion                     varchar(100)                     not null  ,
 PRIMARY KEY CLUSTERED ( idinstituto )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_institucion'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_institucion" >>>>>'
go 

setuser 'dbo'
go 

/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para crear los triggers
  ------------------------------------------------------------------------------------------------------------------------------------
*/

/* Triggers de para mayusculas */

/* Institucion */
create trigger upper_institucion
    on institucion
    for insert,update
    as
    update institucion set idinstituto = upper(descripcion), descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.escolaridad_p'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.escolaridad_p" >>>>>'
go

setuser 'dbo'
go 

create table escolaridad_p (
	idescolaridad                   int                              identity  ,
	descripcion                     varchar(100)                     not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idescolaridad )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_escolaridad_p'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_escolaridad_p" >>>>>'
go 

setuser 'dbo'
go 

/* Escolaridad del personal */
create trigger upper_escolaridad_p
    on escolaridad_p
    for insert,update
    as
    update escolaridad_p set descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.personal_esc'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.personal_esc" >>>>>'
go

setuser 'dbo'
go 

create table personal_esc (
	rfc                             char(13)                         not null  ,
	idescolaridad                   int                              not null  ,
	estado                          char(1)                          not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_personal_esc'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_personal_esc" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_personal_esc
    on personal_esc
    for insert,update
    as
    update personal_esc set rfc = upper(rfc), estado = upper(estado)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.subdireccion'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.subdireccion" >>>>>'
go

setuser 'dbo'
go 

create table subdireccion (
	idsub                           varchar(10)                      not null  ,
	descripcion                     varchar(100)                     not null  ,
	observaciones                   varchar(100)                     not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idsub )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_subdireccion'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_subdireccion" >>>>>'
go 

setuser 'dbo'
go 

/* Subdireccion - Departamento - Puesto */
create trigger upper_subdireccion
    on subdireccion
    for insert,update
    as
    update subdireccion set idsub = upper(idsub), descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.departamento'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.departamento" >>>>>'
go

setuser 'dbo'
go 

create table departamento (
	iddepto                         varchar(10)                      not null  ,
	descripcion                     varchar(100)                     not null  ,
	idsub                           varchar(10)                      not null  ,
	observaciones                   varchar(100)                     not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( iddepto )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_departamento'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_departamento" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_departamento
    on departamento
    for insert,update
    as
    update departamento set iddepto = upper(iddepto), descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.puesto'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.puesto" >>>>>'
go

setuser 'dbo'
go 

create table puesto (
	idpuesto                        varchar(15)                      not null  ,
	descripcion                     varchar(100)                     not null  ,
	iddepto                         varchar(10)                      not null  ,
	observaciones                   varchar(100)                     not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idpuesto )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_puesto'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_puesto" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_puesto
    on puesto
    for insert,update
    as
    update puesto set idpuesto = upper(idpuesto), descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.personal_det'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.personal_det" >>>>>'
go

setuser 'dbo'
go 

create table personal_det (
	rfc                             char(13)                         not null  ,
	idinstituto                     varchar(20)                      not null  ,
	idpuesto                        varchar(15)                      not null  ,
	actividades                     varchar(300)                     not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_personal_det'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_personal_det" >>>>>'
go 

setuser 'dbo'
go 

/* Detalle del Personal */
create trigger upper_personal_det
    on personal_det
    for insert,update
    as
    update personal_det set rfc = upper(rfc), idinstituto = upper(idinstituto), idpuesto = upper(idpuesto), actividades = upper(actividades)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.grupos_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.grupos_cap" >>>>>'
go

setuser 'dbo'
go 

create table grupos_cap (
	idgrupo                         int                              identity  ,
	descripcion                     varchar(60)                      not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idgrupo )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_grupos_cap'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_grupos_cap" >>>>>'
go 

setuser 'dbo'
go 

/* Capacitaciones */
create trigger upper_grupos_cap
    on grupos_cap
    for insert,update
    as
    update grupos_cap set descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.subgrupo_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.subgrupo_cap" >>>>>'
go

setuser 'dbo'
go 

create table subgrupo_cap (
	idsubgrupo                      int                              identity  ,
	descripcion                     varchar(60)                      not null  ,
	idgrupo                         int                              not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idsubgrupo )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_subgrupo_cap'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_subgrupo_cap" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_subgrupo_cap
    on subgrupo_cap
    for insert,update
    as
    update subgrupo_cap set descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.capacidades'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.capacidades" >>>>>'
go

setuser 'dbo'
go 

create table capacidades (
	idcapacidad                     varchar(10)                      not null  ,
	descripcion                     varchar(150)                     not null  ,
	idsubgrupo                      int                              not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idcapacidad )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_capacidades'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_capacidades" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_capacidades
    on capacidades
    for insert,update
    as
    update capacidades set idcapacidad = upper(idcapacidad), descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.otras_capacidades'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.otras_capacidades" >>>>>'
go

setuser 'dbo'
go 

create table otras_capacidades (
	idotracap                       varchar(8)                       not null  ,
	descripcion                     varchar(100)                     not null  ,
	fecha                           datetime                         not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idotracap )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_otras_capacidades'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_otras_capacidades" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_otras_capacidades
    on otras_capacidades
    for insert,update
    as
    update otras_capacidades set idotracap = upper(idotracap), descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.historial_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.historial_cap" >>>>>'
go

setuser 'dbo'
go 

create table historial_cap (
	idhp                            int                              identity  ,
	rfc                             char(13)                         not null  ,
	idcapacidad                     varchar(10)                          null  ,
	idotracap                       varchar(8)                           null  ,
	fecha                           datetime                         not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idhp )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.personal'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.personal" >>>>>'
go

setuser 'dbo'
go 

create table personal (
	rfc                             char(13)                         not null  ,
	clave_centro_seit               char(10)                             null  ,
	clave_area                      char(6)                              null  ,
	curp_empleado                   char(18)                             null  ,
	no_tarjeta                      int                                  null  ,
	apellidos_empleado              varchar(40)                          null  ,
	nombre_empleado                 varchar(40)                          null  ,
	horas_nombramiento              int                                  null  ,
	nombramiento                    char(1)                          not null  ,
	clases                          char(1)                              null  ,
	ingreso_rama                    char(6)                              null  ,
	inicio_gobierno                 char(6)                              null  ,
	inicio_sep                      char(6)                              null  ,
	inicio_plantel                  char(6)                              null  ,
	domicilio_empleado              varchar(60)                          null  ,
	colonia_empleado                varchar(40)                          null  ,
	codigo_postal_empleado          int                                  null  ,
	localidad                       varchar(30)                          null  ,
	telefono_empleado               varchar(30)                          null  ,
	sexo_empleado                   char(1)                              null  ,
	estado_civil                    char(1)                              null  ,
	fecha_nacimiento                datetime                             null  ,
	lugar_nacimiento                int                                  null  ,
	institucion_egreso              varchar(50)                          null  ,
	nivel_estudios                  char(1)                              null  ,
	grado_maximo_estudios           char(1)                              null  ,
	estudios                        varchar(250)                         null  ,
	fecha_termino_estudios          datetime                             null  ,
	fecha_titulacion                datetime                             null  ,
	cedula_profesional              char(15)                             null  ,
	especializacion                 varchar(50)                          null  ,
	idiomas_domina                  varchar(60)                          null  ,
	status_empleado                 char(2)                              null  ,
	foto                            image                                null  ,
	firma                           image                                null  ,
	correo_electronico              varchar(60)                          null  ,
	padre                           varchar(50)                          null  ,
	madre                           varchar(50)                          null  ,
	conyuge                         varchar(50)                          null  ,
	hijos                           varchar(100)                         null  ,
	num_acta                        int                                  null  ,
	num_libro                       int                                  null  ,
	num_foja                        int                                  null  ,
	num_ano                         int                                  null  ,
	num_cartilla_smn                char(15)                             null  ,
	ano_clase                       int                                  null  ,
	pigmentacion                    char(1)                              null  ,
	pelo                            char(1)                              null  ,
	frente                          char(1)                              null  ,
	cejas                           char(1)                              null  ,
	ojos                            char(1)                              null  ,
	nariz                           char(1)                              null  ,
	boca                            char(1)                              null  ,
	estaturamts                     numeric(3,2)                         null  ,
	pesokg                          numeric(5,2)                         null  ,
	senas_visibles                  varchar(255)                         null  ,
	pais                            varchar(30)                          null  ,
	pasaporte                       varchar(40)                          null  ,
	fm                              varchar(30)                          null  ,
	inicio_vigencia                 datetime                             null  ,
	termino_vigencia                datetime                             null  ,
	entrada_salida                  char(1)                              null  ,
	observaciones_empleado          varchar(254)                         null  ,
	area_academica                  char(6)                              null  ,
	tipo_personal                   char(1)                              null  ,
	tipo_control                    char(1)                              null  ,
	rfc2                            char(13)                             null  ,
 PRIMARY KEY CLUSTERED ( rfc )  on 'default' 
)
lock allpages
 on 'default'
go 

sp_placeobject 'default', 'dbo.personal.tpersonal'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.tipo_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.tipo_cap" >>>>>'
go

setuser 'dbo'
go 

create table tipo_cap (
	idtipo                          int                              identity  ,
	descripcion                     varchar(50)                      not null  ,
	observaciones                   varchar(100)                         null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idtipo )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_tipo_cap'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_tipo_cap" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_tipo_cap
    on tipo_cap
    for insert,update
    as
    update tipo_cap set descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.requerimientos_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.requerimientos_cap" >>>>>'
go

setuser 'dbo'
go 

create table requerimientos_cap (
	rfc                             char(13)                         not null  ,
	idcapacidad                     varchar(10)                          null  ,
	idtipo                          int                                  null  ,
	idotracap                       varchar(8)                           null  ,
	fecha                           datetime                         not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.barreras_personal'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.barreras_personal" >>>>>'
go

setuser 'dbo'
go 

create table barreras_personal (
	idbarrera                       int                              identity  ,
	descripcion                     varchar(50)                      not null  ,
	observaciones                   varchar(100)                         null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idbarrera )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_barreras_personal'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_barreras_personal" >>>>>'
go 

setuser 'dbo'
go 

/* Barreras */
create trigger upper_barreras_personal
    on barreras_personal
    for insert,update
    as
    update barreras_personal set descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.barreras_respuestas'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.barreras_respuestas" >>>>>'
go

setuser 'dbo'
go 

create table barreras_respuestas (
	idrespuesta                     int                              identity  ,
	rfc                             char(13)                         not null  ,
	fecha                           datetime                         not null  ,
 PRIMARY KEY CLUSTERED ( idrespuesta )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.barreras_temp'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.barreras_temp" >>>>>'
go

setuser 'dbo'
go 

create table barreras_temp (
	idrespuesta                     int                              identity  ,
	idbarrera                       int                              not null  ,
	importancia                     int                              not null  ,
	fecha                           datetime                         not null  ,
 PRIMARY KEY CLUSTERED ( idrespuesta, idbarrera )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.instructor_emp'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.instructor_emp" >>>>>'
go

setuser 'dbo'
go 

create table instructor_emp (
	idempresa                       varchar(10)                      not null  ,
	descripcion                     varchar(50)                      not null  ,
	direccion                       varchar(100)                     not null  ,
	telefonos                       varchar(60)                      not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idempresa )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_instructor_emp'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_instructor_emp" >>>>>'
go 

setuser 'dbo'
go 

/* Instructor */
create trigger upper_instructor_emp
    on instructor_emp
    for insert,update
    as
    update instructor_emp set idempresa = upper(idempresa), descripcion = upper(descripcion), direccion = upper(direccion), telefonos = upper(telefonos)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.instructor_cap'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.instructor_cap" >>>>>'
go

setuser 'dbo'
go 

create table instructor_cap (
	idinstructor                    varchar(10)                      not null  ,
	titulo                          varchar(5)                       not null  ,
	nombre                          varchar(100)                     not null  ,
	idempresa                       varchar(10)                      not null  ,
	externo                         char(1)                          not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idinstructor )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_instructor_cap'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_instructor_cap" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_instructor_cap
    on instructor_cap
    for insert,update
    as
    update instructor_cap set idinstructor = upper(idinstructor), titulo = upper(titulo), nombre = upper(nombre), idempresa = upper(idempresa)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.horario_gral'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.horario_gral" >>>>>'
go

setuser 'dbo'
go 

create table horario_gral (
	idhorario                       varchar(10)                      not null  ,
	fechainicio                     datetime                         not null  ,
	fechafin                        datetime                         not null  ,
	totalhoras                      int                              not null  ,
	observaciones                   varchar(100)                     not null  ,
 PRIMARY KEY CLUSTERED ( idhorario )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_horario_gral'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_horario_gral" >>>>>'
go 

setuser 'dbo'
go 

/* Horarios */
create trigger upper_horario_gral
    on horario_gral
    for insert,update
    as
    update horario_gral set idhorario = upper(idhorario)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.horario_det'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.horario_det" >>>>>'
go

setuser 'dbo'
go 

create table horario_det (
	idhd                            varchar(10)                      not null  ,
	dia                             varchar(12)                      not null  ,
	fecha_dia                       datetime                         not null  ,
	horainicio                      varchar(11)                      not null  ,
	horamedio                       varchar(23)                      not null  ,
	horafin                         varchar(11)                      not null  ,
 PRIMARY KEY CLUSTERED ( idhd )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_horario_det'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_horario_det" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_horario_det
    on horario_det
    for insert,update
    as
    update horario_det set idhd = upper(idhd)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.horario_gral_det'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.horario_gral_det" >>>>>'
go

setuser 'dbo'
go 

create table horario_gral_det (
	idhorario                       varchar(10)                      not null  ,
	idhd                            varchar(10)                      not null  ,
 PRIMARY KEY CLUSTERED ( idhorario, idhd )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.capacidad_autorizada'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.capacidad_autorizada" >>>>>'
go

setuser 'dbo'
go 

create table capacidad_autorizada (
	clavecurso                      varchar(25)                      not null  ,
	idcapacidad                     varchar(10)                          null  ,
	idotracap                       varchar(8)                           null  ,
	idinstructor                    varchar(10)                      not null  ,
	idhorario                       varchar(10)                      not null  ,
	limite_personal                 int                              not null  ,
	idinstituto                     varchar(20)                      not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( clavecurso )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_capacidad_autorizada'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_capacidad_autorizada" >>>>>'
go 

setuser 'dbo'
go 

/* Capacidad autorizada */
create trigger upper_capacidad_autorizada
    on capacidad_autorizada
    for insert,update
    as
    update capacidad_autorizada set clavecurso = upper(clavecurso), idcapacidad = upper(idcapacidad), idinstructor = upper(idinstructor), idhorario = upper(idhorario), idinstituto = upper(idinstituto)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.evaluacion_criterios'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.evaluacion_criterios" >>>>>'
go

setuser 'dbo'
go 

create table evaluacion_criterios (
	idcriterio                      int                              identity  ,
	descripcion                     varchar(100)                     not null  ,
	observaciones                   varchar(100)                         null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idcriterio )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_evaluacion_criterios'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_evaluacion_criterios" >>>>>'
go 

setuser 'dbo'
go 

/* Criterios de evaluacion */
create trigger upper_evaluacion_criterios
    on evaluacion_criterios
    for insert,update
    as
    update evaluacion_criterios set descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.evaluacion_crit_tmp'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.evaluacion_crit_tmp" >>>>>'
go

setuser 'dbo'
go 

create table evaluacion_crit_tmp (
	idcet                           int                              identity  ,
	idcriterio                      int                              not null  ,
	escala                          int                              not null  ,
	fecha                           datetime                         not null  ,
 PRIMARY KEY CLUSTERED ( idcet )  on 'default',
CHECK  (escala > 0 and escala < 7))
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.evaluacion_ins'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.evaluacion_ins" >>>>>'
go

setuser 'dbo'
go 

create table evaluacion_ins (
	idinstructor                    varchar(10)                      not null  ,
	idcet                           int                              not null  ,
	fecha                           datetime                         not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.eventos_gral'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.eventos_gral" >>>>>'
go

setuser 'dbo'
go 

create table eventos_gral (
	idevento                        varchar(10)                      not null  ,
	semestre                        varchar(50)                      not null  ,
	anio                            int                              not null  ,
 PRIMARY KEY CLUSTERED ( idevento )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_eventos_gral'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_eventos_gral" >>>>>'
go 

setuser 'dbo'
go 

/* Eventos */
create trigger upper_eventos_gral
    on eventos_gral
    for insert,update
    as
    update eventos_gral set semestre = upper(semestre)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.eventos_rel'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.eventos_rel" >>>>>'
go

setuser 'dbo'
go 

create table eventos_rel (
	idevento                        varchar(10)                      not null  ,
	clavecurso                      varchar(25)                      not null  ,
	fecha                           datetime                         not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.criterios_grupos'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.criterios_grupos" >>>>>'
go

setuser 'dbo'
go 

create table criterios_grupos (
	idgrupo                         int                              identity  ,
	descripcion                     varchar(20)                      not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idgrupo )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_criterios_grupos'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_criterios_grupos" >>>>>'
go 

setuser 'dbo'
go 

/* Encuesta */
create trigger upper_criterios_grupos
    on criterios_grupos
    for insert,update
    as
    update criterios_grupos set descripcion = upper(descripcion)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.criterios_enc'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.criterios_enc" >>>>>'
go

setuser 'dbo'
go 

create table criterios_enc (
	idcritenc                       int                              identity  ,
	descripcion                     varchar(100)                     not null  ,
	observaciones                   varchar(100)                         null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
	idgrupo                         int                                  null  ,
 PRIMARY KEY CLUSTERED ( idcritenc )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_criterios_enc'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_criterios_enc" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_criterios_enc
    on criterios_enc
    for insert,update
    as
    update criterios_enc set descripcion = upper(descripcion), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.encuesta_instructor'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.encuesta_instructor" >>>>>'
go

setuser 'dbo'
go 

create table encuesta_instructor (
	idencuesta                      varchar(10)                      not null  ,
	clavecurso                      varchar(25)                      not null  ,
	observaciones                   varchar(200)                     not null  ,
	estado                          char(1)                         DEFAULT  '1'
  not null  ,
 PRIMARY KEY CLUSTERED ( idencuesta )  on 'default' 
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_encuesta_instructor'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_encuesta_instructor" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_encuesta_instructor
    on encuesta_instructor
    for insert,update
    as
    update encuesta_instructor set idencuesta = upper(idencuesta), clavecurso = upper(clavecurso), observaciones = upper(observaciones)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for Table 'pruebaCap.dbo.enc_cri_ins'
-----------------------------------------------------------------------------
print '<<<<< CREATING Table - "pruebaCap.dbo.enc_cri_ins" >>>>>'
go

setuser 'dbo'
go 

create table enc_cri_ins (
	idencuesta                      varchar(10)                      not null  ,
	idcritenc                       int                              not null  ,
	valor                           int                              not null   
)
lock allpages
 on 'default'
go 


setuser
go 

-----------------------------------------------------------------------------
-- DDL for Trigger 'pruebaCap.dbo.upper_enc_cri_ins'
-----------------------------------------------------------------------------

print '<<<<< CREATING Trigger - "pruebaCap.dbo.upper_enc_cri_ins" >>>>>'
go 

setuser 'dbo'
go 

create trigger upper_enc_cri_ins
    on enc_cri_ins
    for insert,update
    as
    update enc_cri_ins set idencuesta = upper(idencuesta)
go 

setuser
go 

-----------------------------------------------------------------------------
-- DDL for View 'pruebaCap.dbo.sysquerymetrics'
-----------------------------------------------------------------------------

print '<<<<< CREATING View - "pruebaCap.dbo.sysquerymetrics" >>>>>'
go 

-- Cannot Generate DDL for system view(s).
-----------------------------------------------------------------------------
-- Dependent DDL for Object(s)
-----------------------------------------------------------------------------
use pruebaCap
go 

sp_addthreshold pruebaCap, 'logsegment', 4680, sp_thresholdaction
go 

Grant Select on dbo.sysalternates to public
go

Grant Select on dbo.sysattributes to public
go

Grant Select on dbo.syscolumns to public
go

Grant Select on dbo.syscomments to public
go

Grant Select on dbo.sysconstraints to public
go

Grant Select on dbo.sysdepends to public
go

Grant Select on dbo.sysindexes to public
go

Grant Select on dbo.sysjars to public
go

Grant Select on dbo.syskeys to public
go

Grant Select on dbo.syslogs to public
go

Grant Select on dbo.sysobjects(cache) to public
go

Grant Select on dbo.sysobjects(ckfirst) to public
go

Grant Select on dbo.sysobjects(crdate) to public
go

Grant Select on dbo.sysobjects(deltrig) to public
go

Grant Select on dbo.sysobjects(erlchgts) to public
go

Grant Select on dbo.sysobjects(expdate) to public
go

Grant Select on dbo.sysobjects(id) to public
go

Grant Select on dbo.sysobjects(identburnmax) to public
go

Grant Select on dbo.sysobjects(indexdel) to public
go

Grant Select on dbo.sysobjects(instrig) to public
go

Grant Select on dbo.sysobjects(loginame) to public
go

Grant Select on dbo.sysobjects(name) to public
go

Grant Select on dbo.sysobjects(objspare) to public
go

Grant Select on dbo.sysobjects(schemacnt) to public
go

Grant Select on dbo.sysobjects(seltrig) to public
go

Grant Select on dbo.sysobjects(spacestate) to public
go

Grant Select on dbo.sysobjects(sysstat) to public
go

Grant Select on dbo.sysobjects(sysstat2) to public
go

Grant Select on dbo.sysobjects(type) to public
go

Grant Select on dbo.sysobjects(uid) to public
go

Grant Select on dbo.sysobjects(updtrig) to public
go

Grant Select on dbo.sysobjects(userstat) to public
go

Grant Select on dbo.sysobjects(versionts) to public
go

Grant Select on dbo.syspartitionkeys to public
go

Grant Select on dbo.syspartitions to public
go

Grant Select on dbo.sysprocedures to public
go

Grant Select on dbo.sysprotects to public
go

Grant Select on dbo.sysqueryplans to public
go

Grant Select on dbo.sysreferences to public
go

Grant Select on dbo.sysroles to public
go

Grant Select on dbo.syssegments to public
go

Grant Select on dbo.sysslices to public
go

Grant Select on dbo.sysstatistics to public
go

Grant Select on dbo.systabstats to public
go

Grant Select on dbo.systhresholds to public
go

Grant Select on dbo.systypes to public
go

Grant Select on dbo.sysusermessages to public
go

Grant Select on dbo.sysusers to public
go

Grant Select on dbo.sysxtypes to public
go

alter table pruebaCap.dbo.personal_esc
add constraint personal_e_393049405 FOREIGN KEY (rfc) REFERENCES pruebaCap.dbo.personal(rfc)
go

alter table pruebaCap.dbo.personal_esc
add constraint personal_e_409049462 FOREIGN KEY (idescolaridad) REFERENCES pruebaCap.dbo.escolaridad_p(idescolaridad)
go

alter table pruebaCap.dbo.departamento
add constraint departamen_489049747 FOREIGN KEY (idsub) REFERENCES pruebaCap.dbo.subdireccion(idsub)
go

alter table pruebaCap.dbo.puesto
add constraint puesto_537049918 FOREIGN KEY (iddepto) REFERENCES pruebaCap.dbo.departamento(iddepto)
go

alter table pruebaCap.dbo.personal_det
add constraint personal_d_585050089 FOREIGN KEY (rfc) REFERENCES pruebaCap.dbo.personal(rfc)
go

alter table pruebaCap.dbo.personal_det
add constraint personal_d_601050146 FOREIGN KEY (idinstituto) REFERENCES pruebaCap.dbo.institucion(idinstituto)
go

alter table pruebaCap.dbo.personal_det
add constraint personal_d_617050203 FOREIGN KEY (idpuesto) REFERENCES pruebaCap.dbo.puesto(idpuesto)
go

alter table pruebaCap.dbo.subgrupo_cap
add constraint subgrupo_c_697050488 FOREIGN KEY (idgrupo) REFERENCES pruebaCap.dbo.grupos_cap(idgrupo)
go

alter table pruebaCap.dbo.capacidades
add constraint capacidade_745050659 FOREIGN KEY (idsubgrupo) REFERENCES pruebaCap.dbo.subgrupo_cap(idsubgrupo)
go

alter table pruebaCap.dbo.historial_cap
add constraint historial__825050944 FOREIGN KEY (rfc) REFERENCES pruebaCap.dbo.personal(rfc)
go

alter table pruebaCap.dbo.historial_cap
add constraint historial__841051001 FOREIGN KEY (idcapacidad) REFERENCES pruebaCap.dbo.capacidades(idcapacidad)
go

alter table pruebaCap.dbo.historial_cap
add constraint historial__857051058 FOREIGN KEY (idotracap) REFERENCES pruebaCap.dbo.otras_capacidades(idotracap)
go

alter table pruebaCap.dbo.requerimientos_cap
add constraint requerimie_937051343 FOREIGN KEY (rfc) REFERENCES pruebaCap.dbo.personal(rfc)
go

alter table pruebaCap.dbo.requerimientos_cap
add constraint requerimie_953051400 FOREIGN KEY (idcapacidad) REFERENCES pruebaCap.dbo.capacidades(idcapacidad)
go

alter table pruebaCap.dbo.requerimientos_cap
add constraint requerimie_969051457 FOREIGN KEY (idtipo) REFERENCES pruebaCap.dbo.tipo_cap(idtipo)
go

alter table pruebaCap.dbo.requerimientos_cap
add constraint requerimie_985051514 FOREIGN KEY (idotracap) REFERENCES pruebaCap.dbo.otras_capacidades(idotracap)
go

alter table pruebaCap.dbo.barreras_respuestas
add constraint barreras_r_1065051799 FOREIGN KEY (rfc) REFERENCES pruebaCap.dbo.personal(rfc)
go

alter table pruebaCap.dbo.barreras_temp
add constraint barreras_t_1113051970 FOREIGN KEY (idbarrera) REFERENCES pruebaCap.dbo.barreras_personal(idbarrera)
go

alter table pruebaCap.dbo.barreras_temp
add constraint barreras_t_1129052027 FOREIGN KEY (idrespuesta) REFERENCES pruebaCap.dbo.barreras_respuestas(idrespuesta)
go

alter table pruebaCap.dbo.instructor_cap
add constraint instructor_1209052312 FOREIGN KEY (idempresa) REFERENCES pruebaCap.dbo.instructor_emp(idempresa)
go

alter table pruebaCap.dbo.horario_gral_det
add constraint horario_gr_1321052711 FOREIGN KEY (idhorario) REFERENCES pruebaCap.dbo.horario_gral(idhorario)
go

alter table pruebaCap.dbo.horario_gral_det
add constraint horario_gr_1337052768 FOREIGN KEY (idhd) REFERENCES pruebaCap.dbo.horario_det(idhd)
go

alter table pruebaCap.dbo.capacidad_autorizada
add constraint capacidad__1385052939 FOREIGN KEY (idcapacidad) REFERENCES pruebaCap.dbo.capacidades(idcapacidad)
go

alter table pruebaCap.dbo.capacidad_autorizada
add constraint capacidad__1401052996 FOREIGN KEY (idinstructor) REFERENCES pruebaCap.dbo.instructor_cap(idinstructor)
go

alter table pruebaCap.dbo.capacidad_autorizada
add constraint capacidad__1417053053 FOREIGN KEY (idhorario) REFERENCES pruebaCap.dbo.horario_gral(idhorario)
go

alter table pruebaCap.dbo.capacidad_autorizada
add constraint capacidad__1433053110 FOREIGN KEY (idinstituto) REFERENCES pruebaCap.dbo.institucion(idinstituto)
go

alter table pruebaCap.dbo.evaluacion_crit_tmp
add constraint evaluacion_1529053452 FOREIGN KEY (idcriterio) REFERENCES pruebaCap.dbo.evaluacion_criterios(idcriterio)
go

alter table pruebaCap.dbo.evaluacion_ins
add constraint evaluacion_1577053623 FOREIGN KEY (idinstructor) REFERENCES pruebaCap.dbo.instructor_cap(idinstructor)
go

alter table pruebaCap.dbo.evaluacion_ins
add constraint evaluacion_1593053680 FOREIGN KEY (idcet) REFERENCES pruebaCap.dbo.evaluacion_crit_tmp(idcet)
go

alter table pruebaCap.dbo.eventos_rel
add constraint eventos_re_1673053965 FOREIGN KEY (idevento) REFERENCES pruebaCap.dbo.eventos_gral(idevento)
go

alter table pruebaCap.dbo.eventos_rel
add constraint eventos_re_1689054022 FOREIGN KEY (clavecurso) REFERENCES pruebaCap.dbo.capacidad_autorizada(clavecurso)
go

alter table pruebaCap.dbo.criterios_enc
add constraint criterios__idgrup_1365576872 FOREIGN KEY (idgrupo) REFERENCES pruebaCap.dbo.criterios_grupos(idgrupo)
go

alter table pruebaCap.dbo.encuesta_instructor
add constraint encuesta_i_1865054649 FOREIGN KEY (clavecurso) REFERENCES pruebaCap.dbo.capacidad_autorizada(clavecurso)
go

alter table pruebaCap.dbo.enc_cri_ins
add constraint enc_cri_in_1913054820 FOREIGN KEY (idencuesta) REFERENCES pruebaCap.dbo.encuesta_instructor(idencuesta)
go

alter table pruebaCap.dbo.enc_cri_ins
add constraint enc_cri_in_1929054877 FOREIGN KEY (idcritenc) REFERENCES pruebaCap.dbo.criterios_enc(idcritenc)
go


