/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para crear las tablas de la base de datos, por ahora es la primera que se ejecuta xk no se si se van a hacer roles jejejeje
  ------------------------------------------------------------------------------------------------------------------------------------
*/

/* Institucion */
create table cp_institucion(
    idinstituto varchar(20) not null,   /* clave del Instituto Tecnol[ogico o Centro en caso del ITT es 20DIT0004L*/
    descripcion varchar(100) not null,  /* nombre del IT o Centro es este caso: Instituto Tecnológico de Tlaxiaco*/
    primary key(idinstituto)
);


/* Tablas para llevar el control de la escolaaridad de los empleados */
create table cp_escolaridad_p(
    idescolaridad integer identity,
    descripcion varchar(100) not null,
    estado char(1) default '1',
    primary key(idescolaridad)
);

create table cp_personal_esc(
    rfc char(13) not null,
    idescolaridad integer,
    estado char(1) default '1',
    foreign key(rfc) references personal(rfc),
    foreign key(idescolaridad) references cp_escolaridad_p(idescolaridad)
);

/* Tabla sobre detalle de personal, el departamento y el puesto aun no tengo esa informacion pero tendre que tenerla xk servira despues */
create table cp_subdireccion(
    idsub varchar(10) not null,
    descripcion varchar(100),
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(idsub)
);

create table cp_departamento(
    iddepto varchar(10) not null,
    descripcion varchar(100) not null,
    idsub varchar(10) not null,
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(iddepto),
    foreign key(idsub) references cp_subdireccion(idsub)
);

create table cp_puesto(
    idpuesto varchar(15) not null,
    descripcion varchar(100) not null,
    iddepto varchar(10) null,
    idsub varchar(10) null,
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(idpuesto),
    foreign key(iddepto) references cp_departamento(iddepto),
    foreign key(idsub) references cp_subdireccion(idsub)
);

create table cp_personal_det(
    rfc char(13) not null,
    idinstituto varchar(20),
    idpuesto varchar(15) not null,
    actividades varchar(300) null,           /* se almacena una activadad tras otra con un delimitante ';' entre ellos*/
    estado char(1) default '1',
    foreign key(rfc) references personal(rfc),
    foreign key(idinstituto) references cp_institucion(idinstituto),
    foreign key(idpuesto) references cp_puesto(idpuesto)
);

/* Tablas para llevar el control del almacen de las capacidades disponibles que el empleado puede seleccionar para una posible y posterior capacitaci[on */
create table cp_grupos_cap(
    idgrupo integer identity,
    descripcion varchar(60) not null,
    estado char(1) default '1',
    primary key(idgrupo)
);
create table cp_subgrupo_cap(
    idsubgrupo integer identity,
    descripcion varchar(60) not null,
    idgrupo integer,
    estado char(1) default '1',
    primary key(idsubgrupo),
    foreign key(idgrupo) references cp_grupos_cap(idgrupo)
);

create table cp_capacidades(		/** actualizada -> redireccionar los datos desde cp_otras_capacidades **/
    idcapacidad varchar(10) not null,
    descripcion varchar(150) not null,
    idsubgrupo integer,
    enlistado char(1) default '0',		/* si capacidad pertenece a PTA y POA */
    fecha datetime null,
    estado char(1) default '1',
    primary key(idcapacidad),
    foreign key(idsubgrupo) references cp_subgrupo_cap(idsubgrupo)
);

/* Tala de control de Eventos llevados a cabo al a;o */
create table cp_eventos_gral(
    idevento varchar(10) not null,
    semestre varchar(50),       /* solo permite agosto-diciembre-2013, enero-junio-2013; la fecha es del semestre actual asi que sera transparente al usuario ... aunk no se si es necesario */
    anio integer,               /* creo que tambien ira verano-2013, o intersemestrales */
    estado char(1) default '2',
    primary key(idevento)
);

/* Capacidades introducidas por el usuario que no estan dentro del catalogo asignamos un id temporal y creo que aqui se quedan xk no me han dicho 
si una vez que se obtiene la clave del curso se va a utilizar despues o no jejejeje */
/** create table cp_otras_capacidades(
    idotracap varchar(8) not null,
    descripcion varchar(150),
    fecha datetime not null,   
    idsubgrupo integer, 
    estado char(1) default '1',
    primary key(idotracap),
    foreign key(idsubgrupo) references cp_subgrupo_cap(idsubgrupo)
); **/
create table cp_historial_cap(	
    idhp integer identity,
    rfc char(13),
    idcapacidad varchar(10) null,
    idotracap varchar(8) null,
    fecha datetime,
    estado char(1) default '1',
    idevento varchar(10) null,			/** actualizado **/
    primary key(idhp),
    foreign key(rfc) references personal(rfc),
    foreign key(idcapacidad) references cp_capacidades(idcapacidad),
    foreign key(idotracap) references cp_otras_capacidades(idotracap),
    foreign key(idevento) references cp_eventos_gral(idevento)
);

/* Requerimentos de capacitacion hecho por el empleado tambien */
create table cp_tipo_cap(  						/* aunque el tipo de competencia no son mas que tres y es muy posible que no se cambien de todos modos es para control ... creo ¬_¬ */
    idtipo integer identity,
    descripcion varchar(50) not null,
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(idtipo)
);
create table cp_requerimientos_cap(				-- Falta procedimiento
    rfc char(13) not null,
    idcapacidad varchar(10) null,
    idotracap varchar(8) null,
    idtipo integer null,
    fecha datetime not null,
	estado char(1) default '2',
	idevento varchar(10) null,			/** actualizado **/
    foreign key(rfc) references personal(rfc),
    foreign key(idcapacidad) references cp_capacidades(idcapacidad),
    foreign key(idtipo) references cp_tipo_cap(idtipo),
    foreign key(idotracap) references cp_otras_capacidades(idotracap),
    foreign key(idevento) references cp_eventos_gral(idevento)
);

create table cp_barreras_personal(     /* no encontre otro nombre mas original pero hace referencia a un cuestionario sobre las barreras y el orden en que pueden afectar  */
    idbarrera integer identity,       /* el buen desempe;o de sus funciones*/
    descripcion varchar(50) not null,
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(idbarrera)
);
create table cp_barreras_respuestas(   /* y esta es la tabla de respuestas de los empleados, por cierto se crea una nueva respuesta antes de escribir*/
    idrespuesta integer identity,     /* los datos en la tabla de barreras_temp*/
    rfc char(13) not null,
    fecha datetime,		
    estado char(1) default '1',
    idevento varchar(10) null,			/** actualizado **/
    primary key(idrespuesta),
    foreign key(rfc) references personal(rfc),
    foreign key(idevento) references cp_eventos_gral(idevento)
);
create table cp_barreras_temp(
    idrespuesta integer,
    idbarrera integer not null,
    importancia integer not null,
    fecha datetime,
    foreign key(idbarrera) references cp_barreras_personal(idbarrera),
    foreign key(idrespuesta) references cp_barreras_respuestas(idrespuesta)
);

/* Tablas control de capacidades autorizadas, incluytendo datos como instructor, fecha y horario */
create table cp_instructor_emp(
    idempresa varchar(10) not null,
    descripcion varchar(50) not null,
    pais varchar(50) default 'MÉXICO',
    estadoe varchar(50) default 'OAXACA',
    ciudad varchar(50) default 'TLAXIACO',
    direccion varchar(100),
    email varchar(50) null,
    telefonos varchar(60) null,  /* solo puede almacenar maximo 4 numeros con un delimitante ';' entre cada numero*/
    estado char(1) default '1',
    primary key(idempresa)
);
create table cp_instructor_cap(
    idinstructor varchar(10) not null,
    titulo varchar(5) not null,      /* default 'C.' */
    nombre varchar(100) not null,   /*con apellidos*/
    direccion varchar(100) null,	
    email varchar(50) null,
    telefono varchar(15) null,
    idempresa varchar(10),
    externo char(1) null,
    estado char(1) default '1',
	rfc char(13) null,
	curp varchar(20) null,
    primary key(idinstructor),
    foreign key(idempresa) references cp_instructor_emp(idempresa)
);

create table cp_horario_gral(
    idhorario varchar(10) not null,
    fechainicio datetime not null,      /* fecha de realizacion de la capacitacion dentro del evento .... mmm quiza evento no nos vaya a servir de mucho pero bueno jeje */
    fechafin datetime not null,
    totalhoras integer null,                 /* duracion de la capacitacion*/
    observaciones varchar(100) null,
    estado char(1) default '2',			-- estado = 2 significa que esta activo y es el ultimo registro insertado
    primary key(idhorario)				-- el valor cambia a 1 cuando se ha insertado un nuevo registro
);										-- y 0 cuando esta desactivado el horario
create table cp_horario_det(
    idhd varchar(10) not null,
    dia varchar(12),					-- el dia se define con nombres: "lunes", "martes"
    fecha_dia datetime,
    horainicio varchar(11) null,      /* la hora se debe describir en la bd en un formato de 24 hrs como sigue: 10:30, 10:00-12:00, 14:00-15:30 */
    horamedio varchar(23) null,       /* la hora se debe describir en formato de 24 hrs como sigue: 11:00, 10:00-11:30, 13:00-15:00;17:00-19:00 se admiten hasta dos lapsos */
    horafin varchar(11) null,         /* la hora se debe describir de la misma forma que horainicio, en ambas solo se admitira 1 hora o 2 horas separadas por un '-'. */
    estado char(1) default '2',			
    primary key(idhd)
);
create table cp_horario_gral_det(
    idhorario varchar(10) not null,
    idhd varchar(10) not null,
    foreign key(idhorario) references cp_horario_gral(idhorario),
    foreign key(idhd) references cp_horario_det(idhd),
    primary key(idhorario,idhd)
);

create table cp_temario_cap(
	idtemario varchar(10) not null,
	observaciones varchar(100) null,
	fecha datetime null,
	idevento varchar(10) null,			/** actualizado **/
	estado char(1) default '2',
	primary key(idtemario),
	foreign key(idevento) references cp_temario_cap(idevento)
);
create table cp_temas_cap(
	idtema varchar(10) not null,
	descripcion varchar(100) null,
	horas integer,
	estado char(1) default '2',
	primary key(idtema)
);
create table cp_temario_cap_tmp(
	idtemario varchar(10) not null,
	idtema varchar(10) not null,
	foreign key (idtemario) references cp_temario_cap(idtemario),
	foreign key (idtema) references cp_temas_cap(idtema)
);

create table cp_costo_total(
	idcosto varchar(10) not null,
	hinstructor smallmoney null,
	hcafeteria smallmoney null,
	htraslado smallmoney null,
	hhospedaje smallmoney null, -- hospedaje y alimentacion
	hsalon smallmoney null,
	hmateriales smallmoney null,
	observaciones varchar(100) null,
	fecha datetime null,
	estado char(1) default '2',
	primary key(idcosto)
);
create table cp_curso_dir(
	iddirigido varchar(10) not null,
	director integer null,
	subdirector integer null,
	jefedeptos integer null,
	jefeoficina integer null,
	personaladmin integer null,
	personalsec integer null,
	personalserv integer null,
	estado char(1) default '2',
	primary key(iddirigido)
);
create table cp_cap_propuesta(  -- registro de curso
	idcapacidad_p varchar(15) not null,		-- hacer procedimiento de insercion y de generacion de codigo
	idcapacidad varchar(10) null,
	idotracap varchar(8) null,
	idinstructor varchar(10),
	idinstituto varchar(20),
	incprograma char(2), 		-- "A": Incluido en el PAC, "B": PTA y POA, "C": No incluido en el PAC, "D": No incluido en el PTA y POA
	duracion integer null, 		-- duracion total del curso en horas
	tipo char(1),				-- Tipo de servicio, "C": Curso, "T": Taller, "D": Diplomado, "O": Otro
	tipo_otro varchar(50) null,	-- solo si tipo = "O"
	modalidad char(1) null,		-- "P": Presencial, "L": En linea, "O": Otro
	modalidad_otro char(1) null,	-- solo si modalidad = "O"
	justificacion varchar(250),
	objetivo varchar(250),
	evaluacion varchar(70),		-- de forma escrita, la forma de evaluar
	idtemario varchar(10),
	iddirigido varchar(10),			-- "DR": Director, "SD": Subdirector, "JD": Jefes de departamento, "JO": Jefes de Oficina, "PA": Personal Administrativo, "PS": Personal Secretarial, "PR": Personal de Servicios
	idcosto	varchar(10),
	costototal smallmoney null,
	limite_personal integer,
	idhorario varchar(10),
	idcoordinador char(13), 	-- referencia a personal(rfc) por default Ing Audel.
	fecha datetime null,
	idevento varchar(10) null,			/** actualizado **/
	estado char(1) default '2',
	primary key(idcapacidad_p),
	foreign key(idcapacidad) references cp_capacidades(idcapacidad),
	foreign key(idinstructor) references cp_instructor_cap(idinstructor),
	foreign key(idinstituto) references cp_institucion(idinstituto),
	foreign key(idtemario) references cp_temario_cap(idtemario),
	foreign key(iddirigido) references cp_curso_dir(iddirigido),
	foreign key(idcosto) references cp_costo_total(idcosto),
	foreign key(idhorario) references cp_horario_gral(idhorario),
	foreign key(idcoordinador) references personal(rfc),
	foreign key(idevento) references cp_eventos_gral(idevento)
);
create table cp_pac_capacidades(			/** nueva **/			-- se reduce a un PAC por a;o
	idpac varchar(10) not null,
	idevento varchar(10) not null,
	fecha datetime null,
	anio integer,
	estado char(1) default '2',
	primary key(idpac),
	foreign key(idevento) references cp_eventos_gral(idevento)
);
create table cp_pac_lista(					/** nueva **/
	idlista identity,
	idpac varchar(10) not null,
	idcapacidad varchar(10) not null,
	iddirigido varchar(10) not null,
	fechainicio datetime null,
	fechafin datetime null,
	estado char(1) default '2',
	primary key(idlista),
	foreign key(idpac) references cp_pac_capacidades(idpac),
	foreign key(idcapacidad) references cp_capacidades(idcapacidad),
	foreign key(iddirigido) references cp_curso_dir(iddirigido)
);

create table cp_capacidad_autorizada(
    clavecurso varchar(25) not null,
    idcapacidad varchar(10) null
    idotracap varchar(8) null,
    idinstructor varchar(10),
    idhorario varchar(10),
    limite_personal integer,
    idinstituto varchar(20),
    estado char(1) default '1',
    fecha datetime null,
    idevento varchar(10) null,			/** actualizado **/
    primary key(clavecurso),
    foreign key(idcapacidad) references cp_capacidades(idcapacidad),
    foreign key(idinstructor) references cp_instructor_cap(idinstructor),
    foreign key(idhorario) references cp_horario_gral(idhorario),
    foreign key(idinstituto) references cp_institucion(idinstituto),
    foreign key(idevento) references cp_eventos_gral(idevento)
);

create table cp_cap_autorizada_personal(
	clavecurso varchar(25) not null,
	rfc char(13) not null,
	fecha datetime,
	estado char(1) default '1',
	foreign key(clavecurso) references cp_capacidad_autorizada(clavecurso),
	foreign key(rfc) references personal(rfc)
);

/* Tablas de control de criterio para seleccionar instructor */
create table cp_evaluacion_criterios(
    idcriterio integer identity,
    descripcion varchar(100),
    observaciones varchar(100) null,
    estado char(1) default '1',
    primary key(idcriterio)
);
create table cp_evaluacion_crit_tmp(
    idcet integer identity,
    idcriterio integer not null,
    escala integer not null,
    fecha datetime,
    primary key(idcet),
    foreign key(idcriterio) references cp_evaluacion_criterios(idcriterio),
    check (escala > 0 and escala < 7)
);
create table cp_evaluacion_ins(
    idinstructor varchar(10) not null,
    idcet integer not null,
    fecha datetime,
    estado char(1) default '1',
    idcapacidad varchar(10) null,
    aceptado char(1) null,
    idinstituto varchar(20) null,
    idevento varchar(10) null,			/** actualizado **/
    foreign key(idinstructor) references cp_instructor_cap(idinstructor),
    foreign key(idcet) references cp_evaluacion_crit_tmp(idcet),
    foreign key(idevento) references cp_eventos_gral(idevento)
);

/* Tablas de control de eventos (un evento es un lapso de tiempo, un semestre, en el que se realizan uno o varias capacitaciones) */
create table cp_eventos_rel(
    idevento varchar(10) not null,
    clavecurso varchar(25) not null,
    fecha datetime,
    foreign key(idevento) references cp_eventos_gral(idevento),
    foreign key(clavecurso) references cp_capacidad_autorizada(clavecurso)
);

/* Tablas de control de la encuesta de opinion al instructor */
create table cp_criterios_grupos(
    idgrupo integer identity,
    descripcion varchar(20) not null,
    estado char(1) default '1',
    primary key(idgrupo)
);

create table cp_criterios_enc(
    idcritenc integer identity,
    descripcion varchar(100) not null,
    observaciones varchar(100) null,
    idgrupo integer not null,
    estado char(1) default '1',
    primary key(idcritenc),
    foreign key(idgrupo) references cp_criterios_grupos(idgrupo)
);

create table cp_encuesta_instructor(
    idencuesta varchar(10) not null,
    clavecurso varchar(25) not null,
    observaciones varchar(200) null,
    estado char(1) default '2',
    fecha datetime null,
    primary key (idencuesta),
    foreign key(clavecurso) references cp_capacidad_autorizada(clavecurso)
);

create table cp_enc_cri_ins(
    idencuesta varchar(10) not null,
    idcritenc integer not null,
    valor integer not null,
    foreign key(idencuesta) references cp_encuesta_instructor(idencuesta),
    foreign key(idcritenc) references cp_criterios_enc(idcritenc)
);

create table cp_doomsday_mod(	-- a;adir a la BD actual para poder calcular los dias de la semana de una fecha dada
	idmes integer identity,
	mes varchar(15) not null,
	mesingles varchar(15) not null,
	anioreg integer not null,
	aniobis integer not null,
	primary key(idmes)
);
create table cp_doomsday_days(
	iddia integer identity,
	dia varchar(12),
	primary key(iddia)
);


-- Hola mundo!
create table cp_seguimiento(
	idseguimiento integer identity,
	descripcion varchar(250) not null, 
	estado char(1) default '1',
	primary key(idseguimiento)
);
create table cp_seguimiento_enc(
	idseg integer identity,
	idseguimiento integer,
	valor integer null,
	fecha datetime null,
	primary key(idseg),
	foreign key(idseguimiento) references cp_seguimiento(idseguimiento)
);
create table cp_seguimiento_jefe(
	rfcjefe char(13) not null,
	rfcapoyo char(13) not null,
	clavecurso varchar(25) not null,
	fecha datetime null,
	semestre varchar(50) null,
	equipo char(1) default '1',	
	apoyo char(1) default '1',
	otro text null,
	comentarios text null,
	foreign key(rfcjefe) references personal(rfc),
	foreign key(rfcapoyo) references personal(rfc),
	foreign key(clavecurso) references cp_capacidad_autorizada(clavecurso)
);
create table cp_seguimiento_personal(
	rfcjefe char(13) not null,
	idseg integer not null,
	foreign key(idseg) references cp_seguimiento_enc(idseg)
);


-- Recheck 2
/*
create table personal (
	rfc                             char(13)                         not null  primary key,
	clave_centro_seit               char(10)                             null  ,
	clave_area                      char(6)                              null  ,
	curp_empleado                   char(18)                             null  ,
	no_tarjeta                      int                                  null  ,
	apellidos_empleado              varchar(40)                          null  ,
	nombre_empleado                 varchar(40)                          null  ,
	horas_nombramiento              int                                  null  ,
	nombramiento                    char(1)                          not null  ,
	clases                          char(1)                              null  ,
	ingreso_rama                    char(6)                              null  ,
	inicio_gobierno                 char(6)                              null  ,
	inicio_sep                      char(6)                              null  ,
	inicio_plantel                  char(6)                              null  ,
	domicilio_empleado              varchar(60)                          null  ,
	colonia_empleado                varchar(40)                          null  ,
	codigo_postal_empleado          int                                  null  ,
	localidad                       varchar(30)                          null  ,
	telefono_empleado               varchar(30)                          null  ,
	sexo_empleado                   char(1)                              null  ,
	estado_civil                    char(1)                              null  ,
	fecha_nacimiento                datetime                             null  ,
	lugar_nacimiento                int                                  null  ,
	institucion_egreso              varchar(50)                          null  ,
	nivel_estudios                  char(1)                              null  ,
	grado_maximo_estudios           char(1)                              null  ,
	estudios                        varchar(250)                         null  ,
	fecha_termino_estudios          datetime                             null  ,
	fecha_titulacion                datetime                             null  ,
	cedula_profesional              char(15)                             null  ,
	especializacion                 varchar(50)                          null  ,
	idiomas_domina                  varchar(60)                          null  ,
	status_empleado                 char(2)                              null  ,
	foto                            image                                null  ,
	firma                           image                                null  ,
	correo_electronico              varchar(60)                          null  ,
	padre                           varchar(50)                          null  ,
	madre                           varchar(50)                          null  ,
	conyuge                         varchar(50)                          null  ,
	hijos                           varchar(100)                         null  ,
	num_acta                        int                                  null  ,
	num_libro                       int                                  null  ,
	num_foja                        int                                  null  ,
	num_ano                         int                                  null  ,
	num_cartilla_smn                char(15)                             null  ,
	ano_clase                       int                                  null  ,
	pigmentacion                    char(1)                              null  ,
	pelo                            char(1)                              null  ,
	frente                          char(1)                              null  ,
	cejas                           char(1)                              null  ,
	ojos                            char(1)                              null  ,
	nariz                           char(1)                              null  ,
	boca                            char(1)                              null  ,
	estaturamts                     numeric(3,2)                         null  ,
	pesokg                          numeric(5,2)                         null  ,
	senas_visibles                  varchar(255)                         null  ,
	pais                            varchar(30)                          null  ,
	pasaporte                       varchar(40)                          null  ,
	fm                              varchar(30)                          null  ,
	inicio_vigencia                 datetime                             null  ,
	termino_vigencia                datetime                             null  ,
	entrada_salida                  char(1)                              null  ,
	observaciones_empleado          varchar(254)                         null  ,
	area_academica                  char(6)                              null  ,
	tipo_personal                   char(1)                              null  ,
	tipo_control                    char(1)                              null  ,
	rfc2                            char(13)                             null   
)
*/
