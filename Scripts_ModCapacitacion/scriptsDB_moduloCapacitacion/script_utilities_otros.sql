alter table cp_capacidad_autorizada add fecha datetime null;

alter table cp_cap_autorizada_personal add  fecha datetime null;

create procedure cp_crear_clave_temario_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_temario_cap) <> 0
	    begin
			set @clavea = (select idtemario from cp_temario_cap where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_temas_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_temas_cap) <> 0
	    begin
			set @clavea = (select idtema from cp_temas_cap where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_costo_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_costo_total) <> 0
	    begin
			set @clavea = (select idcosto from cp_costo_total where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_cursodir_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_curso_dir) <> 0
	    begin
			set @clavea = (select iddirigido from cp_curso_dir where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_capacidadp_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_cap_propuesta) <> 0
	    begin
			set @clavea = (select idcapacidad_p from cp_cap_propuesta where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;


create procedure i_temario @fecha datetime,
	@echo char(1) output
	as
	declare @idp varchar(10), @fh datetime
		-- crear id
         exec cp_crear_clave_temario_10 "TMR", @idp output
	
	if @fecha = ""
	begin
		set @fh = (select getdate())
	end
	else
	begin
		set @fh = @fecha
	end

	if (select count(*) from cp_temario_cap where idtemario = @idp) = 0
    begin
		update cp_temario_cap set estado = '1' where estado = '2'
	
		insert into cp_temario_cap(idtemario,fecha) values(upper(@idp),@fh)
        print 'Temario dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Temario dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_temas @des varchar(100), @hrs integer,
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_temas_10 "TMA", @idp output

	if (select count(*) from cp_temas_cap where idtema = @idp) = 0
    begin
		update cp_temas_cap set estado = '1' where estado = '2'
		
		insert into cp_temas_cap(idtema,descripcion,horas) values(upper(@idp),upper(@des),@hrs)
		
		set @idtmr = (select idtemario from cp_temario_cap where estado='2')
		insert into cp_temario_cap_tmp(idtemario,idtema) values(upper(@idtmr),upper(@idp))
		
        print 'Tema dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Tema dada de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_costo_total @hins smallmoney, @hcaf smallmoney, @htras smallmoney, @hhos smallmoney, @hsal smallmoney, @hmat smallmoney, 
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_costo_10 "CST", @idp output

	if (select count(*) from cp_costo_total where idcosto = @idp) = 0
    begin
		update cp_costo_total set estado = '1' where estado = '2'
		
		insert into cp_costo_total(idcosto,hinstructor,hcafeteria,htraslado,hhospedaje,hsalon,hmateriales)
			values(upper(@idp),@hins,@hcaf,@htras,@hhos,@hsal,@hmat)
		
        print 'Costo dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Costo dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_curso_dir @ndir integer, @nsub integer, @njdeptos integer, @njoficina integer, @npadmin integer, @npsec integer, @npserv integer,
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_cursodir_10 "CDG", @idp output

	if (select count(*) from cp_curso_dir where iddirigido = @idp) = 0
    begin
		update cp_curso_dir set estado = '1' where estado = '2'
		
		insert into cp_curso_dir(iddirigido,director,subdirector,jefedeptos,jefeoficina,personaladmin,personalsec,personalserv)
			values(upper(@idp),@ndir,@nsub,@njdeptos,@njoficina,@npadmin,@npsec,@npserv)
		
        print 'Datos de Curso dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Curso dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_curso_propuesto @idcap varchar(10), @idinstr varchar(10), @idinsti varchar(20), @incp char(1), @cdur integer, @tipo char(1),
		@t_otro varchar(50), @mod char(1), @m_otro varchar(50), @jus varchar(250), @obj varchar(250), @eva varchar(70), @costo smallmoney,
		@idc char(13), @fecha datetime,
		
	@echo char(1) output
	as
	declare @idp varchar(10), @fh datetime, @v1 varchar(10), @v2 varchar(10), @v3 varchar(10), @v4 varchar(10)
		-- crear id
         exec cp_crear_clave_capacidadp_10 "CPP", @idp output
	
	if @fecha = ""
	begin
		set @fh = (select getdate())
	end
	else
	begin
		set @fh = @fecha
	end
	
	if (select count(*) from cp_cap_propuesta where idcapacidad_p = @idp) = 0
    begin
		update cp_cap_propuesta set estado = '1' where estado = '2'
		
		set @v1 = (select idtemario from cp_temario_cap where estado='2')
		set @v2 = (select iddirigido from cp_curso_dir where estado='2')
		set @v3 = (select idcosto from cp_costo_total where estado='2')
		set @v4 = (select idhorario from cp_instructor_cap where estado='2')
		
		insert into cp_cap_propuesta(idcapacidad_p,idcapacidad,idinstructor,idinstituto,incprograma,duracion,tipo,tipo_otro,modalidad,modalidad_otro,
			justificacion,objetivo,evaluacion,idtemario,iddirigido,idcosto,costototal,idhorario,idcoordinador,fecha)
			
			values(upper(@idp),upper(@idcap),upper(@idinstr),upper(@idinsti),upper(@incp),@cdur,upper(@tipo),upper(@t_otro),upper(@mod),
				upper(@m_otro), upper(@jus), upper(@obj), upper(@eva), upper(@v1), upper(@v2), upper(@v3),@costo, upper(@v4), upper(@idc), @fh)
		
        print 'Datos de Curso dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Curso dado de alta previamente'
		select @echo = cast(0 as char)
    end
;


