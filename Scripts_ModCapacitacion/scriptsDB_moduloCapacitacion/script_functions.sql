/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para crear las funciones
  ------------------------------------------------------------------------------------------------------------------------------------
*/

-- Funciones de consulta
/*create function s_personal 
    returns cursor
    begin
        declare personal_c scroll cursor
        for select idinstituto, descripcion from institucion;

        return personal_c
    end


create function UltimoDiaDelMes (@fecha datetime)
returns int
as
begin
    declare @UltimoDia int
    set @UltimoDia = day(dateadd(day,-1,dateadd(month,1,dateadd(day,1-day(@fecha),@fecha))))
    return @UltimoDia
end 


-- Funciones de Retorno de una Clave
create function crear_clave_10 (@des varchar(100), @suf varchar(3))
	returns varchar(10)
	begin
		declare @v1 varchar(100), @v2 varchar(10), @len int
		set @v1 = (select str_replace(@des," EL "," "))
		set @v1 = (select str_replace(@v1," Y "," "))
		set @v1 = (select str_replace(@v1," SU "," "))
		set @v1 = (select str_replace(@v1," DEL ",""))
		set @v1 = (select str_replace(@v1," LA "," "))
		set @v1 = (select str_replace(@v1,"SUBDIRECCIÓN ",NULL))
		set @v1 = (select str_replace(@v1,"DEPARTAMENTO ",NULL))
		set @v1 = (select str_replace(@v1,"PUESTO ",NULL))
		
		set @len = (select len(@vr1))
		set @v2 = @suf + "-" + (select substring(@v1,1,1)) + (select substring(@v1,3,1)) + (select substring(@v1,2,1)) + (select substring(@v1,4,1)) + cast((@len-3),varchar)
		
		return @v2
	end
*/



