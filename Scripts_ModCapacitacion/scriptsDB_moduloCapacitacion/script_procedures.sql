/*
  ------------------------------------------------------------------------------------------------------------------------------------
    M[odulo de Capacitaci[on del SGC para el SII
    
    Script para crear los procedimientos
  ------------------------------------------------------------------------------------------------------------------------------------
*/


-- Institucion
--drop procedure i_escolaridad_p;

-- condicion para elimiar desde un evento hasta un horario, es que no se puede eliminar un evento u horario reciente sin que se cree otro nuevo
-- de esta forma se cuida la integridad de los datos y se puede administrar mejor xk para la creacion de una clave nueva
-- es necesario que el estado='2' que simboliza la ultima tupla insertada

-- Procedimientos Para la Generación de Identificadores de Tupla

create procedure cp_crear_clave_10 @des varchar(100), @suf varchar(3),
    @echo varchar(10) output
    as
    declare @v1 varchar(100), @len int
	
	    set @v1 = (select str_replace(@des," EL "," "))
		set @v1 = (select str_replace(@v1," Y "," "))
		set @v1 = (select str_replace(@v1," SU "," "))
		set @v1 = (select str_replace(@v1," DEL ",""))
		set @v1 = (select str_replace(@v1," LA "," "))
		set @v1 = (select str_replace(@v1,"SUBDIRECCIÓN ",NULL))
		set @v1 = (select str_replace(@v1,"DEPARTAMENTO ",NULL))
		set @v1 = (select str_replace(@v1,"PUESTO ",NULL))
		
		set @len = (select len(@v1)) - 3
		set @echo = @suf + "-" + (select substring(@v1,1,1)) + (select substring(@v1,3,1)) + (select substring(@v1,2,1)) + (select substring(@v1,4,1)) + cast(@len as varchar)
        
        select upper(@echo)
;

create procedure cp_crear_clave_8 @des varchar(150), @suf varchar(3),
    @echo varchar(8) output
    as
    declare @v1 varchar(150), @len int
	
	    set @v1 = (select str_replace(@des," EL "," "))
		set @v1 = (select str_replace(@v1," Y "," "))
		set @v1 = (select str_replace(@v1," SU "," "))
		set @v1 = (select str_replace(@v1," DEL ",""))
		set @v1 = (select str_replace(@v1," LA "," "))
		set @v1 = (select str_replace(@v1,"SUBDIRECCIÓN ",NULL))
		set @v1 = (select str_replace(@v1,"DEPARTAMENTO ",NULL))
		set @v1 = (select str_replace(@v1,"PUESTO ",NULL))
		
		set @len = ((select len(@v1)) - 3)
		if @len >= 100
		begin
			set @len = @len - 51
		end
		set @echo = @suf + "-" + (select substring(@v1,1,1)) + (select substring(@v1,3,1)) + (select substring(@v1,2,1)) + (select substring(@v1,4,1)) + cast(@len as varchar)
        
        select upper(@echo)
;

create procedure cp_crear_clave_hora_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @horarioa varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_horario_gral) <> 0
	    begin
			set @horarioa = (select idhorario from cp_horario_gral where estado = '2')
			
			set @v1 = (select right(@horarioa, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_horadet_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @horarioa varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_horario_det) <> 0
	    begin
			set @horarioa = (select idhd from cp_horario_det where estado = '2')
			
			set @v1 = (select right(@horarioa, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_eventos_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_eventos_gral) <> 0
	    begin
			set @clavea = (select idevento from cp_eventos_gral where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_encuesta_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_encuesta_instructor) <> 0
	    begin
			set @clavea = (select idencuesta from cp_encuesta_instructor where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_calcular_daysdiff @fechaini datetime, @fechafin datetime,
	@echo integer output
	as
    declare @v1 integer
    set @v1 = (select datediff(dd,@fechaini,@fechafin))

	select @echo = (select abs(@v1))
;

create procedure cp_calcular_dia @fecha datetime,
	@echo varchar(12) output
	as
	declare @dia integer, @daysdiff integer, @anio integer, @f varchar(12), @fa datetime, @fa2 datetime, @op1 integer, @op2 integer, @op3 integer, @op4 integer, @mesact varchar(15)
	
	set @anio = (select datepart(yy,@fecha))
    set @f = "02/01/" + (select cast(@anio as varchar))
    set @fa = (select cast(@f as datetime))
	
    set @fa2 = (select dateadd(mm,1,@fa))
	exec cp_calcular_daysdiff @fa, @fa2, @daysdiff output
	
    set @mesact = (select datename(mm, @fecha))

	if @daysdiff = 28
	begin
		set @op1 = ((@anio - 1) % 7)
		set @op2 = ( ((@anio -1)/4) - (3 * ( (((@anio - 1)/100) + 1) / 4)) ) % 7
		set @op3 = (select anioreg from cp_doomsday_mod where mes = upper(@mesact) or mesingles = upper(@mesact))
		set @op4 = (select datepart(dd,@fecha))
		set @dia = (@op1 + @op2 + @op3 + @op4) % 7
		
		set @dia = (select floor(@dia)) + 1
		select @echo = (select dia from cp_doomsday_days where iddia = @dia)
	end
	else
	begin
		set @op1 = ((@anio - 1) % 7)
		set @op2 = ( ((@anio -1)/4) - (3 * ( (((@anio - 1)/100) + 1) / 4)) ) % 7
		set @op3 = (select aniobis from cp_doomsday_mod where mes = upper(@mesact) or mesingles = upper(@mesact))
		set @op4 = (select datepart(dd,@fecha))
		set @dia = (@op1 + @op2 + @op3 + @op4) % 7
		
        set @dia = (select floor(@dia)) + 1
		select @echo = (select dia from cp_doomsday_days where iddia = @dia)
	end	
;

create procedure cp_calcular_semestre @fecha datetime = NULL,
	@echo varchar(50) output
	as	
	declare @anio integer, @mes varchar(12), @v1 varchar(12), @sem integer
	
	if @fecha = NULL
	begin
		set @fecha = (select getdate())
	end
	
	set @anio = (select datepart(yy,@fecha))
	set @mes = (select datename(mm,@fecha))
	
	set @sem = (select datepart(wk,@fecha))
	
	if @sem >= 3 and @sem < 24
	begin
		set @echo = ('enero-junio' + '-' + cast(@anio as varchar))
        select upper(@echo)
	end
	else
	begin
		if @sem >= 24 and @sem < 32
		begin
			set @echo = ('verano' + '-' + cast(@anio as varchar))
            select upper(@echo)
		end
		else
		begin
			if @sem >= 32 and @sem < 49
			begin
				set @echo = ('agosto-diciembre' + '-' + cast(@anio as varchar))
                select upper(@echo)
			end
			else
			begin
				set @echo = ('invierno' + '-' + cast(@anio as varchar))
                select upper(@echo)
			end
		end
	end
;



-- Procedimientos de Insercion 

create procedure i_escolaridad_p @des varchar(100), 
	@echo char(1) output
    as
    if  (select count(*) from cp_escolaridad_p where descripcion = upper(@des)) = 0
    begin		
        insert into cp_escolaridad_p(descripcion) values(upper(@des))
        print 'Escolaridad dada de alta Correctamente'
        select @echo = cast(1 as char)
    end
    else
    begin
		if (select estado from cp_escolaridad_p where descripcion = upper(@des)) = '0'
		begin
			update cp_escolaridad_p set estado = '1' where descripcion = upper(@des)
            print 'Escolaridad dada de alta Correctamente'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Escolaridad dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_personal_esc @rfc char(13), @idesc integer,
	@echo char(1) output
    as
	declare @aux0 integer
	if @idesc > 10
	begin
		set @aux0 = @idesc / 10
		set @aux0 = (select floor(@aux0))
	end
	else 
	begin
		set @aux0 = @idesc
	end
		
    if  (select count(*) from cp_personal_esc where rfc = upper(@rfc) and idescolaridad=@aux0) = 0
    begin
		if @idesc > 10
		begin
			insert into cp_personal_esc(rfc,idescolaridad,estado) values(upper(@rfc),@aux0,'0')
			print 'Escolaridad del Personal dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			insert into cp_personal_esc(rfc,idescolaridad,estado) values(upper(@rfc),@aux0,'1')
			print 'Escolaridad del Personal dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
    end
    else
    begin
		if @idesc > 10
		begin
			update cp_personal_esc set estado = '0'  where rfc = upper(@rfc) and idescolaridad = @aux0
			print 'Escolaridad del Personal dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			update cp_personal_esc set estado = '1'  where rfc = upper(@rfc) and idescolaridad = @aux0
			print 'Escolaridad del Personal dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		
    end
;

create procedure i_subdireccion @des varchar(100),
	@echo char(1) output
	as
	declare @idp varchar(10)
		-- crear id
         exec cp_crear_clave_10 @des, "SUB", @idp output

	if (select count(*) from cp_subdireccion where idsub = @idp and descripcion = upper(@des)) = 0
    begin
		insert into cp_subdireccion(idsub,descripcion) values(upper(@idp),upper(@des))
        print 'Subdireccion dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		if (select estado from cp_subdireccion where idsub = @idp and descripcion = upper(@des)) = '0'
		begin
			update cp_subdireccion set estado = '1' where idsub = @idp and descripcion = upper(@des)
            print 'Subdireccion dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Subdireccion dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_departamento @des varchar(100), @idsub varchar(10),
	@echo char(1) output
	as
    declare @idp varchar(10)
        exec cp_crear_clave_10 @des, "DPT", @idp output

	if (select count(*) from cp_departamento where descripcion = upper(@des) and idsub = upper(@idsub)) = 0
    begin
		insert into cp_departamento(iddepto,descripcion,idsub) values(upper(@idp),upper(@des),upper(@idsub))
        print 'Departamento dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_departamento where descripcion = upper(@des) and idsub = upper(@idsub)) = '0'
		begin
			update cp_departamento set estado = '1' where descripcion = upper(@des) and idsub = upper(@idsub)
            print 'Departamento dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Departamento dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_puesto @des varchar(100), @iddepto varchar(10),
	@echo char(1) output
	as
    declare @idp varchar(10), @idpt varchar(10)
    set @idpt = (select right(@iddepto, 3))
    
        exec cp_crear_clave_10 @des, @idpt, @idp output

	if (select count(*) from cp_puesto where descripcion = upper(@des) and iddepto = upper(@iddepto)) = 0
    begin
		insert into cp_puesto(idpuesto,descripcion,iddepto) values(upper(@idp),upper(@des),upper(@iddepto))
        print 'Puesto dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_puesto where descripcion = upper(@des) and iddepto = upper(@iddepto)) = '0'
		begin
			update cp_puesto set estado = '1' where descripcion = upper(@des) and iddepto = upper(@iddepto)
            print 'Puesto dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Puesto dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;
create procedure i_puesto_sub @des varchar(100), @idsub varchar(10),
	@echo char(1) output
	as
    declare @idp varchar(10), @idpt varchar(10)
    set @idpt = (select right(@idsub, 3))
    
        exec cp_crear_clave_10 @des, @idpt, @idp output

	if (select count(*) from cp_puesto where descripcion = upper(@des) and idsub = upper(@idsub)) = 0
    begin
		insert into cp_puesto(idpuesto,descripcion,idsub) values(upper(@idp),upper(@des),upper(@idsub))
        print 'Puesto dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_puesto where descripcion = upper(@des) and idsub = upper(@idsub)) = '0'
		begin
			update cp_puesto set estado = '1' where descripcion = upper(@des) and idsub = upper(@idsub)
            print 'Puesto dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Puesto dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_personal_det @rfc char(13), @idins varchar(20) = '20DIT0004L', @idp varchar(15),
	@echo char(1) output
	as
	if (select count(*) from cp_personal_det where rfc = upper(@rfc) and idinstituto = upper(@idins) and idpuesto = upper(@idp)) = 0
    begin
		insert into cp_personal_det(rfc,idinstituto,idpuesto) values(upper(@rfc),upper(@idins),upper(@idp))
        print 'Detalle del Personal dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Detalle del Personal dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_grupos_cap @des varchar(60),
	@echo char(1) output
	as
	if (select count(*) from cp_grupos_cap where descripcion = upper(@des)) = 0
    begin
		insert into cp_grupos_cap(descripcion) values(upper(@des))
        print 'Grupo de Capacitacion dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_grupos_cap where descripcion = upper(@des)) = '0'
		begin
			update cp_grupos_cap set estado = '1' where descripcion = upper(@des)
            print 'Grupo de Capacitacion dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Grupo de Capacitacion dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_subgrupo_cap @des varchar(60), @idg integer,
	@echo char(1) output
	as
	if (select count(*) from cp_subgrupo_cap where descripcion = upper(@des) and idgrupo = @idg) = 0
    begin
		insert into cp_subgrupo_cap(descripcion,idgrupo) values(upper(@des),@idg)
		print 'Subgrupo de Capacitacion dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_subgrupo_cap where descripcion = upper(@des) and idgrupo = @idg) = '0'
		begin
			update cp_subgrupo_cap set estado = '1' where descripcion = upper(@des) and idgrupo = @idg
			print 'Subgrupo de Capacitacion dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Subgrupo de Capacitacion dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_capacidades @idp varchar(10), @des varchar(150), @ids integer,
	@echo char(1) output
	as
	if (select count(*) from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = 0
    begin
		insert into cp_capacidades(idcapacidad,descripcion,idsubgrupo) values(upper(@idp),upper(@des),@ids)
		print 'Capacidad dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = '0'
		begin
			update cp_capacidades set estado = '1' where idcapacidad = upper(@idp) and descripcion = upper(@des)
			print 'Capacidad dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Capacidad dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

/** actualizada **/
create procedure i_otras_capacidades @idoc varchar(8) = NULL, @des varchar(150), @ids integer = NULL,
	@echo char(1) output
	as
    declare @idp varchar(10)
    if (@idoc = NULL)
    begin
        exec cp_crear_clave_8 @des, "C", @idp output
    end
    else
    begin
		set @idp = @idoc
    end

	if (select count(*) from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = 0
    begin
		insert into cp_capacidades(idcapacidad,descripcion,fecha,idsubgrupo) values(upper(@idp),upper(@des),getdate(),@ids)
		print 'Capacidad dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = '0'
		begin
			update cp_capacidades set estado = '1' where idcapacidad = upper(@idp) and descripcion = upper(@des)
			print 'Capacidad dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Capacidad dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_historial_cap @rfc varchar(13), @idcap varchar(10) = NULL,  @idotrac varchar(8) = NULL,		/** actualizado **/
	@echo char(1) output
	as
	declare @fecha datetime, @sem varchar(50), @semact varchar(50), @idevt varchar(10)
	
	set @semact = (select semestre from cp_eventos_gral where estado = '2')
	set @fecha = (select getdate())
	
		exec cp_calcular_semestre @fecha, @sem output
		
	if ((select count(*) from cp_historial_cap where rfc = upper(@rfc) and idcapacidad = upper(@idcap) and idotracap = upper(@idotrac)) = 0) and (upper(@sem) != upper(@semact))
    begin
		set @idevt = (select idevento from cp_eventos_gral where estado = '2')
		insert into cp_historial_cap(rfc,idcapacidad,idotracap,fecha,idevento) values(upper(@rfc),upper(@idcap),upper(@idotrac),getdate(),upper(@idevt))
		print 'Historial dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Historial dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_barreras_respuestas @rfc varchar(13),													/** actualizado **/
	@echo char(1) output
	as
	declare @fecha datetime, @sem varchar(50), @idevt varchar(10)
	set @fecha = (select fecha from cp_barreras_respuestas where rfc = upper(@rfc) and estado = '1')
	
		exec cp_calcular_semestre @fecha, @sem output
	
	if ((select count(*) from cp_barreras_respuestas where rfc = upper(@rfc)) = 0) and (upper(@sem) != (select semestre from cp_eventos_gral where estado = '2'))
    begin
		set @idevt = (select idevento from cp_eventos_gral where estado = '2')
		insert into cp_barreras_respuestas(rfc,fecha,idevento) values(upper(@rfc),getdate(),upper(@idevt))
		print 'Respuesta de Barreras dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Respuesta de Barreras dada de alta previamente'
		select @echo = cast(0 as char)
	end
;

create procedure i_barreras_temp @rfc varchar(13), @idb integer, @imp integer,
	@echo char(1) output
	as
	declare @fecha datetime, @sem varchar(50), @idres integer, @semact varchar(50)
	set @idres = (select max(idrespuesta) from cp_barreras_respuestas where rfc = upper(@rfc))
	
	
	set @fecha = (select getdate())
		exec cp_calcular_semestre @fecha, @semact output
	set @fecha = (select fecha from cp_barreras_temp where idrespuesta = @idres and idbarrera = @idb)
		exec cp_calcular_semestre @fecha, @sem output
	if ((select count(*) from cp_barreras_temp where idrespuesta = @idres and idbarrera = @idb) = 0) and (@sem <> @semact)
    begin
		insert into cp_barreras_temp(idrespuesta,idbarrera,importancia,fecha) values(@idres,@idb,@imp,getdate())
		print 'Respuesta de Barreras dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Respuesta de Barreras dada de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_instructor_emp @des varchar(50), @pais varchar(50) = "México", @estado varchar(50) = "Oaxaca", @ciudad varchar(50) = "Tlaxiaco", @dir varchar(100), @email varchar(50) = NULL, @tel varchar(60) = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10)
        exec cp_crear_clave_10 @des, "EPR", @idp output
        
	if (select count(*) from cp_instructor_emp where idempresa = upper(@idp) and descripcion = upper(@des)) = 0
    begin
		insert into cp_instructor_emp(idempresa,descripcion,pais,estadoe,ciudad,direccion,email,telefonos) 
			values(upper(@idp),upper(@des),upper(@pais),upper(@estado),upper(@ciudad),upper(@dir),upper(@email),upper(@tel))
		print 'Empresa dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_instructor_emp where idempresa = upper(@idp) and descripcion = upper(@des)) = '0'
		begin
			update cp_instructor_emp set estado = '1' where idempresa = upper(@idp) and descripcion = upper(@des)
		    print 'Empresa dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Empresa dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_instructor_cap @tit varchar(10) = "C.", @nom varchar(50), @dir varchar(100) = NULL, @email varchar(50) = NULL, @tel varchar(15) = NULL, @idemp varchar(10), @ext char(1) = NULL, @rfc char(13) = NULL, @curp varchar(20) = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10)
        exec cp_crear_clave_10 @nom, "INS", @idp output
        
	if (select count(*) from cp_instructor_cap where idinstructor = upper(@idp) and nombre = upper(@nom) and idempresa = upper(@idemp)) = 0
    begin
		insert into cp_instructor_cap(idinstructor,titulo,nombre,direccion,email,telefono,idempresa,externo,rfc,curp) 
			values(upper(@idp),upper(@tit),upper(@nom),upper(@dir),upper(@email),upper(@tel),upper(@idemp),@ext,upper(@rfc),upper(@curp))
        print 'Instructor dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_instructor_cap where idinstructor = upper(@idp) and nombre = upper(@nom) and idempresa = upper(@idemp)) = '0'
		begin
			update cp_instructor_cap set estado = '1' where idinstructor = upper(@idp) and nombre = upper(@nom) and idempresa = upper(@idemp)
            print 'Instructor dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Instructor dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_horario_gral @fini datetime, @ffin datetime, @th integer = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10)
        exec cp_crear_clave_hora_10 "HRH", @idp output
        
	if (select count(*) from cp_horario_gral where idhorario=@idp) = 0
    begin
		update cp_horario_gral set estado = '1' where estado = '2'
		
		insert into cp_horario_gral(idhorario,fechainicio,fechafin,totalhoras)
			values(upper(@idp),@fini,@ffin,@th)
        print 'Horario General dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_horario_gral where fechainicio = @fini and fechafin = @ffin and totalhoras = @th) = '0'
		begin
			update cp_horario_gral set estado = '1' where fechainicio = @fini and fechafin = @ffin and totalhoras = @th
            print 'Horario General dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Horario General dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_horario_det @fd datetime, @hini varchar(11) = NULL, @hmed varchar(23) = NULL, @hfin varchar(12) = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10), @idpa varchar(10), @dia varchar(12)
        exec cp_crear_clave_horadet_10 "HRD", @idp output
        set @idpa = (select idhorario from cp_horario_gral where estado = '2')
        
	if (select count(*) from cp_horario_det where idhd=@idp ) = 0
    begin
		-- buscar funcion para devolver el dia dependiendo de la fecha, asi que es el unico elemento a considerar
		-- set @dia = (select datepart(dd, current_date))
		-- calcular el dia de la semana dependiendo de la fecha
		exec cp_calcular_dia @fd, @dia output
		
		update cp_horario_det set estado = '1' where estado = '2'
		
		insert into cp_horario_det(idhd,dia,fecha_dia,horainicio,horamedio,horafin)
			values(@idp,@dia,@fd,@hini,@hmed,@hfin)
		
		insert into cp_horario_gral_det(idhorario, idhd) values(upper(@idpa),upper(@idp))
		
        print 'Detalle de Horario dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_horario_det where fecha_dia = @fd and horainicio = @hini and horamedio = @hmed and horafin = @hfin) = '0'
		begin
			update cp_horario_det set estado = '1' from cp_horario_det where fecha_dia = @fd and horainicio = @hini and horamedio = @hmed and horafin = @hfin
            print 'Detalle de Horario dado de alta CORRECTAMENTE'
			insert into cp_horario_gral_det(idhorario, idhd) values(upper(@idpa),upper(@idp))
		end
		else
		begin
			print 'Detalle de Horario dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

-- create procedure i_horario_gral_det @idh varchar(10), @idhd varchar(10),
--	@echo char(1) output
--	as       
--	if (select count(*) from cp_horario_gral_det where idhorario = @idh and idhd = @idhd ) = 0
--    begin
--    		
--		insert into cp_horario_det(idhorario,idhd) values(@idh, @idhd)
--		select @echo = cast(1 as char)
--    end
--	else
--    begin
--		print 'Enlace Horario dado de alta previamente'
--		select @echo = cast(0 as char)
--    end
--;

create procedure i_capacidad_autorizada @clave varchar(25), @idcap varchar(10) = NULL, @idotra varchar(8) = NULL, @idins varchar(10), @idh varchar(10), @fa datetime = NULL, @lim integer = 50, @itt varchar(20) = '20DIT0004L',
	@echo char(1) output
	as
	declare @faux datetime
	if @fa = NULL begin
		set @faux = (select getdate())
	end else begin
		set @faux = @fa
	end
	
	if (select count(*) from cp_capacidad_autorizada where clavecurso = upper(@clave)) = 0
    begin
    		
		insert into cp_capacidad_autorizada(clavecurso,idcapacidad,idotracap,idinstructor,idhorario,limite_personal,idinstituto,fecha)
			values(@clave,@idcap,@idotra,@idins,@idh,@lim,@itt,@faux)
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_capacidad_autorizada where clavecurso = upper(@clave)) = '0'
		begin
			update cp_capacidad_autorizada set estado = '1' where clavecurso = upper(@clave)
			print 'Capacidad Autorizada dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Capacidad Autorizada dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

create procedure i_evaluacion_crit_tmp @idcrit integer, @escala integer,
	@echo integer output
	as
        
	if (select count(*) from cp_evaluacion_crit_tmp where idcriterio = @idcrit and escala = @escala ) = 0
    begin
		insert into cp_evaluacion_crit_tmp(idcriterio,escala,fecha) values(@idcrit, @escala, getdate())
		select @echo = (select idcet from cp_evaluacion_crit_tmp where idcriterio = @idcrit and escala = @escala )
    end
	else
    begin
		select @echo = (select idcet from cp_evaluacion_crit_tmp where idcriterio = @idcrit and escala = @escala )
    end
;


/** Actualizada **/
create procedure i_evaluacion_ins @idins varchar(10), @fecha datetime, @capa varchar(10), @ace char(1), @ins varchar(20) = '20DIT0004L', @idcrit integer, @esc integer,
	@echo char(1) output
	as
    declare @semact varchar(50), @sem varchar(50), @semi datetime, @idcet integer, @idevt varchar(10)
	
		set @semact = (select semestre from cp_eventos_gral where estado = '2')
        set @sem = (select getdate())
		exec cp_calcular_semestre @sem, @sem output
		
		exec i_evaluacion_crit_tmp @idcrit, @esc, @idcet output
    
    if (@fecha = '')
    begin
        set @semi = (select getdate())
    end
    else
    begin
       set @semi = @fecha
    end

	if ( (select count(*) from cp_evaluacion_ins where idinstructor = @idins and idcet = @idcet and estado = '1' ) = 0 and (@semact != @sem) )
    begin
		set @idevt = (select idevento from cp_eventos_gral where estado = '2')
		insert into cp_evaluacion_ins(idinstructor,idcet,fecha,idcapacidad,aceptado,idinstituto,idevento) 
			values(@idins, @idcet, @semi, @capa, @ace, @ins, upper(@idevt))
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Evaluacion del Instructor dado de alta previamente'
		select @echo = cast(0 as char)
    end
;



create procedure i_eventos @fecha datetime = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10), @sem varchar(50), @dia integer, @mes varchar(15), @anio integer, @days varchar(12)
        exec cp_crear_clave_eventos_10 "EVT", @idp output
	
    if @fecha = NULL
    begin
        set @fecha = (select getdate())
    end
    
	set @anio = (select datepart(year, @fecha))
	set @mes = (select datename(mm, @fecha))
	set @dia = (select datepart(dd, @fecha))
	
	exec cp_calcular_semestre @fecha, @sem output
		
	if (select count(*) from cp_eventos_gral where semestre = upper(@sem) ) = 0
    begin
    	
    	update cp_eventos_gral set estado = '1' where estado = '2'
    	
		insert into cp_eventos_gral(idevento,semestre,anio) values(upper(@idp),upper(@sem),@anio)
		print 'Nuevo Evento creado'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Evento dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_eventos_rel @idev varchar(10), @clavec varchar(25),
	@echo char(1) output
	as
	if (select count(*) from cp_eventos_rel where idevento = upper(@idev) and clavecurso = upper(@clavec) ) = 0
    begin
    		
		insert into cp_eventos_rel(idevento,clavecurso,fecha) values(upper(@idev),upper(@clavec),getdate())
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Relacion de Evento con Clave de curso dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_encuesta_instructor @clavec varchar(25), @observaciones varchar(200) = NULL,
	@echo char(1) output
	as
	declare @idp varchar(10)
        exec cp_crear_clave_encuesta_10 "ENC", @idp output
	
		
	if (select count(*) from cp_encuesta_instructor where clavecurso = upper(@clavec) ) = 0
    begin
    	
    	update cp_encuesta_instructor set estado = '1' where estado = '2'
    	
		insert into cp_encuesta_instructor(idencuesta,clavecurso,observaciones,fecha) 
			values(upper(@idp),upper(@clavec),upper(@observaciones),getdate())
		print 'Encuesta a Instructor dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Encuesta hecha a Instructor anteriormente'
		select @echo = cast(0 as char)
    end
;


create procedure i_enc_cri_ins @idenc varchar(10), @idcrit integer, @valor integer,
	@echo char(1) output
	as
	if (select count(*) from cp_enc_cri_ins where idencuesta = upper(@idenc) and idcritenc = @idcrit ) = 0
    begin
        	
		insert into cp_enc_cri_ins(idencuesta,idcritenc,valor) values(upper(@idenc),@idcrit,@valor)
		select @echo = cast(1 as char)
    end
	else
    begin
		print 'Encuesta hecha a Instructor anteriormente'
		select @echo = cast(0 as char)
    end
;



create procedure cp_crear_clave_temario_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_temario_cap) <> 0
	    begin
			set @clavea = (select idtemario from cp_temario_cap where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_temas_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_temas_cap) <> 0
	    begin
			set @clavea = (select idtema from cp_temas_cap where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_costo_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_costo_total) <> 0
	    begin
			set @clavea = (select idcosto from cp_costo_total where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_cursodir_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_curso_dir) <> 0
	    begin
			set @clavea = (select iddirigido from cp_curso_dir where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_capacidadp_10 @suf varchar(3),
    @echo varchar(10) output
    as
    declare @clavea varchar(10), @v1 varchar(10), @len int
		
	    if (select count(*) from cp_cap_propuesta) <> 0
	    begin
			set @clavea = (select idcapacidad_p from cp_cap_propuesta where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @len = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@len), 6))
            select upper(@echo)
	    end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
            select upper(@echo)
		end
;

create procedure cp_crear_clave_pac_10 @suf varchar(3),				/** nueva **/
	@echo varchar(10) output
	as
	declare @clavea varchar(10), @v1 varchar(10), @num int
		
		if(select count(*) from cp_pac_capacidades) <> 0
		begin
			set @clavea = (select idpac from cp_pac_capacidades where estado = '2')
			
			set @v1 = (select right(@clavea, 6))
			
			set @num = hextoint(@v1) + 1
			
			set @echo = @suf + "-" + (select right(inttohex(@num), 6))
			select upper(@echo)
		end
		else
		begin
			set @echo = @suf + "-" + (select right(inttohex(1), 6))
			select upper(@echo)
		end	
;

create procedure i_temario @fecha datetime,									/** Actualizada **/
	@echo char(1) output
	as
	declare @idp varchar(10), @fh datetime, @idevt varchar(10)
		-- crear id
         exec cp_crear_clave_temario_10 "TMR", @idp output
	
	if @fecha = ""
	begin
		set @fh = (select getdate())
	end
	else
	begin
		set @fh = @fecha
	end

	if (select count(*) from cp_temario_cap where idtemario = @idp) = 0
    begin
		update cp_temario_cap set estado = '1' where estado = '2'
		
		set @idevt = (select idevento from cp_eventos_gral where estado = '2')
		insert into cp_temario_cap(idtemario,fecha,idevento) values(upper(@idp),@fh,@idevt)
        print 'Temario dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Temario dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_temas @des varchar(100), @hrs integer,
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_temas_10 "TMA", @idp output

	if (select count(*) from cp_temas_cap where idtema = @idp) = 0
    begin
		update cp_temas_cap set estado = '1' where estado = '2'
		
		insert into cp_temas_cap(idtema,descripcion,horas) values(upper(@idp),upper(@des),@hrs)
		
		set @idtmr = (select idtemario from cp_temario_cap where estado='2')
		insert into cp_temario_cap_tmp(idtemario,idtema) values(upper(@idtmr),upper(@idp))
		
        print 'Tema dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Tema dada de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_costo_total @hins smallmoney, @hcaf smallmoney, @htras smallmoney, @hhos smallmoney, @hsal smallmoney, @hmat smallmoney, 
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_costo_10 "CST", @idp output

	if (select count(*) from cp_costo_total where idcosto = @idp) = 0
    begin
		update cp_costo_total set estado = '1' where estado = '2'
		
		insert into cp_costo_total(idcosto,hinstructor,hcafeteria,htraslado,hhospedaje,hsalon,hmateriales)
			values(upper(@idp),@hins,@hcaf,@htras,@hhos,@hsal,@hmat)
		
        print 'Costo dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Costo dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

create procedure i_curso_dir @ndir integer, @nsub integer, @njdeptos integer, @njoficina integer, @npadmin integer, @npsec integer, @npserv integer,
	@echo char(1) output
	as
	declare @idp varchar(10), @idtmr varchar(10)
		-- crear id
         exec cp_crear_clave_cursodir_10 "CDG", @idp output

	if (select count(*) from cp_curso_dir where iddirigido = @idp) = 0
    begin
		update cp_curso_dir set estado = '1' where estado = '2'
		
		insert into cp_curso_dir(iddirigido,director,subdirector,jefedeptos,jefeoficina,personaladmin,personalsec,personalserv)
			values(upper(@idp),@ndir,@nsub,@njdeptos,@njoficina,@npadmin,@npsec,@npserv)
		
        print 'Datos de Curso dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Curso dado de alta previamente'
		select @echo = cast(0 as char)
    end
;

/** Actualizada **/
create procedure i_curso_propuesto @idcap varchar(10), @idinstr varchar(10), @idinsti varchar(20), @incp char(1), @cdur integer, @tipo char(1),
		@t_otro varchar(50), @mod char(1), @m_otro varchar(50), @jus varchar(250), @obj varchar(250), @eva varchar(70), @costo smallmoney,
		@idc char(13), @fecha datetime,
		
	@echo char(1) output
	as
	declare @idp varchar(10), @fh datetime, @v1 varchar(10), @v2 varchar(10), @v3 varchar(10), @v4 varchar(10), @idevt varchar(10)
		-- crear id
         exec cp_crear_clave_capacidadp_10 "CPP", @idp output
	
	if @fecha = ""
	begin
		set @fh = (select getdate())
	end
	else
	begin
		set @fh = @fecha
	end
	
	if (select count(*) from cp_cap_propuesta where idcapacidad_p = @idp) = 0
    begin
		update cp_cap_propuesta set estado = '1' where estado = '2'
		
		set @v1 = (select idtemario from cp_temario_cap where estado='2')
		set @v2 = (select iddirigido from cp_curso_dir where estado='2')
		set @v3 = (select idcosto from cp_costo_total where estado='2')
		set @v4 = (select idhorario from cp_horario_gral where estado='2')
		set @idevt = (select idevento from cp_eventos_gral where estado ='2')
		
		insert into cp_cap_propuesta(idcapacidad_p,idcapacidad,idinstructor,idinstituto,incprograma,duracion,tipo,tipo_otro,modalidad,modalidad_otro,
			justificacion,objetivo,evaluacion,idtemario,iddirigido,idcosto,costototal,idhorario,idcoordinador,fecha,idevento)
			
			values(upper(@idp),upper(@idcap),upper(@idinstr),upper(@idinsti),upper(@incp),@cdur,upper(@tipo),upper(@t_otro),upper(@mod),
				upper(@m_otro), upper(@jus), upper(@obj), upper(@eva), upper(@v1), upper(@v2), upper(@v3),@costo, upper(@v4), upper(@idc), @fh, upper(@idevt))
		
        print 'Datos de Curso dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
    else
    begin
		print 'Datos de Curso dado de alta previamente'
		select @echo = cast(0 as char)
    end
;


create procedure i_inscripcion_p @clave varchar(25), @rfc char(13), @cupo integer,
	@echo char(1) output
	as
	declare @faux datetime, @cont integer
		set @faux = (select getdate())
	
	set @cont = (select count(*) from cp_cap_autorizada_personal where clavecurso = upper(@clave) and estado = '1')
	
	if ((select count(*) from cp_cap_autorizada_personal where clavecurso = upper(@clave) and rfc = upper(@rfc) ) = 0 and (@cont <= @cupo) )
    begin
    		
		insert into cp_cap_autorizada_personal(clavecurso,rfc,fecha)
			values(upper(@clave),upper(@rfc),@faux)
		print 'Personal inscrito CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if ((select estado from cp_cap_autorizada_personal where clavecurso = upper(@clave) and rfc = upper(@rfc)) = '0' and (@cont <= @cupo) )
		begin
			update cp_cap_autorizada_personal set estado = '1' where clavecurso = upper(@clave) and rfc = upper(@rfc)
			print 'Personal inscrito CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Personal inscrito Previamente o se alacanzó el Cupo Límite'
			select @echo = cast(0 as char)
		end
    end
;

/** Actualizada **/
create procedure i_requerimientos_cap @rfc char(13), @idcap varchar(10) = NULL, @idotracap varchar(10) = NULL, @idtipo integer,
	@echo char(1) output
	as
	declare @fecha datetime, @semact varchar(10), @sem varchar(10), @idevt varchar(10)
		set @fecha = ( select getdate() )
		exec cp_calcular_semestre @fecha, @semact output
		set @idevt = (select idevento from cp_eventos_gral where estado ='2')
	
	if (@idotracap = NULL and @idcap <> NULL)
	begin
		if ((select count(*) from cp_requerimientos_cap where rfc = upper(@rfc) and idcapacidad = @idcap and idtipo = @idtipo and estado='2') = 0 )
		begin
			insert into cp_requerimientos_cap(rfc,idcapacidad,idotracap,idtipo,fecha,idevento)
			values(upper(@rfc),upper(@idcap),upper(@idotracap),@idtipo,getdate(),upper(@idevt))
		
			print 'Requerimiento dado de Alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			set @fecha = (select fecha from cp_requerimientos_cap where rfc = upper(@rfc) and idcapacidad = @idcap and idtipo = @idtipo and estado='2' )
			exec cp_calcular_semestre @fecha, @sem output
			
			if upper(@semact) = upper(@sem)
			begin
				print 'Requerimiento dado de Alta previamente'
				select @echo = cast(0 as char)
			end
			else 
			begin
				update cp_requerimientos_cap set estado = '1' where rfc = upper(@rfc) and idcapacidad = @idcap and idtipo = @idtipo and estado='2'
				
				insert into cp_requerimientos_cap(rfc,idcapacidad,idotracap,idtipo,fecha,idevento)
				values(upper(@rfc),upper(@idcap),upper(@idotracap),@idtipo,getdate(),upper(@idevt))
			
				print 'Requerimiento dado de Alta CORRECTAMENTE'
				select @echo = cast(1 as char)
			end
		end
	end
	
	if (@idotracap <> NULL and @idcap = NULL)
	begin
		if ((select count(*) from cp_requerimientos_cap where rfc = upper(@rfc) and idotracap = @idotracap and idtipo = @idtipo and estado='2') = 0 )
		begin
			insert into cp_requerimientos_cap(rfc,idcapacidad,idotracap,idtipo,fecha,idevento)
			values(upper(@rfc),upper(@idcap),upper(@idotracap),@idtipo,getdate(),upper(@idevt))
		
			print 'Requerimiento dado de Alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			set @fecha = (select fecha from cp_requerimientos_cap where rfc = upper(@rfc) and idotracap = @idotracap and idtipo = @idtipo and estado='2' )
			exec cp_calcular_semestre @fecha, @sem output
			
			if @semact = @sem
			begin
				print 'Requerimiento dado de Alta previamente'
				select @echo = cast(0 as char)
			end
			else 
			begin
				update cp_requerimientos_cap set estado = '1' where rfc = upper(@rfc) and idotracap = @idotracap and idtipo = @idtipo and estado='2'
				
				insert into cp_requerimientos_cap(rfc,idcapacidad,idotracap,idtipo,fecha,idevento)
				values(upper(@rfc),upper(@idcap),upper(@idotracap),@idtipo,getdate(),upper(@idevt))
			
				print 'Requerimiento dado de Alta CORRECTAMENTE'
				select @echo = cast(1 as char)
			end
		end
	end
;


create procedure i_otras_cap_cedula @rfc char(13), @des varchar(150), @idtipo integer,
	@echo char(1) output
	as
    declare @idp varchar(10), @other varchar(8), @ids integer
        exec cp_crear_clave_8 @des, 'C', @idp output

	if(select count(idsubgrupo) from cp_subgrupo_cap where descripcion = upper('otros')) = 0
	begin
		if(select count(idgrupo) from cp_grupos_cap where descripcion = upper('otros')) = 0
		begin
			insert into cp_grupos_cap(descripcion) values(upper('otros'))
			set @ids = (select idgrupo from cp_grupos_cap where descripcion = upper('otros'))
		end else begin
			set @ids = (select idgrupo from cp_grupos_cap where descripcion = upper('otros'))
		end
		
		insert into cp_subgrupo_cap(descripcion,idgrupo) values(upper('otros'),@ids)
			set @ids = (select idsubgrupo from cp_subgrupo_cap where descripcion = upper('otros'))
	end
	else
	begin
		set @ids = (select idsubgrupo from cp_subgrupo_cap where descripcion = upper('otros'))
	end

	if (select count(*) from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = 0
    begin
		insert into cp_capacidades(idcapacidad,descripcion,fecha,idsubgrupo) values(upper(@idp),upper(@des),getdate(),@ids)
		exec i_requerimientos_cap @rfc, NULL, @idp, @idtipo, '1'
		
		print 'Capacidad dada de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		exec i_requerimientos_cap @rfc, NULL, @idp, @idtipo, '1'
		
		if (select estado from cp_capacidades where idcapacidad = upper(@idp) and descripcion = upper(@des)) = '0'
		begin
			update cp_capacidades set estado = '1' where idcapacidad = upper(@idp) and descripcion = upper(@des)
			exec i_requerimientos_cap @rfc, NULL, @idp, @idtipo, '1'
			
			print 'Capacidad dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Capacidad dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
;

/** nueva **/
create procedure i_pac_gral
	@echo char(1) output
	as
	declare @idp varchar(10), @tmp varchar(10), @evento varchar(10), @anio integer, @fecha datetime
		exec cp_crear_clave_pac_10 "PAC", @idp output
	
	set @fecha = (select getdate())
	set @evento = (select idevento from cp_eventos_gral where estado = '2')
	set @anio = (select datepart(yy,@fecha))
		
	if((select count(*) from cp_pac_capacidades where anio = @anio) = 0)
	begin
		update cp_pac_capacidades set estado = '1' where estado = '2'
		
		insert into cp_pac_capacidades(idpac, idevento, fecha, anio) values (upper(@idp), upper(@evento), getdate(), @anio)
		print 'PAC dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
	else
	begin
		print 'PAC dado de alta PREVIAMENTE'
		select @echo = cast(0 as char)
	end
;
/** nueva **/
create procedure i_pac_lista @idcapa varchar(10), @iddir varchar(10), @fechai datetime, @fechaf datetime,
	@echo char(1) output
	as
	declare @idpac varchar(10)
		set @idpac = (select idpac from cp_pac_capacidades where estado = '2')
		
	if((select count(*) from cp_pac_lista where idpac = @idpac and idcapacidad = @idcapa) = 0)
	begin
		update cp_pac_lista set estado = '1' where estado = '2'
		
		insert into cp_pac_lista(idpac, idcapacidad, iddirigido, fechainicio, fechafin)
			values(upper(@idpac), upper(@idcapa), upper(@iddir), @fechai, @fechaf)
		
		print 'Capacidad enlistada CORRECTAMENTE'
		select @echo = cast(1 as char)
	end
	else
	begin
		print 'Capacidad enlistada PREVIAMENTE'
		select @echo = cast(0 as char)
	end
;


-- Procedimientos de consulta

-- Consulta Institucion
/* 
create procedure s_institucion
    as
    select idinstituto, descripcion from institucion
;

create procedure s_personal
    as   
    select rfc, apellidos_empleado + "" + nombre_empleado as personal, estudios, correo_electronico from personal
;
*/
