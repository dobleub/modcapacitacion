<?php 
	/********************************************************
		Puesto

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 				   |
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(10,10);	$this->Cell(196, 30,' ', 1, 2, 'C', 0);
			$this->SetXY(163,10);	$this->Cell(43, 10,'ITTXO-CA-PO-001-01', 1, 2, 'C', 0); 
			$this->Cell(43, 10,'Revisi�n: 0', 1, 2, 'C', 0); 
			$this->SetXY(53,10);	$this->Cell(110, 20,'Relaci�n: Puestos del Instituto', 1, 2, 'C', 0); 
			$this->Cell(110, 10,'Referencia a la Norma ISO 9001:2008   5.2, 7.2.3', 1, 2, 'C', 0);
			$this->SetXY(170,32);	$this->Cell(25,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,62);	$this->Cell(198,168,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(155,230);	$this->MultiCell(50,5,'V� B�',0,'J');
			$this->SetXY(122,238);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(147,245);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');
			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(10,265);	$this->Cell(196,4,"SNEST-AC-PO-004-01",0,1,'L', 0);
			$this->SetXY(193,265);	$this->Cell(10,4,"Rev. 4",0,1,'L', 0);
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 250, 20, 0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales del Grupo impartido por Docente ************************/	
	$pdf->SetXY(10,45);	$pdf->Cell(100,10,"Fecha:  ________________________",0,0,'L');
	$pdf->SetXY(24,44);	$pdf->Cell(100,10,formato_de_fecha(date('d/m/Y')),0,0,'L');

	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 25
	$altura_celda = 6;
	$cont=1;
	$j=62;
	$titulos=1;
	$registro_max=26;
	$qry_select = "select descripcion_puesto, nivel_puesto from puestos";
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
		if($titulos==1){
			$pdf->SetFont('Arial','','7');
			$pdf->SetXY(  8,$j);	$pdf->Cell(8,$altura_celda,'No.',0,0,'C');
			$pdf->SetXY( 49,$j);	$pdf->Cell(59,$altura_celda,'PUESTO',0,0,'C');
			$pdf->SetXY(120,$j);	$pdf->Cell(120,$altura_celda,'NIVEL PUESTO',0,0,'C');
			$titulos=0;
			}
	if($cont < $registro_max){ 
		$d_puesto = $registro->fields('descripcion_puesto');
		$se = $registro->fields('nivel_puesto');

		$pdf->SetXY(10,($j=$j+$altura_celda));	$pdf->Cell(140,$altura_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(15,$j);			$pdf->Cell(191,$altura_celda,' ',1, 2, 'C', 0);

		$pdf->SetXY(8,$j);	$pdf->Cell(8,$altura_celda,$cont,0,0,'C');
		$pdf->SetXY(15,$j);	$pdf->Cell(32,$altura_celda,$d_puesto,0,0,'J');
		$pdf->SetXY(150,$j);	$pdf->Cell(32,$altura_celda,(($se=="1")?"DIRECTOR":(($se=="2")?"SUBDIRECTOR":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel"))))))),0,0,'J');

		if($cont==($registro_max - 1))
			{$registro_max=$registro_max+26;
			$titulos=1;
			$j=62; // 54
			$pdf->SetFont('Arial','','10');
			$pdf->SetXY(10,225);	$pdf->Cell(196,6,"Continua...",0,0,'C');
			$pdf->AddPage();
			}
		}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
