<?php 
	/********************************************************
		Puesto de  acuedo a departamento

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * *|
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/ 
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(53,10);	$this->Cell(110, 30,'Relaci�n de personal del Instituto: Edad-G�nero', 0, 2, 'C', 0);  
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 11, 25, 0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,62);	$this->Cell(198,168,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(155,230);	$this->MultiCell(50,5,'V� B�',0,'J');
			$this->SetXY(122,238);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(147,245);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');
			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(10,265);	$this->Cell(196,4,"Departamento de Recursos Humanos",0,1,'L', 0);
			$this->SetXY(150,265);	$this->Cell(10,4,'Hoy es '.formato_de_fecha(date('d/m/Y')),0,1,'L', 0);
			$this->SetXY(160,270);	$this->Cell(10,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF

	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales del Grupo impartido por Docente ************************/	
	
	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 26 
	$cont=1;
	$contador=1;
	$j=62; 
	$titulos=1;
	$registro_max=26;
	$altura_celda = 6;
	$qry_select = "select PE.rfc, apellidos_empleado, nombre_empleado, O.descripcion_area, descripcion_puesto from personal PE, puestos_personal PP, puestos P, organigrama O where (status_empleado = '01' or status_empleado = '02') and PP.rfc = PE.rfc and PP.clave_puesto = P.clave_puesto and PE.clave_area = O.clave_area order by descripcion_puesto, PE.clave_area, apellidos_empleado, nombre_empleado";

	$registro = ejecutar_sql($qry_select);
	$puesto_inicial = $registro->fields('descripcion_puesto');

	//$pdf->SetXY(94,$j);	$pdf->Cell(0,$altura_celda,$puesto_inicial,0,0,'J');

			$pdf->SetXY(94,($j=$j+$altura_celda));	$pdf->Cell(0,$altura_celda,$puesto_inicial,0,0,'J');
			$titulos=1;
			if($titulos==1){ 
				$j = $j+$altura_celda;
				$pdf->SetFont('Arial','','7');
				$pdf->SetXY(8,$j);	$pdf->Cell(8,$altura_celda,'No.',0,0,'C');
				$pdf->SetXY(35,$j);	$pdf->Cell(35,$altura_celda,'NOMBRE COMPLETO',0,0,'C');
				$pdf->SetXY(91,$j);	$pdf->Cell(91,$altura_celda,'DEPARTAMENTO',0,0,'C');
				$titulos=0;}
			$cont = $cont + 2;



	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA



	if($cont < $registro_max){ 

		$rfc = $registro->fields('rfc');
		$area = $registro->fields('descripcion_area');
		$puesto = $registro->fields('descripcion_puesto');

		$j=$j+$altura_celda;
		if ($puesto_inicial != $puesto){
			$puesto_inicial = $puesto;
			$pdf->SetXY(94,($j=$j+$altura_celda));	$pdf->Cell(0,$altura_celda,$puesto,0,0,'J');
	/**/		$titulos=1;
			if($titulos==1){ 
				$j = $j+$altura_celda;
				$pdf->SetFont('Arial','','7');
				$pdf->SetXY(8,$j);	$pdf->Cell(8,$altura_celda,'No.',0,0,'C');
				$pdf->SetXY(35,$j);	$pdf->Cell(35,$altura_celda,'NOMBRE COMPLETO',0,0,'C');
				$pdf->SetXY(91,$j);	$pdf->Cell(91,$altura_celda,'DEPARTAMENTO',0,0,'C');
				$titulos=0;}
			$cont = $cont + 2;
		}
 

		$pdf->SetXY(10,($j=$j+$altura_celda));	$pdf->Cell(82,$altura_celda,' ',1, 2, 'C', 0);
	 	$pdf->SetXY(15,$j);			$pdf->Cell(180,$altura_celda,' ',1, 2, 'C', 0);
		$pdf->SetFont('Arial','','7');

		$pdf->SetXY(8,$j);	$pdf->Cell(8,$altura_celda,$contador,0,0,'C');
		$pdf->SetXY(15,$j);	$pdf->Cell(0,$altura_celda,$registro->fields('apellidos_empleado').' '.$registro->fields('nombre_empleado'),0,0,'J');
		//$pdf->SetXY(94,$j);	$pdf->Cell(0,$altura_celda,$puesto,0,0,'J');
		$pdf->SetXY(92,$j);	$pdf->Cell(0,$altura_celda,$area,0,0,'J');
		
		if($cont==($registro_max - 1))
			{$registro_max=$registro_max+26;
			$titulos=1;
			$j=62; 
			$pdf->SetFont('Arial','','10');
			$pdf->SetXY(10,225);	$pdf->Cell(196,6,"Continua...",0,0,'C');
			$pdf->AddPage();
			}
	}
	$cont++;
	$contador++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
