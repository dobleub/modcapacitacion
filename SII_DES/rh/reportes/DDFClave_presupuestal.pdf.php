<?php 
	/********************************************************
		Clave Presupuestal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 				   |
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(53,10);	$this->Cell(110, 30,'Relaci�n: Movimiento-Personal', 0, 2, 'C', 0);  
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 11, 25, 0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,62);	$this->Cell(198,168,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(155,230);	$this->MultiCell(50,5,'V� B�',0,'J');
			$this->SetXY(122,238);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(147,245);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');
			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(10,265);	$this->Cell(196,4,"Departamento de Recursos Humanos",0,1,'L', 0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/

	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales del Grupo impartido por Docente ************************/	
	$pdf->SetXY(10,45);	$pdf->Cell(100,10,"Fecha:  ________________________",0,0,'L');
	$pdf->SetXY(24,44);	$pdf->Cell(100,10,formato_de_fecha(date('d/m/Y')),0,0,'L');

	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 26
	$altura_celda = 6;
	$cont=1;
	$j=62; 
	$titulos=1;
	$registro_max=26;
	$qry_select = "select * from movimiento_personal group by rfc order by rfc,cant_mov";
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
		if($titulos==1){ 
			$pdf->SetFont('Arial','','7');
			$pdf->SetXY(8,$j);	$pdf->Cell(8,$altura_celda,'No.',0,0,'C');
			$pdf->SetXY(18,$j);	$pdf->Cell(20,$altura_celda,'FILIACI�N',0,0,'C');
			$pdf->SetXY(52,$j);	$pdf->Cell(70,$altura_celda,'NOMBRE COMPLETO',0,0,'C');
			$pdf->SetXY(96,$j);	$pdf->Cell(96,$altura_celda,'MOVIMIENTO',0,0,'C');
			$pdf->SetXY(112,$j);	$pdf->Cell(112,$altura_celda,'EFECTOS INICIALES',0,0,'C');
			$pdf->SetXY(129,$j);	$pdf->Cell(129,$altura_celda,'EFECTOS FINALES',0,0,'C');
			$pdf->SetFont('Arial','','7');
			$titulos=0;
			}
		if($cont < $registro_max){ 
			$rfc=$registro->fields('rfc'); 
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('nombre_empleado').' '.$reg->fields('apellidos_empleado');
			$movimiento = $registro->fields('movimiento');
			$efecto_iniciales = $registro->fields('e_iniciales');
			$efecto_finales = $registro->fields('e_finales');

			$pdf->SetXY( 10,($j=$j+$altura_celda));	$pdf->Cell(123,$altura_celda,' ',1, 2, 'C', 0);
			$pdf->SetXY( 17,$j);			$pdf->Cell(189,$altura_celda,' ',1, 2, 'C', 0);
			$pdf->SetXY( 40,$j);			$pdf->Cell(115,$altura_celda,' ',1, 2, 'C', 0);
			$pdf->SetXY(155,$j);			$pdf->Cell( 26,$altura_celda,' ',1, 2, 'C', 0);
			
			if ($registro->fields('status')!=1){
				$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(155,155,155);
			}else
			{	$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(0,0,0);
			}
			$pdf->SetXY(  9,$j);	$pdf->Cell(9,$altura_celda,$cont,0,0,'C');
			$pdf->SetXY( 17,$j);	$pdf->Cell(0,$altura_celda,$rfc,0,0,'J');
			$pdf->SetXY( 40,$j);	$pdf->Cell(0,$altura_celda,$nombre,0,0,'J');
			$pdf->SetXY(141,$j);	$pdf->Cell(0,$altura_celda,$movimiento,0,0,'J');
			$pdf->SetXY(163,$j);	$pdf->Cell(0,$altura_celda,$efecto_iniciales,0,0,'J');
			$pdf->SetXY(188,$j);	$pdf->Cell(0,$altura_celda,$efecto_finales,0,0,'J');

			

			if($cont==($registro_max - 1))
				{
				$registro_max=$registro_max+26;
				$titulos=1;
				$j=62; // 54
				$pdf->SetFont('Arial','','10');
				$pdf->SetXY(10,225);	$pdf->Cell(196,6,"Continua...",0,0,'C');
				$pdf->AddPage();
				}
			}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
