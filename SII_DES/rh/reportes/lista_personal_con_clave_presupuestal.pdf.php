<?php 
	/********************************************************
		Faltaaaaaaaaaaaaa

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('JDP');
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	$id_soli= $_GET['id_sol'];

	class PDF extends PDF_MC_Table
	  {	
/*____________________________________________________________________________________________________
		* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 			
_____________________________________________________________________________________________________*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			
			//Recuadro Principal Encabezado.
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(10, 10);
			$this->Cell(196, 30,' ', 1, 2, 'C', 0);
			$this->SetXY(163, 10);
			$this->Cell(43, 10,'ITTXO-AD-PO-001-04', 0, 2, 'C', 0);
			$this->SetXY(163, 20);
			$this->Cell(43, 10,' ', 1, 2, 'C', 0);
			$this->SetXY(163, 20);
			$this->Cell(43, 10,'Revisi�n: 0', 0, 2, 'C', 0);
			$this->SetXY(10, 10);
			$this->Cell(43, 30,' ', 1, 2, 'C', 0);
			$this->SetXY(163, 10);
			$this->Cell(43, 30,' ', 1, 2, 'C', 0);
			$this->SetXY(53, 10);
			$this->Cell(110, 20,'Registro para Orden de Trabajo de Mantenimiento', 0, 2, 'C', 0);
			$this->SetFont('Arial','b','12');
			$this->Cell(110, 10,'Referencia al punto de la norma ISO 9001:2008 6.3, 6.4', 1, 2, 'C', 0);
//			$this->Ln(10);
			$this->SetXY(170, 32);
			$this->Cell(25,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			$this->SetLineWidth(0);
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];
			$this->SetLineWidth(0);			
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Helvetica','','7');
			$this->SetFillColor(255,255,255);			
			$this->SetXY(9,240);
			$this->Cell(80,4,"c.c.p. Departamento de Planeaci�n Programaci�n y Presupuestaci�n",0,0, 'L');
			$this->SetXY(9,244);
			$this->Cell(30,4,"c.c.p. �rea Solicitante",0,0,'L');
			$this->SetXY(7,247); 
			$this->Cell(196,4,"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",0,0,'L');
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 250, 20, 0);
		  }
	  }//Termina las funciones para CLASS PDF
	/* ------------------------------------------------------------------------------------------------------------------ 
______________________________________________________________________________________________________
			* * * * * INICIO DEL DOCUMENTO  * * * * *   _____________________________________________________________________________________________________*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);

	//Llamada de procedimiento para obtener los datos de la solicitud de mantenimiento.
	$qry_sol = "EXEC proc_jdp_datos_sol $id_soli,0,'',1";
	$res_sol = ejecutar_sql($qry_sol);
	$Folio=$res_sol->Fields('folio'); // Folio de la solicitud conforme a un id_solicitud	
	$qry_ord ="select t_mantenimiento,t_servicio,solicitante,responsable,fecha_man,trabajo_realizado from ord_mantenimiento where id_solicitud=$id_soli";
	$res_ord = ejecutar_sql($qry_ord);
	$c1=$res_ord->Fields('t_mantenimiento');
	$c2=$res_ord->Fields('t_servicio');
	$c3=$res_ord->Fields('solicitante');
	$c4=$res_ord->Fields('responsable');
	$c5=$res_ord->Fields('fecha_man');
	$c6=$res_ord->Fields('trabajo_realizado');
	
	if (!$Folio){$Folio="S/N";}
	else if($Folio<10){$Folio="000".$Folio;}
	else if ($Folio<100){$Folio="00".$Folio;}
	else if ($Folio<1000){$Folio="0".$Folio;}
	
	/************************ Datos generales del Grupo impartido por Docente ************************/	
	
        $pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','','13');
        // FECHA
	$pdf->SetXY(57,45);
	$pdf->Cell(100,10,"ORDEN DE TRABAJO DE MANTENIMIENTO",0,0,'C');


$pdf->SetFont('Arial','','12');

        // NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO
	$pdf->SetXY(150,55); $pdf->Cell(25,10,"N�mero de control:________",0,0,'L');
	$pdf->SetXY(190,55); $pdf->Cell(10,10,$Folio,0,0,'L');
        // NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO

        // TIPO DE MANTENIMIENTO APLICADO
	$pdf->SetXY(9,66); $pdf->Cell(198,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(10,67); $pdf->Cell(100,10,"Mantenimiento                                          Interno                                                  Externo",0,0,'L');
	$pdf->SetXY(103,70); $pdf->Cell(3,4,' ', 1, 2, 'C', 0);
	$pdf->SetXY(176,70); $pdf->Cell(3,4,' ', 1, 2, 'C', 0);
	if($c1=="Interno"){$pdf->SetXY(103,70); $pdf->Cell(3,4,'x', 1, 2, 'C', 0);}
	else{$pdf->SetXY(176,70); $pdf->Cell(3,4,'x', 1, 2, 'C', 0);}
        // TIPO DE MANTENIMIENTO APLICADO

        // TIPO DE SERVICIO APLICADO
	$pdf->SetXY(9,77); $pdf->Cell(198,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(10,78); $pdf->Cell(100,10,'Tipo de servicio: '.$c2.'',0,0,'L');
        // TIPO DE SERVICIO APLICADO

        // ASIGNADO A:
	$pdf->SetXY(9,88); $pdf->Cell(198,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(10,89); $pdf->Cell(100,10,'Asignado a: C. '.$c4.'',0,0,'L');
	// ASIGNADO A:
	
	// FECHA DE REALIZACION DE MANTENIMIENTO
	$pdf->SetXY(9,107); $pdf->Cell(198,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(10,108); $pdf->MultiCell(197,10,'Fecha de realizaci�n: '.$c5.'',0,'J');
	// FECHA DE REALIZACION DE MANTENIMIENTO

	//DESCRIPCION DE TRABAJO REALIZADO
	$pdf->SetXY(9,118); $pdf->Cell(198,95,' ',1, 2, 'C', 0);
	$pdf->SetXY(10,119); $pdf->MultiCell(196,5,'Trabajo Realizado: '.$c6.'',0,'J');
	//DESCRIPCION DE TRABAJO REALIZADO
	
	//SOLICITANTE
	$pdf->SetXY(9,213); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
	$pdf->SetXY(10,214); $pdf->MultiCell(124,5,'Verificado y Liberado por: C. '.$c3.'',0,'J');
	//SOLICITANTE
	
	//RESPONSABLE
	$pdf->SetXY(9,224); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
	$pdf->SetXY(10,225); $pdf->MultiCell(124,5,'Aprobado por: C. '.$c4.'',0,'J');
	//RESPONSABLE
	$pdf->SetXY(134,213); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(134,214); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
	$pdf->SetXY(134,224); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
	$pdf->SetXY(134,225); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
	
	//MATERIALES OCUPADOS EN EL MANTENIMIENTO ---------------------------------------------------------
	
	// NUMERO MAXIMO DE MATERIALES EN LA ORDEN DE MANTENIMIENTO ES DE "7"
		$qry_nomat = "select count(id_sol) as no_mat from mat_mantenimiento where id_sol=$id_soli";
		$res_nomat = ejecutar_sql($qry_nomat);
		$no_mat=$res_nomat->fields('no_mat');
	if($no_mat > 6){
	$pdf->SetXY(80,203); $pdf->Cell(25,10,"Continua...",0,0,'L');
	}
	$cont=1;
	$ban1=0;
	$j=148;
	$tituls=1;
	$n_reg=1;
	$max_reg=7;
	
		$qry_mat = "select id_sol,cantidad,producto from mat_mantenimiento where id_sol=$id_soli";
		$res_mat = ejecutar_sql($qry_mat);
	while (!$res_mat->EOF)
	{
	
	//TITULOS DE LISTA
	if($tituls==1){
	$pdf->SetFont('Arial','','12');
	$pdf->SetXY(13,$j); $pdf->Cell(40,10,'Cantidad',0,0,'C');
	$pdf->SetXY(53,$j); $pdf->Cell(120,10,'Producto',0,0,'C');
	$pdf->SetXY(12,$j); $pdf->Cell(40,8,' ',1, 2, 'C', 0);
	$pdf->SetXY(52,$j); $pdf->Cell(120,8,' ',1, 2, 'C', 0);
	$pdf->SetFont('Arial','','10');
	$tituls=0;
	}
	
	
	if(($cont >= $n_reg) and ($cont < $max_reg)){ 
	
	$can=($res_mat->fields('cantidad'));
	$mat=($res_mat->fields('producto'));
	$pdf->SetXY(12,($j=$j+8)); $pdf->Cell(40,8,' ',1, 2, 'C', 0);
	$pdf->SetXY(52,($j)); $pdf->Cell(120,8,' ',1, 2, 'C', 0);
	//---- CANTIDAD Y MATERIAL OCUPADO EN EL MANTENIMIENTO 
	$pdf->SetXY(13,$j); $pdf->Cell(39,8,$can,0,0,'J');
	$pdf->SetXY(53,($j+1)); $pdf->MultiCell(119,3,$mat,0,'J');

		if($cont==($max_reg - 1)){
			if($cont > 6){
			$pdf->SetFont('Arial','','12');
		        // NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO
			$pdf->SetXY(150,42); $pdf->Cell(25,10,"N�mero de control:________",0,0,'L');
			$pdf->SetXY(190,42); $pdf->Cell(10,10,$Folio,0,0,'L');
		        // NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO

			$pdf->SetXY(80,204); $pdf->Cell(25,10,"Continua...",0,0,'L');
			$pdf->SetXY(9,52); $pdf->Cell(198,($j-37),' ',1, 2, 'C', 0);
			//SOLICITANTE
			$pdf->SetXY(9,213); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
			$pdf->SetXY(10,214); $pdf->MultiCell(124,5,'Verificado y Liberado por: C. '.$c3.'',0,'J');
			//SOLICITANTE
	
			//RESPONSABLE
			$pdf->SetXY(9,224); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
			$pdf->SetXY(10,225); $pdf->MultiCell(124,5,'Aprobado por: C. '.$c4.'',0,'J');
			//RESPONSABLE
			$pdf->SetXY(134,213); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
			$pdf->SetXY(134,214); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
			$pdf->SetXY(134,224); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
			$pdf->SetXY(134,225); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
			$pdf->SetFont('Arial','','10');
			}
		$n_reg=$cont;
		$max_reg=$max_reg+18;
		$tituls=1;
		$j=54;
		if($no_mat != 6){$pdf->AddPage();}
		}
		
		if($cont==$no_mat and $cont > 6){
		$pdf->SetXY(9,52); $pdf->Cell(198,($j-37),' ',1, 2, 'C', 0);
			$pdf->SetFont('Arial','','12');
			// NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO
			$pdf->SetXY(150,42); $pdf->Cell(25,10,"N�mero de control:________",0,0,'L');
			$pdf->SetXY(190,42); $pdf->Cell(10,10,$Folio,0,0,'L');
		        // NUMERO DE CONTROL DE ORDEN DE MANTENIMIENTO CONFORME A CADA DEPARTAMENTO
			//SOLICITANTE
			$pdf->SetXY(9,($j=$j+15)); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
			$pdf->SetXY(10,($j+1)); $pdf->MultiCell(124,5,'Verificado y Liberado por: C. '.$c3.'',0,'J');
			$pdf->SetXY(134,$j); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
			$pdf->SetXY(134,($j+1)); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
			//SOLICITANTE
	
			//RESPONSABLE
			$pdf->SetXY(9,($j=$j+11)); $pdf->Cell(125,11,' ', 1, 2, 'J', 0);
			$pdf->SetXY(134,$j); $pdf->Cell(73,11,' ', 1, 2, 'C', 0);
			$pdf->SetXY(10,($j+1)); $pdf->MultiCell(124,5,'Aprobado por: C. '.$c4.'',0,'J');
			$pdf->SetXY(134,($j+1)); $pdf->MultiCell(50,5,'Fecha y Firma',0,'J');
			//RESPONSABLE
			$pdf->SetFont('Arial','','10');
		}
	}

	$cont++;
	$res_mat ->movenext();
	}
	$pdf->Output();
?>	
