<?php 
	/********************************************************
		Personal General

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxaico
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');
	$_SESSION['t']= $_GET['t'];

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * *|
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			//$this->SetXY(10,10);	$this->Cell(259, 30,' ', 1, 2, 'C', 0);
			//$this->SetXY(226,10);	$this->Cell(43, 10,'ITTXO-CA-PO-001-01', 1, 2, 'C', 0);
			//$this->Cell(43, 10,'Revisi�n: 0', 1, 2, 'C', 0);

			// Colores de los bordes, fondo y texto
			$this->SetDrawColor(0,80,180); //Bordes azul
			//$this->SetFillColor(230,230,0); //Color de relleno
			$this->SetTextColor(220,50,50);   // Rojo paran titulos

			$this->SetXY(53,10);	$this->Cell(185, 30,'Listado General del Personal Mov 95, 10: '.(($_SESSION['t']=='A')?' Administrativos':' Docentes'), 0, 2, 'C', 0);
			//$this->Cell(173, 10,'Referencia a la Norma ISO 9001:2008   5.2, 7.2.3', 1, 2, 'C', 0);
			//$this->SetXY(233,30);	$this->Cell(25,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			$this->Image($CFG->imgDir."/escudo.jpg", 240, 11, 25, 0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,43);	$this->Cell(261,132,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(215,175);	$this->MultiCell(50,5,'V� B�',0,'J'); 
			$this->SetXY(182,183);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(207,190);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');

			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(120,200);	$this->Cell(1,1,"Departamento de Recursos Humanos",0,1,'L', 0);
			$this->SetXY(220,200);	$this->Cell(10,4,'Hoy es '.formato_de_fecha(date('d/m/Y')),0,1,'L', 0);
			$this->SetXY(225,205);	$this->Cell(10,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('L','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	/*doc controlado*/
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales ************************/	
  
	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 26
	$alto_celda = 4;
	$cont = 1;
	$j = 46; 
	$titulos = 1;
	$registro_max = 30;
	$id = false;

	$qry_select = "select distinct (P.rfc), apellidos_empleado, nombre_empleado, sexo_empleado, nombramiento, O.descripcion_area, PU.descripcion_puesto, status_empleado from personal P, movimiento__personal MP, organigrama O, puestos_personal PP, puestos PU where (status_empleado = '01' or status_empleado = '02') and (nombramiento='".$_SESSION['t']."') 
and (MP.movimiento != 20) and P.rfc = MP.rfc and O.clave_area = P.clave_area and PP.rfc = P.rfc and PP.clave_puesto = PU.clave_puesto and MP.estatus = 1 order by apellidos_empleado, nombre_empleado, P.rfc";
// select P.rfc, apellidos_empleado, nombre_empleado, sexo_empleado, nombramiento, O.descripcion_area, PU.descripcion_puesto from personal P, movimiento_personal MP, organigrama O, puestos_personal PP, puestos PU where (status_empleado = '01' or status_empleado = '02') and (nombramiento='".$_SESSION['t']."') and (MP.movimiento = 10 or MP.movimiento = 95) and P.rfc = MP.rfc and O.clave_area = P.clave_area and PP.rfc = P.rfc and PP.clave_puesto = PU.clave_puesto order by apellidos_empleado, nombre_empleado
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
	if($titulos==1){ 
		$pdf->SetFont('Arial','','7');
		$pdf->SetFillColor(200,200,200); //Color de relleno
		$pdf->SetXY( 10,$j);	$pdf->Cell( 5,$alto_celda,'No.',0,0,'C');
		$pdf->SetXY( 15,$j);	$pdf->Cell(16,$alto_celda,'RFC',0,0,'C'); //
		$pdf->SetXY( 31,$j);	$pdf->Cell(60,$alto_celda,'NOMBRE COMPLETO',0,0,'C');
		$pdf->SetXY( 91,$j);	$pdf->Cell(89,$alto_celda,'PUESTO',0,0,'C');
		$pdf->SetXY(180,$j);	$pdf->Cell(89,$alto_celda,'AREA',0,0,'C'); // 269
		$titulos=0;}

	if($cont < $registro_max){ 
		$rfc = $registro->fields('rfc');
		$sexo = $registro->fields('sexo_empleado');
		$descripcion_area=$registro->fields('descripcion_area');
		$descripcion_puesto=$registro->fields('descripcion_puesto');

		$pdf->SetDrawColor(0,80,180); //Bordes azul
		$pdf->SetXY(10,($j=$j+$alto_celda));	$pdf->Cell( 87,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(15,$j);			$pdf->Cell(254,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(37,$j);			$pdf->Cell(147,$alto_celda,' ',1, 2, 'C', 0);


		$pdf->SetFillColor(245,245,245); //Color de relleno
		$pdf->SetXY( 10,$j);	$pdf->Cell( 5,$alto_celda,$cont,0,0,'C');
		$pdf->SetXY( 15,$j);	$pdf->Cell(22,$alto_celda,$rfc,0,0,'J'); // 
		$pdf->SetXY( 37,$j);	$pdf->Cell(60,$alto_celda,$registro->fields('apellidos_empleado').' '.$registro->fields('nombre_empleado'),0,0,'J');
		$pdf->SetXY( 97,$j);	$pdf->Cell(87,$alto_celda,$descripcion_puesto,0,0,'J');
		$pdf->SetXY(184,$j);	$pdf->Cell(85,$alto_celda,$descripcion_area,0,0,'J');


		$id = ($id==true)?false:true;

		if($cont==($registro_max - 1))
			{
			$registro_max=$registro_max+30;
			$titulos=1;
			$j=46;
			$pdf->SetFont('Arial','','10');
			$pdf->SetXY(10,170); $pdf->Cell(259,6,"Continua...",0,0,'C');
			$pdf->AddPage(); 	
			}
		}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>
