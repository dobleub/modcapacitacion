<?php 
	/********************************************************
		Faltaaaaaaaaaaaaaaaaaaa

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 				   |
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(10,10);	$this->Cell(259, 30,' ', 1, 2, 'C', 0);
			$this->SetXY(226,10);	$this->Cell(43, 10,'ITTXO-CA-PO-001-01', 1, 2, 'C', 0);
			$this->Cell(43, 10,'Revisi�n: 0', 1, 2, 'C', 0);
			$this->SetXY(53,10);	$this->Cell(173, 20,'Relaci�n de Puestos del Instituto', 1, 2, 'C', 0);
			$this->Cell(173, 10,'Referencia a la Norma ISO 9001:2008   5.2, 7.2.3', 1, 2, 'C', 0);
			$this->SetXY(233,30);	$this->Cell(25,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,55);	$this->Cell(261,120,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(215,175);	$this->MultiCell(50,5,'V� B�',0,'J'); 
			$this->SetXY(182,183);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(207,190);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');

			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(120,200);	$this->Cell(1,1,"SNEST-AC-PO-004-01",0,1,'L', 0);
			$this->SetXY(257,208);	$this->Cell(1,1,"Rev. 4",0,1,'L', 0);
			$this->Image($CFG->imgDir."/escudo.jpg", 255, 189, 15, 0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('L','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	/*doc controlado*/
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales ************************/	
	$pdf->SetXY(10,45);	$pdf->Cell(100,10,"Fecha:  ________________________",0,0,'L');
	$pdf->SetXY(24,44);	$pdf->Cell(100,10,formato_de_fecha(date('d/m/Y')),0,0,'L');   
	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 26
	$alto_celda = 4;
	$cont = 1;
	$j = 62; 
	$titulos = 1;
	$registro_max = 26;

	$qry_select = "select distinct(PE.rfc), PE.no_tarjeta, PE.curp_empleado, PE.apellidos_empleado, PE.nombre_empleado, PE.ingreso_rama, PE.inicio_gobierno, PE.inicio_sep, PE.inicio_plantel from personal PE, plantilla_personal PP where PE.rfc = PP.rfc and ( PE.status_empleado = '01' or PE.status_empleado = '02' )  order by PE.apellidos_empleado, PE.nombre_empleado, PE.rfc";
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
	if($titulos==1){ 
		$pdf->SetFont('Arial','','7');
		$pdf->SetXY(8,$j); $pdf->Cell(8,$alto_celda,'TAR',0,0,'C');
		$pdf->SetXY(25,$j); $pdf->Cell(25,$alto_celda,'NOMBRE COMPLETO',0,0,'C');
		$pdf->SetXY(47,$j); $pdf->Cell(48,$alto_celda,'FILIACI�N',0,0,'C');
		$pdf->SetXY(57,$j); $pdf->Cell(58,$alto_celda,'GOB.',0,0,'C');
		$pdf->SetXY(63,$j); $pdf->Cell(64,$alto_celda,'SEP',0,0,'C');
		$pdf->SetXY(69,$j); $pdf->Cell(69,$alto_celda,'RAMA',0,0,'C');
		$pdf->SetXY(75,$j); $pdf->Cell(76,$alto_celda,'I.T.T.',0,0,'C');
		$pdf->SetXY(92,$j); $pdf->Cell(93,$alto_celda,'CLAVE',0,0,'C');
		$pdf->SetXY(120,$j); $pdf->Cell(122,$alto_celda,'CATEGOR�A',0,0,'C');
		$titulos=0;}

	if($cont < $registro_max){ 
		$rfc=($registro->fields('rfc'));
		$tarjeta=$registro->fields('no_tarjeta');
		$curp=($registro->fields('curp_empleado'));
		$nombre=($registro->fields('nombre_empleado'))." ".$registro->fields('apellidos_empleado'); 
		$rama=$registro->fields('ingreso_rama');
		$gob=($registro->fields('inicio_gobierno'));
		$sep=$registro->fields('inicio_sep');
		$itt=($registro->fields('inicio_plantel'));

		$pdf->SetXY(10,($j=$j+$alto_celda));	$pdf->Cell(5,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(15,$j);			$pdf->Cell(205,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(75,$j);			$pdf->Cell(23,$alto_celda,' ',1, 2, 'C', 0);
	 	$pdf->SetXY(199,$j);			$pdf->Cell(70,$alto_celda,' ',1, 2, 'C', 0); 

		//$pdf->SetFont('Arial','','7');
		$pdf->SetXY(8,$j); $pdf->Cell(8,$alto_celda,$tarjeta,0,0,'C');
		$pdf->SetXY(14,$j); $pdf->Cell(0,$alto_celda,$nombre,0,0,'J');
		$pdf->SetXY(61,$j); $pdf->Cell(0,$alto_celda,$rfc,0,0,'J');
		$pdf->SetXY(81,$j); $pdf->Cell(0,$alto_celda,$gob,0,0,'J');
		$pdf->SetXY(90,$j); $pdf->Cell(0,$alto_celda,$sep,0,0,'J');
		$pdf->SetXY(99,$j); $pdf->Cell(0,$alto_celda,$rama,0,0,'J');
		$pdf->SetXY(108,$j); $pdf->Cell(0,$alto_celda,$itt,0,0,'J');
		$pdf->SetXY(117,$j); $pdf->Cell(0,$alto_celda,$clave,0,0,'J');

		if($cont==($registro_max - 1))
			{
			$registro_max=$registro_max+26;
			$titulos=1;
			$j=62;
			$pdf->SetFont('Arial','','10');
			$pdf->SetXY(10,170); $pdf->Cell(259,6,"Continua...",0,0,'C');
			$pdf->AddPage(); 	
			}
		}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
