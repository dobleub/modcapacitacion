<?php 
	/********************************************************
		Listado General de Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxaico
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 				   |
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');

			// Colores de los bordes, fondo y texto
			$this->SetDrawColor(0,80,180); //Bordes azul
			//$this->SetFillColor(230,230,0); //Color de relleno
			$this->SetTextColor(220,50,50);   // Rojo paran titulos

			$this->SetXY(53,10);	$this->Cell(120, 30,'Listado General del Personal: Activos-Licencia', 0, 2, 'C', 0);  
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 11, 25, 0);
    			//$this->Cell(53,20,'prueba',1,1,'C',true);

			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,42);	$this->Cell(198,188,'D O C U M E N T O   C O N T R O L A D O', 0, 2, 'C', 0);  // 42
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(155,230);	$this->MultiCell(50,5,'V� B�',0,'J');
			$this->SetXY(122,238);	$this->Cell(30,10,"_____________________________________", 0,0,'L');
			$this->SetXY(147,245);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');
			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(10,265);	$this->Cell(196,4,"Departamento de Recursos Humanos",0,1,'L', 0);
			$this->SetXY(150,265);	$this->Cell(10,4,'Hoy es '.formato_de_fecha(date('d/m/Y')),0,1,'L', 0);
			$this->SetXY(160,270);	$this->Cell(10,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales ************************/ 

	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 45
	$alto_celda = 4;
	$cont=1;
	$j=42; 
	$titulos=1;
	$registro_max=45;
	$id = false;
	// and (tipo_personal = 'B' or tipo_personal = 'X')
	$qry_select = "select rfc, no_tarjeta, curp_empleado, inicio_sep, apellidos_empleado, nombre_empleado from personal where (status_empleado = '01' or status_empleado = '02')  order by apellidos_empleado, nombre_empleado, rfc";
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
		if($titulos==1){ 
			$pdf->SetFont('Arial','','7');
			$pdf->SetFillColor(200,200,200); //Color de relleno
			$pdf->SetXY(10,$j);	$pdf->Cell(5,$alto_celda,'No.',0,0,'C');
			$pdf->SetXY(15,$j);	$pdf->Cell(6,$alto_celda,'TAR',0,0,'C');
			$pdf->SetXY(21,$j);	$pdf->Cell(107,$alto_celda,'NOMBRE COMPLETO',0,0,'C');
			$pdf->SetXY(128,$j);	$pdf->Cell(23,$alto_celda,'FILIACI�N',0,0,'C');
			$pdf->SetXY(151,$j);	$pdf->Cell(31,$alto_celda,'CURP',0,0,'C');
			$pdf->SetXY(182,$j);	$pdf->Cell(24,$alto_celda,'INGRESO SEP',0,0,'C');

			/*$pdf->SetXY(11,$j);	$pdf->Cell(3,$alto_celda,'No.',0,0,'C', true);
			$pdf->SetXY(16,$j);	$pdf->Cell(4,$alto_celda,'TAR',0,0,'C', true);
			$pdf->SetXY(22,$j);	$pdf->Cell(105,$alto_celda,'NOMBRE COMPLETO',0,0,'C', true);
			$pdf->SetXY(129,$j);	$pdf->Cell(21,$alto_celda,'FILIACI�N',0,0,'C', true);
			$pdf->SetXY(152,$j);	$pdf->Cell(29,$alto_celda,'CURP',0,0,'C', true);
			$pdf->SetXY(183,$j);	$pdf->Cell(22,$alto_celda,'INGRESO SEP',0,0,'C', true);*/

			$titulos=0;}

		if ($cont < $registro_max){ 
			$rfc=$registro->fields('rfc');
			$tarjeta=$registro->fields('no_tarjeta'); 
			$curp = $registro->fields('curp_empleado');
			$sep = $registro->fields('inicio_sep');
			$estatus = $registro->fields('status_empleado');

			$pdf->SetDrawColor(0,80,180); //Bordes azul
			$pdf->SetXY(10,($j=$j+$alto_celda));	$pdf->Cell(5,$alto_celda,' ',1, 2, 'C', 0);
			$pdf->SetXY(15,$j);			$pdf->Cell(113,$alto_celda,' ',1, 2, 'C', 0);
		 	$pdf->SetXY(151,$j);			$pdf->Cell(31,$alto_celda,' ',1, 2, 'C', 0); 
			$pdf->SetXY(21,$j);			$pdf->Cell(185,$alto_celda,' ',1, 2, 'C', 0);
			$pdf->SetFont('Arial','','7');

			$pdf->SetFillColor(245,245,245); //Color de relleno

			$pdf->SetXY(10,$j);	$pdf->Cell(5,$alto_celda,$cont,0,0,'C');
			$pdf->SetXY(15,$j);	$pdf->Cell(6,$alto_celda,$tarjeta,0,0,'C');
			$pdf->SetXY(21,$j);	$pdf->Cell(107,$alto_celda,$registro->fields('apellidos_empleado').' '.$registro->fields('nombre_empleado'),0,0,'J');
			$pdf->SetXY(128,$j);	$pdf->Cell(23,$alto_celda,$rfc,0,0,'J');
			$pdf->SetXY(151,$j);	$pdf->Cell(31,$alto_celda,$curp,0,0,'J');
			$pdf->SetXY(182,$j);	$pdf->Cell(24,$alto_celda,$sep,0,0,'C');

			$id = ($id==true)?false:true;

			if($cont==($registro_max - 1))
				{	
				$registro_max=$registro_max+44;
				$titulos=1;
				$j=42;
				$pdf->SetFont('Arial','','10');
				$pdf->SetXY(10,225);	$pdf->Cell(196,6,"Continua...",0,0,'C');
				$pdf->AddPage();  
				}
			}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
