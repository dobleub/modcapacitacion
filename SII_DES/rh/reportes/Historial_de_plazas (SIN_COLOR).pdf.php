<?php 
	/********************************************************
		Historial de plazas del personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxaico
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');

	$_SESSION['idp']= $_GET['idp'];

	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * *|
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			//$this->SetXY(10,10);	$this->Cell(259, 30,' ', 1, 2, 'C', 0);
			//$this->SetXY(226,10);	$this->Cell(43, 10,'ITTXO-CA-PO-001-01', 1, 2, 'C', 0);
			//$this->Cell(43, 10,'Revisi�n: 0', 1, 2, 'C', 0);
			$this->SetXY(53,10);	$this->Cell(185, 30,'Historial de Claves Presupuestales de '.$_SESSION['idp'], 0, 2, 'C', 0);
			//$this->Cell(173, 10,'Referencia a la Norma ISO 9001:2008   5.2, 7.2.3', 1, 2, 'C', 0);
			//$this->SetXY(233,30);	$this->Cell(25,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			$this->Image($CFG->imgDir."/escudo.jpg", 240, 11, 25, 0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,43);	$this->Cell(261,132,'D O C U M E N T O   C O N T R O L A D O',1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(215,175);	$this->MultiCell(50,5,'V� B�',0,'J'); 
			$this->SetXY(182,183);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(207,190);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');

			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(120,200);	$this->Cell(1,1,"Departamento de Recursos Humanos",0,1,'L', 0);
			$this->SetXY(220,200);	$this->Cell(10,4,'Hoy es '.formato_de_fecha(date('d/m/Y')),0,1,'L', 0);
			$this->SetXY(225,205);	$this->Cell(10,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/
	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('L','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	/*doc controlado*/
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales ************************/	
  
	// NUMERO MAXIMO DE REGISTROS DE POR HOJA = 30
	$alto_celda = 4;
	$cont = 1;
	$j = 46; 
	$titulos = 1;
	$registro_max = 30;



	$qry_select = "select id_clave_presupuestal_personal, clave_antes_punto, clave_despues_punto, horas_clave_plaza, descripcion_categoria, CPP.fecha_registro, CPP.hora_registro, CPP.estatus, movimiento, e_iniciales, e_finales, MP.estatus as status from movimiento__personal MP, claves_presupuestales_personal CPP, claves_presupuestales CP, categorias C 
where MP.id_mov = CPP.id_movimiento and MP.rfc='".$_SESSION['idp']."' and CP.id_clave = CPP.id_clave and CPP.categoria = C.categoria order by id_clave_presupuestal_personal desc";

	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
	if($titulos==1){ 
		$pdf->SetFont('Arial','','7');
		$pdf->SetXY(8,$j);	$pdf->Cell(8,$alto_celda,'No.',0,0,'C');
		$pdf->SetXY(30,$j);	$pdf->Cell(30,$alto_celda,'CLAVE PRESUPUESTAL',0,0,'C');
		$pdf->SetXY(75,$j);	$pdf->Cell(75,$alto_celda,'CATEGOR�A',0,0,'C');
		$pdf->SetXY(112,$j);	$pdf->Cell(112,$alto_celda,'F REGISTRO',0,0,'C');
		$pdf->SetXY(124,$j);	$pdf->Cell(124,$alto_celda,'MOVIMIENTO',0,0,'C');
		$pdf->SetXY(140,$j);	$pdf->Cell(140,$alto_celda,'EFECTOS INICIALES',0,0,'C');
		$pdf->SetXY(159,$j);	$pdf->Cell(159,$alto_celda,'EFECTOS FINALES',0,0,'C');
		$pdf->SetXY(173,$j);	$pdf->Cell(173,$alto_celda,'H REGISTRO',0,0,'C');
		$titulos=0;}

	if($cont < $registro_max){ 
		$clave_antes_punto = $registro->fields('clave_antes_punto');
		$clave_despues_punto = $registro->fields('clave_despues_punto');
		$horas_clave_plaza = $registro->fields('horas_clave_plaza');

		$clave_presupuestal = $clave_antes_punto.'.'.$clave_despues_punto.' C/'.$horas_clave_plaza.' HRS.';

		$descripcion_categoria = $registro->fields('descripcion_categoria');
		$fecha_registro = $registro->fields('fecha_registro');
		$estatus = $registro->fields('estatus');

		$movimiento = $registro->fields('movimiento');
		$e_iniciales = $registro->fields('e_iniciales');
		$e_finales = $registro->fields('e_finales');
		//$status = $registro->fields('status');
		$f_registro = $registro->fields('hora_registro');

		$pdf->SetXY(10,($j=$j+$alto_celda));	$pdf->Cell(5,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(15,$j);			$pdf->Cell(161,$alto_celda,' ',1, 2, 'C', 0);
		$pdf->SetXY(75,$j);			$pdf->Cell(177,$alto_celda,' ',1, 2, 'C', 0);
	 	$pdf->SetXY(160,$j);			$pdf->Cell(65,$alto_celda,' ',1, 2, 'C', 0); 
	 	$pdf->SetXY(195,$j);			$pdf->Cell(74,$alto_celda,' ',1, 2, 'C', 0);

 			if ($estatus!=1){
				$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(155,155,155);
			}else
			{	$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(0,0,0);
			}

		$pdf->SetFont('Arial','','7');
		$pdf->SetXY(11,$j);	$pdf->Cell(3,$alto_celda,$cont,0,0,'C');
		$pdf->SetXY(15,$j);	$pdf->Cell(0,$alto_celda,$clave_presupuestal,0,0,'J');
		$pdf->SetXY(75,$j);	$pdf->Cell(0,$alto_celda,$descripcion_categoria,0,0,'J');
		$pdf->SetXY(161,$j);	$pdf->Cell(0,$alto_celda,$fecha_registro,0,0,'J');

		$pdf->SetXY(183,$j);	$pdf->Cell(0,$alto_celda,$movimiento,0,0,'J');
		$pdf->SetXY(205,$j);	$pdf->Cell(0,$alto_celda,$e_iniciales,0,0,'J');
		$pdf->SetXY(234,$j);	$pdf->Cell(0,$alto_celda,$e_finales,0,0,'J');
		$pdf->SetXY(253,$j);	$pdf->Cell(0,$alto_celda,$f_registro,0,0,'J');

		if($cont==($registro_max - 1))
			{
			$registro_max=$registro_max+30;
			$titulos=1;
			$j=46;
			$pdf->SetFont('Arial','','10');
			$pdf->SetXY(10,170); $pdf->Cell(259,6,"Continua...",0,0,'C');
			$pdf->AddPage(); 	
			}
		}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>
