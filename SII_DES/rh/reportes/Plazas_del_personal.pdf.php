<?php 
	/********************************************************
		Clave Presupuestal Historial

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
   // Librerias y funciones.
	require_once("../../../includes/config.inc.php");
	require_once($CFG->fpdfDir."/fpdf.php");
	require_once($CFG->fpdfDir."/mc_table.php");
	seguridad('');
	$_SESSION['idp']= $_GET['idp'];
	$_SESSION['m']= $_GET['m'];
	//Funci�n Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
	
	// Obtenci�n de variables por METODO GET para la generaci�n del documento PDF.
	/*	 _________________________________________________________________________________________________________________
		|							* * * * * FUNCIONES PARA CLASS PDF * * * * *  		  |
		|_________________________________________________________________________________________________________________|
	    CLASE PDF para declaraci�n de funciones adicionales para la generaci�n del documento.
	*/
	class PDF extends PDF_MC_Table
	  {	
			/*_____________________________________________________________________________________________________
			 |					* * *   E N C A B E Z A D O   D E L   D O C U M E N T O   * * * 				   |
			 |_____________________________________________________________________________________________________|
			*/

		function Header()
		  {
			// Obtenci�n y declaraci�n de varialbes por medio del m�todo GET para la clase PDF.
			$CFG = $GLOBALS['CFG'];
			/************************ Encabezado Principal ************************/
			$this->Image($CFG->imgDir."/logo_dgest.jpg", 13, 15, 38, 0);
			$this->SetLineWidth(0.5);
			$this->SetFont('Arial','b','12');
			$this->SetXY(53,10);	$this->Cell(120, 30,'Claves Presupuestales de '.$_SESSION['idp'], 0, 2, 'C', 0);  
			$this->Image($CFG->imgDir."/escudo.jpg", 174, 11, 25, 0);
			$this->SetLineWidth(0);

			$this->SetFont('Arial','b','12');
			$this->SetTextColor(200,200,200);
			$this->SetXY(9,62);	$this->Cell(198,168,'D O C U M E N T O   C O N T R O L A D O', 1, 2, 'C', 0);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','','10');
			$this->SetXY(155,230);	$this->MultiCell(50,5,'V� B�',0,'J');
			$this->SetXY(122,238);	$this->Cell(30,10,"_____________________________________",0,0,'L');
			$this->SetXY(147,245);	$this->MultiCell(50,5,'Nombre y Firma',0,'J');
			/************************ Encabezado Principal ************************/
		  }
	//Pie de p�gina
		function Footer()
		  {
			/************************ Leyenda final ************************/	
			$CFG = $GLOBALS['CFG'];			
			$this->SetFont('Helvetica','','10');
			$this->SetFillColor(0,0,0);
			$this->SetTextColor(155,155,155);  // Este es el color de las graf�as
	 		$this->SetXY(10,265);	$this->Cell(196,4,"Departamento de Recursos Humanos",0,1,'L', 0);
			$this->SetXY(150,265);	$this->Cell(10,4,'Hoy es '.formato_de_fecha(date('d/m/Y')),0,1,'L', 0);
			$this->SetXY(160,270);	$this->Cell(10,5,'P�gina '.$this->PageNo().' de {nb}',0,0,'L',0);
			/************************ Leyenda final ************************/
		  }
	  }//Termina las funciones para CLASS PDF


	/* ------------------------------------------------------------------------------------------------------------------ 
		 _________________________________________________________________________________________________________________
		|							* * * * * INICIO DEL DOCUMENTO  * * * * *                 |
		|_________________________________________________________________________________________________________________|
	*/

	//Definici�n del tama�o y orientaci�n del documento.
	$pdf=new PDF('P','mm','Letter');
	$pdf->Open();
	//Funciones para margen inferior y pie de p�gina
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(1,20);
	/************************ Datos generales del Grupo impartido por Docente ************************/	

	$consulta_datos_personal="select apellidos_empleado, nombre_empleado, ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel from personal where rfc ='".$_SESSION['idp']."'";
	$registros_ = ejecutar_sql($consulta_datos_personal);
		$nombre = $registros_->fields('nombre_empleado').' '.$registros_->fields('apellidos_empleado');
		$ingreso_rama= $registros_->fields('ingreso_rama');
		$inicio_gobierno = $registros_->fields('inicio_gobierno');
		$inicio_sep = $registros_->fields('inicio_sep');
		$inicio_plantel= $registros_->fields('inicio_plantel');

	$pdf->SetXY(90,45);	$pdf->Cell(100,10,"Empleado:  __________________________________________________",0,0,'L');
	$pdf->SetXY(110,44);	$pdf->Cell(100,10,$nombre,0,0,'L');

	$pdf->SetXY(45,51);	$pdf->Cell(100,10,"Gob: ",0,0,'L');
	$pdf->SetXY(55,52);	$pdf->Cell(100,10,$inicio_gobierno,0,0,'L');
	$pdf->SetXY(75,51);	$pdf->Cell(100,10,"SEP: ",0,0,'L');
	$pdf->SetXY(85,52);	$pdf->Cell(100,10,$inicio_sep,0,0,'L');
	$pdf->SetXY(110,51);	$pdf->Cell(100,10,"Rama: ",0,0,'L');
	$pdf->SetXY(125,52);	$pdf->Cell(100,10,$ingreso_rama,0,0,'L');
	$pdf->SetXY(150,51);	$pdf->Cell(100,10,"ITT: ",0,0,'L');
	$pdf->SetXY(160,52);	$pdf->Cell(100,10,$inicio_plantel,0,0,'L');

	// NUMERO MAXIMO DE REGISTROS POR HOJA = 26
	$altura_celda = 6;
	$cont=1;
	$j=62; 
	$titulos=1;
	$registro_max=26;

	$qry_select = "select id_clave_presupuestal_personal, MP.rfc, clave_antes_punto, clave_despues_punto, horas_clave_plaza, descripcion_categoria, CPP.fecha_registro, CPP.estatus from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP, categorias C, claves_presupuestales CP having MP.id_mov = CPP.id_movimiento and MP.rfc=PE.rfc and MP.id_mov = ".$_SESSION['m']." and CP.id_clave = CPP.id_clave and C.categoria = CPP.categoria
order by CPP.estatus, id_clave_presupuestal_personal desc";
	$registro = ejecutar_sql($qry_select);

	while (!$registro->EOF)
	{ //TITULOS DE LISTA/TABLA
		if($titulos==1){ 
			$pdf->SetFont('Arial','','7');
			$pdf->SetFillColor(200,200,200); //Color de relleno
			$pdf->SetXY(10,$j);	$pdf->Cell(7,$altura_celda,'No.',0,0,'C', true);
			$pdf->SetXY(17,$j);	$pdf->Cell(73,$altura_celda,'CLAVE PRESUPUESTAL',0,0,'C', true);
			$pdf->SetXY(90,$j);	$pdf->Cell(80,$altura_celda,'CATEGORIA',0,0,'C', true);
			$pdf->SetXY(170,$j);	$pdf->Cell(36,$altura_celda,'FECHA DE REGISTRO',0,0,'C', true);
			$titulos=0;
			}
		if($cont < $registro_max){ 
			$rfc=$registro->fields('rfc'); 
 
			$clave_presupuestal = $registro->fields('clave_antes_punto').'.'.$registro->fields('clave_despues_punto').' C/'.$registro->fields('horas_clave_plaza').' HRS.';
			$categoria = $registro->fields('descripcion_categoria');
			$fecha_registro = $registro->fields('fecha_registro');

			$pdf->SetXY( 17,($j=$j+$altura_celda));		$pdf->Cell(189,$altura_celda,' ',1, 2, 'C', 0);
			$pdf->SetXY( 90,$j);				$pdf->Cell(80,$altura_celda,' ',1, 2, 'C', 0);
			
			if ($registro->fields('estatus')!=1){
				$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(155,155,155);
			}else
			{	$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(0,0,0);
			}
			$pdf->SetXY( 10,$j);	$pdf->Cell( 7,$altura_celda,$cont,0,0,'C');
			$pdf->SetXY( 17,$j);	$pdf->Cell(73,$altura_celda,$clave_presupuestal,0,0,'J');
			$pdf->SetXY( 90,$j);	$pdf->Cell(80,$altura_celda,$categoria,0,0,'J');
			$pdf->SetXY(170,$j);	$pdf->Cell(36,$altura_celda,$fecha_registro,0,0,'C');
			
			if($cont==($registro_max - 1))
				{
				$registro_max=$registro_max+26;
				$titulos=1;
				$j=62; // 54
				$pdf->SetFont('Arial','','10');
				$pdf->SetXY(10,225);	$pdf->Cell(196,6,"Continua...",0,0,'C');
				$pdf->AddPage();
				}
			}
	$cont++;
	$registro ->movenext();
	}
	$pdf->Output();
?>	
