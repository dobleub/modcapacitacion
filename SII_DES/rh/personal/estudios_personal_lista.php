<?php
	/********************************************************
		Información Estudios

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Estudios del Personal ::.</title>
	</head>

<body>
<?php
	if (isset($_GET['todos']) and !isset($_POST['todos']))	$todos = $_GET['todos'];	else   $todos = $_POST['todos'];
	 $rfc_personal=$_POST['rfc_personal'];
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<h2 align="center"> Registrar/Actualizar Datos de estudio(s) de Empleado(s) (Activos)</h2>

    <!-- INICIO MODIFICACION -->
<form name="solicitud_ver" method="post" action="">
	<h3 align="center"> Opciones de Visualizaci&oacute;n: </h3>
	<table width="30%" align="center" title="Secci&oacute;n para buscar un empleado">
		<tr align="center">
			<th>Ver: </th>
		</tr>
		<tr align="center">
               <!-- 
		<th><select name="elegir"> <option selected value="RFC"> RFC
<?php
        $contador = 0;
	$consultar_rfcs="select rfc from personal where ( status_empleado = '01' or status_empleado = '02' )";
	$registros_rfc=ejecutar_sql($consultar_rfcs);
while(!$registros_rfc->EOF){

		$contador = $contador + 1;
		$rfc_especifico = $registros_rfc->fields('rfc');
                echo '<option value="'.$rfc_especifico.'"> '.$rfc_especifico.''; // '$rfc_especifico'
		$registros_rfc->MoveNext();
	}
?>
                    </select>
		</th> -->
		   <th>
			<select name="rfc_personal" title="Presione aqu&iacute; para seleccionar el empleado" onChange="javascript:document.solicitud_ver.submit();">
					<?php
					$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);

					if(!$datos_rfc->rowcount()){
						echo "<option value='0' selected> No hay personal dado de alta </option>";
					}
					else
					{
						echo "<option value='0' selected> -- Seleccione Personal -- </option>";

						while(!$datos_rfc->EOF){
							echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
							$datos_rfc->MoveNext();
						}
					}
					?>
			</select>
		   </th>
		</tr>
</table>
<br>
<!--  MODIFICAR 081111
<div align='center'>
<input name="Buscar" type="submit" class="boton" id="Guardar" value="Buscar"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>   -->

    <!-- FIN MODIFICACION -->

 <input type='hidden' name='todos' value='1'>

<table align="center" title="Tabla que muestra el listado del personal al que se puede modificar sus estudios">
  <tr align="center">
    <th align="center"> No. </th>
    <th align="center"> No. tarjeta </th>
    <th align="center"> RFC </th>
    <th> Nombre completo </th>
	<!-- <th> Estatus </th> -->
    <th> Tipo de Trabajador </th>
    <th> Modificar </th>
  </tr>
  <?php
     //// INICIO MODIFICACION
        if ($rfc_personal == '0' or $todos == 't')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, no_tarjeta, status_empleado, tipo_personal from personal where ( status_empleado = '02' )  order by apellidos_empleado, nombre_empleado, rfc, tipo_personal"; 
        else if ($rfc_personal != '0')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, no_tarjeta, status_empleado, tipo_personal from personal where ( status_empleado = '02' )  and  ( rfc = '".$rfc_personal."' )";
     // FIN MODIFICACION 

	$registros=ejecutar_sql($consulta_registros);

	$id = "non";
	$contador = 0;

	while(!$registros->EOF){

		$contador = $contador + 1;
		$nt = $registros->fields('no_tarjeta');
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$se = $registros->fields('status_empleado');
		$tipo_personal=$registros->fields('tipo_personal');

		echo "<tr id='$id'>
		<td align='center'> $contador </td>
		<td align='center'> $nt </td>
		<td align='center'> $rfc </td>
		<td> $ae $ne </td>";
		
	//echo "<td align='center'>".(($se=="02")?"Activo":(($se=="00")?"Baja por renuncia":(($se=="01")?"Licencia":(($se=="03")?"Baja por jubilaci&oacute;n":(($se=="04")?"Inactivo por cambio de RFC":(($se=="05")?"Baja por fallecimiento":(($se=="06")?"Inactivo":" ")))))))."</td>";
 
		$tipo_personal_mensaje=(($tipo_personal=="H")?"Profr. Visit.":(($tipo_personal=="B")?"Base":(($tipo_personal=="X")?"Mixto":"Sin Tipo")));
		echo "<td align='center'> $tipo_personal_mensaje </td>
		      <td align='center'>";?>
  <a href="estudios_personal.php?rfc=<?php echo $rfc;  ?>" > <img border="0" src="../../../img/iconos/lista.gif" width='15' height='20' title="Presione aqu&iacute; para modificar los datos de estudio de este trabajador"/></a><?php echo"</td>
		</tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext(); // estudios_personal.php?rfc=$rfc";  
	}
	?>
</table>
</form>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
</form>
</body>
</html>
