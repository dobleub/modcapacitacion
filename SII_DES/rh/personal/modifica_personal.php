<?php
	/********************************************************
		Modificaciones

		Modificado por:	Residente C. Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__     permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8);
	if (isset($_GET['todos']) and !isset($_POST['todos']))	$todos = $_GET['todos'];	else   $todos = $_POST['todos']; 
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Modificaciones ::.</title>
	</head>
<body>
	<?php
		$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	?>

<h2 align="center"> Modificar Registro de Empleado </h2>

<form name="solicitud_ver" method="post" action="">
	<h3 align="center"> Opciones de Visualizaci&oacute;n: </h3>
	<table width="30%" align="center" title="Secci&oacute;n para buscar por su RFC a un trabajador">
		<tr align="center">
			<th>Ver: </th>
		</tr>
		<tr align="center">
			<th>
				<select name="rfc_personal" title="Seleccione el nivel de estudios" onChange="javascript:document.solicitud_ver.submit();">
					<?php
					$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where status_empleado != '00' order by apellidos_empleado, nombre_empleado, rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);

					if(!$datos_rfc->rowcount())	echo "<option value='0' selected> No hay personal dado de alta </option>";
					else
					{
						echo "<option value='0' selected> -- Seleccione Personal -- </option>";
						while(!$datos_rfc->EOF){
							echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
							$datos_rfc->MoveNext();
						}
					}
					?>
				</select>
			</th>
		</tr>
	</table>
	<br>
 <input type='hidden' name='todos' value='1'>

  <?php
        if ($rfc_personal == '0' or $todos == 't')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, status_empleado, tipo_personal from personal where status_empleado != '00' order by status_empleado, tipo_personal, apellidos_empleado, nombre_empleado, rfc"; 
        else if ($rfc_personal != '0')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, status_empleado, tipo_personal from personal where status_empleado != '00' and  ( rfc = '".$rfc_personal."' )";

	//$consulta_registros="";
	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0;
	$ei = "";
	while(!$registros->EOF){

		$contador = $contador + 1;
		//$nt = $registros->fields('no_tarjeta');
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$se = $registros->fields('status_empleado');
		$tipo_personal=$registros->fields('tipo_personal');

		if ($se=="02"){$se = "Activo";}
		//if ($se=="00"){$se = "Baja por renuncia";}
		if ($se=="01"){$se = "Licencia";}
		if ($se=="03"){$se = "Baja por jubilaci&oacute;n";}
		//if ($se=="04"){$se = "Inactivo por cambio de RFC";}
		if ($se=="05"){$se = "Baja por fallecimiento";}
		if ($se=="06"){$se = "Inactivo";}

		if  ($se <> $ei){
			echo "<br>";
			echo '<table align="center" width="60%" title="Tabla que muestra la lista de trabajadores del instituto">';
			echo "<tr id='$id'><th colspan='6'>$se</th></tr>";
			echo '  <tr align="center">
		    		<th width="3%"> No. </th>
				<th width="13%"> RFC </th>
		    		<th> Nombre  </th>
				<th width="15%"> Tipo trabajador </th>
		    		<th width="10%"> Modificar </th>
		  		</tr>';	
			$ei = $se;
			$contador  = 1;
		}
		echo "<tr id='$id'>
			<td align='center'> $contador </td>
			<td> $rfc </td>
			<td> $ae $ne </td>		
			<td align='center'>".(($tipo_personal=='Z')?'Sin Tipo':(($tipo_personal=='B')?'Base':(($tipo_personal=='H')?'Honorarios':(($tipo_personal=='X')?'Mixto':'Sin Registro'))))."</td>
			<td align='center'>";
?>
  <a href="manto_personal.php?accion=actualiza&rfc=<?php echo $rfc; ?>" > <img border="0" src="../img/lista.gif" width='15' height='20' title="Presione aqu&iacute; para modificar los datos de este trabajador"/></a>
<?php
			echo"</td>
		      </tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
	</table>
</form>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
</form>
</body>
</html>
