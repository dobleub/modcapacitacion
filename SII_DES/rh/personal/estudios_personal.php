<?php
	/********************************************************
		estudios_personal.php

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	require_once($CFG->funciones_phpDir."/funciones_fechas.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

	$regresar = "window.location = 'estudios_personal_lista.php?todos=t'";
	$rfc = $_GET['rfc'];

	$consulta_select_sql= "select count(rfc) as Total from estudios_personal where rfc='$rfc'";
        $sql_consulta = ejecutar_sql($consulta_select_sql) ;
        $cantidad_personal= $sql_consulta->fields('Total'); 

        if ($cantidad_personal == 0){
		$consulta_select_sql_1= "select apellidos_empleado, nombre_empleado from personal where rfc='$rfc'";
		$sql_consulta_1 = ejecutar_sql($consulta_select_sql_1) ;
		$apellidos_empleado= $sql_consulta_1->fields('apellidos_empleado');
		$nombre_empleado= $sql_consulta_1->fields('nombre_empleado');
	 
		$qry_estudios_personal = "insert into estudios_personal (rfc, apellidos_empleado, nombre_empleado) values ('$rfc', '$apellidos_empleado', '$nombre_empleado')";
		$creacion_de_personal_estudios = ejecutar_sql($qry_estudios_personal);
		}

	if ($rfc == ""){ $rfc = $_POST['rfco']; }

	$fecha_actual = date('Y/m/d');
	$anyo_actual = date('Y');

	$qry_p = "select * from estudios_personal where rfc = '$rfc'";
	$res_p = ejecutar_sql($qry_p);
	$ne = $_POST['nivel_estudios'];

		if ($ne == ""){ $ne = $res_p->fields('nivel_estudios');	}
		$ano_inicio_estudios = $res_p->fields('ano_inicio_estudios');
		$ano_termino_estudios = $res_p->fields('ano_termino_estudios');
		$grado_maximo_estudios = $res_p->fields('grado_maximo_estudios');
		$titulo_num_foja = $res_p->fields('titulo_num_foja');
		$titulo_num_libro = $res_p->fields('titulo_num_libro');
		$titulo_num_ano = $res_p->fields('titulo_num_ano');
		$titulo_num = $res_p->fields('titulo_num');
		$institucion_egreso = $res_p->fields('institucion_egreso');
		$instituto_titulacion = $res_p->fields('instituto_titulacion');
		$institucion_expide_titulo = $res_p->fields('institucion_expide_titulo');
		$estudios = $res_p->fields('estudios'); 
		$fecha_titulacion =  $res_p->fields('fecha_titulacion'); 
		$especializacion = $res_p->fields('especializacion');
		$cedula_profesional = $res_p->fields('cedula_profesional');
		$fecha_cedula = $res_p->fields('fecha_cedula');
		$institucion_expide_cedula = $res_p->fields('institucion_expide_cedula');
		$idiomas_domina = $res_p->fields('idiomas_domina');
		$documento_obtenido = $res_p->fields('documento_obtenido');
		$documento_obtenido_institucion = $res_p->fields('documento_obtenido_institucion');
		$documento_obtenido_fecha = $res_p->fields('documento_obtenido_fecha');
		$id_titulo = $res_p->fields('id_titulo');

	$actualiza = $_POST['actualiza']; 

	if($actualiza=="Actualizar"){
		$nivel_estudios = $_POST['nivel_estudios'];
		$grado_maximo_estudios = $_POST['grado_maximo_estudios']; 
		$ano_inicio_estudios = $_POST['ano_inicio_estudios'];
		$ano_termino_estudios = $_POST['ano_termino_estudios']; 
		$titulo_num_foja = $_POST['titulo_num_foja']; 
		if($titulo_num_foja==""){$titulo_num_foja = 0;}
		$titulo_num_ano = $_POST['titulo_num_ano']; 
		if($titulo_num_ano==""){$titulo_num_ano = 0;}
		$titulo_num_libro = $_POST['titulo_num_libro'];
		$titulo_num = $_POST['titulo_num']; 
		$institucion_egreso = $_POST['institucion_egreso'];
		$instituto_titulacion = $_POST['instituto_titulacion'];
		$institucion_expide_titulo = $_POST['institucion_expide_titulo'];
		$estudios = $_POST['estudios'];  
		$fecha_titulacion = $_POST['fecha_titulacion']; 
		$especializacion = $_POST['especializacion'];
		$cedula_profesional = $_POST['cedula_profesional']; 
		$fecha_cedula = $_POST['fecha_cedula'];
		$institucion_expide_cedula = $_POST['institucion_expide_cedula'];
		$idiomas_domina = $_POST['idiomas_domina']; 
		$documento_obtenido = $_POST['documento_obtenido'];
		$documento_obtenido_institucion = $_POST['documento_obtenido_institucion'];
		$documento_obtenido_fecha = $_POST['documento_obtenido_fecha'];
		$id_titulo = $_POST['id_titulo'];

		$qry_actualiza = "update estudios_personal set 
		nivel_estudios = '$nivel_estudios', 
		grado_maximo_estudios = '$grado_maximo_estudios', 
		ano_inicio_estudios = $ano_inicio_estudios,
		ano_termino_estudios = $ano_termino_estudios, 
		titulo_num_foja = $titulo_num_foja, 
		titulo_num_libro = '$titulo_num_libro',
		titulo_num_ano = $titulo_num_ano, 
		titulo_num = '$titulo_num', 
		institucion_egreso = '$institucion_egreso',
		instituto_titulacion = '$instituto_titulacion',
		institucion_expide_titulo = '$institucion_expide_titulo',
		estudios = '$estudios',  
		fecha_titulacion = '$fecha_titulacion', 
		especializacion = '$especializacion',
		cedula_profesional = '$cedula_profesional', 
		fecha_cedula = '$fecha_cedula',
		institucion_expide_cedula = '$institucion_expide_cedula',
		idiomas_domina = '$idiomas_domina', 
		documento_obtenido = '$documento_obtenido',
		documento_obtenido_institucion = '$documento_obtenido_institucion',
		documento_obtenido_fecha = '$documento_obtenido_fecha',
		id_titulo = $id_titulo
	    	where rfc = '$rfc' ";
	
	$res_actualiza = ejecutar_sql($qry_actualiza);
	echo '<script>alert("Registro actualizado con exito");</script>';
	echo '<script>window.location.href = "estudios_personal_lista.php?todos=t";</script>';
}//Fin actualizar

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/popcalendar.js"></script>

		<SCRIPT LANGUAGE="JavaScript">
		<!--
		function Enviar(form) {
			for (i = 0; i < form.elements.length; i++) {
			if (form.elements[i].type == "text" && form.elements[i].value == "") {  
			alert("Por favor complete todos los campos del formulario"); form.elements[i].focus(); 
			return false; }
			}
			form.submit();
		}
		// -->
		</SCRIPT>

		<SCRIPT LANGUAGE="JavaScript">
		<!--
		function validar_campos() {
			var form = eval("document."+formulario.name);
			//var form = document.estudios;
			var band = 0;
			for (i = 0; i < form.elements.length; i++) {
				if (form.elements[i].id == "numerico") {  		
				var campo = form.elements[i];		
					if ((isNaN(campo.value))||(campo.value=="")){
						//alert('Introduzca un valor numerico'); 
						campo.value = ""; 
						campo.focus();
						campo.style.background="#FFB0B0";
						band = 1;
					}
					else{   campo.style.background="#FFFFFF";
						band = 0;
					}		
				}//if id numerico
			}//fin for
			if (band == 0){return true;} else{return false;}
		}//fin funcion
		-->
		</SCRIPT>
		<title>Estudios personal</title>
	</head>

<body>

<form action="estudios_personal.php" method="post" name="estudios" onSubmit="return validar_campos();">
<h2 align="center"><?php echo $rfc." ".$res_p->fields('apellidos_empleado')." ".$res_p->fields('nombre_empleado'); ?></h2>
<h3 align="center">Estudios</h3>

<input name="rfco" type="hidden" value="<?php echo $rfc; ?>">

<table width="100%" border="0" cellpadding="0" cellspacing="0" title="Secci&oacute;n para modificar los datos de estudio del trabajador">
  <tr align="center">
    <th>Nivel de Estudios</th>
    <th width="13%">Grado m&aacute;ximo (0-9)</th>
    <th>A&ntilde;o inicio estudios</th>
    <th>Ano termino estudios </th>
    <th>T&iacute;tulo </th>
  </tr>
  <tr align="center">
    
    <td><select name="nivel_estudios" title="Presione aqu&iacute; para seleccionar el nivel de estudios" onChange="javascript:document.estudios.submit();">
		 <?php 
		$qry_nivel_estudios = "select * from nivel_de_estudios";
		$res_nivel_estudios = ejecutar_sql($qry_nivel_estudios);
		echo "<option value='-1'> -- Seleccione -- </option>";
		while(!$res_nivel_estudios->EOF){
			 if ($ne==$res_nivel_estudios->fields('nivel_estudios')){$sel = "selected";} else {$sel = "";}
		?>  
       
<option value="<?php echo $res_nivel_estudios->fields('nivel_estudios'); ?>"  <?php echo $sel;?> > <?php echo $res_nivel_estudios->fields('descripcion_nivel_estudios'); ?></option>
		<?php
        $res_nivel_estudios->MoveNext();
		}	  
	   	?>
    </select>
    </td>
    
    <td><input type="text" value="<?php if ($grado_maximo_estudios != ''){echo $grado_maximo_estudios;}else{echo '0';} ?>" title="Capture el grado m&aacute;ximo de estudios. Debe ser un n&uacute;mero entre 0 y 9. (0 significa terminado)" name="grado_maximo_estudios" maxlength="1" size="1" id = "numerico" /></td>
    <td><select name="ano_inicio_estudios" title="Presione aqu&iacute; para seleccionar el a&ntilde;o de inicio de estudios">
      <?php for ($r=$anyo_actual-70;$r<=$anyo_actual;$r++){
		  if ($ano_inicio_estudios == $r){$seleccion = "selected";}else{$seleccion = "";}
		  ?>
      <option value="<?php echo $r;?>" <?php echo $seleccion;?> > <?php echo $r;?></option>
      <?php }?>
    </select></td>
    <td><select name="ano_termino_estudios" title="Presione aqu&iacute; para seleccionar el a&ntilde;o de termino de estudios">
      <?php for ($r=$anyo_actual-68;$r<=$anyo_actual;$r++){
			if ($ano_termino_estudios == $r){$seleccion = "selected";}else{$seleccion = "";}
			?>
      <option value="<?php echo $r;?>" <?php echo $seleccion;?> > <?php echo $r;?></option>
      <?php }?>
    </select></td>
	<td>
		<select name="id_titulo" title="Presione aqu&iacute; para seleccionar el t&iacute;tulo">
					<option value="0"> Seleccione Titulo </option>
					<?php
					$consulta_titulo="select id_titulo, titulo from titulos order by id_titulo";
					$datos_t=ejecutar_sql($consulta_titulo);
					
					while(!$datos_t->EOF){
						$selecciona=(($datos_t->fields('id_titulo')==$id_titulo)?" selected":"");
						echo '<option value="'.$datos_t->fields('id_titulo').'"'.$selecciona.'>'.$datos_t->fields('titulo').'</option>\n';
						$datos_t->MoveNext();
					}
					?>
		</select>
	</td>
  </tr>
</table>

<?php if  (   (($nivel_estudios=="I")||($nivel_estudios=="J")||($nivel_estudios=="K"))  || (($ne=="I")||($ne=="J")||($ne=="K"))   ) {?>

<br>

<h3 align="center">T&iacute;tulo profesional</h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0" title="Secci&oacute;n para modificar los datos de estudio profesional del trabajador">
  <tr align="center">
    <th>N&uacute;mero foja t&iacute;tulo</th>
    <th> Estudios realizados (carrera)</th>
  </tr>
  <tr align="center">
    <td><input name="titulo_num_foja" type="text" size="4" maxlength="3" value= "<?php echo $titulo_num_foja;?>"  id = "numerico" title="Capture el n&uacute;mero de foja del t&iacute;tulo"/></td>
    <td><input type="text" name="estudios" value="<?php echo $estudios; ?>" maxlength="100" size="100" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n del t&iacute;tulo profesional"/></td>
  </tr>
  <tr align="center">
    <th>N&uacute;mero a&ntilde;o t&iacute;tulo</th>
    <th>Instituci&oacute;n de egreso</th>
  </tr>
  <tr align="center">
    <td><input name="titulo_num_ano" type="text" size="4" maxlength="4" value= "<?php echo $titulo_num_ano;?>" id = "numerico" title="Capture el a&ntilde;o del t&iacute;tulo"/></td>
    <td><input name="institucion_egreso" type="text" size="50" maxlength="50" value= "<?php echo $institucion_egreso;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la instituci&oacute;n que egreso" /></td>
  </tr>
  <tr align="center">
    <th>N&uacute;mero libro t&iacute;tulo </th>
    <th>Instituci&oacute;n donde se titul&oacute;</th>
  </tr>
  <tr align="center">
    <td><input name="titulo_num_libro" type="text" size="4" maxlength="3" value= "<?php echo $titulo_num_libro;?>" title="Capture el n&uacute;mero del libro del t&iacute;tulo"/></td>
    <td><input name="instituto_titulacion" type="text" value= "<?php echo $instituto_titulacion;?>" size="50" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la instituci&oacute;n donde se titul&oacute;"/></td>
  </tr>
  <tr align="center">
    <th>N&uacute;mero titulo</th>
    <th>Instituci&oacute;n que expide t&iacute;tulo </th>
  </tr>
  <tr align="center">
    <td><input name="titulo_num" type="text" size="10" maxlength="10" value= "<?php echo $titulo_num;?>" title="Capture el n&uacute;mero del t&iacute;tulo"/></td>
    <td><input name="institucion_expide_titulo" type="text"  size="100" maxlength="60" value= "<?php echo $institucion_expide_titulo;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la instituci&oacute;n donde realiz&oacute; su titulaci&oacute;n"/></td>
  </tr>
  <tr align="center">
    <th>Fecha titulo</th>
    <th>Especializaci&oacute;n</th>
  </tr>
  <tr align="center">
    <td><input name="fecha_titulacion" title="Seleccione la fecha de expedici&oacute;n del t&iacute;tulo" onFocus="document.personal.boton_titulo.click()" id = "excepcion" type="text" value="<?php echo strftime('%Y/%m/%d', strtotime($fecha_titulacion));?>" size="15" maxlength="10" readonly="true" title="Seleccione la fecha del t&iacute;tulo"/>
      <input name="boton_titulo" class="calendario" type="button" value="..." size="3" maxlength="3" onClick="return popUpCalendar(boton_titulo, fecha_titulacion, 'yyyy/mm/dd');" title="Presione aqu&iacute; para elegir la fecha del t&iacute;tulo"/></td>
    <td><input name="especializacion" type="text" value= "<?php echo $especializacion;?>" size="100" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la especializaci&oacute;n del trabajador"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<br>

<h3 align="center">C&eacute;dula profesional</h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0" title="Secci&oacute;n para modificar los datos de c&eacute;dula profesional del trabajador">
  <tr align="center">
    <th>C&eacute;dula</th>
    <th>Fecha de expedici&oacute;n de c&eacute;dula</th>
    <th>Instituci&oacute;n que expide c&eacute;dula profesional </th>
  </tr>
  <tr align="center">
    <td><input name="cedula_profesional" type="text" maxlength="15"  value= "<?php echo $cedula_profesional;?>" title="Capture la descripci&oacute;n de la c&eacute;dula profesional"/></td>
    <td><input name="fecha_cedula" title="Seleccione la fecha de expedici&oacute;n de c&eacute;dula" onFocus="document.personal.boton_cedula.click()" type="text" value="<?php echo strftime('%Y/%m/%d', strtotime($fecha_cedula));?>" size="15" maxlength="10" readonly="true" />
      <input name="boton_cedula" class="calendario" type="button" value="..." size="3" maxlength="3" onClick="return popUpCalendar(boton_cedula, fecha_cedula, 'yyyy/mm/dd');" title="Presione aqu&iacute; para seleccionar la fecha expedici&oacute;n de la c&eacute;dula"/></td>
    <td><input name="institucion_expide_cedula" type="text"  size="100" maxlength="60" value= "<?php echo $institucion_expide_cedula;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la instituci&oacute;n que expide la c&eacute;dula"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<br>

<h3 align="center">Otros datos</h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0" title="Secci&oacute;n para modificar otros datos del trabajador">
  <tr align="center">
    <th>Idiomas que domina</th>
  </tr>
  <tr align="center">
    <td><input name="idiomas_domina" type="text"  size="100" maxlength="60" value= "<?php echo $idiomas_domina;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de los idiomas que domina"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>


<?php 
	}
  else{
	if (   (($nivel_estudios!="-1") && ($nivel_estudios!="")) || ($ne!="")  ){	
?>
<br>
<h3 align="center">Datos</h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0" title="Secci&oacute;n para modificar los datos de estudio general del trabajador">
  <tr align="center">
    <th>Documento obtenido </th>
    <th>Instituci&oacute;n que lo expide</th>
    <th>Fecha de expedici&oacute;n</th>
  </tr>
  <tr align="center">
    <td><input name="documento_obtenido" type="text"  size="60" maxlength="40" value= "<?php echo $documento_obtenido;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n del documento obtenido por la instituci&oacute;n"/></td>
    <td><input name="documento_obtenido_institucion" type="text"  size="60" maxlength="30" value= "<?php echo $documento_obtenido_institucion;?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la descripci&oacute;n de la instituci&oacute;n que expide el documento"/></td>
    <td>
      <input name="documento_obtenido_fecha" title="Seleccione la fecha de expedici&oacute;n de documento" onFocus="document.personal.boton_documento_obtenido_fecha.click()" type="text" value="<?php echo strftime('%Y/%m/%d', strtotime($documento_obtenido_fecha));?>" size="15" maxlength="10" readonly="true" />
      <input name="boton_documento_obtenido_fecha" class="calendario" type="button" value="..." size="3" maxlength="3" onClick="return popUpCalendar(boton_documento_obtenido_fecha, documento_obtenido_fecha, 'yyyy/mm/dd');" title="Presione aqu&iacute; para seleccionar la fecha expedici&oacute;n del documento"/>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<?php }}?>
<br>
<br>
<div align="center">
		<?php if (   (($nivel_estudios!="-1") && ($nivel_estudios!="")) || ($ne!="")  ){?>
		<input type="submit" name="actualiza" class="boton" value="Actualizar" title="Presione este bot&oacute;n para guardar los cambios">
        
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php }?>
		<input type="button" name="regresa" class="boton" value="Regresar" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n">
	</div></body>
</html>
