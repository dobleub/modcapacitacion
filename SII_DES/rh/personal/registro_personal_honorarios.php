<?php
	/********************************************************
		Registro Personal

		Modificado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 54);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<title>.:: Recursos Humanos &raquo; Honorarios ::.</title>
</head>
<body>
<h3 align="center"> Presonal con Honorarios </h3>
<?php
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

	/*$qry_jefes = "exec pac_honorarios '$rfc'";
	$res_jefes = ejecutar_sql($qry_jefes);
	$clave_area = $res_jefes->fields('horas_honorario');
	$rfc_jefe = $res_jefes->fields('rfc');*/

//echo $jefe_area;
?>
<form name="registro_personal_honorarios" action="registro_personal_honorarios.php" method="post">
	<table align="center" width="300" title="Secci&oacute;n para actualizar/eliminar honorarios a un trabajador">
		<tr align="center">
			<th width="200"> Personal: </th>
			<th> Horas: </th>
		</tr>
		<tr align="center">
			<td id="non"> 
				<select name="rfc_personal_honorarios" title="Presione aqu&iacute; para asignar al personal con honorarios">
					<option value="0"> Seleccione Personal </option>
					<?php

					$consulta_rfc="select P.rfc, P.apellidos_empleado, P.nombre_empleado from personal P, estudios_personal EP where tipo_personal = 'H' and status_empleado = '02' and P.rfc = EP.rfc order by P.apellidos_empleado, P.nombre_empleado, P.rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);
					
					while(!$datos_rfc->EOF){
						$selecciona=(($datos_rfc->fields('rfc')==$rfc_jefe)?" selected":"");
						echo '<option value="'.$datos_rfc->fields('rfc').'"'.$selecciona.'>'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>\n';
						$datos_rfc->MoveNext();
					}
					?>
				</select> 	
			</td>
			<td id="non"> 
				<select name="horas_honorarios" title="Primero debe elegir el total de horas para este trabajador">
				<?php for ($r=1;$r<=40;$r++){
					if ($horas_honorarios == $r){$seleccion = "selected";}else{$seleccion = "";}
				?>
			      		<option value="<?php echo $r;?>" <?php echo $seleccion;?> > <?php echo $r;?></option>
			     	<?php }?>
				</select>
			</td>
		</tr>
	</table>
	<br />
	<div align="center">
		<input type="button" class="boton" value="Actualizar" onclick="javascript: document.registro_personal_honorarios.action = 'registro_personal_honorarios_bd.php'; document.registro_personal_honorarios.submit()" title="Presione este bot&oacute;n para guardar los cambios">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" class="boton" value="Regresar" onclick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n">
	</div>
<!-- MODIFICACION 101111-->
<br><br>
  <?php
	$consulta_registros="select nombre_empleado, apellidos_empleado, rfc, horas_honorarios from personal where tipo_personal = 'H' and horas_honorarios != 0 and status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0; 

	if(!$registros->EOF)
		echo "<table align='center' title='Tabla que muestra la relaci&oacute;n de personal por honorarios'>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		  <tr align='center'>
		    <th> No. </th>
		    <th> RFC </th>
		    <th> Nombre completo </th>
		    <th> Horas </th>
		    <th> Eliminar </th>
		  </tr>";
	else	echo ' <table align="center" title="Esta tabla muestra el personal por honorarios">
		  	<tr align="center">
			<td> No exite personal registrado </th>
		  	</tr>';

	while(!$registros->EOF){
		$contador = $contador + 1; 
		$rfc = $registros->fields('rfc');
		$horas_honorarios = $registros->fields('horas_honorarios');
		echo "<tr id='$id'>
		 <td> $contador </td>
		 <td> $rfc </td> 
		 <td>".$registros->fields('apellidos_empleado')." ".$registros->fields('nombre_empleado')."</td>
		 <td align='center'> $horas_honorarios </td>
		 <td align='center'>";?>
  <a href="borrar_puesto_honorarios.php?rfc=<?php echo $rfc; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este dato ?');"> <img border="0" src="../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar este registro"/></a><?php echo "</td>
		</tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
</table>
<!-- FIN MODIFICACION -->
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Personal_honorarios.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Honorarios"/></a>
	</div>
	<? } ?>
</body>
</html>
