<?php
	/********************************************************
		Registro de Claves/plazas

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);

	$Crear = $_POST['Crear'];
	$clave_antes_punto = $_POST['clave_antes_punto'];
	$clave_despues_punto = $_POST['clave_despues_punto'];
	$horas_clave_plaza = $_POST['horas_clave_plaza'];
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

	$qry_maximo_clave="select max(id_clave) as maximo from claves_presupuestales";
	$res_maximo_clave=ejecutar_sql($qry_maximo_clave);
	$maximo=$res_maximo_clave->fields('maximo'); 	

	if($Crear=="Crear"){ 
		if(($clave_antes_punto=="")or($clave_despues_punto=="")){
			echo "<script>alert('Escriba la clave antes y despues del punto y las horas que desea asignar');</script>";
		}
		else{  
		$consulta_select_sql= "select count(id_clave) as Total from claves_presupuestales where clave_antes_punto='$clave_antes_punto' and clave_despues_punto='$clave_despues_punto' and horas_clave_plaza=$horas_clave_plaza";
		$sql_consulta = ejecutar_sql($consulta_select_sql) ;
		$cantidad= $sql_consulta->fields('Total'); 

		if ($cantidad == 0){
			$qry_inserta_puesto="insert into claves_presupuestales (id_clave, clave_antes_punto, clave_despues_punto, horas_clave_plaza) values (".($maximo+1).",'$clave_antes_punto','$clave_despues_punto', $horas_clave_plaza)"; 
			$res_inserta_puesto=ejecutar_sql($qry_inserta_puesto);
			echo "<script>alert('La nueva clave presupuestal se agrego correctamente');</script>";
		}
		else {
			$msg = "No Puede duplicar una Clave Presupuestal";
			$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
		?>
	<script language="javascript" type="text/javascript">
		msg = '<?php echo $msg; ?>'
		alert(msg)
		<?php echo $regresar; ?>
	</script>

	<?php
		}
		}
	}
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>

		<title>.:: Registro de Claves/plazas ::.</title>
	</head>
<body>
	<h2 align="center"> Registro de Claves/plazas</h2>

<form name="personal" action="agrega_clave_plaza.php" method="post">

<input name="id_clave" type="hidden" id="id_clave" value="<?php echo ($maximo+1); ?>" />

	<table align="center" title="Secci&oacute;n para capturar claves presupuestales">
		<tr> 
			<th width="150"> Clave Presupuestal: </th>
			<td id="non">
				<input name="clave_antes_punto" type="text" size="5" maxlength="13" tabindex="1" onkeyup="checkLen(this, this.value)" onblur="this.value = this.value.toUpperCase()" title="Ingrese los primeros (as) d&iacute;gitos/letras hasta antes del punto">
				.
				<input name="clave_despues_punto" type="text" size="4" maxlength="10" tabindex="2" onkeyup="checkLen(this, this.value)" onblur="this.value = this.value.toUpperCase()" title="Ingrese los d&iacute;gitos/letras que corresponden despu&eacute;s del punto">
				<select name="horas_clave_plaza" title="Seleccione el total de horas para esta clave presupuestal">
					<?php for ($r=1;$r<=40;$r++){
					if ($horas_clave_plaza == $r){$seleccion = "selected";}else{$seleccion = "";}
					?>
      				<option value="<?php echo $r;?>" <?php echo $seleccion;?> > <?php echo $r;?></option>
     					<?php }?>
				</select>
			</td>
		</tr>
	</table>
<br><br>
  <div align="center">
    <input type="submit" name="Crear" class="boton" value="Crear" title="Presione este bot&oacute;n para guardar los cambios" />
    <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Claves Presupuestales existentes en el instituto">
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		<?php
		$consulta_registros="select * from claves_presupuestales order by id_clave"; // order by descripcion_puesto
		$registros=ejecutar_sql($consulta_registros);
		$id = "non";
		$contador = 0;
	if(!$registros->EOF)
		echo "<tr align='center'>
			    		<th> No. </th>
			    		<th align='center'> Clave Presupuestal </th>
					<th> Hora (s) </th>
					<th> Eliminar </th>
		      </tr>";
	else	echo '<tr align="center">
			 <td> No exite alguna clave presupestal registrada </th>
		      </tr>';

		while(!$registros->EOF){
			++$contador;
			$id_clave = $registros->fields('id_clave');
			$cap = $registros->fields('clave_antes_punto');
			$cdp = $registros->fields('clave_despues_punto');
			$hcp = $registros->fields('horas_clave_plaza');
			echo "<tr id='$id'>
			 <td align='center'> $id_clave </td>
			 <td> $cap . $cdp </td>
			 <td align='center'> $hcp </td>
			 <td align='center'>";?>
	  <a href="borrar_clave_presupuestal.php?id_clave=<?php echo $id_clave; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar esta clave?');"> <img border="0" src="../../../img/eliminar.gif" title="Presione aqu&iacute; para eliminar este registro" /></a><?php echo"</td></tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}
		?>
	</table>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Claves_presupuestales.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Claves Presupuestales"/></a>
	</div>
	<? } ?>
</body>
</html>
