<?php
	/********************************************************
		Edad & Género 

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH'); 
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Edad & Género ::.</title>
	</head>
<body>
	<h2 align="center">Listado de Personal Genero - Edad (Licencia/Activos)</h2>
  <?php	// Condición: Activos e Inactivos
	$consulta_registros="select rfc, nombre_empleado, apellidos_empleado, sexo_empleado, fecha_nacimiento, nombramiento, status_empleado from personal where (status_empleado = '01' or status_empleado = '02') order by nombramiento, sexo_empleado, fecha_nacimiento, apellidos_empleado, nombre_empleado, rfc";

	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;
	$contador_sex = 0;
	$nom_inicial = $registros->fields('nombramiento');

	if(!$registros->EOF)
		echo '<table align="center" width="50%" title="Tabla que muestra la relaci&oacute;n del Personal: Edad & G&eacute;nero">
 			<tr><td colspan = "5" align="center"><b>'.(($nom_inicial=='D')?'Docente':(($nom_inicial == 'A')?'Administrativo':(($nom_inicial == 'Z')?'Sin Tipo':'Sin Registro'))).'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center" width="100%">
			<th width="5%"> No. </th>
			<th> Nombre completo </th>			
			<th width="10%"> Edad </th>
			<th width="14%"> Sexo </th>	
		  </tr>';
	else	echo ' <table align="center" title="No hay personal registrado">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		++$contador;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado'); 
		$sexo = $registros->fields('sexo_empleado');
		$nom = $registros->fields('nombramiento');
		
		$rfc_sin=substr($rfc,-9);  // ejemplo cusm890901 asc
		$fecha=substr($rfc_sin,0,6);
		
		if (substr($rfc_sin,0,1) > 1)	$anio=intval(substr($fecha,0,2))+1900;
		else	$anio=intval(substr($fecha,0,2))+2000;

		$edad=date("Y",time())-$anio;

				

			if ($nom_inicial != $nom){
				echo '<tr><th colspan = "3">Total</th><th>'.(--$contador).'</th></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr>
					 <td colspan = "5" align="center"><b>'.(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro'))).'</b></td>
					 </tr>
					 </table>
					 <table align="center" width="50%" title="Tabla que muestra la relaci&oacute;n del Personal: Edad & G&eacute;nero"><tr><td>&nbsp;</td></tr>
					  	<tr align="center" width="100%">
						<th width="5%"> No. </th>
						<th> Nombre completo </th>			
						<th width="10%"> Edad </th>
						<th width="14%"> Sexo </th>	
					  </tr>';
					$total = $total + (--$contador); // 
					$nom_inicial = $nom;
					$contador = 1;
					$contador_sex=0;
				}

		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
			 <td align='center'> $contador </td>
			 <td> $ae $ne </td>		
			 <td align='center'> $edad </td>
			 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td>
			 </tr>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>
			 <td align='center'> $contador </td>
			 <td> $ae $ne </td>		
			 <td align='center'> $edad </td>
			 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td>
			 </tr>";
		}
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
		}
		echo "<tr><th colspan = '3'>Total</th><th> $contador </th></tr>";
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=3 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
	</div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Edad_genero.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Edad - G&eacute;nero" /></a>
	</div>
	<? } ?>
</body>
</html>
