<?php
	/********************************************************
		Plantilla de Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Plantilla de Personal ::.</title>
	</head>
<body>
<?php
	$rfc_personal=$_POST['rfc_personal'];
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
	<h2 align="center"> Plantilla de personal Actualizado </h2>

    <!-- INICIO MODIFICACION -->
<form name="solicitud_ver" method="post" action=""> <!--
	<h3 align="center"> Opciones de Visualizaci&oacute;n: </h3>
	<table width="30%" align="center">
		<tr align="center">
			<th>Ver: </th>
		</tr>
		<tr align="center">
<th>
<select name="rfc_personal" title="Seleccione el nivel de estudios" onChange="javascript:document.solicitud_ver.submit();">
					<?php
					$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);

					if(!$datos_rfc->rowcount()){
						echo "<option value='0' selected> No hay personal dado de alta </option>";
					}
					else
					{
						echo "<option value='0' selected> -- Seleccione Personal -- </option>";

						while(!$datos_rfc->EOF){
							echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
							$datos_rfc->MoveNext();
						}
					}
					?>
</select>
</th>


		</tr>
</table> -->
<br>
<!--  MODIFICAR 081111
<div align='center'>
<input name="Buscar" type="submit" class="boton" id="Guardar" value="Buscar"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div> -->

    <!-- FIN MODIFICACION -->

	<table align="center" width="95%" title="Tabla que muestra la lista de trabajadores con sus respectivas claves presupuestales">
		<tr><td>&nbsp;</td></tr>
  <?php

$consulta_registros="select distinct(PE.rfc), PE.no_tarjeta, PE.curp_empleado, PE.apellidos_empleado, PE.nombre_empleado, PE.ingreso_rama, PE.inicio_gobierno, PE.inicio_sep, PE.inicio_plantel from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and id_movimiento=id_mov and CPP.estatus = 1 and ( PE.status_empleado = '01' or PE.status_empleado = '02' ) order by PE.apellidos_empleado, PE.nombre_empleado, PE.rfc";

	$registros=ejecutar_sql($consulta_registros);

	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<tr align="center">
			<th> No. Tarjeta</th>
			<th> Nombre completo </th>
			<th> Filiacion </th>
			<th> Curp </th>
			<th> Gob. </th>
			<th> SEP </th>
			<th> Rama </th>
			<th> I.T.T. </th>
			<th> Clave </th>
			<th> Categoria </th> 
		  </tr>';
	else	echo '<tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	while(!$registros->EOF){

		$contador = $contador + 1;
		$no_tarjeta= $registros->fields('no_tarjeta');
		$rfc= $registros->fields('rfc');
		$curp_empleado= $registros->fields('curp_empleado');
		$nombre_completo = $registros->fields('apellidos_empleado').' '.$registros->fields('nombre_empleado');;
		$ingreso_rama= $registros->fields('ingreso_rama');
		$inicio_gobierno = $registros->fields('inicio_gobierno');
		$inicio_sep = $registros->fields('inicio_sep');
		$inicio_plantel= $registros->fields('inicio_plantel');

		echo "<tr id='$id'>
		 <td align='center'> $no_tarjeta </td>
		 <td> $nombre_completo </td>
		 <td> $rfc </td>
		 <td> $curp_empleado </td>
		 <td> $inicio_gobierno </td>
		 <td> $inicio_sep </td>
		 <td> $ingreso_rama </td>
		 <td> $inicio_plantel </td>
		 <td width='20%'>";
			$consulta_clave="select CPP.id_clave from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and PE.rfc ='$rfc' and id_movimiento = id_mov and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){
			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}		
		echo "</td>
		 <td width='20%'>";
			$consulta_categoria="select CPP.categoria from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and PE.rfc ='$rfc' and id_movimiento = id_mov and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			$registros_categoria=ejecutar_sql($consulta_categoria);
			while(!$registros_categoria->EOF){
				$categoria= $registros_categoria->fields('categoria');
		  		/**/$qry_categoria="select distinct categoria,descripcion_categoria from categorias where categoria ='$categoria'";
		  		$res_categoria=ejecutar_sql($qry_categoria);
		  		$d_categoria=$res_categoria->fields('descripcion_categoria');
				echo $d_categoria." <br>"; 
				$registros_categoria->MoveNext();
			}
		//.$d_categoria.
		echo "</td>
		</tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
</table>

</form>

<form name="personal" method="post" >
  <br/><br/>
	<div align="center">
<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	<? if ($contador > 0) { ?>
	<br><br>
  <a href="../reportes/Plantilla_de_personal_actualizado.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Plazas Actuales"/></a>
	<? } ?>
	</div>
</form>
</body>
</html>
