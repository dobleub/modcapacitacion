<?php
	/********************************************************
		manto_personal_bd.php

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<title>.:: Recursos Humanos ::.</title>
</head>

<body>
<?php
	$accion = $_POST['accion'];
        $rfc = $_POST['rfc'];
        $enviar = $_POST['enviar']; 
        $apellidos_empleado = $_POST['apellidos_empleado']; 
        $nombre_empleado = $_POST['nombre_empleado'];
        $curp_empleado = $_POST['curp_empleado'];  
	$sexo_empleado = $_POST['sexo_empleado'];
        $estado_civil = $_POST['estado_civil'];
	$num_cartilla_smn = $_POST['num_cartilla_smn'];
        $correo_electronico = $_POST['correo_electronico'];
	$lugar_nacimiento = $_POST['lugar_nacimiento'];
        $fecha_nacimiento = $_POST['fecha_nacimiento']; 
        $acta_nacimiento_numero = $_POST['acta_nacimiento_numero'];
        $acta_nacimiento_foja = $_POST['acta_nacimiento_foja'];
        $acta_nacimiento_libro = $_POST['acta_nacimiento_libro'];
        $acta_nacimiento_ano = $_POST['acta_nacimiento_ano'];
        $codigo_postal_empleado = $_POST['codigo_postal_empleado'];
        $domicilio_empleado = $_POST['domicilio_empleado']; 		
        $colonia_empleado = $_POST['colonia_empleado']; 
        $localidad = $_POST['localidad'];
   	//$entidad_federativa = $_POST['entidad_federativa'];
	$telefono_empleado = $_POST['telefono_empleado'];
    	$pais = $_POST['pais'];
    	$pasaporte = $_POST['pasaporte'];
    	$fm = $_POST['fm'];
    	$inicio_vigencia = $_POST['pasaporte_vigencia_inicio'];
    	$termino_vigencia = $_POST['pasaporte_vigencia_fin'];
    	$padre = $_POST['padre'];
    	$madre = $_POST['madre'];
    	$conyuge = $_POST['conyuge'];
    	$hijos = $_POST['hijos'];
    	$tipo_personal=$_POST['tipo_personal'];
	$nombramiento = $_POST['nombramiento'];
    	$clave_area = $_POST['clave_area'];
    	$area_academica = $_POST['area_academica'];
    	$no_tarjeta = $_POST['no_tarjeta'];
    	$status_empleado = $_POST['status_empleado']; 
	$ingreso_rama = $_POST['ingreso_ramaA'].$_POST['ingreso_ramaQ'];
	$inicio_gobierno = $_POST['inicio_gobiernoA'].$_POST['inicio_gobiernoQ'];
	$inicio_sep = $_POST['inicio_sepA'].$_POST['inicio_sepQ'];
	$inicio_plantel = $_POST['inicio_plantelA'].$_POST['inicio_plantelQ'];
	$inactivo_rc=$_POST['inactivo_rc'];
    	$clave_centro_seit = $_POST['clave_centro_seit'];
	$nacionalidad = $_POST['nacionalidad'];
	$municipio_empleado = $_POST['municipio_empleado'];
	$pasaporte_puesto_autorizado = $_POST['pasaporte_puesto_autorizado'];
	$emergencia = $_POST['emergencia']; 
	$otros_dependientes = $_POST['otros_dependientes'];
if($tipo_personal == "V"){
	$ingreso_rama="190001";
	$inicio_gobierno="190001";
	$inicio_sep="190001";
	$inicio_plantel="190001";
}

if(!$codigo_postal_empleado){$codigo_postal_empleado = 0;}
if ($domicilio_empleado_numero == ""){$domicilio_empleado_numero = 0; }
if ($acta_nacimiento_numero == ""){$acta_nacimiento_numero = 0; }
if ($acta_nacimiento_libro == ""){$acta_nacimiento_libro = 0; } //031111
if ($acta_nacimiento_foja == ""){$acta_nacimiento_foja = 0; }
if ($acta_nacimiento_ano == ""){$acta_nacimiento_ano = 0; }
if ($entidad_federativa == ""){$entidad_federativa = 0; }
if ($pasaporte == ""){$pasaporte = 0; }
if ($hijos == ""){$hijos = 0; }
if ($otros_dependientes == ""){$otros_dependientes = 0; }

if ($accion == "nuevo"){
$qry_manto_personal = "insert into personal 
	(rfc, clave_centro_seit, clave_area, curp_empleado, no_tarjeta, apellidos_empleado, nombre_empleado, 
	nombramiento, ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel, domicilio_empleado, 
	colonia_empleado, codigo_postal_empleado, localidad, telefono_empleado, sexo_empleado, estado_civil, 
	fecha_nacimiento, lugar_nacimiento, status_empleado, correo_electronico, padre, madre, 
	conyuge, hijos, num_acta, num_libro, num_foja, num_ano, 
	num_cartilla_smn, pais, pasaporte , fm, inicio_vigencia, termino_vigencia, area_academica, 
	tipo_personal, nacionalidad, municipio_empleado, pasaporte_puesto_autorizado, emergencia, 
	otros_dependientes, bandera_foto, bandera_firma)
	values
	('$rfc', '$clave_centro_seit', '$clave_area', '$curp_empleado', $no_tarjeta, '$apellidos_empleado', '$nombre_empleado', '$nombramiento', '$ingreso_rama', '$inicio_gobierno', '$inicio_sep', '$inicio_plantel', '$domicilio_empleado', '$colonia_empleado', $codigo_postal_empleado, '$localidad', '$telefono_empleado','$sexo_empleado', '$estado_civil', '$fecha_nacimiento', $lugar_nacimiento, '$status_empleado', '$correo_electronico', '$padre', '$madre', '$conyuge' ,'$hijos', $acta_nacimiento_numero, $acta_nacimiento_libro, $acta_nacimiento_foja, $acta_nacimiento_ano, '$num_cartilla_smn', '$pais', '$pasaporte', '$fm', '$inicio_vigencia', '$termino_vigencia', '$area_academica', '$tipo_personal', '$nacionalidad', $municipio_empleado, '$pasaporte_puesto_autorizado', '$emergencia', $otros_dependientes,0,0)";
// , entidad_federativa		, $entidad_federativa
	$consulta_select_sql= "select count(rfc) as Total from estudios_personal where rfc='$rfc'";
        $sql_consulta = ejecutar_sql($consulta_select_sql) ;
        $cantidad = $sql_consulta->fields('Total');

        if ($cantidad == 0){
	$qry_estudios_personal = "insert into estudios_personal (rfc, apellidos_empleado, nombre_empleado) values ('$rfc', '$apellidos_empleado', '$nombre_empleado')";
	$creacion_de_personal_estudios = ejecutar_sql($qry_estudios_personal);
	}
}
else{

	$qry_manto_personal = "update personal set 
	clave_centro_seit = '$clave_centro_seit',
	clave_area = '$clave_area',
	curp_empleado = '$curp_empleado', 
	no_tarjeta = $no_tarjeta, 
	apellidos_empleado = '$apellidos_empleado',
	nombre_empleado = '$nombre_empleado',
	nombramiento = '$nombramiento', 
	ingreso_rama = '$ingreso_rama', 
	inicio_gobierno = '$inicio_gobierno', 
	inicio_sep = '$inicio_sep', 
	inicio_plantel = '$inicio_plantel', 
	domicilio_empleado = '$domicilio_empleado',
	colonia_empleado = '$colonia_empleado', 
	codigo_postal_empleado = $codigo_postal_empleado,
	localidad = '$localidad',
	telefono_empleado = '$telefono_empleado',  
	sexo_empleado = '$sexo_empleado', 
	estado_civil = '$estado_civil', 
	fecha_nacimiento = '$fecha_nacimiento',
	lugar_nacimiento = $lugar_nacimiento,
	status_empleado = '$status_empleado', 
	correo_electronico = '$correo_electronico',
	padre = '$padre', 
	madre = '$madre', 
	conyuge = '$conyuge' , 
	hijos = '$hijos' ,
	num_acta = $acta_nacimiento_numero ,  
	num_libro = $acta_nacimiento_libro ,
	num_foja = $acta_nacimiento_foja , 
	num_ano = $acta_nacimiento_ano ,
	num_cartilla_smn = '$num_cartilla_smn' ,
	pais = '$pais', 
	pasaporte = '$pasaporte' ,
	fm = '$fm', 
	inicio_vigencia = '$inicio_vigencia' , 
	termino_vigencia = '$termino_vigencia' ,
	area_academica = '$area_academica',
	tipo_personal = '$tipo_personal',
	nacionalidad = '$nacionalidad', 
	municipio_empleado = $municipio_empleado,
	pasaporte_puesto_autorizado = '$pasaporte_puesto_autorizado',
	emergencia = '$emergencia', 
	otros_dependientes = $otros_dependientes
	where rfc = '$rfc'"; // entidad_federativa = $entidad_federativa
}

$res_manto_personal = ejecutar_sql($qry_manto_personal);

	$update_estudios_personal = "update estudios_personal set apellidos_empleado='$apellidos_empleado',  nombre_empleado='$nombre_empleado' where rfc='$rfc'";
	$de_personal_estudios = ejecutar_sql($update_estudios_personal);

/*
	$tamano_firma = $_FILES["firma"]['size'];
        $tipo_firma = $_FILES["firma"]['type'];
        $archivo_firma = $rfc.".jpg";
	if (!(strpos($tipo_firma, "jpg") && ($tamano_firma > 10 and $tamano_firma < 640000)))
		{       $destino_firma = $CFG->imgDir."/firmas/personal/".$archivo_firma;    // ../../../img
			copy($_FILES['imagen']['tmp_name'],$destino_firma);
		}*/

$msg = "Registro guardado con exito";

$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

?>
<script language="javascript" type="text/javascript">
	msg = '<?php echo $msg; ?>'
	alert(msg)
	<?php echo $regresar; ?>
</script>
</body>
</html>
