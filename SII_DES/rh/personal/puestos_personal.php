<?php
	/********************************************************
		Asignar puesto

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__     permiso_sobre_funcion($_SESSION['susr'], 64);

	$Asignar = $_POST['Asignar'];
	$rfc_personal = $_POST['rfc_personal'];
	$puesto = $_POST['puesto'];

	if($Asignar=="Asignar")
		if(($rfc_personal=="0")or($puesto=="0"))
			echo "<script>alert('Por favor seleccione al personal y el puesto que desea asignarle');</script>";
		else{   $qry_busca="select rfc from puestos_personal where rfc = '$rfc_personal'";
			$res_busca=ejecutar_sql($qry_busca);

			if($res_busca->EOF){
				$fecha=date("Y/m/d");
				$inserta="insert into puestos_personal (rfc, clave_puesto, fecha_ingreso_puesto) values ('$rfc_personal',$puesto, '$fecha') ";
				$res_inserta=ejecutar_sql($inserta);
				echo '<script> alert("Puesto agregado con exito"); </script>';			
			     }
			else{   $actualiza="update puestos_personal set clave_puesto = $puesto where rfc = '$rfc_personal'";
				$res_actualiza=ejecutar_sql($actualiza);
				echo '<script> alert("Puesto actualizado con exito"); </script>';		
			     }//fin else
		     }//fin else
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<title>.:: Asignar puesto ::.</title>
</head>

<body>
<?php	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";	?>
<h2 align="center"> Asignaci&oacute;n de Puesto al Personal (Activos)</h2>

<form name="personal" action="puestos_personal.php" method="post">

	<table align="center" title="Secci&oacute;n para agregar puestos al personal">
		<tr>
		<th>Personal</th>
		<th>Puesto</th>
		</tr>
		<tr>
		<td>
			<select name="rfc_personal" title="Presione aqu&iacute; para seleccionar el empleado">
		<?php
		$consulta_rfc="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";

		$datos_rfc=ejecutar_sql($consulta_rfc);

		if(!$datos_rfc->rowcount())	echo "<option value='0' selected> No hay personal dado de alta </option>";
		else    { echo "<option value='0' selected> -- Seleccione Personal -- </option>";
			  while(!$datos_rfc->EOF){
			  echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
			  $datos_rfc->MoveNext();
			  }
			}
		?>
			</select>
		</td>
		<td>
			<select name="puesto" title="Presione aqu&iacute; para seleccionar el puesto que desea asignar">
	      		<?php 	
			$qry_puestos = "select * from puestos order by descripcion_puesto";
			$res_puestos = ejecutar_sql($qry_puestos);
			if(!$res_puestos->rowcount())	echo "<option value='0' selected> No hay puestos dados de alta </option>";
			else {
				echo "<option value='0' selected> -- Seleccione Puesto -- </option>";
				while(!$res_puestos->EOF)
					{
					$value = trim($res_puestos->fields("clave_puesto"));
					$nombre = $res_puestos->fields('descripcion_puesto');
					echo "<option value='".$value."'>".$nombre."</option>";
					$res_puestos->MoveNext();
					}
			     }						
			?>
			</select>
		</td>
		</tr>
		</table>
<br><br>
	<div align="center">
		<input type="submit" name="Asignar" class="boton" value="Asignar" title="Presione este bot&oacute;n para guardar los cambios"/>
		<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
<br><br>
	<table align="center" title="Tabla que muestra la lista de cada trabajador con la descripci&oacute;n de su puesto">
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td> 
	  </tr>
	  <?php
		$qry_docente = "select clave_puesto from puestos";  
		$res_docente = ejecutar_sql($qry_docente);
		$clave_docente = $res_docente->fields('clave_puesto');

		$consulta_registros="select PE.rfc, apellidos_empleado, nombre_empleado, P.descripcion_puesto from personal PE, puestos P, puestos_personal PP where PP.rfc = PE.rfc and PP.clave_puesto = P.clave_puesto and status_empleado = '02' order by apellidos_empleado, nombre_empleado, PP.rfc";   
		$registros=ejecutar_sql($consulta_registros);

		$id = "non";
		$contador = 0;

	if(!$registros->EOF)
		echo '<tr align="center">
		    <th> No. </th>
		    <th> RFC </th>
		    <th> Nombre completo </th>
		    <th> Descripcion puesto </th>
		    <th> Eliminar </th>
		  </tr>';
	else	echo '<tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

		while(!$registros->EOF){
			$contador = $contador + 1;
			$nt = $registros->fields('no_tarjeta');
			$rfc = $registros->fields('rfc');
			$ae = $registros->fields('apellidos_empleado');
			$ne = $registros->fields('nombre_empleado');
			$des = $registros->fields('descripcion_puesto');

			echo "<tr id='$id'>
			 <td align='center'> $contador </td>
			 <td align='center'> $rfc </td>
			 <td> $ae $ne </td>
			 <td> $des </td>
			 <td align='center'>";?>
	  <a href="borrar_puesto.php?rfc=<?php echo $rfc; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este registro ?');"> <img border="0" src="../../../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar este registro" /></a><?php echo"</td>
			 </tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		?>
	</table>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Puestos_de_personal.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Puestos del Personal" /></a>
	</div>
	<? } ?>
</body>
</html>
