<?php
	/********************************************************
		Grado de estudios

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH'); 
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Listado de Personal por Grado de Estudios< ::.</title>
	</head>
<body>
	<h2 align="center">Listado de Personal Grado de Estudios (Licencia/Activos)</h2>

  <?php // Condición: que esten Activos o Inactivos
	$consulta_registros="select rfc, nombre_empleado, apellidos_empleado, sexo_empleado, nombramiento, nivel_estudios, status_empleado from personal where (status_empleado = '01' or status_empleado = '02') order by nombramiento, sexo_empleado, apellidos_empleado, nombre_empleado";  
//    order by nombramiento, nivel_estudios, sexo_empleado, apellidos_empleado, nombre_empleado
	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	$nom_inicial = $registros->fields('nombramiento');

	if(!$registros->EOF)
		echo '<table align="center" width="50%" title="Tabla que muestra la relaci&oacute;n de Personal y su grado de estudios">
 			<tr><td colspan = "5" align="center"><b>'.(($nom_inicial =='D')?'Docente':(($nom_inicial == 'A')?'Administrativo':(($nom_inicial == 'Z')?'Sin Tipo':'Sin Registro'))).'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center" width="100%">
			<th width="5%"> No. </th>
			<th> Nombre completo </th>			
			<th width="35%"> Nivel maximo de estudios</th>			
			<th width="10%"> Sexo </th>	 
		  </tr>';
	else	echo ' <table align="center" title="No hay personal registrado">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		++$contador;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$sexo = $registros->fields('sexo_empleado');
		
		$qry_estudios="select nivel_estudios, grado_maximo_estudios from estudios_personal where rfc='$rfc'";
		$res_estudios=ejecutar_sql($qry_estudios);
		$nivel = $res_estudios->fields('nivel_estudios');
		$grado = $res_estudios->fields('grado_maximo_estudios');

			if (($nivel == null) and ($grado == null))
				$descripcion_nivel_estudios = 'Sin Registro';
			else {	$qry_nivel_estudios = "select descripcion_nivel_estudios from nivel_de_estudios where nivel_estudios='$nivel'";
				$res_nivel_estudios = ejecutar_sql($qry_nivel_estudios);
				if ($res_nivel->EOF){
					$qry_nivel_escolar="select descripcion_nivel from nivel_escolar where nivel_escolar='$grado'";
					$res_nivel_escolar=ejecutar_sql($qry_nivel_escolar);
					$descripcion_nivel_estudios = $res_nivel_escolar->fields('descripcion_nivel');
					}
				else	$descripcion_nivel_estudios = $res_nivel_estudios->fields('descripcion_nivel_estudios'); 
			}

		$nom = $registros->fields('nombramiento');

				//if ($sexo == 'F') {$contador_sex++;}

			if ($nom_inicial != $nom){ 
				//$contador_sex--; <tr><th colspan = "2">Hombres</th><th colspan = "2">'.(--$contador-($contador_sex)).'</th></tr>
					 //<tr><th colspan = "2">Mujeres</th><th colspan = "2">'.($contador_sex).'</th></tr>
					 
				echo '<tr><th colspan = "3">Total</th><th>'.(--$contador).'</th></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr>
					 <td colspan = "5" align="center"><b>'.(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro'))).'</b></td>
					 </tr>
					 </table>
					 <table align="center" width="50%" title="Tabla que muestra la relaci&oacute;n del Personal: Edad & G&eacute;nero"><tr><td>&nbsp;</td></tr>
					  	<tr align="center" width="100%">
							<th width="5%"> No. </th>
							<th> Nombre completo </th>			
							<th width="35%"> Nivel maximo de estudios</th>			
							<th width="10%"> Sexo </th>		
					  </tr>';
					$total = $total + (--$contador); // 
					$nom_inicial = $nom;
					$contador = 1;
					$contador_sex=0;
				}

		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
			 <td align='center'> $contador </td>
			 <td> $ae $ne </td>		
			 <td> $descripcion_nivel_estudios </td>
			 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td> 
			</tr>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>
			 <td align='center'> $contador </td>
			 <td> $ae $ne </td>		
			 <td> $descripcion_nivel_estudios </td>
			 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td> 
			</tr>";
		}
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	// <tr><th colspan = '2'>Hombres</th><th colspan = '2'>".($contador-$contador_sex--)."</th></tr>
	//	<tr><th colspan = '2'>Mujeres</th><th colspan = '2'>".($contador-($contador-$contador_sex))."</th></tr>
	// .strtoupper($puesto_inicial).	
	echo "<tr><th colspan = '3'>Total</th><th>".$contador."</th>";
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=3 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Grado_de_estudios.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Grado de Estudios" /></a>
	</div>
	<? } ?>
</body>
</html>
