<?php
	/********************************************************
		Personal General

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");

	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 48);
	seguridad('DRH');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css">
		<title>Untitled Document</title>
	</head>
<body>
	<h2 align="center">Listado de Datos Generales del Personal (Licencia/Activos)</h2>

<table align="center" title="Tabla que muestra la relaci&oacute;n del Personal con datos personales importantes">
	<tr><td>&nbsp;</td></tr>

    <?php
	
	$qry_empleado="select rfc, apellidos_empleado, nombre_empleado, domicilio_empleado, colonia_empleado, codigo_postal_empleado, telefono_empleado, correo_electronico, localidad, municipio_empleado, nombramiento, status_empleado from personal where (status_empleado ='01' or status_empleado ='02') order by apellidos_empleado, nombre_empleado, rfc";
	$registros=ejecutar_sql($qry_empleado);
	$id="non";
	$c = 1;

	if(!$registros->EOF)  // <th align="center"> Domicilio </th> <th align="center"> C.P. </th>
		echo '<tr>
		    	<th align="center"> No. </th>
			<th align="center"> RFC </th>
			<th > Nombre Completo </th>
			<th align="center"> Nombramiento </th>
			<th align="center"> Puesto </th>
			<th align="center"> Edad </th>
			<th > Municipio </th>
			<th > Localidad </th>
			<th > Domicilio </th>
			<th align="center"> Telefono </th>
			<th > Correo Electronico </th>
		    </tr>';
	else	echo '<tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	while(!$registros->EOF){
		$apellidos=$registros->fields('apellidos_empleado');
		$nombre=$registros->fields('nombre_empleado');
		$domicilio=$registros->fields('domicilio_empleado');
		$rfc=$registros->fields('rfc');
		$colonia=$registros->fields('colonia_empleado');
		$cp=$registros->fields('codigo_postal_empleado');
		if ($registros->fields('telefono_empleado') != ' ') $telefono=$registros->fields('telefono_empleado'); else $telefono='Sin Registro';
		$correo=$registros->fields('correo_electronico');
		if ($registros->fields('localidad') != ' ') $localidad=$registros->fields('localidad');	else $localidad='Sin Registro';
		$id_municipio=$registros->fields('municipio_empleado');

		$nom=$registros->fields('nombramiento');

		$rfc_sin=substr($rfc,-9);  // ejemplo cusm890901 asc
		$fecha=substr($rfc_sin,0,6);
		
		if (substr($rfc_sin,0,1) > 1)	$anio=intval(substr($fecha,0,2))+1900;
		else	$anio=intval(substr($fecha,0,2))+2000;

		$edad=date("Y",time())-$anio;

/*		$qry_movimiento_personal="select movimiento from movimiento_personal where rfc='$rfc'";
		$res_movimiento_personal=ejecutar_sql($qry_movimiento_personal);
		$tipo_movimiento=$res_movimiento_personal->fields('movimiento');*/
		// Funcion
		$qry_puesto_personal="select clave_puesto from puestos_personal where rfc='$rfc'";
		$res_puesto_personal=ejecutar_sql($qry_puesto_personal);
		$clave_puesto=$res_puesto_personal->fields('clave_puesto');
		
		if(empty($clave_puesto)) $clave_puesto=0;
		
		$qry_puesto="select descripcion_puesto from puestos where clave_puesto=$clave_puesto";
		$res_puesto=ejecutar_sql($qry_puesto);
		if ($res_puesto->fields('descripcion_puesto') != null) $descripcion_puesto=$res_puesto->fields('descripcion_puesto'); else $descripcion_puesto='Sin Puesto';
	
		/**/
		if ($id_municipio > 0 ){   
			$qry_municipio_personal="select municipio from municipios_oaxaca where id_municipio = 397";
			$res_municipio_personal=ejecutar_sql($qry_municipio_personal);
			$municipio=$res_municipio_personal->fields('municipio');
			}
		else	$municipio = 'Sin Registro';

		if ($colonia != ' ' or $domicilio != 0 or $cp != null) $d_domicilio = $colonia.(($colonia==' ')?'':',').(($domicilio==0)?'':$domicilio).(($cp==null)?'':' C.P.').(($cp==0)?'':$cp);
		else $d_domicilio = 'Sin Registro';

		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
		    echo "<tr bgcolor='#FAAC58' align='center'>
			<td> $c </td>
			<td> $rfc </td>
			<td> $apellidos $nombre </td>
			<td align='center'>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
			<td> $descripcion_puesto </td>
			<td align='center'> $edad </td>
			<td> $municipio </td>
			<td> $localidad </td>  
			<td>".$d_domicilio."</td>
			<td> $telefono </td>
			<td> $correo </td>
		    </tr>";
		$activar_flag = 1;
		} else {
		    echo "<tr id=$id align='center'>
			<td> $c </td>
			<td> $rfc </td>
			<td> $apellidos $nombre </td>
			<td align='center'>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
			<td> $descripcion_puesto </td>
			<td align='center'> $edad </td>
			<td> $municipio </td>
			<td> $localidad </td>  
			<td>".$d_domicilio."</td>
			<td> $telefono </td>
			<td> $correo </td>
		    </tr>";
		}
		$id=($id=="non")?"par":"non";
		$c++;
		$registros->movenext();
	}
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=10 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
		</div>
	</form>
	<? if ($c > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Personal_general.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Datos generales" /></a>
	</div>
	<? } ?>
</body>
</html>
