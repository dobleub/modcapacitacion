<?php
	/********************************************************
		registro_personal_honorarios_bd.php

		Desarrolado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>SII :: Honorarios</title>
	</head>
<body>
	<?php

	$regresar = "window.location = 'registro_personal_honorarios.php'";
	// Si no hay datos retorna a solicitarlos
	if(($horas_honorarios=='0')or($rfc_personal_honorarios=="0")){
		echo "<script>alert('Seleccione el personal y las horas que desea asignarle');</script>";
		echo "<script>".$regresar."</script>";
	}

			$upd_honorarios = "exec pam_honorarios $horas_honorarios , '$rfc_personal_honorarios'";
			$res_jefes = ejecutar_sql($upd_honorarios);
	?>
<script type="text/javascript">
	alert('Registro actualizado con exito');
	document.location = "registro_personal_honorarios.php"
</script> 
</body>
</html>
