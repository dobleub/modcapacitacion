<?php
	/********************************************************
		personal.php

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	$accion = $_GET['accion'];
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	//MIIIIII__     permiso_sobre_funcion($_SESSION['susr'], 38);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<script type="text/javascript">
	function validacampos()
	{
		rfcA = document.personal.rfcA.value
		rfcN = document.personal.rfcN.value
		rfcH = document.personal.rfcH.value
		if(rfcA == "")// rfcH == "")
		{
	 		alert("Por favor ingrese las 4 letras del R.F.C.");
			document.personal.rfcA.focus();
			return false;
		}
		else if(rfcN == "")
		{
	 		alert("Por favor ingrese el a�o, mes y d�a de nacimiento");
			document.personal.rfcN.focus();
			return false;
		}
		else if(rfcH == "")
		{
	 		alert("Por favor ingrese homoclave");
			document.personal.rfcH.focus();
			return false;
		}
		document.personal.rfc.value = rfcA+rfcN+rfcH
	}

	function checkLen(x,y)
	{
		if(y.length == x.maxLength)
		{
			var next = x.tabIndex
			if(next < 3)
			{
				document.getElementById("myForm").elements[next].focus()
			}
		}
	}
	</script>
	<title>.:: Personal ::.</title>
</head>

<body onLoad="document.personal.rfcA.focus()">
<?php
$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

if($accion == 'nuevo')	$encabezado = "Alta de Personal";
else if($accion == 'actualiza')	$encabezado = "Actualizaci�n de Datos";

?>
<h2 align="center"> <?php echo $encabezado; ?> </h2>
<form name="personal" id="myForm" method="post" action="manto_personal.php" onSubmit="return validacampos()" >
	<table align="center" title="Ingrese un RFC valido para prodecer la busqueda">
		<tr> 
			<th width="80"> R.F.C.: </th>
			<td id="non">
				<input name="rfcA" type="text" size="5" maxlength="4" tabindex="1" onkeyup="checkLen(this, this.value)" onkeypress="valida_cadena()" onblur="this.value = this.value.toUpperCase()" title="Teclee las cuatro primeras letras de su RFC, ejemplo CUSM">
				<input name="rfcN" type="text" size="4" maxlength="6" tabindex="2" onkeyup="checkLen(this, this.value)" onkeypress="valida_entero()" title="Teclee los seis numeros siguientes a las letras de su RFC, ejemplo 890901">
				<input name="rfcH" type="text" size="3" maxlength="3" tabindex="3" onblur="this.value = this.value.toUpperCase()" title="Teclee la homonimia">
			</td>
		</tr>
	</table>
	<br />
	<div align="center">
		<input type="hidden" name="accion" value="<?php echo $accion; ?>">
		<input type="hidden" name="rfc" />
		<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="4" title="Presione este boton para proceder la busqueda"/>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este boton para cancelar la operacion"/>
	</div>
</form>
</body>
</html>
