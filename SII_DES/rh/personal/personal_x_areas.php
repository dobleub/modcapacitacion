<?php

/**************************************************

	Desarrollador: Ing. Roberto Agustín Chiu Lara
	Instituto Tecnológico de Minatitlán
	
**************************************************/

	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');

	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
<title>SII :: Personal por Areas</title>

<script language="javascript">
	function valida(){
		form=document.areas;
		if(form.area.value == "0"){
			alert('Seleccione el area');
			return false;
		}
		
		return true;
	}
</script>

</head>

<body>
<h2 align="center"> Personal por areas </h2>
<form method="post" action="personal_x_areas.php" onsubmit="return valida();" name="areas">
	<table align="center">
    	<tr>
        	<th> Area: </th>
            <td id="non">
            	<select name="area">
                	<option value="0"> -- Seleccione el area -- </option>
                    <option value="todos"<?=($area=="todos"?" selected":"");?>> Todos </option>
                    <?php
						$qry_organigrama="select clave_area,descripcion_area from organigrama order by clave_area";
						$res_organigrama=ejecutar_sql($qry_organigrama);
						
						while(!$res_organigrama->EOF){
							$clave_area=$res_organigrama->fields('clave_area');
							$descripcion_area=$res_organigrama->fields('descripcion_area');
							$seleccionado=(($clave_area==$area)?" selected":"");
						?>
					<option value="<?=$clave_area;?>"<?=$seleccionado;?>><?=$descripcion_area;?></option>
                        <?php
							$res_organigrama->movenext();
						}
					?>
                </select>
            </td>
        </tr>
    </table>
    <br /><br />
    <div align="center">
    	<input type="submit" value="Cargar" class="boton" />
		<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" />
  	</div>
</form>

<br /><br />
<?php
if(!empty($area)){

	if($area == "todos"){
		$qry_organigrama="select clave_area,descripcion_area from organigrama order by clave_area";
	}else{
		$qry_organigrama="select clave_area,descripcion_area from organigrama where clave_area='$area'";
		//$qry_personal="select apellidos_empleado,nombre_empleado from personal where clave_area='$area' and status_empleado='02'";
	}

	$res_organigrama=ejecutar_sql($qry_organigrama);
	?>
    
    <table align="center">
    <?php
	while(!$res_organigrama->EOF){
		
		$clave_area=$res_organigrama->fields('clave_area');
		$descripcion_area=$res_organigrama->fields('descripcion_area');
		
	?>
    	<tr>
        	<th align="center" colspan="2"><?=$descripcion_area;?></th>
        </tr>
    <?php
		
		$qry_personal="select rfc,apellidos_empleado,nombre_empleado from personal where clave_area='$clave_area' and status_empleado='02' order by apellidos_empleado,nombre_empleado";
		$res_personal=ejecutar_sql($qry_personal);
		$id="non";
		while(!$res_personal->EOF){
			$apellidos_empleado=$res_personal->fields('apellidos_empleado');
			$nombre_empleado=$res_personal->fields('nombre_empleado');
			$rfc=$res_personal->fields('rfc');
	?>
    	<tr id="<?=$id;?>">
        	<td align="center"><?=$rfc;?></td>
        	<td align="left"><?=$apellidos_empleado;?>&nbsp;<?=$nombre_empleado;?>&nbsp;&nbsp;</td>
        </tr>
    <?php
			$id=($id=="non")?"par":"non";
			$res_personal->movenext();
		}
		
		$res_organigrama->movenext();
	}
	?>
    </table>
    <?php
}
?>
</body>
</html>