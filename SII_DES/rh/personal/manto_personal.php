<?php
	/********************************************************
		Informaci�n General

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

function fmunicipios($municipio=NULL)
	{
	$qry_municipios = "SELECT * from municipios_oaxaca order by municipio";
	$res_qry_municipios = ejecutar_sql($qry_municipios);
	 while(!$res_qry_municipios->EOF)
	 	{
       		if($municipio == $res_qry_municipios->Fields("id_municipio"))
			 echo "<option value='".$res_qry_municipios->Fields("id_municipio")."' selected>".$res_qry_municipios->Fields("municipio")."</option>";
		else
			 echo "<option value='".$res_qry_municipios->Fields("id_municipio")."'>".$res_qry_municipios->Fields("municipio")."</option>";
		$res_qry_municipios->MoveNext();
	 	}
	}


	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	require_once($CFG->funciones_phpDir."/funciones_fechas.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/popcalendar.js"></script>
	<script type="text/javascript">

	function valida(formulario)
	{
		form = eval("document."+formulario.name);
	
			correo = form.correo_electronico.value;
		
			var arroba = correo.indexOf("@")
			if(arroba == -1 || arroba == 0){
				alert("Capture un correo electr�nico v�lido");
				form.correo_electronico.focus();
				return false
			}
			else{
				dominio = correo.substr(arroba+1)
				if(dominio.length == 0){
					alert("Capture un correo electr�nico v�lido")
					form.correo_electronico.focus()
					return false
				}
			
				punto = correo.lastIndexOf(".")
		      longitud = correo.length - 1
	    	  if(punto == -1 || punto == longitud){
					alert("Capture un dominio v�lido")
					form.correo_electronico.focus()
					return false
			  }
			}

		if (form.clave_area.value==0){
			alert("Capture el departamento de adscripcion");
			return false;
		}

		var band = 0;
		for (i = 0; i < form.elements.length; i++) {
			if (form.elements[i].id == "numerico") {  
		
			var campo = form.elements[i];
		
				if ((isNaN(campo.value))||(campo.value=="")){
					campo.value = "0"; 
					//campo.focus();
					campo.style.background="#FFB0B0";
					band = 1;
				}
				else{
					campo.style.background="#FFFFFF";
					band = 0;
				}		
		
			}//if id numerico
		}//fin for

		var band2 = 0;
		for (i = 0; i < form.elements.length; i++) {
		
			if (form.elements[i].id == "obligatorio") {  
		
			var campo = form.elements[i];
		
				if (campo.value==""){
					campo.focus();
					campo.style.background="#FFB0B0";
					band2 = 1;
				}
				else{
					campo.style.background="#FFFFFF";
					band2 = 0;
				}		
		
			}//if id numerico
		}//fin for
	
		var band3 = 0;
		for (i = 0; i < form.elements.length; i++) {
		
			if (form.elements[i].id == "numerico4") {  
		
			var campo = form.elements[i];
		
				if (campo.value==""){
					campo.value="1900";
					campo.style.background="#FFB0B0";
					band3 = 1;
				}
				else{
					campo.style.background="#FFFFFF";
					band3 = 0;
				}		
		
			}//if id numerico
		}//fin for
	
	
		if ((band == 0)&&(band2 == 0)&&(band3 == 0)){
			return true;
		}
		else{
			if (band2 == 1){
				alert("Llene los campos en rojo son obligatorios");
			}		
			return false;
		}	
	}//fin funcion

	function completa_quicena(obj){
		if(obj.value < 10 )
		  if(obj.value.substr(0, 1)!=0)
				obj.value = "0"+obj.value
	}
		</script>
		<title>.:: Personal ::.</title>
</head>

<body>
<?php

$rfc = strtoupper($rfc);
//$rfc = strtoupper($_GET['$rfc']);

$regresar = "window.location = 'personal.php?accion=".$accion."'";

$qry_p = "select * from personal where rfc = '$rfc'";
$res_p = ejecutar_sql($qry_p);
$rows = $res_p->recordcount();


if($rows == 0){
		$encabezado = "Alta de Personal";
		$botonSubmit = "Dar de Alta";
		$regresar = "window.location = 'personal.php'";
		// Imagen
		$archivo_foto = "documentos/fotos/personal/sin_foto.jpg";
		$foto = "<img src='$archivo_foto' width='97' height='95' align='center'>";
		//Firma
		$archivo_firma = "documentos/firmas/personal/sin_firma.jpg";
		$firma = "<img src='$archivo_firma' width='97' height='95' align='center'>";

		$accion = "nuevo";

		$qry_instituto = "select clave_centro_seit from instituto";
		$res_instituto = ejecutar_sql($qry_instituto);
		$clave_centro_seit = $res_instituto->fields('clave_centro_seit');
		$no_tarjeta = 0;  // Today 091111
}
else
{ 
	$accion = "actualiza";
	$encabezado = "Actualizar Datos Empleado";
	$botonSubmit = "Actualizar";
	$regresar = "window.location = 'modifica_personal.php?todos=t'";

	// Foto
	$flag_foto = $res_p->fields('bandera_foto');
	if ($flag_foto == 0)	$archivo_foto = "../expediente/documentos/fotos/personal/sin_foto.jpg";
	if ($flag_foto == 1)	$archivo_foto = "../expediente/documentos/fotos/personal/$rfc.jpg";
	$foto = "<img src='$archivo_foto' width='95' height='98' align='center'>";
	
	//Firma
	$flag_firma = $res_p->fields('bandera_firma');
	if ($flag_firma == 0)	$archivo_firma = "../expediente/documentos/firmas/personal/sin_firma.jpg";
	if ($flag_firma == 1)	$archivo_firma = "../expediente/documentos/firmas/personal/$rfc.jpg";
	$firma = "<img src='$archivo_firma' width='95' height='98' align='center'>";
 
	$clave_centro_seit = $res_p->fields('clave_centro_seit');
	$clave_area = $res_p->fields('clave_area'); 
	$curp_empleado = $res_p->fields('curp_empleado');
	$no_tarjeta = $res_p->fields('no_tarjeta');
	$apellidos_empleado = $res_p->fields('apellidos_empleado');
	$nombre_empleado = $res_p->fields('nombre_empleado'); 
	$nombramiento = $res_p->fields('nombramiento');
	$ingreso_rama = $res_p->fields('ingreso_rama');
	$inicio_gobierno = $res_p->fields('inicio_gobierno');
	$inicio_sep = $res_p->fields('inicio_sep');
	$inicio_plantel = $res_p->fields('inicio_plantel');
	$domicilio_empleado = $res_p->fields('domicilio_empleado');
	$colonia_empleado = $res_p->fields('colonia_empleado');
	$codigo_postal_empleado = $res_p->fields('codigo_postal_empleado');
	$localidad = $res_p->fields('localidad'); 
	$telefono_empleado = $res_p->fields('telefono_empleado');
	$sexo_empleado = $res_p->fields('sexo_empleado');
	$estado_civil = $res_p->fields('estado_civil');
	$fecha_nacimiento = $res_p->fields('fecha_nacimiento');
	$lugar_nacimiento = $res_p->fields('lugar_nacimiento'); 
	$status_empleado = $res_p->fields('status_empleado');
	$correo_electronico = $res_p->fields('correo_electronico'); 
	$padre = $res_p->fields('padre'); 
	$madre = $res_p->fields('madre'); 
	$conyuge = $res_p->fields('conyuge'); 
	$hijos = $res_p->fields('hijos');
	$acta_nacimiento_numero = $res_p->fields('num_acta');
	$acta_nacimiento_libro = $res_p->fields('num_libro');
	$acta_nacimiento_foja = $res_p->fields('num_foja'); 
	$acta_nacimiento_ano = $res_p->fields('num_ano');
	$num_cartilla_smn = $res_p->fields('num_cartilla_smn');
	$pais = $res_p->fields('pais');	
	$pasaporte = $res_p->fields('pasaporte');    
	$fm = $res_p->fields('fm');
	$pasaporte_vigencia_inicio = $res_p->fields('inicio_vigencia'); 
	$pasaporte_vigencia_fin = $res_p->fields('termino_vigencia'); 
	$area_academica = $res_p->fields('area_academica');
	$tipo_personal=$res_p->fields('tipo_personal'); 
	$nacionalidad=$res_p->fields('nacionalidad');
	$municipio_empleado=$res_p->fields('municipio_empleado');

	$entidad_federativa=$res_p->fields('entidad_federativa');

	$pasaporte_puesto_autorizado=$res_p->fields('pasaporte_puesto_autorizado');
	$emergencia=$res_p->fields('emergencia');
	$otros_dependientes=$res_p->fields('otros_dependientes');
}

fdeptos($clave_area);

switch($estado_civil){
	case 'C':		$selectedC = "selected"; break;
	case 'D':		$selectedd = "selected"; break;
	case 'V':		$selectedV = "selected"; break;
	case 'U':		$selectedU = "selected"; break;
	case 'O':		$selectedO = "selected"; break;
	default:		$selectedS = "selected"; break;
}
if($sexo_empleado == 'F'){ $selectedF = " selected"; } else{ $selectedM = " selected"; }
if(!is_numeric($codigo_postal)){ $codigo_postal = 0; }
switch($nombramiento){
	case 'Z' :	$seleZ = " selected"; $docente = false; break;
	case 'D' :	$seleD = " selected"; $docente = true; break;
	case 'A' :	$seleA = " selected"; $docente = false; break;
	}
if($ingreso_rama){
	$ingreso_ramaA = substr($ingreso_rama, 0, 4);
	$ingreso_ramaQ = substr($ingreso_rama, -2);
	}
if($inicio_gobierno){
	$inicio_gobiernoA = substr($inicio_gobierno, 0, 4);
	$inicio_gobiernoQ = substr($inicio_gobierno, 4, 2);
	}
if($inicio_sep){
	$inicio_sepA = substr($inicio_sep, 0, 4);
	$inicio_sepQ = substr($inicio_sep, 4, 2);
	}
if($inicio_plantel){
	$inicio_plantelA = substr($inicio_plantel, 0, 4);
	$inicio_plantelQ = substr($inicio_plantel, 4, 2);
	}
?>
<h2 align="center"> <?php echo $encabezado; ?> </h2>
<form name="personal" action="manto_personal_bd.php" method="post" onSubmit="return valida(this)" enctype="multipart/form-data">

  <!-- DATOS GENERALES -->
  <h3 align="center"> Datos Generales </h3> <!--  Inicia primera tabla-->
	<table width="100%"  align="center" title="Secci&oacute;n de datos generales"> <!-- border="0" cellpadding="0" cellspacing="0" -->
		<tr align="center">
			<td width="9%" rowspan="6"><a href="../expediente/cargar_foto.php?var=<?php echo $rfc;?>" target="_blank"> <?php echo $foto; ?> </a></td>
         <th width="36%"> Apellido (s)</th>
			<th>Nombre</th>
			<th>R.F.C.</th>
      <th width="16%">  </th> <!--  -->
			<!----><td width="9%" rowspan="6"> <a href="../expediente/cargar_firma.php?var=<?php echo $rfc;?>" target="_blank"> <?php echo $firma; ?> </a></td> 
    </tr>
    <tr align="center">
	  	<td> 
<!-- INICIO MODIFICA -->
<input name="apellidos_empleado" title="Capture los apellidos del empleado" type="text" value="<?php echo $apellidos_empleado; ?>" size="40" maxlength="45" onChange="javascript:this.value=this.value.toUpperCase();" id = "obligatorio">
<!-- FIN MODIFICA -->
		</td>
      <td align="center"><input name="nombre_empleado" title="Capture nombre(s) del empleado" type="text" value="<?php echo $nombre_empleado; ?>" size="35" maxlength="35" onChange="javascript:this.value=this.value.toUpperCase();"  id = "obligatorio"></td>
      <td>
      	<input name="rfc" type="text" size="15" maxlength="13" value="<?php echo $rfc; ?>" readonly="true" title="El RFC no puede ser modificado">      </td>
    </tr>
    <tr align="center">
	  	<th>CURP </th>
      <th width="17%">Nacionalidad</th>
      <th width="22%">Sexo </th>
      <th width="16%">Estado civil </th>
    </tr>
    <tr align="center">
      <td><input name="curp_empleado" type="text" size="21" maxlength="18" value="<?php echo $curp_empleado; ?>" title="Capture la CURP del empleado" onChange="javascript:this.value=this.value.toUpperCase();"></td>
      <td>
      	<select name="nacionalidad" title="Seleccione la nacionalidad del empleado">
  	    <option value="MEXICANA" <?php if ($nacionalidad=="MEXICANA"){echo "selected";}?>>MEXICANA</option>
  	    <option value="EXTRANJERA" <?php if ($nacionalidad=="EXTRANJERA"){echo "selected";}?>>EXTRANJERA</option>
	</select>  	  
      </td>
      <td align="center">
				<select name="sexo_empleado" title="Seleccione el sexo del empleado">
					<option value="-1"> -- SELECCIONE -- </option>
					<option value="M" <?php echo $selectedM; ?>> MASCULINO </option>
					<option value="F"	<?php echo $selectedF; ?>> FEMENINO </option>
				</select>			
      </td>
      <td align="center">
       <select name="estado_civil" title="Seleccione el estado civil del empleado">
          <option value="S" <?php echo $selectedS; ?>> SOLTERO(A) </option>
          <option value="C" <?php echo $selectedC; ?>> CASADO(A) </option>
          <option value="D" <?php echo $selectedD; ?>> DIVORCIADO(A) </option>
          <option value="V" <?php echo $selectedV; ?>> VIUDO(A) </option>
          <option value="U" <?php echo $selectedU; ?>> UNI&Oacute;N LIBRE </option>
          <option value="O" <?php echo $selectedO; ?>> OTRO </option>
        </select>      
       </td>
    </tr>
    <tr align="center">
      <th>N&uacute;mero cartilla servicio militar nacional</th>
      <th>Correo electr&oacute;nico </th>
      <th>Lugar nacimiento</th>
      <th>Fecha nacimiento</th>
    </tr>
    <tr align="center">
      <td><input name="num_cartilla_smn" type="text" value="<?php echo $num_cartilla_smn;?>" size="10" maxlength="10" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture el n&uacute;mero de la cartilla de servicio militar nacional"></td>
      <td><input name="correo_electronico" type="text" title="Capture correo electr�nico" size="20" maxlength="60"  value="<?php echo $correo_electronico; ?>" onChange="javascript:this.value=this.value.toLowerCase();"></td>
      <td><select name="lugar_nacimiento" title="Seleccione lugar de Nacimiento"> <?php fentidades($lugar_nacimiento); ?>
          </select>
      </td>
      <td><input name="fecha_nacimiento" title="Seleccione la fecha de nacimiento del empleado" onFocus="document.personal.boton.click()" type="text" value="<? echo strftime('%Y/%m/%d', strtotime($fecha_nacimiento));?>" size="15" maxlength="10" readonly="true">
          <input name="boton" class="calendario" type="button" value="..." size="3" maxlength="3" onClick="return popUpCalendar(boton, fecha_nacimiento, 'yyyy/mm/dd');">
      </td>
    </tr>
    <tr align="center">
      <th width="9%">Foto</th>
      <th>Acta nacimiento n&uacute;mero</th>
      <th>Acta nacimiento foja</th>
      <th>Acta nacimiento libro</th>
      <th>Acta nacimiento a&ntilde;o</th>
      <!----><th width="9%"> Firma</th> 
    </tr>
    <tr align="center">
      <td><!--&nbsp; --> 
	<!--<i>Actualice la Foto <a href="../expediente/cargar_foto.php?var=<?php echo $rfc;?>" target="_blank"> Aqu� </a></i>-->
      </td>
      <td><input name="acta_nacimiento_numero" type="text" value="<?php echo $acta_nacimiento_numero;?>" size="5" maxlength="5" id = "numerico" title="Capture el n&uacute;mero de acta de Nacimiento"></td>
      <td><input name="acta_nacimiento_foja" type="text" value="<?php echo $acta_nacimiento_foja;?>"  size="5" maxlength="5" id = "numerico" title="Capture el n&uacute;mero de foja del acta de Nacimiento"></td>
      <td><input name="acta_nacimiento_libro" type="text" value="<?php echo $acta_nacimiento_libro;?>" size="5" maxlength="5" title="Capture el n&uacute;mero de libro del acta de Nacimiento"></td>
      <td><input name="acta_nacimiento_ano" type="text" value="<?php echo $acta_nacimiento_ano;?>" size="5" maxlength="4" id = "numerico" title="Capture el a�o de registro del acta de Nacimiento"></td>
      <td>
	<!--<i>Actualice la Firma <a href="../expediente/cargar_firma.php?var=<?php echo $rfc;?>" target="_blank"> Aqu� </a></i>-->
      </td> 
    </tr>
  </table> <!-- Finaliza primera tabla -->

<!-- Informacion laboral-->
<h3 align="center"> Informaci&oacute;n Laboral </h3>
	<table align="center" width="100%" title="Secci&oacute;n de datos laborales">
	<tr align="center">
        	<th > Tipo de Trabajador </th>
		<th >Tipo de Plaza</th> <!--  Nombramiento  -->
		<th > Departamento de adscripci&oacute;n </th>
		<th > Departamento acad&eacute;mico </th>
	</tr>
	<tr align="center">
        	<td width="20%">
            	<select name="tipo_personal" title="Elija el tipo de personal">
                    <option value="Z"<?php echo ($tipo_personal=="Z")?" selected":""; ?>> SIN TIPO </option>
                    <option value="B"<?php echo ($tipo_personal=="B")?" selected":""; ?>> BASE </option>
                    <option value="H"<?php echo ($tipo_personal=="H")?" selected":""; ?>> HONORARIOS </option>
                    <option value="V"<?php echo ($tipo_personal=="V")?" selected":""; ?>> PROF. VISITANTE </option>
                    <option value="X"<?php echo ($tipo_personal=="X")?" selected":""; ?>> MIXTO </option>
                </select>

            </td>
	 <td width="20%">
		<select name="nombramiento" title="Seleccione el tipo de nombramiento">


			<option value="Z" <?php echo $seleZ;?>> SIN TIPO </option>
			<option value="D" <?php echo $seleD;?>> DOCENTE </option>
			<option value="A" <?php echo $seleA;?>> ADMINISTRATIVO </option>
		</select>
	</td>
	<td width="20%"> <select name="clave_area" title="Seleccione el Departamento de Adscripci�n"> <?php 
	// echo fdeptos($clave_area); 	
		$qry_deptos = "select * from organigrama";
	$res_deptos = ejecutar_sql($qry_deptos);
	
				if(!$res_deptos->rowcount())
				echo "<option value='0' selected> No hay departamentos Registrados </option>"; 
			else
			{       echo "<option value='0' selected> -- Seleccione una Departamento -- </option>";
		 	while(!$res_deptos->EOF){

			if($res_deptos->fields('clave_area')==$clave_area){
				$selecciona=" selected";}
			else{ $selecciona="";}

			echo '<option value="'.$res_deptos->fields("clave_area").'"'.$selecciona.'>'.$res_deptos->fields("descripcion_area").'</option>';
			$res_deptos->MoveNext();
			} 
			}
	 ?> </select> </td>
	<td width="20%"> <select name="area_academica" <?php echo ($docente)?'title="Seleccione Departamento Acad�mico"':''; ?>> <?php 
	
	echo fdeptos($area_academica, 'D'); 
	
	?> </select> </td>
	</tr>
	</table>
	<table align="center" width="100%">
		<tr align="center">
			<th> N&uacute;mero empleado/tarjeta</th>
			<th> Estatus empleado </th>
			<th> Ingreso a la rama/subsecretar&iacute;a </th>
			<th> Ingreso a gobierno </th>
			<th> Ingreso a S.E.P. </th>
			<th> Ingreso al plantel </th>
		</tr>
		<tr align="center">
		<td> <input type="text" title="Capture el n�mero del empleado" name="no_tarjeta" value="<?php echo $no_tarjeta; ?>" size="5" maxlength="4" id = "numerico" /> </td>
		<td>
			<select name="status_empleado" title="seleccione el estatus del empleado">
				<option value="02" <?php if($status_empleado=="02"){ echo "selected";} ?>> ACTIVO </option>
				<option value="00" <?php if($status_empleado=="00"){ echo "selected";} ?>> BAJA POR RENUNCIA </option>
				<option value="01" <?php if($status_empleado=="01"){ echo "selected";} ?>> LICENCIA </option>
				<option value="03" <?php if($status_empleado=="03"){ echo "selected";} ?>> BAJA POR JUBILACI&Oacute;N </option>
				<!--<option value="04" <?php if($status_empleado=="04"){ echo "selected";} ?>> INACTIVO POR CAMBIO DE RFC </option> -->
				<option value="05" <?php if($status_empleado=="05"){ echo "selected";} ?>> BAJA POR FALLECIMIENTO </option>
				<option value="06" <?php if($status_empleado=="06"){ echo "selected";} ?>> INACTIVO </option>
			</select>
		</td>
		<td>
			<input name="ingreso_ramaA" title="Capture el a�o de ingreso a la Subsecretar�a" type="text" value="<?php echo $ingreso_ramaA; ?>" size="3" maxlength="4" id = "numerico4" onBlur="valida_valor(1900, 2100, this)">
			<input name="ingreso_ramaQ" title="Capture la quincena de ingreso a la Subsecretar�a" type="text" value="<?php echo (empty($ingreso_ramaQ))?'01':$ingreso_ramaQ; ?>" size="1" maxlength="2" id = "numerico" onBlur="valida_valor(1, 24, this); completa_quicena(this)">
		</td>
<!-- echo (empty($ingreso_ramaQ))?"01":$ingreso_ramaQ; -->
		<td>
			<input name="inicio_gobiernoA" title="Capture el a�o de ingreso a Gobierno" type="text" value="<?php echo $inicio_gobiernoA; ?>" size="3" maxlength="4" id = "numerico4" onBlur="valida_valor(1900, 2100, this)">
			<input name="inicio_gobiernoQ" title="Capture la quincena de ingreso a Gobierno" type="text" value="<?php echo (empty($inicio_gobiernoQ))?'01':$inicio_gobiernoQ; ?>" size="1" maxlength="2" id = "numerico" onBlur="valida_valor(1, 24, this); completa_quicena(this)">
		</td>
<!--echo (empty($inicio_gobiernoQ))?"01":$inicio_gobiernoQ;  -->
		<td>
			<input name="inicio_sepA" title="Capture el a�o de ingreso a la SEP" type="text" value="<?php echo $inicio_sepA; ?>" size="3" maxlength="4" id = "numerico4" onBlur="valida_valor(1900, 2100, this)">
			<input name="inicio_sepQ" title="Capture la quincena de ingreso a la SEP" type="text" value="<?php echo (empty($inicio_sepQ))?'01':$inicio_sepQ; ?>" size="1" maxlength="2" id = "numerico" onBlur="valida_valor(1, 24, this); completa_quicena(this)">
		</td>
<!-- echo (empty($inicio_sepQ))?"01":$inicio_sepQ; -->
		<td>
			<input name="inicio_plantelA" title="Capture el a�o de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelA))?date('Y'):$inicio_plantelA; ?>" size="3" maxlength="4" id = "numerico4" onBlur="valida_valor(1900, 2100, this)">
			<input name="inicio_plantelQ" title="Capture la quincena de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelQ))?((date('d')<16)?date('m')*2-1:date('m')*2):$inicio_plantelQ; ?>" size="1" maxlength="2" id = "numerico" onBlur="valida_valor(1, 24, this); completa_quicena(this)">
		</td>
<!-- echo (empty($inicio_plantelA))?date("Y"):$inicio_plantelA; -->
<!-- echo (empty($inicio_plantelQ))?((date("d")<16)?date("m")*2-1:date("m")*2):$inicio_plantelQ; -->
		</tr> 

    <!-- INICIO MODIFICACION -->
         <tr>
        	<th> Clave de Centro de Trabajo</th>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input name="clave_ct" type="text" value="<?php echo $clave_centro_seit; ?>" size="10" readonly="true" title="La Clave del Centro de Trabajo no puede ser modificada"></td>
	</tr>
    <!-- FIN MODIFICACION -->
	</table>

<!-- DATOS DOMICILIO --->
<h3 align="center"> Datos Domicilio </h3>    <table width="100%" align="center" title="Secci&oacute;n de datos del domicilio"> <!-- border="0" cellpadding="0" cellspacing="0" -->

      <tr align="center">
        <th>C&oacute;digo Postal </th>
        <th width="36%"> Calle</th>
        <th width="17%"></th>
        <th width="17%"> Colonia </th>
        <th width="16%"> Ciudad o localidad </th>
      </tr>
      <tr align="center">
        <td><input name="codigo_postal_empleado" type="text" id="codigo_postal_empleado" value="<?php echo (!$codigo_postal_empleado)?0:$codigo_postal_empleado; ?>" size="8" maxlength="5" title="Capture el c&oacute;digo postal"></td>
        <td><input name="domicilio_empleado" type="text" size="40" maxlength="60" value="<?php echo $domicilio_empleado; ?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture el nombre de la calle y el n&uacute;mero del domicilio"></td>
        <td></td>
        <td><input name="colonia_empleado" type="text" size="20" maxlength="40" value="<?php echo $colonia_empleado; ?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture el nombre de la colonia del empleado">
        </td>
        <td><input name="localidad" type="text" size="20" maxlength="30" value="<?php echo $localidad; ?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture el nombre de la localidad"></td>
      </tr>
      <tr align="center">
        <th> Entidad federativa </th>
        <th>Municipio</th>
        <th> Tel&eacute;fono </th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
      <tr align="center">
        <td><select name="entidad_federativa" title="Seleccione una entidad federativa"> <?php fentidades($entidad_federativa); ?> </select> </td>
        <td><select name="municipio_empleado" title="Seleccione el nombre del municipio del empleado"> <?php fmunicipios($municipio_empleado); ?> </select> </td>
        <td><input name="telefono_empleado" type="text" size="15" maxlength="15" value="<?php echo $telefono_empleado; ?>" title="Capture el numero tel&eacute;fonico del empleado"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>

<!-- DATOS DE EXTRANJERO-->
    <h3 align="center">Datos de Extranjeros</h3>
    <table width="100%" align="center" title="Secci&oacute;n exclusiva para extranjeros"> <!-- border="0" cellpadding="0" cellspacing="0" -->
      <tr align="center">
        <th width="15%">Pa&iacute;s </th>
        <th width="15%">N&uacute;mero pasaporte</th>
        <th width="15%">FM</th>
        <!--<th width="15%">Vigencia inicio pasaporte </th> -->
        <th width="15%">Vigencia fin pasaporte</th>
        <th width="25%">Puesto autorizado pasaporte</th>
      </tr>
      <tr align="center">
        <td><input name="pais" type="text" value="<?php echo $pais; ?>" size="30" maxlength="30" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture el nombre del pais"></td>
        <td><input name="pasaporte" type="text" value="<?php echo $pasaporte; ?>" size="10" maxlength="10" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la serie del pasaporte"></td>
        <td><input type="text" name="fm" value="<?php echo $fm; ?>" onChange="javascript:this.value=this.value.toUpperCase();" title="Capture la fm del empleado"></td>
        <td><input name="pasaporte_vigencia_fin" title="Seleccione la fecha de pasaporte vigencia fin" onFocus="document.personal.boton_pasaporte2.click()" type="text" value="<?php if ($pasaporte_vigencia_fin!='') {echo strftime('%Y/%m/%d', strtotime($pasaporte_vigencia_fin));} ?>" size="10" maxlength="10" readonly="true">
          <input name="boton_pasaporte2" class="calendario" type="button" value="..." size="3" maxlength="3" onClick="return popUpCalendar(boton_pasaporte2, pasaporte_vigencia_fin, 'yyyy/mm/dd');"></td>
        <td><input name="pasaporte_puesto_autorizado" type="text" onChange="javascript:this.value=this.value.toUpperCase();" value="<?php echo $pasaporte_puesto_autorizado; ?>" size="20" maxlength="20" title="Capture el nombre del puesto que autorizo el pasaporte"></td>
      </tr>
    </table>

<!-- <?php if ($pasaporte_vigencia_fin!='') {echo strftime('%Y/%m/%d', strtotime($pasaporte_vigencia_fin));} ?> <?php echo strftime('%Y/%m/%d', strtotime($pasaporte_vigencia_fin)); ?>-->

<!-- DATOS SOCIECONOMICOS -->
  <h3 align="center">Datos Socieconomicos</h3>
  <table width="100%" align="center" title="Secci&oacute;n de datos sociecon&oacute;micos"> <!--  border="0" cellpadding="0" cellspacing="0" -->
    <tr align="center">
      <th width="33%">Nombre padre</th>
      <th width="33%">Nombre madre</th>
      <th width="34%">Nombre del conyuge</th>
    </tr>
    <tr align="center">
      <td><input name="padre" type="text" value="<?php echo $padre; ?>" size="50" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Escriba el nombre del padre"></td>
      <td><input name="madre" type="text" value="<?php echo $madre; ?>" size="50" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Escriba el nombre de la madre"> </td>
      <td><input name="conyuge" type="text" value="<?php echo $conyuge; ?>" size="50" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Escriba el nombre de su conyuge"></td>
    </tr>
    <tr align="center">
      <th>En caso de emergencia avisar a:</td>
      <th>Nombre(s) de hij(os/as)</th> <!-- hijos varchar(100) -->
      <th>Otros dependientes (n&uacute;mero)</th>
    </tr>
    <tr align="center">
      <td><input name="emergencia" type="text" value="<?php echo $emergencia; ?>" size="50" maxlength="50" onChange="javascript:this.value=this.value.toUpperCase();" title="Teclee el nombre y/o tel&eacute;fono de un responsable en caso de emergencia"></td>
      <td><input name="hijos" type="text" value="<?php echo $hijos; ?>" size="50" maxlength="100" title="Capture el nombre de los hijos que tiene" onChange="javascript:this.value=this.value.toUpperCase();"></td>
      <td><input name="otros_dependientes" type="text"  value="<?php echo $otros_dependientes; ?>" size="3" maxlength="2" id = "numerico" title="Capture el n&uacute;mero de personas que dependen de usted excepto sus hijos"></td>
    </tr>
  </table>
<!-- INFORMACION LABORA L -->
	<input type="hidden" name="clave_centro_seit" value="<?php echo $CFG->clv_instituto; ?>" />
    <input type="hidden" name="accion" value="<?php echo $accion; ?>" />
	<br />
	<div align="center">
		<input type="submit" name="actualiza" class="boton" value="<?php echo $botonSubmit; ?>" title="Presione este bot&oacute;n para guardar los cambios">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="regresa" class="boton" value="Regresar" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n">
	</div>
</form>
</body>
</html>
