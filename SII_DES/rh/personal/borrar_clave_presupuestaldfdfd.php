<?php
	/********************************************************
		borrar_clave_presupuestal.php

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/

//Librerias y funciones
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	
	$id_clave= $_GET['id_clave'];
        $qry_maximo_claves_antes="select count(id_clave) as maximo from claves_presupuestales";
	$res_maximo_claves_antes=ejecutar_sql($qry_maximo_claves_antes);
	$maximo_antes=$res_maximo_claves_antes->fields('maximo');	

 	$query_result = sybase_query( "delete from claves_presupuestales where id_clave =$id_clave and id_clave not in (select id_clave from plantilla_personal)")or die( "<script> alert('No se pudo eliminar la clave presupuestal ya que esta asociada a uno o más trabajadores. En Mantenimiento > Plazas y Movimientos > Agregar Plaza(s) Personal'); window.location='agrega_clave_plaza.php'; </script>" );
       
        $qry_maximo_claves_despues="select count(id_clave) as maximo from claves_presupuestales";
	$res_maximo_claves_despues=ejecutar_sql($qry_maximo_claves_despues);
	$maximo_despues=$res_maximo_claves_despues->fields('maximo');

	if ($maximo_despues == $maximo_antes)
	 echo "<script> alert('No se pudo eliminar la clave presupuestal ya que esta asociada a uno o mas trabajadores. En Mantenimiento > Plazas y Movimientos > Agregar Plaza(s) Personal'); window.location='agrega_clave_plaza.php'; </script>";
	
	else  echo '<script> alert("Registro eliminado correctamente"); </script>';
	echo '<script> window.location="agrega_clave_plaza.php"; </script>';
?>
