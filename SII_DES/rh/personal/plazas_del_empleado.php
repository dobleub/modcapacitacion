<?php
	/********************************************************
		Plazas del Empleado

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 

	$volver = "agrega_personal_a_plantilla.php";
	$rfc=$_GET['idp'];

		$consulta_nombre="select apellidos_empleado, nombre_empleado, ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel from personal where rfc ='$rfc'";
			$reg=ejecutar_sql($consulta_nombre);
			$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');
			$ingreso_rama= $reg->fields('ingreso_rama');
			$inicio_gobierno = $reg->fields('inicio_gobierno');
			$inicio_sep = $reg->fields('inicio_sep');
			$inicio_plantel= $reg->fields('inicio_plantel');
?>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />


		<h2 align="center">Historial de Plazas de: <?php echo $rfc." - ".$nombre; ?></h2>

<form method="post" action="efecto_movimiento_personal_listado.php" name="datos" >
<br><br>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="70%" title="Tabla que lista el personal con su respectivo movimiento">
  <tr>
    <td>&nbsp;</td> 
  </tr>
  <tr align="center">
    <td colspan = "2"> Gob : <b><?php echo $inicio_gobierno;?></b></td> <td > Sep : <b><?php echo $inicio_sep;?></b></td> <td colspan = "2"> Rama : <b><?php echo $ingreso_rama;?></b></td> <td colspan = "2"> ITT : <b><?php echo $inicio_plantel;?></b></td>   
  </tr>
<?php 
	$consulta_id="select * from movimiento_personal where status=2 and rfc='$rfc' order by id_mov";
	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;
	if(!$registros->EOF)
		echo '<tr><td>&nbsp;</td></tr><tr>
			    <th>Contador</th>
			    <th>Clave</th>
			    <th>Categoria</th>
			    <th width="20%">Movimiento</th>
			    <th>Efectos Iniciales</th>
			    <th>Efectos Finales</th>
			    <th>Status</th>
			  </tr>';
	else	echo '<tr align="center">
			<td> No exite personal registrado </th>
		  	</tr>';

		while(!$registros->EOF){
			$contador = $contador + 1;
			$id_mov = $registros->fields('id_mov');
			$su_rfc = $registros->fields('rfc');
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$su_rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');

			$movimiento = $registros->fields('movimiento');
			$e_iniciales = $registros->fields('e_iniciales');
			$e_finales = $registros->fields('e_finales');

			$status = $registros->fields('status');
// (date('d/m/Y') 07/03/2012
			echo "<tr id='$id'>
				 <td align='center'>".$contador."</td>
				 <td align='center'>".$su_rfc."</td>
				 <td>".$nombre."</td>
				 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
				 <td align='center'>".$e_iniciales."</td>
				 <td align='center'>".$e_finales."</td>
				 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
			     </tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}
	?>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
</table>
	<br>
	<br>
	<div align="center">
		<input type="button" value="Cancelar" class="boton" onclick="javascript:window.location = '<?php echo $volver; ?>'" />
	</div>

</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Clave_presupuestal_historial.pdf.php?idp=<?php echo $rfc; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Historial Movimiento"/></a>
	</div>
	<? } ?>
