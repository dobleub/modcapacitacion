<?php
	/********************************************************
		Eliminación (baja)

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	//MIIIIII__     permiso_sobre_funcion($_SESSION['susr'], 39);
	if (isset($_GET['todos']) and !isset($_POST['todos']))	$todos = $_GET['todos'];	else   $todos = $_POST['todos']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>

		<title>.:: Eliminaci&oacute;n de Datos del Trabajador (baja) ::.</title>
	</head>

<body>
<?php
$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

?>
<h2 align="center"> Eliminar Registro de Empleado </h2>

<form name="solicitud_ver" method="post" action="">
	<h3 align="center"> Opciones de Visualizaci&oacute;n: </h3>
	<table width="30%" align="center" title="Secci&oacute;n para buscar por su RFC a un trabajador">
		<tr align="center">
			<th>Ver: </th>
		</tr>
		<tr align="center">
			<th>
				<select name="rfc_personal" title="Seleccione el nivel de estudios" onChange="javascript:document.solicitud_ver.submit();">
					<?php
					$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal order by apellidos_empleado, nombre_empleado, rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);

					if(!$datos_rfc->rowcount())	echo "<option value='0' selected> No hay personal dado de alta </option>";
					else
					{
						echo "<option value='0' selected> -- Seleccione Personal -- </option>";
						while(!$datos_rfc->EOF){
							echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
							$datos_rfc->MoveNext();
						}
					}
					?>
				</select>
			</th>
		</tr>
	</table>
	<br>
 <input type='hidden' name='todos' value='1'>

	<table align="center" title="Lista de todo el personal del instituto">
		  <tr><td>&nbsp;</td></tr>
  
  <?php
        if ($rfc_personal == '0' or $todos == 't')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, no_tarjeta, status_empleado from personal order by apellidos_empleado, nombre_empleado, rfc"; 
        else if ($rfc_personal != '0')
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, no_tarjeta, status_empleado from personal where( rfc = '".$rfc_personal."' )";

	//$consulta_registros="select * from personal order by status_empleado, apellidos_empleado, nombre_empleado, rfc";
	$registros=ejecutar_sql($consulta_registros);

	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo "<tr align='center'>
		    <th> No. </th>
		    <th> No. tarjeta </th>
		    <th> RFC </th>
		    <th> Nombre completo </th>
		    <th> Eliminar </th>
		    <th> Status </th>
		  </tr>";
	else	echo '<tr align="center">
			 <td> No exite personal registrado </th>
		      </tr>';

	while(!$registros->EOF){

		++$contador;
		$nt = $registros->fields('no_tarjeta');
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$se = $registros->fields('status_empleado');

		echo "<tr id='$id'>
		 <td> $contador </td>
		 <td> $nt </td>
		 <td> $rfc n</td>
		 <td> $ae $ne </td>
		 <td align='center'>".(($se=="02")?"Activo":(($se=="00")?"Baja por renuncia":(($se=="01")?"Licencia":(($se=="03")?"Baja por jubilaci&oacute;n":(($se=="04")?"Inactivo por cambio de RFC":(($se=="05")?"Baja por fallecimiento":(($se=="06")?"Inactivo":" ")))))))."</td>
		 <td align='center'>";
if ($se == '02'){ ?>
  <a href="borrar_empleado.php?rfc=<?php echo $rfc;?>&nt=<?php echo $nt;?>&ae=<?php echo $ae;?>&ne=<?php echo $ne;?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar permanentemente los datos de este empleado ?');"> <img border="0" src="../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar de la base de datos este empleado" /></a>
<?php		} else { ?>
  <a href="#" onclick="return confirm('&iquest;El trabajador debe estar activo !');"> <img border="0" src="../img/rechazar.png" width='15' height='15' title="Presione este bot&oacute;n para eliminar de la base de datos este empleado" /></a>
<?php		} 
		echo"</td>
		 </tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
</table>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
</form>
</body>
</html>
