<?php
	/********************************************************
		Modificación de Puestos

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Modificación de Puestos ::.</title>
	</head>

<body>
	<h2 align="center"> Modificaci&oacute;n de Puestos</h2>

<form name="personal" method="post">
<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Puestos existentes en el instituto" >

  <?php

	$consulta_registros="select * from puestos order by clave_puesto"; // order by descripcion_puesto
	$registros=ejecutar_sql($consulta_registros);

	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<table align="center" width="70%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n"><tr><td>&nbsp;</td></tr>
		 	  <tr align="center">
			    <th> No. </th>
			    <th> Clave puesto </th>
			    <th> Puesto </th>
			    <th> Nivel puesto </th>
			    <th> Modificar </th>
			  </tr>';
	else	echo '<tr align="center">
			<td> No exite personal registrado </th>
		  	</tr>';

	while(!$registros->EOF){
		++$contador;
		$clave_puesto = $registros->fields('clave_puesto');
		$descripcion_puesto = $registros->fields('descripcion_puesto');
		$se = $registros->fields('nivel_puesto');

		echo "<tr id='$id'>
		 <td> $contador </td>
		 <td> $clave_puesto </td>
		 <td> $descripcion_puesto </td>
		 <td align='center'>".(($se=="1")?"DIRECCI&Oacute;N":(($se=="2")?"SUBDIRECCI&Oacute;N":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))))."</td>
		 <td align='center'>";?>
  <a href="modifica_puesto.php?clave=<?php echo $clave_puesto;?>&n=<?php echo $se;?>&d=<?php echo$descripcion_puesto;?>" onClick="return confirm('&iquest;Est&aacute; seguro que desea modificar este dato ?');"> <img border="0" src="../../../img/iconos/lista.gif" title="Presione aqu&iacute; para modificar este registro"/></a><?php echo"</td>
		 </tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
	</table>
</form>
</body>
</html>
