<?php
	/********************************************************
		Asignación de Nivel a puesto

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/ 
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 55);

	$Asignar = $_POST['Asignar'];
	$nivel = $_POST['nivel'];
	$puesto = $_POST['puesto'];

if($Asignar=="Asignar"){
	if(($nivel==0)or($puesto==0)){
		echo "<script>alert('Seleccione el puesto y el nivel que desea asignar');</script>";
	}
	else{
		$qry_nivel = "update puestos set nivel_puesto = $nivel where clave_puesto = $puesto";
		$res_nivel = ejecutar_sql($qry_nivel);
		echo "<script>alert('Nivel de puesto actualizado correctamente');</script>";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Asignar Nivel a Puestos ::.</title>
	</head>
<body>

<?php	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";	?>

<h2 align="center"> Asignar Nivel a Puestos</h2>

<form name="personal" action="asigna_nivel_puesto.php" method="post">
	<table align="center" title="Secci&oacute;n para cambiar el nivel a un Puestos">
		<tr>
			<th>Puesto</th>
			<th>Nivel</th>
		</tr>
		<tr>
			<td>
				<select name="puesto" title="Presione aqu&iacute; para seleccionar el puesto">
					<?php 
						$qry_puestos = "select * from puestos order by descripcion_puesto";
						$res_puestos = ejecutar_sql($qry_puestos);
						//$res_puestos2 = ejecutar_sql($qry_puestos);
					
						if(!$res_puestos->rowcount())
							echo "<option value='0' selected> No hay puestos dados de alta </option>";
						else
						{
							echo "<option value='0' selected> -- Seleccione Puesto -- </option>";
							
							while(!$res_puestos->EOF)
							{
								$value = trim($res_puestos->fields("clave_puesto"));
								$nombre = $res_puestos->fields('descripcion_puesto');
								echo "<option value='".$value."'>".$nombre."</option>";
								$res_puestos->MoveNext();
							}
							//echo "<option value='Todos' selected>TODAS LAS CARRERAS</option>";
						}						
					?>
				</select>
			</td>
			<td>
				<select name="nivel" title="Presione aqu&iacute; para seleccionar el nivel del puesto">
				  <option value="0">-- Seleccione Nivel --</option>
				  <option value="1">DIRECCION</option>
				  <option value="2">SUBDIRECCION</option>
				  <option value="3">JEFES DE DEPARTAMENTO</option>
				  <option value="4">JEFES DE AREA, COORDINADORES</option>
				  <option value="5">JEFES DE PROYECTO Y LABORATORIOS</option>
				  <option value="6">SECRETARIA</option>
				  <option value="7">AUXILIAR</option>
				</select>
			</td>
		</tr>
	</table>
<br><br>
	<div align="center">
	  <input type="submit" name="Asignar" class="boton" value="Asignar" title="Presione este bot&oacute;n para guardar los cambios" />
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
	</div>
<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Puestos existentes en el instituto">
		  <tr> <td>&nbsp;</td> </tr>
	  <?php
		$consulta_registros="select * from puestos order by clave_puesto"; // order by descripcion_puesto
		$registros=ejecutar_sql($consulta_registros);

		$id = "non";
		$contador = 0;

	if(!$registros->EOF)
		echo "<tr align='center'>
			<th> No. </th>
			<th> Clave puesto </th>
			<th> Puesto </th>
			<th> Nivel puesto </th>
		  </tr>";
	else	echo '<tr align="center">
			 <td> No exite personal registrado </th>
		      </tr>';

		while(!$registros->EOF){

			$contador = $contador + 1;
			$clave_puesto = $registros->fields('clave_puesto');
			$descripcion_puesto = $registros->fields('descripcion_puesto');
			$se = $registros->fields('nivel_puesto');

			echo "<tr id='$id'>
			 <td> $contador </td>
			 <td> $clave_puesto </td>
			 <td> $descripcion_puesto </td>
			 <td align='center'>".(($se=="1")?"DIRECCI&Oacute;N":(($se=="2")?"SUBDIRECCI&Oacute;N":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":" ")))))))."</td>
			 </tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		?>
		  <tr> <td>&nbsp;</td> </tr>
	</table>
</form>
</body>
</html>
