<?php
	/********************************************************
		Reordenar Clave de Puestos

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 66);

	$Reordenar = $_POST['Reordenar'];

if($Reordenar=="Reordenar"){
		$maximo=0;
		$qry_puestos="select clave_puesto,descripcion_puesto, nivel_puesto from puestos order by descripcion_puesto";
		$res_puestos=ejecutar_sql($qry_puestos);
		
		if($res_puestos->EOF){ echo '<script> alert("No existe registro de puestos en la base de datos"); </script>';	}
		else{
				//Ordenar una vez para eliminar conflictos de clave
				$qry_maximo_puesto="select max(clave_puesto) as maximo from puestos";
				$res_maximo_puesto=ejecutar_sql($qry_maximo_puesto);
				$maximo=$res_maximo_puesto->fields('maximo');	
				
				$nuevo_id=1;
				while(!$res_puestos->EOF){

				$clave_anterior=$res_puestos->fields('clave_puesto');
				$descripcion_puesto=$res_puestos->fields('descripcion_puesto');
				$nivel_puesto=$res_puestos->fields('nivel_puesto');								

				if($nivel_puesto==""){
					$qry_actualiza_puesto="insert into puestos 
					(clave_puesto, descripcion_puesto) values (".($maximo+$nuevo_id)." ,'$descripcion_puesto')";
					}
					else{
					$qry_actualiza_puesto="insert into puestos (clave_puesto, descripcion_puesto, nivel_puesto) 
					values (".($maximo+$nuevo_id).",'$descripcion_puesto', $nivel_puesto)";
					}
					
					$res_actualiza_puesto=ejecutar_sql($qry_actualiza_puesto);			
					$qry_actualiza_personal="update puestos_personal set clave_puesto = ".($maximo+$nuevo_id)." where clave_puesto = $clave_anterior";		
					$res_actualiza_personal=ejecutar_sql($qry_actualiza_personal);
					$qry_borra="delete from puestos where clave_puesto = $clave_anterior";
					$res_borra=ejecutar_sql($qry_borra);	
					$nuevo_id++;
					$res_puestos->MoveNext();

			}//fin while
			//Fin ordenar una vez para eliminar conflictos de clave
			//Ordenamiento final que empieza la numeracion en 1
			
			$qry_puestos="select clave_puesto,descripcion_puesto, nivel_puesto from puestos order by descripcion_puesto";
			$res_puestos=ejecutar_sql($qry_puestos);
			$nuevo_id=1;
			
			while(!$res_puestos->EOF){
	
				$clave_anterior=$res_puestos->fields('clave_puesto');
				$descripcion_puesto=$res_puestos->fields('descripcion_puesto');
				$nivel_puesto=$res_puestos->fields('nivel_puesto');	
			
					if($nivel_puesto==""){
						$qry_inserta_puesto="insert into puestos (clave_puesto, descripcion_puesto) values (".$nuevo_id." ,'$descripcion_puesto')";
						$res_inserta_puesto=ejecutar_sql($qry_inserta_puesto);			
					}
					else{
						$qry_inserta_puesto="insert into puestos (clave_puesto, descripcion_puesto,nivel_puesto) values (".$nuevo_id." ,'$descripcion_puesto', $nivel_puesto)";
						$res_inserta_puesto=ejecutar_sql($qry_inserta_puesto);			
					}
	
					$qry_actualiza_personal="update puestos_personal set clave_puesto = $nuevo_id where clave_puesto = $clave_anterior";
					$res_actualiza_personal=ejecutar_sql($qry_actualiza_personal);
						
					$qry_delete_puesto="delete from puestos where clave_puesto = $clave_anterior";
					$res_delete_puesto=ejecutar_sql($qry_delete_puesto);
					
				$res_puestos->MoveNext();
				$nuevo_id++;			
			}//Fin while
			//Ordenamiento final que empieza la numeracion en 1
	}//fin else
echo '<script> alert("Puestos actualizados con exito"); </script>';			
}//fin if Reordenar

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Reordenar Clave de Puestos ::.</title>
	</head>

<body>
<?php	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";	?>
	<h2 align="center"> Reordenar Clave de Puestos de Personal </h2>

<form name="personal" action="reorganiza_puestos.php" method="post">

	<div align="center">
	  <input type="submit" name="Reordenar" class="boton" value="Reordenar" title="Presione este bot&oacute;n para guardar los cambios"/>
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>

<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Puestos existentes en el instituto">
		  <tr> <td>&nbsp;</td> </tr>
	  
	  <?php
		$consulta_registros="select * from puestos order by descripcion_puesto";
		$registros=ejecutar_sql($consulta_registros);
		$id = "non";
		$contador = 0;

		if(!$registros->EOF)
			echo '<tr align="center">
				    <th> No. </th>
				    <th> Clave puesto </th>
				    <th> Puesto </th>
			      </tr>';
		else	echo '<tr align="center">
				<td> No exite personal registrado </th>
			      </tr>';

		while(!$registros->EOF){
			$contador = $contador + 1;
			$clave_puesto = $registros->fields('clave_puesto');
			$descripcion_puesto = $registros->fields('descripcion_puesto');
			$se = $registros->fields('nivel_puesto');
			echo "<tr id='$id'>
			 <td> $contador </td>
			 <td> $clave_puesto </td>
			 <td> $descripcion_puesto </td>
			 </tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		?>
		  <tr>  <td>&nbsp;</td> </tr>
	</table>
</form>
</body>
</html>
