<?php
	/********************************************************
		Creación de Puestos

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);

	$Crear = $_POST['Crear'];
	$nivel_puesto = $_POST['nivel_puesto'];
	$descripcion_puesto = $_POST['descripcion_puesto'];

	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

$qry_maximo_puesto="select max(clave_puesto) as maximo from puestos";
$res_maximo_puesto=ejecutar_sql($qry_maximo_puesto);
$maximo=$res_maximo_puesto->fields('maximo');	

if($Crear=="Crear"){
	if(($nivel_puesto==0)or($descripcion_puesto==""))
		echo "<script>alert('Escriba la descripción para el nuevo puesto y el nivel que desea asignar');</script>";
	else{   $qry_inserta_puesto="insert into puestos (clave_puesto, descripcion_puesto, nivel_puesto) values (".($maximo+1).",'$descripcion_puesto', $nivel_puesto)";
		$res_inserta_puesto=ejecutar_sql($qry_inserta_puesto);
		echo "<script>alert('El nuevo puesto se agrego correctamente');</script>";
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>

	<title>.:: Creaci&oacute;n de Puestos ::.</title>
</head>

<body>
	<h2 align="center"> Creaci&oacute;n de Puestos</h2>
	<form name="personal" action="agrega_puesto.php" method="post">
	<input name="clave_puesto" type="hidden" id="clave_puesto" value="<?php echo ($maximo+1); ?>" size="20" maxlength="4" />

	<table align="center" title="Secci&oacute;n para crear Puestos en el instituto">
	 <tr>
	  <th>Descripcion de puesto</th>
	  <th>Nivel</th>
	 </tr>
	 <tr>
	  <td><input name="descripcion_puesto" type="text" id="descripcion_puesto" size="50" maxlength="85" onChange="javascript:this.value=this.value.toUpperCase();" title="Teclee la descripci&oacute;n del puesto nuevo"/></td>
	  <td>
	   <select name="nivel_puesto" title="Presione aqu&iacute; para seleccionar el nivel del puesto">
			  <option value="0">-- Seleccione Nivel --</option>
			  <option value="1">DIRECCION</option>
			  <option value="2">SUBDIRECCION</option>
			  <option value="3">JEFES DE DEPARTAMENTO</option>
			  <option value="4">JEFES DE AREA, COORDINADORES</option>
			  <option value="5">JEFES DE PROYECTO Y LABORATORIOS</option>
			  <option value="6">SECRETARIA</option>
			  <option value="7">AUXILIAR</option>
	   </select>
	  </td>
	 </tr>
	</table>
	<br><br>
	<div align="center">
	    <input type="submit" name="Crear" class="boton" value="Crear" title="Presione este bot&oacute;n para guardar los cambios"/>
	    <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
	<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Puestos existentes en el instituto">
		  <tr>  <td>&nbsp;</td> </tr>
	<?php
		$consulta_registros="select * from puestos order by clave_puesto"; // order by descripcion_puesto
		$registros=ejecutar_sql($consulta_registros);

		$id = "non";
		$contador = 0;

	if(!$registros->EOF)
		echo "<tr align='center'>
			    <th> Clave puesto </th>
			    <th> Puesto </th>
			    <th> Nivel puesto </th>
			 </tr>";
	else	echo '<tr align="center">
			 <td> No exite personal registrado </th>
		      </tr>';

		while(!$registros->EOF){
			++$contador;
			$clave_puesto = $registros->fields('clave_puesto');
			$descripcion_puesto = $registros->fields('descripcion_puesto');
			$se = $registros->fields('nivel_puesto');

			echo "<tr id='$id'>
			 <td align='center'> $clave_puesto </td>
			 <td> $descripcion_puesto </td>
			 <td align='center'>".(($se=="1")?"DIRECCIÓN":(($se=="2")?"SUBDIRECCIÓN":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))))."</td>
			 </tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}
	?>
		  <tr> <td>&nbsp;</td> </tr>
	</table>
	</form>
	<? if ($contador > 0) { ?>
		<br><br><br>
		<div align="center">  <a href="../reportes/Puesto.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Puestos"/></a>
		</div>
	<? } ?>
</body>
</html>
