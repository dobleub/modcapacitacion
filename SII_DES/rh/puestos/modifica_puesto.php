<?php
	/********************************************************
		Modificación de Puestos

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);

	$n = $_GET['n'];
	$Actualizar = $_POST['Actualizar'];
	$nivel_puesto = $_POST['nivel_puesto'];
	$clave = $_POST['clave'];
	$descripcion_puesto = $_POST['descripcion_puesto'];

	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";


if($Actualizar=="Actualizar")
	if(($nivel_puesto==0)or($descripcion_puesto==""))
		echo "<script>alert('Escriba la descripcion para el nuevo puesto y el nivel que desea asignar');</script>";
	else{
		$qry_inserta_puesto="update puestos set descripcion_puesto = '$descripcion_puesto', nivel_puesto = $nivel_puesto where clave_puesto = $clave";
		$res_inserta_puesto=ejecutar_sql($qry_inserta_puesto);
		echo "<script>alert('El puesto ha sido modifico correctamente');</script>";
		echo '<script>window.location.href = "modifica_puesto_lista.php" ; </script>';
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>

	<title>.:: Modificaci&oacute;n de Puestos ::.</title>
</head>

<body>

<h2 align="center"> Modificaci&oacute;n de Puestos</h2>

<form name="personal" action="modifica_puesto.php" method="post">
<table align="center"  title="Secci&oacute;n para modificar Puestos en el instituto">
<tr>
	<th>Descripci&oacute;n de puesto</th>
	<th>Nivel</th>
</tr>
<tr>
<td>
	<input name="clave" type="hidden"  value="<?php echo $_GET['clave'];  ?>" />
	<input name="descripcion_puesto" type="text"  size="50" maxlength="85"  value="<?php echo $_GET['d']; ?>" onChange="javascript:this.value=this.value.toUpperCase();"  title="Presione aqu&iacute; para modificar la descripci&oacute;n existente del puesto"/></td>
<td>
	<select name="nivel_puesto" title="Presione aqu&iacute; para seleccionar el nivel del puesto">
			  <option value="1" <?php if ($n=="1"){echo "selected";}?>>DIRECCION</option>
			  <option value="2" <?php if ($n=="2"){echo "selected";}?>>SUBDIRECCION</option>
			  <option value="3" <?php if ($n=="3"){echo "selected";}?>>JEFES DE DEPARTAMENTO</option>
			  <option value="4" <?php if ($n=="4"){echo "selected";}?>>JEFES DE AREA, COORDINADORES</option>
			  <option value="5" <?php if ($n=="5"){echo "selected";}?>>JEFES DE PROYECTO Y LABORATORIOS</option>
			  <option value="6" <?php if ($n=="6"){echo "selected";}?>>SECRETARIA</option>
			  <option value="7" <?php if ($n=="7"){echo "selected";}?>>AUXILIAR</option>
	</select>
</td>
</tr>
</table>
<br><br>

  <div align="center">
    <input type="submit" name="Actualizar" class="boton" value="Actualizar" title="Presione este bot&oacute;n para guardar los cambios" />
    <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
<br><br>
</form>
</body>
</html>
