<?php
	/********************************************************
		encabezado.php

		El encabezado es el archivo que contiene los cuatro menus principales del modulo de recursos humanos 
		SII-MRH y sus respectivos submenus.
		

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		Octubre 2011
	********************************************************/

	//Librerias y funciones
	  require_once("../../includes/config.inc.php");
	  require_once($CFG->funciones_phpDir."/funciones_nombres.php");
	  seguridad('DRH');
	  $web->Seguridad($_SESSION['susr'],8);
?>

<html>
<!-- DW6 -->
<head>

<title>SII :: Home</title>

<link rel="stylesheet" href="<?php echo $CFG->cssDir; ?>/tec_estilo_encabezado.css" type="text/css" />

</head>

<body>

<div id="masthead">
  <table width="800" align="center">
  	<tr>
      <td ><img src="<?php echo $CFG->imgDir; ?>/logo_hoja.jpg" width="230" height="70"  /> </td>
	  	<td align="center">
					<font class="titulo1"><?php echo $CFG->nombre_sistema; ?></font>
					<br>
					<font class="titulo2"><?php echo $CFG->instituto; ?></font>
					<br>
					<font class="titulo2"><?php echo modulo($_SESSION['stipo']); ?></font>
      </td>
	  	<td><img src="<?php echo $CFG->imgDir; ?>/dgest.png" width="100" height="50" /> </td>
  	</tr>
  </table>
	
	<script type='text/javascript'>function Go(){return}</script>
	<script type='text/javascript'>
		
		/****Configurar****/
		var NoOffFirstLineMenus = 11;		// Number of main menu items
		var raiz_modulo = "/modulos/rh/";
		var ruta_imagenes = "/img/menu/";
		var ruta_consultas = "/modulos/cons/";
		var BaseHref="<?php echo $CFG->rootDirServ; ?>";
	
		
	</script>
	
	<script type='text/javascript' src="<?php echo $CFG->funciones_jsDir; ?>/var_menu.js"></script> <!-- M "-->
	
	<script type='text/javascript'>		

		var alto_menus = 22;
		var alto_submenus = 20;
		var ancho_orillas = 5;
		var alineacion_submenus = "left";

		Menu1 = new Array("","",BaseHref + ruta_imagenes + "glbnav_left.gif",0,alto_menus,ancho_orillas,"","","","","","",-1,1,-1,"","");

		var ancho_menu2 = 140;
		Menu2 = new Array("Mantenimiento", "", image, 6, alto_menus, ancho_menu2, "", "", "", "", "", "", -1, 1, -1, "", "Actualizacion de la Base de Datos");
			
			var ancho_sub_menu2 = 200; 	// 5 para poner plazas
			Menu2_1 = new Array("Personal", "", "", 6, alto_submenus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Agregar Nuevo Personal  y Actualizacion de los Datos del Personal");
			
				var ancho_sub_menu2_1 = 160;
				Menu2_1_1 = new Array("Registro (alta)", "", "", 2, alto_menus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Registro de datos");  
				
				var ancho_sub_menu2_1_1 = 180;
				Menu2_1_1_1 = new Array("Informaci�n General",raiz_modulo +"personal/personal.php?accion=nuevo","",0,alto_submenus,ancho_sub_menu2_1_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Agrega un nuevo Empleado");
				Menu2_1_1_2 = new Array("Informaci�n Estudios",raiz_modulo +"personal/estudios_personal_lista.php?todos=t","",0,alto_submenus,ancho_sub_menu2_1_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Agrega un nuevo Empleado");		  
				Menu2_1_2 = new Array("Eliminaci�n (baja)",raiz_modulo +"personal/baja_personal.php?todos=t","",0,alto_submenus,ancho_sub_menu2_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Elimina un empleado de la base de datos");
				Menu2_1_3 = new Array("Modificaciones",raiz_modulo +"personal/modifica_personal.php?todos=t","",0,alto_submenus,ancho_sub_menu2_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Modifica los Datos Generales del Personal");
				Menu2_1_4 = new Array("Asignar Puesto",raiz_modulo +"personal/puestos_personal.php","",0,alto_submenus,ancho_sub_menu2_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Asignaci�n de Puesto a Personal");
				Menu2_1_5 = new Array("Expediente",raiz_modulo +"expediente/seleccione_personal.php","",0,alto_submenus,ancho_sub_menu2_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Asignaci�n de Puesto a Personal");
				Menu2_1_6 = new Array("T�tulos",raiz_modulo +"reportes_plazas_personal/titulos_personal.php","",0,alto_submenus,ancho_sub_menu2_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Creaci�n de t�tulos para el Personal");


			Menu2_2 = new Array("Jefes", raiz_modulo +"jefes/jefes.php", "", 0, alto_submenus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Datos de Jefes de Departamentos");
			
			var ancho_sub_menu2_4 = 330;	
			Menu2_3 = new Array("Reportes", "", "", 15, alto_submenus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Agregar Plazas"); 

				Menu2_3_1 = new Array("Listado General de Personal",raiz_modulo +"reportes_plazas_personal/listado_general_personal.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Listado General de Personal");
				Menu2_3_2 = new Array("Listado seg�n la Situaci�n",raiz_modulo +"reportes_plazas_personal/listado_personal_situacion.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Listado segun la Situaci�n");
				Menu2_3_3 = new Array("Plantilla de Personal",raiz_modulo +"reportes_plazas_personal/plantilla_personal.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Plantilla de Personal");				
				Menu2_3_4 = new Array("Puestos de Personal - Departamento",raiz_modulo +"reportes_plazas_personal/puestos_personal_departamento.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Puestos de acuerdo a Estructura");
				Menu2_3_5 = new Array("Personal con Categoria Docente / Puesto",raiz_modulo +"reportes_plazas_personal/puesto_administrativo.php?tipo=S","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Docentes");
				Menu2_3_6 = new Array("Personal con Categoria Administrativa / Puesto",raiz_modulo +"reportes_plazas_personal/puesto_administrativo.php?tipo=C","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrativos");				
		
				Menu2_3_7 = new Array("Personal con Categoria Docente / Mov 20",raiz_modulo +"reportes_plazas_personal/personal_categoria.php?tipo=D","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Personal con Categoria por Fecha Promoci�n");
				Menu2_3_8 = new Array("Personal con Categoria Administrativa / Mov 20",raiz_modulo +"reportes_plazas_personal/personal_categoria.php?tipo=A","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Personal con Categoria Administrativa");		
				Menu2_3_9 = new Array("Personal con Honorarios",raiz_modulo +"reportes_plazas_personal/personal_honorarios.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Personal con Categoria Honorarios");	
				Menu2_3_10 = new Array("Personal-Tiempo","","",3,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Total de horas del Personal");

				var ancho_sub_menu2_3_10 = 150;
					Menu2_3_10_1= new Array("Tiempo Completo",raiz_modulo +"reportes_plazas_personal/personal_40_horas.php?t=a","",0,alto_submenus,ancho_sub_menu2_3_10,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Personal con Tiempo Completo");
					Menu2_3_10_2= new Array("3/4 de Tiempo",raiz_modulo +"reportes_plazas_personal/personal_40_horas.php?t=b","",0,alto_submenus,ancho_sub_menu2_3_10,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Personal con 3/4 de Tiempo");
					Menu2_3_10_3= new Array("Medio Tiempo",raiz_modulo +"reportes_plazas_personal/personal_40_horas.php?t=c","",0,alto_submenus,ancho_sub_menu2_3_10,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus," Personal con medio Tiempo");


				Menu2_3_11 = new Array("Horas de plaza",raiz_modulo +"reportes_plazas_personal/horas_plaza.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Reporte de Personal");
				Menu2_3_12 = new Array("Edad & G�nero",raiz_modulo +"personal/edad_genero.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Reporte de Personal");								
				Menu2_3_13 = new Array("Grado de Estudios",raiz_modulo +"personal/grado_estudios.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Reporte de Personal");								
				Menu2_3_14 = new Array("Datos Generales",raiz_modulo +"personal/personal_general.php","",0,alto_submenus,ancho_sub_menu2_4,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Reporte de Personal");								
 				Menu2_3_15 = new Array("Personal Detalle", "", "", 3, alto_menus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Clasificaci�n de Categorias");  

				var ancho_sub_menu2_3_8 = 200;
					Menu2_3_15_1= new Array("Administrativos",raiz_modulo +"reportes_plazas_personal/personal_detalle.php?tipo=A","",0,alto_submenus,ancho_sub_menu2_3_8,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Formatos de Plaza");
					Menu2_3_15_2= new Array("Docentes",raiz_modulo +"reportes_plazas_personal/personal_detalle.php?tipo=D","",0,alto_submenus,ancho_sub_menu2_3_8,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Formatos de Plaza");
					Menu2_3_15_3= new Array("Jefes",raiz_modulo +"reportes_plazas_personal/personal_detalle.php?tipo=J","",0,alto_submenus,ancho_sub_menu2_3_8,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Formatos de Plaza");


			Menu2_4 = new Array("Puestos", "", "", 4, alto_menus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Administraci�n de Puestos");  
			var ancho_sub_menu2_5 = 230
				Menu2_4_1 = new Array("Reordenar Clave de Puestos",raiz_modulo +"puestos/reorganiza_puestos.php","",0,alto_submenus,ancho_sub_menu2_5,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Reordenar Clave de Puestos");				
				Menu2_4_2 = new Array("Creaci�n de Puestos",raiz_modulo +"puestos/agrega_puesto.php","",0,alto_submenus,ancho_sub_menu2_5,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Creaci�n de Puestos");
				Menu2_4_3 = new Array("Modificaci�n de Puestos",raiz_modulo +"puestos/modifica_puesto_lista.php","",0,alto_submenus,ancho_sub_menu2_5,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Creaci�n de Puestos");								
				Menu2_4_4 = new Array("Asignaci�n de Nivel a Puesto",raiz_modulo +"puestos/asigna_nivel_puesto.php","",0,alto_submenus,ancho_sub_menu2_5,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Asignaci�n de Nivel a Puesto");
							

// today 141111
				Menu2_5 = new Array("Plazas y Movimientos", "", "", 3, alto_menus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Plazas y Movimientos");  
				
					var ancho_sub_menu2_6 = 230	
					Menu2_5_1 = new Array("Claves Presupuestales",raiz_modulo +"personal/agrega_clave_plaza.php","",0,alto_submenus,ancho_sub_menu2_6,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Registro de Clave presupuestal");
					Menu2_5_2 = new Array("Efecto/Movimiento Personal",raiz_modulo +"reportes_plazas_personal/efecto_movimiento_personal.php","",0,alto_submenus,ancho_sub_menu2_6,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Efectos Movimientos del Personal");
					/*Menu2_5_3 = new Array("Agregar Plaza(s) Personal",raiz_modulo +"personal/agrega_personal_a_plantilla.php","",0,alto_submenus,ancho_sub_menu2_6,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Agregar plaza(s) al Personal");*/
					Menu2_5_3 = new Array("Plantilla de Personal Actualizado",raiz_modulo +"personal/plantilla_personal_lista.php","",0,alto_submenus,ancho_sub_menu2_6,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Plantilla del Personal");

					Menu2_6 = new Array("Honorarios", raiz_modulo +"personal/registro_personal_honorarios.php", "", 0, alto_menus, ancho_sub_menu2, sub_LowBgColor, sub_HighBgColor, sub_FontLowColor, sub_FontHighColor, sub_BorderColor, "", -1, 1, -1, alineacion_submenus, "Personal con Honorarios");  
					/*Menu2_6_1 = new Array("Registro Personal",raiz_modulo +"personal/registro_personal_honorarios.php","",0,alto_submenus,ancho_sub_menu2_3_10,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Registro de Clave presupuestal"); */ //  ancho_sub_menu2_6
					Menu2_6_2 = new Array("---",raiz_modulo +"reportes_plazas_personal/efecto_movimiento_personal.php","",0,alto_submenus,ancho_sub_menu2_6,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Efectos Movimientos del Personal");
		
		
		//Linea de divisi�n
		Menu3 = new Array("|","",image,0,alto_menus,6,"","","#000000","#000000","","",-1,-1,-1,"","");

		// --------- Men at Work
		var ancho_menu4 = 150;
		Menu4 = new Array("Capacitaci�n","",image,5,alto_menus,ancho_menu4,"","","","","","",-1,1,-1,"","Modulo de Capacitaci�n");
			
			var ancho_sub_menu4_0 = 200;
				var ancho_sub_menu4_0_0 = 250;
			
			Menu4_1 = new Array("Personal","","",2,alto_submenus,ancho_sub_menu4_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Mantenimiento de Personal");
				Menu4_1_1 = new Array("Detalles Personal",raiz_modulo +"capacitacion/personal_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar detalles del Personal como: Puesto, Departamento y Escolaridad");
				Menu4_1_2 = new Array("Admin Puestos",raiz_modulo +"capacitacion/subdireccion_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Puestos que seran asignados despues al Personal respectivo");
				
			Menu4_2 = new Array("Capacidades","","",3,alto_submenus,ancho_sub_menu4_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Mantenimiento de cursos");
				Menu4_2_1 = new Array("Admin Capacidades",raiz_modulo +"capacitacion/capacidades_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Capacidades/Cursos posibles que se pueden impartir");
				Menu4_2_2 = new Array("Admin Capacidades Autorizadas",raiz_modulo +"capacitacion/capacidades_autorizadas_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Grupos y Subgrupos de Cursos");
				Menu4_2_3 = new Array("Admin Grupos y subgrupos",raiz_modulo +"capacitacion/capacidades_sg_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Grupos y Subgrupos de Cursos");
			
			Menu4_3 = new Array("Instructor","","",3,alto_submenus,ancho_sub_menu4_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"mantenimiento de horarios");
				Menu4_3_1 = new Array("Admin Instructor",raiz_modulo +"capacitacion/instructor_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Instructor, Insercion/Modificaci&oacute;n/Eliminaci&oacute;n");
				Menu4_3_2 = new Array("Evaluar Instructor",raiz_modulo +"capacitacion/instructor_eval.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Evaluar Instructor antes de la Capacitaci&oacute;n/Curso");
				Menu4_3_3 = new Array("Admin Empresas",raiz_modulo +"capacitacion/instructor_empresa.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Empresas de donde Provienen los Instructores");
				
			Menu4_4 = new Array("Inscripciones a Cursos",raiz_modulo +"capacitacion/capacidades_personal.php","",0,alto_submenus,ancho_sub_menu4_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Mantenimiento de horarios");
			
			Menu4_5 = new Array("Administrar","","",1,alto_submenus,ancho_sub_menu4_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Herramientas Varias");
				Menu4_5_1 = new Array("Barreras desempe�o Personal",raiz_modulo +"capacitacion/otros_admin.php","",0,alto_submenus,ancho_sub_menu4_0_0,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Administrar Barreras que impiden el buen desempe;o del Personal");



		//Linea de divisi�n
		Menu5 = new Array("|","",image,0,alto_menus,6,"","","#000000","#000000","","",-1,-1,-1,"","");


		var ancho_menu6 = 150;
		Menu6 = new Array("Mensajer�a","",image,3,alto_menus,ancho_menu6,"","","","","","",-1,1,-1,"","Mensajer�a del Sistema");

			var ancho_sub_menu6_1 = 205;
			Menu6_1 = new Array("Recordatorios",raiz_modulo +"mensajeria/recordatorios_de_fechas.php","",0,alto_submenus,ancho_sub_menu6_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Cambia la contrase�a actual del Usuario");
			Menu6_2 = new Array("Antiguedad",raiz_modulo +"mensajeria/antiguedad_personal.php?todos=t","",0,alto_submenus,ancho_sub_menu6_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Consulte la antiguedad del personal en distintos sectores");
			Menu6_3 = new Array("�Qui�n cumple a�os este mes?",raiz_modulo +"mensajeria/cumpleanos.php","",0,alto_submenus,ancho_sub_menu6_1,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Consulte la antiguedad del personal en distintos sectores");

		//Linea de divisi�n
		Menu7 = new Array("|","",image,0,alto_menus,6,"","","#000000","#000000","","",-1,-1,-1,"","");
		

		var ancho_menu8 = 130;
		Menu8 = new Array("Utiler�as","",image,2,alto_menus,ancho_menu8,"","","","","","",-1,1,-1,"","Utilerias Varias del Sistema");

			var ancho_sub_menu8 = 205
			Menu8_1 = new Array("Cambio de Contrase�a","/cambio_contrasena.php","",0,alto_submenus,ancho_sub_menu8,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Cambia la contrase�a actual del Usuario");
			Menu8_2 = new Array("Informaci�n",raiz_modulo +"/informacion.php","",0,alto_submenus,ancho_sub_menu8,sub_LowBgColor,sub_HighBgColor,sub_FontLowColor,sub_FontHighColor,sub_BorderColor,"",-1,1,-1,alineacion_submenus,"Cambia la contrase�a actual del Usuario");
		
		//Linea de divisi�n
		Menu9 = new Array("|","",image,0,alto_menus,6,"","","#000000","#000000","","",-1,-1,-1,"","");

		var ancho_menu10 = 150;
		Menu10=new Array("Cerrar Sesi�n", "/cerrar_sesion.php",image,0,alto_menus,ancho_menu10,"","","","","","",-1,1,-1,"","Termina una Sesi�n de la Cuenta Activa en el Sistema");
		
		
		Menu11=new Array("","",BaseHref + ruta_imagenes + "glbnav_right.gif",0,alto_menus,ancho_orillas,"","","","","","",-1,1,-1,"","");

	</script>
 	
	<script type='text/javascript' src="<?php echo $CFG->funciones_jsDir; ?>/menu.js"></script> <!-- M "-->
	
	<noscript>Tu Navegador no soporta Java Script</noscript>

</div>

</body>
</html>
