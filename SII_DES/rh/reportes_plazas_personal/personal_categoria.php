<?php
	/********************************************************
		Personal Docente/Administrativo con Movimiento 20 (interinato)

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 62);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$tipo= $_GET['tipo'];
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal ::.</title>
	</head>
<body>
<?php
	if ($tipo=='D'){	$titulo = "Personal con Categoria Docente con Movimiento 20";	$tipo_movimiento= 'D';	}
	else{ 			$titulo = "Personal con Categoria Administrativa con Movimiento 20";	$tipo_movimiento= 'A';	} 
		// Empleados Licencia, Activos, 20, A, D de dbo.personal, dbo.plantilla_personal, dbo.puestos_personal, dbo.estudios_personal, dbo.movimiento_personal
		$consulta_registros="select distinct(P.rfc), P.apellidos_empleado, P.nombre_empleado, EP.nivel_estudios, MP.movimiento, inicio_sep, status_empleado
		from personal P, claves_presupuestales_personal CPP, estudios_personal EP, movimiento__personal MP where 
		(status_empleado = '01' or status_empleado = '02') 
		and P.rfc =  MP.rfc
		and id_mov = id_movimiento 
		
	    	and EP.rfc = P.rfc
	    	and MP.movimiento = 20 and MP.estatus = 1
	    	and P.nombramiento = '$tipo_movimiento'
		order by apellidos_empleado, nombre_empleado"; // puestos_personal PU, and PU.rfc = P.rfc 
?>
<h2 align="center"><?php echo $titulo;?> (Licencia/Activos)</h2>

  <?php
	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<table align="center" width="90%" title="Lista de Personal con Movimiento 20" border=0>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>  
		  <tr align="center" width="90%">
			<th width="2%"> No. </th>
			<th width="8%"> RFC </th>
			<th> Nombre completo </th>			
			<th width="2%"> Horas </th>						
			<th width="7%"> Efectos iniciales </th>			
			<th width="7%"> Efectos finales </th>
			<th width="20%"> Plaza </th> '.(($tipo!='D')?'<th> Nivel </th> ':'').
		        '<th width="6%"> Inicio SEP </th>
			<th width="7%"> Años labores</th>
		  </tr>';
	else   echo '	<table align="center"  title="No existe personal con Movimiento 20">
			  <tr align="center">
				<td> No exite personal registrado </th>
			  </tr>';
	//Fin primer registro
	
	$registros->MoveFirst();
	while(!$registros->EOF){
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$nivel = $registros->fields('nivel_estudios');
		
		/*$clave = $registros->fields('clave_area');
	
		$area="select descripcion_area from organigrama where clave_area='$clave'";
			$res_area=ejecutar_sql($area);
			$descripcion_area = $res_area->fields('descripcion_area');
		
		$qry_puesto = "select P.descripcion_puesto from puestos_personal PP, puestos P where PP.rfc = '$rfc' and PP.clave_puesto = P.clave_puesto";
			$res_puesto = ejecutar_sql($qry_puesto);
			$puesto = $res_puesto->fields('descripcion_puesto');*/
	
		//Mostrar las plazas
		$t_nom = ($tipo=='D')?'D':'A';
			// Empleados Activos, Incativos, 20, A, D de dbo.personal, dbo.plantilla_personal, dbo.puestos_personal, dbo.estudios_personal, dbo.movimiento_personal
			$qryp ="select distinct(P.rfc), P.apellidos_empleado, P.nombre_empleado, EP.nivel_estudios, MP.movimiento, inicio_sep, status_empleado
				from personal P, claves_presupuestales_personal CPP,  estudios_personal EP, movimiento__personal MP where 
				(status_empleado = '01' or status_empleado = '02')

				and P.rfc =  MP.rfc
				and id_movimiento = id_mov
				
    				and EP.rfc = P.rfc
    				and MP.movimiento = 20 and MP.estatus = 1
    				and P.nombramiento = '$t_nom'
    				and P.rfc = '$rfc'";  //  order by apellidos_empleado, nombre_empleado   --puestos_personal PU, and PU.rfc = P.rfc
		
		$select_personal=ejecutar_sql($qryp); 
		
		if(!$select_personal->EOF){ 
		
			$contador = $contador + 1;
			$anos = date("Y") - intval(substr($registros->fields('inicio_sep'), 0,4 ) );
				
			$consulta_mov="select * from movimiento__personal where rfc = '$rfc'";
			$movimientos=ejecutar_sql($consulta_mov);
				$movimiento = $movimientos->fields('movimiento');
				//$ei = $movimientos->fields('e_iniciales');
				//$ef = $movimientos->fields('e_finales');

			$qry_nivel="select clave_puesto from puestos_personal where rfc='$rfc'";
			$res_nivel=ejecutar_sql($qry_nivel);

			if (!$res_nivel->EOF){
				$idpuesto = $res_nivel->fields('clave_puesto');
				$qry_nivel2="select descripcion_puesto, nivel_puesto from puestos where clave_puesto=$idpuesto";
				$res_nivel2=ejecutar_sql($qry_nivel2);
					$descripcion_puesto = $res_nivel2->fields('descripcion_puesto');
					$se = $res_nivel2->fields('nivel_puesto');
				}

			$qry_horas ="select sum(horas_clave_plaza) as horas,count(horas_clave_plaza) as plazas from claves_presupuestales CP, claves_presupuestales_personal CPP, movimiento__personal MP where CP.id_clave = CPP.id_clave and id_movimiento=id_mov and MP.rfc='$rfc' and CPP.estatus = 1 and MP.movimiento = 20";  
				$plazas_por_persona = ejecutar_sql($qry_horas); 
				$horas = $plazas_por_persona->fields("horas");
		
				/*if ($horas < 10){$horas = "0".$horas;}
				if (($horas >= 20)&&($tipo =="D")){$horas = "00";}
				if (($horas == 36)&&($tipo =="A")){$horas = "00";}*/

 

		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
			 <td align='center'> $contador </td>
			 <td align='center'>".$registros->fields('rfc')."</td>				
			 <td> $ae $ne </td>";
			echo "<td align='center'> $horas </td>		
			 <td align='center'>";
				$consulta_e_iniciales="select CPP.id_clave, e_iniciales, e_finales from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registro_e_iniciales=ejecutar_sql($consulta_e_iniciales);
				while(!$registro_e_iniciales->EOF){
					$ei= $registro_e_iniciales->fields('e_iniciales');

					$registro_e_iniciales->MoveNext();
					echo $ei." <br>";
					}
			echo "</td>
			 <td align='center'>";
				$consulta_e_finales="select CPP.id_clave, e_iniciales, e_finales from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registro_e_finales=ejecutar_sql($consulta_e_finales);
				while(!$registro_e_finales->EOF){
					$ef= $registro_e_finales->fields('e_finales');

					$registro_e_finales->MoveNext();
					echo $ef." <br>";
					}
			echo "</td>
			 <td>";
				$consulta_clave="select CPP.id_clave, CPP.categoria from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registros_clave=ejecutar_sql($consulta_clave);
				while(!$registros_clave->EOF){
					$clave_presupuesto= $registros_clave->fields('id_clave');
		  			$qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
		  			$res_clave_p=ejecutar_sql($qry_clave_presupuesto);
		  				$clave_ap=$res_clave_p->fields('clave_antes_punto');
		  				$clave_dp=$res_clave_p->fields('clave_despues_punto');
		 				$horas_cp=$res_clave_p->fields('horas_clave_plaza');

					$registros_clave->MoveNext();
					echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
					}
			echo  "</td>";
				if ($tipo!='D'){
			echo "<td>".( ($se=="1")?"DIRECCIÓN":(($se=="2")?"SUBDIRECCIÓN":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))) )."</td>"; /*$nivel*/ }
			echo "<td align='center'>".$registros->fields('inicio_sep')."</td>
			 <td align='center'> $anos </td>
			 </tr>";
		$activar_flag = 1;
		}else {
			echo "<tr id='$id'>
			 <td align='center'> $contador </td>
			 <td align='center'>".$registros->fields('rfc')."</td>				
			 <td> $ae $ne </td>";
			echo "<td align='center'> $horas </td>		
			 <td align='center'>";
				$consulta_e_iniciales="select CPP.id_clave, e_iniciales, e_finales from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registro_e_iniciales=ejecutar_sql($consulta_e_iniciales);
				while(!$registro_e_iniciales->EOF){
					$ei= $registro_e_iniciales->fields('e_iniciales');

					$registro_e_iniciales->MoveNext();
					echo $ei." <br>";
					}
			echo "</td>
			 <td align='center'>";
				$consulta_e_finales="select CPP.id_clave, e_iniciales, e_finales from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registro_e_finales=ejecutar_sql($consulta_e_finales);
				while(!$registro_e_finales->EOF){
					$ef= $registro_e_finales->fields('e_finales');

					$registro_e_finales->MoveNext();
					echo $ef." <br>";
					}
			echo "</td>
			 <td>";
				$consulta_clave="select CPP.id_clave, CPP.categoria from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc =MP.rfc and id_mov = id_movimiento and PE.rfc ='$rfc' and CPP.estatus = 1 and MP.movimiento = 20 order by id_clave_presupuestal_personal desc";
				$registros_clave=ejecutar_sql($consulta_clave);
				while(!$registros_clave->EOF){
					$clave_presupuesto= $registros_clave->fields('id_clave');
		  			$qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
		  			$res_clave_p=ejecutar_sql($qry_clave_presupuesto);
		  				$clave_ap=$res_clave_p->fields('clave_antes_punto');
		  				$clave_dp=$res_clave_p->fields('clave_despues_punto');
		 				$horas_cp=$res_clave_p->fields('horas_clave_plaza');

					$registros_clave->MoveNext();
					echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
					}
			echo  "</td>";
				if ($tipo!='D'){
			echo "<td>".( ($se=="1")?"DIRECCIÓN":(($se=="2")?"SUBDIRECCIÓN":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))) )."</td>"; /*$nivel*/ }
			echo "<td align='center'>".$registros->fields('inicio_sep')."</td>
			 <td align='center'> $anos </td>
			 </tr>";
		}

			$select_personal->Movenext();
			$id = ($id=="non")?"par":"non";	
		}//fin si
		$registros->MoveNext();
	}

if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=8 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Doc_admin_mov_20.pdf.php?t=<?php echo $tipo;?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: <?php echo (($tipo=='A')?'Administrativos':'Docentes'); ?> " /></a>
	</div>
	<? } ?>
</body>
</html>
