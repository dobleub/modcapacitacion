<?php
	/********************************************************
		Plaza(s) Personal Historial

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 

	$volver = "efecto_movimiento_personal.php";
	$rfc=$_GET['idp'];
	$mov = $_GET['m'];  //parametro que indica el movmiento actual
	$flag=$_GET['a'];	// bandera que indica si esta activo o inactivo el movimiento 
?>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />

		<h2 align="center">Historial de Plazas del Empleado: <?php echo $rfc; ?></h2>

<form method="post" action="" name="datos" >
<br><br>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" title="Tabla que lista el historial de plazas del empleado">
  <tr>
    <td>&nbsp;</td> 
  </tr>
<?php 	$consulta_id="select id_clave_presupuestal_personal, clave_antes_punto, clave_despues_punto, horas_clave_plaza, descripcion_categoria, CPP.fecha_registro, CPP.hora_registro, CPP.estatus, movimiento, 
e_iniciales, e_finales, MP.estatus as status
from movimiento__personal MP, claves_presupuestales_personal CPP, claves_presupuestales CP, categorias C 
where MP.id_mov = CPP.id_movimiento and MP.rfc='$rfc' and 
CP.id_clave = CPP.id_clave and CPP.categoria = C.categoria  
order by id_clave_presupuestal_personal desc";

	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;
	if(!$registros->EOF)
		echo '<tr><td>&nbsp;</td></tr><tr>
			    <th align="center" width="4%">Contador</th>
			    <th>Clave presupuestal</th>
			    <th>Categoria</th>
			    <th align="center" width="7%">Fecha Registro</th>
			    <th align="center" width="7%">Hora Registro</th>
			    <th align="center" width="4%">Estatus Clave</th>
			    <th width="10%">Movimiento</th>
			    <th align="center" width="7%">Efectos Iniciales</th>
			    <th align="center" width="7%">Efectos Finales</th>
			    <th>Status Mov.</th>
			  </tr>';
	else	echo '<tr align="center">
			<td> Este empleado no ha tenido plaza(s) Asignada(s) a alg&uacute;n movimiento</th>
		  	</tr>';

		while(!$registros->EOF){
			$contador = $contador + 1;

			$clave_antes_punto = $registros->fields('clave_antes_punto');
			$clave_despues_punto = $registros->fields('clave_despues_punto');
			$horas_clave_plaza = $registros->fields('horas_clave_plaza');

			$clave_presupuestal = $clave_antes_punto.'.'.$clave_despues_punto.' C/'.$horas_clave_plaza.' HRS.';

			$descripcion_categoria = $registros->fields('descripcion_categoria');
			$fecha_registro = $registros->fields('fecha_registro');
			$hora_registro = $registros->fields('hora_registro');
			$estatus = $registros->fields('estatus');

			$movimiento = $registros->fields('movimiento');
			$e_iniciales = $registros->fields('e_iniciales');
			$e_finales = $registros->fields('e_finales');
			$status = $registros->fields('status');

			if ($estatus == 1) // Si estatus esta activo de la clave presupuestal asignada al personal entonces...
				if ($movimiento == $mov)
				echo "<tr bgcolor='D42A2A'>
					 <td align='center'>".$contador."</td>

					 <td align='center'>".$clave_presupuestal."</td>
					 <td>".$descripcion_categoria."</td>
					 <td align='center'>".$fecha_registro."</td>
					 <td align='center'>".$hora_registro."</td>
					 <td align='center'>".(($estatus==1)?'Activo':'Inactivo')."</td>

					 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
					 <td align='center'>".$e_iniciales."</td>
					 <td align='center'>".$e_finales."</td>
					 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
				     </tr>";
				else
				echo "<tr>
					 <td align='center'>".$contador."</td>

					 <td align='center'>".$clave_presupuestal."</td>
					 <td>".$descripcion_categoria."</td>
					 <td align='center'>".$fecha_registro."</td>
					 <td align='center'>".$hora_registro."</td>
					 <td align='center'>".(($estatus==1)?'Activo':'Inactivo')."</td>

					 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
					 <td align='center'>".$e_iniciales."</td>
					 <td align='center'>".$e_finales."</td>
					 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
				     </tr>";
			else
			echo "<tr id='$id'>
				 <td align='center'>".$contador."</td>

				 <td align='center'>".$clave_presupuestal."</td>
				 <td>".$descripcion_categoria."</td>
				 <td align='center'>".$fecha_registro."</td>
				 <td align='center'>".$hora_registro."</td>
				 <td align='center'>".(($estatus==1)?'Activo':'Inactivo')."</td>

				 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
				 <td align='center'>".$e_iniciales."</td>
				 <td align='center'>".$e_finales."</td>
				 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
			     </tr>";

			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}
 	if ($contador > 0) { 
		echo '<tr><td>&nbsp;</td></tr>';
		if ($flag == 2){
			echo '<tr><td colspan = "10"><i> * La filas blancas indican las claves presupesupuestales activas </i></td></tr>';
			echo '<tr><td colspan = "7"><i> * Las filas rojas indican la(s) clave(s) presupuestal(es) actual(es) activ(as) de este movimiento</i></td></tr>';
		}
		echo '<tr><td colspan = "10"><i> * El orden de esta lista es de la &uacute;ltima clave presupuestal registrada (m&aacute;s reciente) a la m&aacute;s antigua </i></td></tr>';
		}
	?>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
</table>
	<br>
	<br>
	<div align="center">
		<input type="button" value="Cancelar" class="boton" onclick="javascript:window.location = '<?php echo $volver; ?>'" />
	</div>

</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Historial_de_plazas.pdf.php?idp=<?php echo $rfc; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Historial Plazas"/></a>
	</div>
	<? } ?>
