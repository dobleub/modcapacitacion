<?php
	/********************************************************
		Personal con Categoria Docente / Puesto
		Personal con Categoria Administrativa / Puesto

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 61);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$tipo= $_GET['tipo'];
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal con Categoria Docente/Administrativa ::.</title>
	</head>
<body>
	<?php if ($tipo=='S')	$titulo = "Personal con Puesto y Plaza Docente"; else if ($tipo=='C')	$titulo = "Personal con Puesto y Plaza Administrativa"; ?>
<h2 align="center"><?php echo $titulo;?> (Licencia/Activos)</h2>

  <?php	// Cuando sea Licencia, Activo, A, D, 10, 95 de dbo.personal y dbo.movimiento_personal

	// Tienen que ser base y tener un puesto
	$consulta_registros="select distinct(P.rfc), apellidos_empleado, nombre_empleado, sexo_empleado, nombramiento, clave_area, status_empleado, status_empleado from personal P, movimiento__personal MP where (status_empleado = '01' or status_empleado = '02') and (nombramiento='A' or nombramiento='D') and (MP.movimiento = 10 or MP.movimiento = 95) and P.rfc = MP.rfc and MP.estatus =1 order by apellidos_empleado, nombre_empleado, P.rfc";
	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<table align="center" width="90%" title="Listado con Tipo de Plaza"><tr><td>&nbsp;</td></tr>
	 		<tr align="center" width="100%">
				<th> No. </th>
				<th> RFC </th>
				<th> Nombre completo </th>	
				<th width="5%"> Sexo </th>		
				<th> Departamento </th>
				<th width="15%"> Puesto </th>
				<th width="10%"> Nivel m&Aacute;ximo de estudios</th>			
				<th width="20%"> Plaza(s) </th>
				<th> Movimiento(s) </th>	
			  </tr>';
	else	echo '	<table align="center" title="No hay personal con Tipo de Plaza">
			  <tr align="center">
				<td> No exite personal registrado </th>
			  </tr>';

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){

		$nom = $registros->fields('nombramiento');
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		//$nivel = $registros->fields('nivel_estudios');
		$sexo = $registros->fields('sexo_empleado');
		$estatus = $registros->fields('status_empleado');	

		// Inicio nivel de estudios
			$qry_estudios="select nivel_estudios, grado_maximo_estudios from estudios_personal where rfc='$rfc'";
			$res_estudios=ejecutar_sql($qry_estudios);
			$nivel = $res_estudios->fields('nivel_estudios');
			$grado = $res_estudios->fields('grado_maximo_estudios');
			if (($nivel == null) and ($grado == null)){
				/*personal		--> nivel_estudios, grado_maximo_estudios
				estudios_personal	--> nivel_estudios, grado_maximo_estudios
				nivel_de_estudios	--> nivel_estudios, descripcion_nivel_estudios
				nivel_escolar		--> nivel_escolar, descripcion_nivel*/
				$descripcion_nivel_estudios = 'Sin Registro';
			}
			else {	$qry_nivel_estudios = "select descripcion_nivel_estudios from nivel_de_estudios where nivel_estudios='$nivel'";
				$res_nivel_estudios = ejecutar_sql($qry_nivel_estudios);
				if ($res_nivel->EOF){
					$qry_nivel_escolar="select descripcion_nivel from nivel_escolar where nivel_escolar='$grado'";
					$res_nivel_escolar=ejecutar_sql($qry_nivel_escolar);
					$descripcion_nivel_estudios = $res_nivel_escolar->fields('descripcion_nivel');
					}
				else{ $descripcion_nivel_estudios = $res_nivel_estudios->fields('descripcion_nivel_estudios'); }
			}
		// Fin Nivel de estudios

		$clave = $registros->fields('clave_area');
		
		$area="select descripcion_area from organigrama where clave_area='$clave'";
		$res_area=ejecutar_sql($area);
		$descripcion_area = $res_area->fields('descripcion_area');
		//
		$qry_puesto = "select P.descripcion_puesto from puestos_personal PP, puestos P where PP.rfc = '$rfc' and PP.clave_puesto = P.clave_puesto";
		$res_puesto = ejecutar_sql($qry_puesto);

		if ($res_puesto->fields('descripcion_puesto') == null)	$puesto = 'Sin Registro';
		else $puesto = $res_puesto->fields('descripcion_puesto');
	
		//Mostrar las plazas
		$tipo_nombramiento = (($tipo == 'S')?'D':(($tipo == 'C')?'A':'X'));
			// de dbo.personal, dbo.plantilla_personal, dbo.puestos_personal, dbo.movimiento_personal
$qryp ="select PE.rfc from personal PE, movimiento_personal MP, organigrama O, puestos_personal PP, puestos PU where PE.nombramiento = '$tipo_nombramiento' and PE.rfc = '$rfc' and PE.rfc = MP.rfc and O.clave_area = PE.clave_area and PP.rfc = PE.rfc and PP.clave_puesto = PU.clave_puesto";
// select distinct(PE.rfc) from personal PE, movimiento__personal MP, claves_presupuestales_personal CPP, puestos_personal PU where PE.rfc = MP.rfc and id_mov = id_movimiento and PE.rfc = PU.rfc and PE.nombramiento='$tipo_nombramiento' and PE.rfc='$rfc'
		$personal=ejecutar_sql($qryp); 
		
		if(!$personal->EOF){
			$contador = $contador + 1;
			
		if ( $estatus == 1){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
			 <td align='center'> $contador </td>
			 <td align='center'> $rfc </td>
			 <td> $ae $ne </td>
			 <td align='center'>".(($sexo=='F')?'Femenino':'Masculino')."</td>		
			 <td> $descripcion_area </td>
			 <td> $puesto </td>
			 <td> $descripcion_nivel_estudios </td>
			 <td>";
			
		  	$consulta_clave="select CPP.id_clave from claves_presupuestales_personal CPP, movimiento__personal MP where MP.rfc ='$rfc' and id_mov = id_movimiento and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){

			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}		
			echo "</td><td align='center'>";
			$consulta_movimiento="select movimiento from movimiento__personal MP where MP.rfc ='$rfc' and MP.estatus = 1 and movimiento != 20";
			$registros_movimientos=ejecutar_sql($consulta_movimiento);
			while(!$registros_movimientos->EOF){
				$movimiento = $registros_movimientos->fields('movimiento');
				$registros_movimientos->MoveNext();
				echo ' -'.$movimiento." ";
			}		
			echo "</td></tr>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>
			 <td align='center'> $contador </td>
			 <td align='center'> $rfc </td>
			 <td> $ae $ne </td>
			 <td align='center'>".(($sexo=='F')?'Femenino':'Masculino')."</td>		
			 <td> $descripcion_area </td>
			 <td> $puesto </td>
			 <td> $descripcion_nivel_estudios </td>
			 <td>";
			
		  	$consulta_clave="select CPP.id_clave from claves_presupuestales_personal CPP, movimiento__personal MP where MP.rfc ='$rfc' and id_mov = id_movimiento and CPP.estatus = 1 and movimiento != 20 order by id_clave_presupuestal_personal desc";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){

			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}		
			echo "</td><td align='center'>";
			$consulta_movimiento="select movimiento from movimiento__personal MP where MP.rfc ='$rfc' and MP.estatus = 1 and movimiento != 20";
			$registros_movimientos=ejecutar_sql($consulta_movimiento);
			while(!$registros_movimientos->EOF){
				$movimiento = $registros_movimientos->fields('movimiento');
				$registros_movimientos->MoveNext();
				echo ' -'.$movimiento." ";
			}		
			echo "</td></tr>";
		    }
			$id = ($id=="non")?"par":"non";
		}//fin si
		$registros->MoveNext();
	}
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=8 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
	</div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Doc_admin_con_plazas.pdf.php?t=<?php echo $tipo_nombramiento;?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: <?php echo (($tipo_nombramiento=='A')?'Administrativos':'Docentes'); ?> " /></a>
	</div>
	<? } ?>
</body>
</html>
