<?php
	/********************************************************
		Personal Medio Tiempo; 3/4 Tiempo; Tiempo Completo

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	
	$tiempo = $_GET['t'];
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

	if ($tiempo == 'a') {$titulo = 'Tiempo Completo'; $time = 40;}
	if ($tiempo == 'b') {$titulo = '3/4 de Tiempo'; $time = 30;}
	if ($tiempo == 'c') {$titulo = 'Medio Tiempo'; $time = 20;}
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal Tiempo Completo ::.</title>
	</head>
<body>

	<h2 align="center">Listado de Personal de <?php echo $titulo;?></h2>
  <?php
	$consulta_registros="select PE.rfc, PE.nombre_empleado, PE.apellidos_empleado, PE.sexo_empleado, PE.nombramiento, CPP.id_clave_presupuestal_personal, CPP.id_clave, CPP.categoria, CP.clave_antes_punto, CP.clave_despues_punto, CP.horas_clave_plaza, status_empleado from personal PE, claves_presupuestales_personal CPP, claves_presupuestales CP, movimiento__personal MP where (PE.status_empleado = '01' or PE.status_empleado = '02')   and PE.rfc = MP.rfc and MP.id_mov = CPP.id_movimiento
and CP.id_clave =CPP.id_clave and CP.horas_clave_plaza = $time and (MP.movimiento =95 or MP.movimiento=10) 
order by PE.sexo_empleado,PE.fecha_nacimiento, PE.nombramiento, PE.apellidos_empleado, PE.nombre_empleado, CPP.id_clave";

	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	$clave_inicial = $registros->fields('clave_area');

	if(!$registros->EOF)
		echo '<table align="center" width="80%" title="Lista de personal de tiempo completo"><tr><td>&nbsp;</td></tr>
		  <tr align="center" width="100%">
			<th width="4%"> No. </th>
			<th> Nombre completo </th>			
			<th width="5%"> Edad </th>
			<th width="7%"> Sexo </th>	
			<th width="10%"> Tipo </th>
			<th> Movimiento(s) </th>
			<th> Clave </th>	
		       </tr> ';
	else	echo '<table align="center" title="No hay personal de tiempo completo">
		  <tr align="center">
			<td> No exite personal registrado, Consulte: Mantenimiento > Plazas y Movimientos > Efecto/Movimiento Personal</th>
		  </tr>';
	
	//Fin primer registro
	$registros->MoveFirst();
	while(!$registros->EOF){
		++$contador;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$sexo = $registros->fields('sexo_empleado');
		$nom = $registros->fields('nombramiento');

		$rfc_sin=substr($rfc,-9);  // ejemplo cusm890901 asc
		$fecha=substr($rfc_sin,0,6);
		
		if (substr($rfc_sin,0,1) > 1)	$anio=intval(substr($fecha,0,2))+1900;
		else	$anio=intval(substr($fecha,0,2))+2000;

		$edad=date("Y",time())-$anio;

		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
		echo "<tr bgcolor='#FAAC58'>
		 <td align='center'> $contador </td>
		 <td> $ae $ne </td>		
		 <td align='center'> $edad </td>
		 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td>
		 <td>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
		 <td align='center'>";

			$consulta_movimiento="select movimiento from movimiento__personal MP where MP.rfc ='$rfc' and MP.estatus = 1";
			$registros_movimientos=ejecutar_sql($consulta_movimiento);
			while(!$registros_movimientos->EOF){
				$movimiento = $registros_movimientos->fields('movimiento');
				$registros_movimientos->MoveNext();
				echo ' -'.$movimiento." ";
			}		
		echo "</td><td>";
			$consulta_clave="select CPP.id_clave from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and MP.rfc ='$rfc' and id_mov = id_movimiento and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			//$consulta_clave="select PP.id_plantilla_personal, PP.id_clave, PP.categoria from personal PE, plantilla_personal PP where PE.rfc = PP.rfc and PE.rfc ='$rfc' order by PP.id_clave";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){
			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto and horas_clave_plaza = $time";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');
				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}
		echo "</td>
		 </tr>";
		$activar_flag = 1;
		} else {
		echo "<tr id='$id'>
		 <td align='center'> $contador </td>
		 <td> $ae $ne </td>		
		 <td align='center'> $edad </td>
		 <td align='center'>".(($sexo == 'M' )?"Masculino":"Femenino")."</td>
		 <td>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
		 <td align='center'>";

			$consulta_movimiento="select movimiento from movimiento__personal MP where MP.rfc ='$rfc' and MP.estatus = 1";
			$registros_movimientos=ejecutar_sql($consulta_movimiento);
			while(!$registros_movimientos->EOF){
				$movimiento = $registros_movimientos->fields('movimiento');
				$registros_movimientos->MoveNext();
				echo ' -'.$movimiento." ";
			}		
		echo "</td><td>";
			$consulta_clave="select CPP.id_clave from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and MP.rfc ='$rfc' and id_mov = id_movimiento and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			//$consulta_clave="select PP.id_plantilla_personal, PP.id_clave, PP.categoria from personal PE, plantilla_personal PP where PE.rfc = PP.rfc and PE.rfc ='$rfc' order by PP.id_clave";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){
			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto and horas_clave_plaza = $time";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');
				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}
		echo "</td>
		 </tr>";
		}

		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=6 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Personal_40_horas.pdf.php?t=<? echo $time;?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal de Tiempo Completo" /></a>
	</div>
	<? } ?>
</body>
</html>
