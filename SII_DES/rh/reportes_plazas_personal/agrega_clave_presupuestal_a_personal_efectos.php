<?php
	/********************************************************
		Efectos de la Clave presupuestal asignada al Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH');

// FUNCIONES 301111

	function num_quincena ($dia, $mes)
	{  if ($dia <= 15){ $quincena = (($mes-1)*2)+1;} else {if ($dia > 15){ $quincena = $mes*2;}}
   	  return $quincena;
	}

	function antiguedad_en_anos_quincenas ($parametro_quincena, $parametro_ano, $quincena_actual, $ano_actual)
	{
		if ($parametro_ano <= $ano_actual) {
		  if (($ano_actual - $parametro_ano) >= 1){   //2012-2008 = 4  16
		    if ($parametro_quincena < $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = $quincena_actual-$parametro_quincena;} //
	   	    if ($parametro_quincena == $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = 0;}
		    if ($parametro_quincena > $quincena_actual){ $anos = $ano_actual-$parametro_ano-1; $quincenas = 24-($parametro_quincena-$quincena_actual);}
			}
		  else {$anos = 0; $quincenas = $quincena_actual - $parametro_quincena;}
		  return $anos." a&ntilde;o(s) y ".$quincenas." quincena(s)";		
		}
		else return "Datos erroneos";
	}  

	$quincena_actual =  num_quincena (date ('d'), date ('m'));

// FIN FUNCIONES

	// Datos para insertar una nueva plaza/ Clave presupuestal a un empleado
	$Crear = $_POST['Crear'];
	$rfc = $_POST['rfc_personal'];
	$clave_presupuestal = $_POST['clave_presupuestal'];
	$categoria_elegida = $_POST['categorias'];
	$mov = $_POST['mov'];

	$regresar = "window.location = 'agrega_clave_presupuestal_a_personal.php?a=2'";
	$volver = "agrega_clave_presupuestal_a_personal.php?a=2";

	// Antiguedad en el plantel
	$consulta_registros="select apellidos_empleado, nombre_empleado, status_empleado, substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel from personal where ( rfc = '".$rfc."' )";
	$registros1=ejecutar_sql($consulta_registros);
		$ap = $registros1->fields('ano_plantel');
		$sp = $registros1->fields('sem_plantel');

	// CALCULO DE EFECTOS INIACIALES
	//saber si tiene alguna plaza
	$consulta_cant_mov_sql="select count(id_clave_presupuestal_personal) as cantidad from movimiento__personal MP, claves_presupuestales_personal CPP where MP.id_mov = CPP.id_movimiento and rfc ='$rfc'";
	$registros_cant_mov=ejecutar_sql($consulta_cant_mov_sql);
	$cantidad_de_movimientos = $registros_cant_mov->fields('cantidad');

	// Si acaso ha tenido por lo menos una plaza el efecto inicial sera por defecto el de la quince y año actuales
	if ($cantidad_de_movimientos > 0) {
		$inicio_plantelA = date ('Y'); 
		$inicio_plantelQ =($quincena_actual< 10)?('0'.$quincena_actual):($quincena_actual);
		}
	else{ //Si no ha tenido sera asignada el ano y quincena de ingreso al plantel   
		$consulta_sql="select substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel from personal where rfc ='$rfc'";
		$registros=ejecutar_sql($consulta_sql); echo '<br>';
			$inicio_plantelA = $registros->fields('ano_plantel');
			$inicio_plantelQ = $registros->fields('sem_plantel');
		}

	// CALCULO DE EFECTOS FINALES DE ACUERDO AL MOVIMIENTO
if (($mov != '0') and ($rfc != "0") and ($clave_presupuestal != '0') and ($categoria_elegida != '0') )
		{
		if ($mov == '20') {  
			if ($inicio_plantelQ <= 13) 
				{$fin_plantelQ = $inicio_plantelQ +11; 
		 		$fin_plantelA = $inicio_plantelA;} 
			if ($inicio_plantelQ > 13) 
				{$fin_plantelQ = (((11-(24 -$inicio_plantelQ))<10)?'0':'').(11-(24 -$inicio_plantelQ));
		 		$fin_plantelA = 1+$inicio_plantelA;}
				}
		if ($mov == '95') {
			if ($inicio_plantelQ <= 13) 
				{$ipQ = $inicio_plantelQ +11; 
		 		$ipA = $inicio_plantelA;} 
			if ($inicio_plantelQ > 13) 
				{$ipQ = (((11-(24 -$inicio_plantelQ))<10)?'0':'').(11-(24 -$inicio_plantelQ));
		 		$ipA = 1+$inicio_plantelA;}

			if ($ipQ <= 9) {$fin_plantelQ = $ipQ +15; $fin_plantelA = $ipA;}
			if ($ipQ > 9) {$ipq = 15-(24 -$ipQ); $fin_plantelQ =(($ipq<10)?"0":"").$ipq;  $fin_plantelA=1+$ipA;}
			}
		if ($mov == '10') { $fin_plantelA = '0000'; $fin_plantelQ = '00'; }
		}

?>
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/popcalendar.js"></script>

	<h2 align="center"> Asignar Efectos para una plaza al empleado <?php echo $rfc;?> con movimiento <?php echo $mov;?></h2>

<form method="post" action="agrega_clave_presupuestal_a_personal.php" name="datos" >
 <table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr><td>&nbsp;</td></tr>
  <tr>
    <th>&nbsp;</th>
    <th>Efectos Iniciales</th>
    <th>Efectos Finales</th>
    <th>Antiguedad(Plantel)</th>
    <th>&nbsp;</th>
  </tr>
  <tr id="non">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="non">
    <td>&nbsp;</td>
    <td align="center">
	<input name="inicio_plantelA" title="Año de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelA))?date('Y'):$inicio_plantelA; ?>" size="4" maxlength="4">
	<input name="inicio_plantelQ" title="Quincena de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelQ))?((date('d')<16)?date('m')*2-1:date('m')*2):$inicio_plantelQ; ?>" size="2" maxlength="2">
    </td>
    <td align="center">
	<input name="fin_plantelA" title="Año de salida del Plantel indefinido" type="text" value="<?php echo $fin_plantelA; ?>" size="4" maxlength="4">
	<input name="fin_plantelQ" title="Quincena de salida del Plantel indefinido" type="text" value="<?php echo $fin_plantelQ; ?>" size="2" maxlength="2">
    </td>
    <?php echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sp, $ap, $quincena_actual, date ('Y'))."</td>"; ?>
    <td>&nbsp;</td>
  </tr>
    <tr id="non">
      <td colspan="5">&nbsp;</td>
	  <input type="hidden" name="Crear" value="<?php echo $Crear; ?>" >
	  <input type="hidden" name="rfc_personal" value="<?php echo $rfc; ?>" >
	  <input type="hidden" name="clave_presupuestal" value="<?php echo $clave_presupuestal; ?>" >
	  <!--<input type="hidden" name="sector_i" value="<?php echo $sector_inicio; ?>" >		 -->
	  <!--<input type="hidden" name="sector_f" value="<?php echo $sector_fin; ?>" >		 -->
	  <input type="hidden" name="categorias" value="<?php echo $categoria_elegida; ?>">
	  <input type="hidden" name="m" value="<?php echo $mov; ?>">
    </tr>
  <tr id="non"> <td colspan="5">&nbsp;</td>
  </tr>
  <tr> <td colspan="5">  </td> </tr>
 </table>

	<br/><br/>
	<div align="center">
<?php if (($mov == '0') or ($rfc == "0") or ($clave_presupuestal == '0') or ($categoria_elegida == '0')) {?>
		<table><tr bgcolor='F5E60E'><td><i>* Si no puede ver los efectos finales es porque los datos no estan completos, por favor aborte la operaci&oacute;n</i></td></tr></table>
<?php }?>
	<br/><br/>
		<input type="submit" value="Aceptar" class="boton" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="Cancelar" class="boton" onclick="javascript:window.location = '<?php echo $volver; ?>'" />
	</div>

</form>
