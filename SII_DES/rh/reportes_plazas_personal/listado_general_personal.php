<?php
/********************************************************
		Listado General de Personal

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 58);
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

		function fecha_ingreso ($quincena, $anyo)
			{
				$mes= intval ($quincena / 2);
			 	if ($quincena%2==1)	{$semana = 'primera'; $mes = $mes+1;}	else $semana = 'segunda';  
				if ($mes==1) $mes="Enero";
				else if ($mes==2) $mes="Febrero";
				else if ($mes==3) $mes="Marzo";
				else if ($mes==4) $mes="Abril";
				else if ($mes==5) $mes="Mayo";
				else if ($mes==6) $mes="Junio";
				else if ($mes==7) $mes="Julio";
				else if ($mes==8) $mes="Agosto";
				else if ($mes==9) $mes="Septiembre";
				else if ($mes==10) $mes="Octubre";
				else if ($mes==11) $mes="Noviembre";
				else if ($mes==12) $mes="Diciembre";
			return 'La '.$semana.' quincena de '.$mes.' de '.$anyo;
			}
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Listado General de Personal ::.</title>
	</head>

<body>
		<h2 align="center"> Listado General de Personal (Licencia/Activos)</h2>

  <?php
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, curp_empleado, no_tarjeta, substring(inicio_sep,1,4) as ano_sep, substring(inicio_sep,5,6) as sem_sep, status_empleado from personal where (status_empleado = '01' or status_empleado = '02') order by status_empleado, apellidos_empleado, nombre_empleado";
	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;
	$status_inicial = $registros->fields('status_empleado');

	if(!$registros->EOF)
		echo '<table border="0" align="center" width="75%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n">
 			<tr><td colspan = "6" align="center"><b>'.(($status_inicial=="02")?"Activo":(($status_inicial=="00")?"Baja por renuncia":(($status_inicial=="01")?"Licencia":(($status_inicial=="03")?"Baja por jubilaci&oacute;n":(($status_inicial=="04")?"Inactivo por cambio de RFC":(($status_inicial=="05")?"Baja por fallecimiento":(($status_inicial=="06")?"Inactivo":" "))))))).'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center" width="100%">
			<th width="4%"> No. </th>
			<th width="8%"> No. tarjeta </th>
			<th> Nombre completo </th>
			<th width="11%"> RFC </th>
			<th width="14%"> CURP </th>
			<th width="28%"> Ingreso SEP </th>	
		  </tr>';
	else	echo ' <table align="center" title="No hay personal registrado">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		++$contador;
		$nt = $registros->fields('no_tarjeta');
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$clave = $registros->fields('clave_area');

		if ( $registros->fields('curp_empleado') == " ") $curp = 'Sin registro';
		else $curp = $registros->fields('curp_empleado');

		$as = $registros->fields('ano_sep');
		$ss = $registros->fields('sem_sep');
		$se = $registros->fields('status_empleado');
		
			if ($status_inicial != $se){ 
				echo '<tr><th colspan = "5">Total</th><th>'.(--$contador).'</th></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr>
					 <td colspan = "6" align="center"><b>'.(($se=="02")?"Activo":(($se=="00")?"Baja por renuncia":(($se=="01")?"Licencia":(($se=="03")?"Baja por jubilaci&oacute;n":(($se=="04")?"Inactivo por cambio de RFC":(($se=="05")?"Baja por fallecimiento":(($se=="06")?"Inactivo":" "))))))).'</b></td>
					 </tr>
					 </table>
					 <table border="0" align="center" width="75%" title="TRelaci&oacute;n de todo el personal de la instituci&oacute;n"><tr><td>&nbsp;</td></tr>
					  	<tr align="center" width="100%">
							<th width="4%"> No. </th>
							<th width="8%"> No. tarjeta </th>
							<th> Nombre completo </th>
							<th width="11%"> RFC </th>
							<th width="14%"> CURP </th>
							<th width="28%"> Ingreso SEP </th>		
					  </tr>';
					$total = $total + (--$contador); // 
					$status_inicial = $se;
					$contador = 1;
					$contador_sex=0;
				}

		echo "<tr id='$id'>
			<td align='center'> $contador </td>
			 <td align='center'>".$registros->fields('no_tarjeta')."</td>	
			 <td> $ae $ne </td>		
			 <td align='center'> $rfc </td>
			 <td align='center'> $curp </td>
			 <td>".fecha_ingreso ($ss, $as)."</td>
			</tr>";
		
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
		}
	echo "<tr><th colspan = '5'>Total</th><th> $contador </th>";
	?>
	
</table>

	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	  </div>
	</form>
	<? if ($contador > 0) { ?>
	<br><br>
	<div align="center">  <a href="../reportes/Listado_general_de_personal.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n:  Personal General"/></a>
	</div>
	<? } ?>
</body>
</html>
