<?php
	/********************************************************
		Personal Detalle
		Muestra Jefes, Administrativos, Docentes con o sin plaza

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 62);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$tipo= $_GET['tipo']; // <td>".$CFG->instituto."</td>
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal Detalle::.</title>
	</head>

<body>
<?php
if ($tipo=='J'){ // Condición: Activo, Inactivo Selecciona Jefes odb.personal, odb.jefes
	$titulo = "Jefes de personal";
	$consulta_registros="select distinct(P.rfc), apellidos_empleado, nombre_empleado, nivel_estudios, inicio_sep, curp_empleado, sexo_empleado, P.clave_area, P.nombramiento, P.status_empleado from personal P, jefes J
	where (status_empleado = '01' or status_empleado = '02') 
	and P.rfc = J.rfc
	order by apellidos_empleado, nombre_empleado";	
// , PU.nivel_puesto, PU.descripcion_puesto		, puestos_personal PUP, puestos PU			and PUP.rfc = P.rfc and PUP.clave_puesto = PU.clave_puesto
	}
else {
	if ($tipo=='D'){ // Condición: Activo, Inactivo, Docente y no sea Jefe Selecciona Docentes odb.personal
		$titulo = "Personal Docente";
		$consulta_registros="select distinct(P.rfc), apellidos_empleado, nombre_empleado, nivel_estudios, inicio_sep, curp_empleado, sexo_empleado, clave_area, status_empleado from personal P where (status_empleado = '01' or status_empleado = '02') and P.rfc not in (select rfc from jefes where rfc is not null) and nombramiento = 'D' 	
	order by apellidos_empleado, nombre_empleado";
//, PU.nivel_puesto, PU.descripcion_puesto	, puestos_personal PUP, puestos PU	and PUP.rfc = P.rfc and PUP.clave_puesto = PU.clave_puesto


		}
	else{ // Condición: Activo, Inactivo, Administrativo y no sea Jefe Selecciona Administrativos odb.personal
		$titulo = "Personal Administrativo";
		$consulta_registros="select distinct(P.rfc), apellidos_empleado, nombre_empleado, nivel_estudios, inicio_sep, curp_empleado, sexo_empleado, clave_area , status_empleado, nombramiento  
		from personal P where 
		(status_empleado = '01' or status_empleado = '02')  
		and nombramiento = 'A'
		and P.rfc not in (select rfc from jefes where rfc is not null)
		order by apellidos_empleado, nombre_empleado";
	//, PU.nivel_puesto, PU.descripcion_puesto	, puestos_personal PUP, puestos PU		and PUP.rfc = P.rfc and PUP.clave_puesto = PU.clave_puesto
		}//fin else
	}//fin else
?>
	<h2 align="center"><?php echo $titulo;?></h2>

  <?php
	$registros=ejecutar_sql($consulta_registros);
	$status_inicial = $registros->fields('status_empleado');
	
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<table align="center" width="100%" title="Tabla que muestra la lista de trabajadores con o sin plaza, con o sin movimiento">
<tr><td colspan = "6" align="center"><b>'.(($status_inicial=="02")?"Activo":(($status_inicial=="00")?"Baja por renuncia":(($status_inicial=="01")?"Licencia":(($status_inicial=="03")?"Baja por jubilaci&oacute;n":(($status_inicial=="04")?"Inactivo por cambio de RFC":(($status_inicial=="05")?"Baja por fallecimiento":(($status_inicial=="06")?"Inactivo":" "))))))).'</b></td></tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		  <tr align="center" width="100%">
			<th align="center"> No. </th>
			<th align="center"> RFC </th>
			<th align="center"> CURP </th>
			<th align="center"> Genero </th>
			<th align="center"> Nombre </th>'
			.(($tipo !='J')?'<th align="center"> Rango </th>':'').
			'<th align="center"> Puesto </th>'
			.(($tipo =='J')?'<th align="center"> Nombramiento </th>':'')
			.(($tipo !='J')?'<th align="center"> Estatus </th>':''). 
			'<th align="center"> Fecha ingreso </th> ';
	else	echo '<table align="center" title="No existe personal registrado">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>' ;

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		$contador = $contador + 1; 
	//$tmd = "Nombramiento";
	$band = 0;

		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$nom = $registros->fields('nombramiento');
		$status_empleado = $registros->fields('status_empleado');
		$sexo = $registros->fields('sexo_empleado');
		$curp = $registros->fields('curp_empleado');

			/**/$qry_nivel="select clave_puesto from puestos_personal where rfc='$rfc'";
			$res_nivel=ejecutar_sql($qry_nivel);

			if (!$res_nivel->EOF){
				$idpuesto = $res_nivel->fields('clave_puesto');
				$qry_nivel2="select descripcion_puesto, nivel_puesto from puestos where clave_puesto=$idpuesto";
				$res_nivel2=ejecutar_sql($qry_nivel2);
				$descripcion_puesto = $res_nivel2->fields('descripcion_puesto');
				$se = $res_nivel2->fields('nivel_puesto');
			}
			else $descripcion_puesto = "Sin Puesto";

		//if ($descripcion_puesto == null)	$descripcion_puesto = "Sin Puesto"; 
/// puestoooooooooo
		$clave = $registros->fields('clave_area');
		
		$area="select descripcion_area, clave_area from organigrama where clave_area='$clave'";
		$res_area=ejecutar_sql($area);
		$descripcion_area = $res_area->fields('descripcion_area');
		
		echo "<tr id='$id'>
		 <td align='center'>".$contador."</td>
		 <td>".$rfc."</td>					
		 <td>".$curp."</td>
		 <td>".(($sexo == "F" )?'Mujer':'Hombre')."</td>
		 <td>".$ne." ".$ae."</td>";


		if ($tipo == "A" or $tipo == "D") {	echo "<td align='center'>".(($se=="1")?"DIRECCIÓN":(($se=="2")?"SUBDIRECCIÓN":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))))."</td>";
			} 

		echo "<td>".$descripcion_puesto."</td>";
				
		$sep  = $registros->fields('inicio_sep');
		$anho = substr($registros->fields('inicio_sep'), 0,4 );
		$quin = substr($registros->fields('inicio_sep'), 5,2 );
				
			switch($quin){
					case "01": $mes = "01"; $dia = "01"; break;
					case "02": $mes = "01"; $dia = "15"; break;
					case "03": $mes = "02"; $dia = "01"; break;
					case "04": $mes = "02"; $dia = "15"; break;
					case "05": $mes = "03"; $dia = "01"; break;
					case "06": $mes = "03"; $dia = "15"; break;
					case "07": $mes = "04"; $dia = "01"; break;
					case "08": $mes = "04"; $dia = "15"; break;
					case "09": $mes = "05"; $dia = "01"; break;
					case "10": $mes = "05"; $dia = "15"; break;
					case "11": $mes = "06"; $dia = "01"; break;
					case "12": $mes = "06"; $dia = "15"; break;
					case "13": $mes = "07"; $dia = "01"; break;
					case "14": $mes = "07"; $dia = "15"; break;
					case "15": $mes = "08"; $dia = "01"; break;
					case "16": $mes = "08"; $dia = "15"; break;
					case "17": $mes = "09"; $dia = "01"; break;
					case "18": $mes = "09"; $dia = "15"; break;
					case "19": $mes = "10"; $dia = "01"; break;
					case "20": $mes = "10"; $dia = "15"; break;
					case "21": $mes = "11"; $dia = "01"; break;
					case "22": $mes = "11"; $dia = "15"; break;
					case "23": $mes = "12"; $dia = "01"; break;
					case "24": $mes = "12"; $dia = "15"; break;
				}//fin switch

		if ($tipo =='J') { echo "<td>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z' )?'Sin Tipo':'Sin Reg.')))."</td>";}

			switch($status_empleado){
				case "00": $status = "BAJA POR RENUNCIA";  		break;
				case "01": $status = "LICENCIA";  			break;
				case "02": $status = "ACTIVO";  			break;
				case "03": $status = "BAJA POR JUBILACI&Oacute;N";  	break;
				case "04": $status = "INACTIVO POR CAMBIO DE RFC"; 	break;
				case "05": $status = "BAJA POR FALLECIMIENTO";  	break;
				case "06": $status = "INACTIVO";  			break;
				}

		if ($tipo !='J') { echo "<td>".$status."</td>";}
	
		echo "<td>".$dia."/".$mes."/".$anho."</td>		
		</tr>";
		//Fin mostrar las plazas		
		$registros->MoveNext();
	}
	?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Jefes_docentes_admin.pdf.php?t=<?php echo $tipo;?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: <?php echo (($tipo=='J')?'Jefes':(($tipo=='A')?'Administrativos':'Docentes')); ?> " /></a>
	</div>
	<? } ?>
</body>
</html>
