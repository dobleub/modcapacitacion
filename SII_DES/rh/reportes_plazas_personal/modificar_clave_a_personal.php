<?php
	/********************************************************
		modificar_clave_a_personal.php

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011

	Antes en personal
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	require_once($CFG->funciones_phpDir."/funciones_fechas.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/popcalendar.js"></script>
	<script type="text/javascript">

	function valida(formulario)
	{
		form = eval("document."+formulario.name);
		if (form.clave_area.value==0){
			alert("Capture el departamento de adscripcion");
			return false;
		}

	}//fin funcion
	</script>

	<title>.:: Modificar/Eliminar Plaza de Personal ::.</title>
</head>

<body>
<?php
	echo $idp = $_GET['idp'];
	$qry_pp = "select categoria, id_clave, rfc from claves_presupuestales_personal CPP, movimiento__personal MP where 
MP.id_mov = CPP.id_movimiento and id_clave_presupuestal_personal = $idp";
	$res_pp = ejecutar_sql($qry_pp);
	$rfc = $res_pp->fields('rfc');
	$post_categoria = $res_pp->fields('categoria');
	$post_id_clave = $res_pp->fields('id_clave');

$rfc = strtoupper($rfc);
//$rfc = strtoupper($_GET['$rfc']);
//$regresar = "window.location = 'agrega_personal_a_plaza.php?accion=".$accion."'";

$qry_p = "select bandera_foto, curp_empleado, no_tarjeta, apellidos_empleado, nombre_empleado, ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel, sexo_empleado, status_empleado, area_academica, tipo_personal from personal where rfc = '$rfc'";  // select * from plantilla_personal where id_plantilla_personal =$idp
$res_p = ejecutar_sql($qry_p);
$rows = $res_p->recordcount();


	$accion = "actualiza";
	$encabezado = "Actualizar Categoria de esta Clave Presupuestal";
	$botonSubmit = "Actualizar";
	$regresar = "window.location = 'agrega_clave_presupuestal_a_personal.php?a=2'";

	// Foto
	$flag_foto = $res_p->fields('bandera_foto');
	if ($flag_foto == 0)	$archivo_foto = "../expediente/documentos/fotos/personal/sin_foto.jpg";
	if ($flag_foto == 1)	$archivo_foto = "../expediente/documentos/fotos/personal/$rfc.jpg";
	$foto = "<img src='$archivo_foto' width='95' height='98' align='center'>";

	$curp_empleado = $res_p->fields('curp_empleado');
	$no_tarjeta = $res_p->fields('no_tarjeta');
	$apellidos_empleado = $res_p->fields('apellidos_empleado');
	$nombre_empleado = $res_p->fields('nombre_empleado'); 
	$ingreso_rama = $res_p->fields('ingreso_rama');
	$inicio_gobierno = $res_p->fields('inicio_gobierno');
	$inicio_sep = $res_p->fields('inicio_sep');
	$inicio_plantel = $res_p->fields('inicio_plantel');
	$sexo_empleado = $res_p->fields('sexo_empleado');
	$status_empleado = $res_p->fields('status_empleado');
	$area_academica = $res_p->fields('area_academica');
	$tipo_personal=$res_p->fields('tipo_personal'); 

        if($sexo_empleado == 'F'){ $selectedF = " selected"; } else{ $selectedM = " selected"; }

if($ingreso_rama){
	$ingreso_ramaA = substr($ingreso_rama, 0, 4);
	$ingreso_ramaQ = substr($ingreso_rama, -2);
	}
if($inicio_gobierno){
	$inicio_gobiernoA = substr($inicio_gobierno, 0, 4);
	$inicio_gobiernoQ = substr($inicio_gobierno, 4, 2);
	}
if($inicio_sep){
	$inicio_sepA = substr($inicio_sep, 0, 4);
	$inicio_sepQ = substr($inicio_sep, 4, 2);
	}
if($inicio_plantel){
	$inicio_plantelA = substr($inicio_plantel, 0, 4);
	$inicio_plantelQ = substr($inicio_plantel, 4, 2);
	}
?>
<h2 align="center"> <?php echo $encabezado; ?> </h2>
<form name="personal" action="modificar_clave_a_personal_bd.php" method="post" onSubmit="return valida(this)">


  <!-- DATOS GENERALES -->
  <h3 align="center"> Datos Generales </h3>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" title="Seccion para modificar plaza al personal">
		<tr align="center">
			<td width="9%" rowspan="6"> <?php echo $foto; ?> </th>
         <th width="36%"> Apellido (s)</th>
			<th>Nombre</th>
			<th>R.F.C.</th>
      <th width="16%">  Sexo </th> <!--  -->
    </tr>
    <tr align="center">
	  	<td> 
<!-- INICIO MODIFICA -->
	<input name="apellidos_empleado" title="Apellidos del empleado" type="text" value="<?php echo $apellidos_empleado; ?>" size="40" maxlength="45" onChange="javascript:this.value=this.value.toUpperCase();" id = "obligatorio" disabled="true">
<!-- FIN MODIFICA -->
		</td>
      <td align="center"><input name="nombre_empleado" title="Nombre(s) del empleado" type="text" value="<?php echo $nombre_empleado; ?>" size="35" maxlength="35" onChange="javascript:this.value=this.value.toUpperCase();"  id = "obligatorio" disabled="true"></td>
      <td>
      	<input name="rfc" type="text" size="15" maxlength="13" value="<?php echo $rfc; ?>" disabled="true" title="El RFC no puede ser modificado">      </td>
      <td>
	<select name="sexo_empleado" title="Sexo del empleado" disabled="true">
		<option value="-1"> -- SELECCIONE -- </option>
		<option value="M" <?php echo $selectedM; ?>> MASCULINO </option>
		<option value="F"	<?php echo $selectedF; ?>> FEMENINO </option>
	</select>
      </td>
    </tr>
    <tr align="center">
      <th>CURP </th>
      <th width="17%">Clave</th>
      <th width="22%">Categoria </th>
      <th width="16%">N&uacute;mero empleado/tarjeta </th>
    </tr>
    <tr align="center">
      <td><input name="curp_empleado" type="text" size="22" maxlength="18" value="<?php echo $curp_empleado; ?>" title="CURP del empleado" disabled="true"></td>
      <td>
		<select name="clave_presupuestal" onChange="MM_jumpMenuCategorias('parent',this,0)" title="Clave presupuestal para el trabajador" disabled="true">		
			<?php
			$sql_clave_presupuestal="select * from claves_presupuestales";
			$clave_presupuestal=ejecutar_sql($sql_clave_presupuestal);
			if(!$clave_presupuestal->rowcount()){
				echo "<option value='0' selected> No hay claves Presupuestales Registradas </option>"; }
			else
			{       echo "<option value='0' selected> -- Seleccione Clave Presupuestal -- </option>";
				/**/while(!$clave_presupuestal->EOF){
			if($clave_presupuestal->fields('id_clave')==$post_id_clave){
				$selecciona=" selected";}
			else{ $selecciona="";}
					echo '<option value="'.$clave_presupuestal->fields('id_clave').'"'.$selecciona.'>'.$clave_presupuestal->fields('clave_antes_punto').'.'.$clave_presupuestal->fields('clave_despues_punto').' C/'.$clave_presupuestal->fields('horas_clave_plaza').' HRS. </option>';
				$clave_presupuestal->MoveNext();
						} 
			}
			?>
		</select> 	  
      </td>
      <td align="center">
		<select name="categorias" onChange="MM_jumpMenuCategorias('parent',this,0)" title="Puede cambiar de categoria al trabajador">		
			<?php
			$sql_categorias="select distinct categoria,descripcion_categoria from categorias";
			$categoria=ejecutar_sql($sql_categorias);

			if(!$categoria->rowcount())
				echo "<option value='0' selected> No hay Categorias Registradas </option>"; 
			else
			{       echo "<option value='0' selected> -- Seleccione una Categoria -- </option>";
		 	while(!$categoria->EOF){

			if($categoria->fields('categoria')==$post_categoria){
				$selecciona=" selected";}
			else{ $selecciona="";}

			echo '<option value="'.$categoria->fields('categoria').'"'.$selecciona.'>'.$categoria->fields('descripcion_categoria').'</option>';
			$categoria->MoveNext();
			} 
			}
			?>
		</select>			
      </td>
      <td align="center">
	<input type="text" title="N&uacute;mero del empleado" name="no_tarjeta" value="<?php echo $no_tarjeta; ?>" size="5" disabled="true"/>     
       </td>
    </tr>
    <tr align="center">
      <th>Ingreso a la rama/subsecretar&iacute;a</th>
      <th>Ingreso a gobierno </th>
      <th>Ingreso a S.E.P.</th>
      <th>Ingreso al plantel</th>
    </tr>
    <tr align="center">
   	<td>
	<input name="ingreso_ramaA" title="A&ntilde;o de ingreso a la Subsecretar&iacute;a" type="text" value="<?php echo $ingreso_ramaA; ?>" size="3" disabled="true">
	<input name="ingreso_ramaQ" title="Quincena de ingreso a la Subsecretaría" type="text" value="<?php echo (empty($ingreso_ramaQ))?'01':$ingreso_ramaQ; ?>" size="1" disabled="true">
	</td>
	<td>
	<input name="inicio_gobiernoA" title="A&ntilde;o de ingreso a Gobierno" type="text" value="<?php echo $inicio_gobiernoA; ?>" size="3" disabled="true">
	<input name="inicio_gobiernoQ" title="Quincena de ingreso a Gobierno" type="text" value="<?php echo (empty($inicio_gobiernoQ))?'01':$inicio_gobiernoQ; ?>" size="1" disabled="true">
	</td>
	<td>
	<input name="inicio_sepA" title="A&ntilde;o de ingreso a la SEP" type="text" value="<?php echo $inicio_sepA; ?>" size="3" disabled="true">
	<input name="inicio_sepQ" title="Quincena de ingreso a la SEP" type="text" value="<?php echo (empty($inicio_sepQ))?'01':$inicio_sepQ; ?>" size="1" disabled="true">
	</td>
	<td>
	<input name="inicio_plantelA" title="A&ntilde;o de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelA))?date('Y'):$inicio_plantelA; ?>" size="3" disabled="true">
	<input name="inicio_plantelQ" title="Quincena de ingreso al Plantel" type="text" value="<?php echo (empty($inicio_plantelQ))?((date('d')<16)?date('m')*2-1:date('m')*2):$inicio_plantelQ; ?>" size="1" disabled="true">
	</td>
    </tr>
   <tr align="center">
      <th width="9%">Foto</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
    <tr align="center">
      <td>&nbsp;</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
    <input type="hidden" name="accion" value="<?php echo $accion; ?>" />
    <input type="hidden" name="idp" value="<?php echo $idp; ?>" />
	<br />
	<div align="center">
		<input type="submit" name="actualiza" class="boton" value="<?php echo $botonSubmit; ?>" title="Presione este bot&oacute;n para guardar los cambios">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="regresa" class="boton" value="Regresar" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n">
	</div>
</form>
</body>
</html>
