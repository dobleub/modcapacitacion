<?php
	/********************************************************
		Efecto/Movimiento Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 

	// Recibir_parametros para el nuevo movimiento
	 $id_mov=$_POST['id_mov'];
	 $rfc=$_POST['rfc'];
	 $mov=$_POST['movimiento'];
	 $sector_inicio= $_POST['inicio_plantelA'].$_POST['inicio_plantelQ'];	 //$sector_inicio= $_POST['sector_i'];
	 $sector_fin= $_POST['fin_plantelA'].$_POST['fin_plantelQ'];	 	 //$sector_fin= $_POST['sector_f'];
	 $cantidad_de_movimientos= $_POST['cant_de_movimientos'];
	 $f_registro = date('d/m/Y');
	////////
	 //echo "<br> sector inicio: ".$sector_inicio;
	 //echo "<br> sector fin: ".$sector_fin."<br>";
	////////

	 $actualizar=$_POST['actualizar'];

if ($actualizar == 'actualizar')
	{
		// Verifica si tiene algun movimiento activo para proceder a desactivarlo e inserta un nuevo movimiento activo
		$consulta_select_sql_1= "select count(status) as cant from movimiento_personal where status = 1 and rfc ='$rfc'";
        	$sql_consulta_1 = ejecutar_sql($consulta_select_sql_1);

		if(!$sql_consulta_1->EOF){

			// Identificar el movimiento al que se deben desactivar sus plazas
			$consulta_movimiento="select id_mov from movimiento_personal where status = 1 and rfc ='$rfc'";
				$registro_movimiento=ejecutar_sql($consulta_movimiento);
				$id_movimiento = $registro_movimiento->fields('id_mov');

			// Identificar si este movimiento tiene alguna(s) plaza(s) asignada(s) para proceder a desactivarla(s)
			$consulta_si_existen_plazas="select id_plantilla_personal from plantilla_personal where estatus = 1 and rfc ='$rfc'";
			$respusta_si_existen_plazas=ejecutar_sql($consulta_si_existen_plazas);
			if(!$respusta_si_existen_plazas->EOF){
				// Desactivar las plazas del movimiento anterior
				$consulta_select_sql= "update plantilla_personal set estatus = 2 where id_movimiento =$id_movimiento";
				$sql_consulta = ejecutar_sql($consulta_select_sql);
				}

			// Desactivar el movimiento anterior
			$consulta_select_sql= "update movimiento_personal set status = 2 where status = 1 and rfc ='$rfc'";
			$sql_consulta = ejecutar_sql($consulta_select_sql);
			}
		// Insertar el nuevo movimiento
		$ejecutar_sql="insert into movimiento_personal (id_mov, rfc, movimiento, e_iniciales, e_finales, status, cant_mov, f_registro) values ($id_mov,'$rfc',$mov,'$sector_inicio','$sector_fin',1,$cantidad_de_movimientos,'$f_registro')";
		$insert_sql=ejecutar_sql($ejecutar_sql);

        echo '<script language="javascript" type="text/javascript"> alert("Registro guardado con exito") </script>';
	}
//date('d/m/Y');
	//Desactivar una plaza
	if (isset($_GET['idm']))
	{
		$update_sql= "update movimiento_personal set estatus=2 where id_mov=".($_GET['idm']);
		$sql_update = ejecutar_sql($update_sql) ;
		echo "<script>alert('El movimiento ha sido desactivada con &eacute;xito');</script>";
	}
?>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />

<form method="post" action="efecto_movimiento_personal_listado.php" name="datos" >

		<h2 align="center">Movimientos del Personal</h2>

<table border="0" align="center" cellpadding="0" cellspacing="0" title="Seleccione el personal y el movimiento que desea asignarle">
  <tr>
    <th>&nbsp;</th>
    <th>Personal</th>
    <th>Movimiento</th>
  </tr>
  <tr id="non">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="non">
    <td>&nbsp;</td>
    <td align="center">
	<select name="rfc" title="Presione aqu&iacute; para eligir un trabajador">
	<?php
		$consulta_rfc="select P.rfc, P.apellidos_empleado, P.nombre_empleado from personal P where P.status_empleado = '02' and (P.tipo_personal != 'H' and P.tipo_personal != 'Z' and nombramiento != 'Z') order by P.apellidos_empleado, P.nombre_empleado, P.rfc";
		$datos_rfc=ejecutar_sql($consulta_rfc);

		if(!$datos_rfc->rowcount()){ echo "<option value='0' selected> No hay personal dado de alta </option>"; }
		else {
			echo "<option value='0' selected> -- Seleccione Personal -- </option>";
			while(!$datos_rfc->EOF){
				echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
				$datos_rfc->MoveNext();
			}
		     }
	?>
	</select>
    </td>
    <td align="center">&nbsp;&nbsp;<select name="mov" title="Presione aqu&iacute; para eligir un tipo de movimiento">
      <!--<option value="0" selected="selected"> -- Seleccione -- </option> -->
      <option value="20">20</option>
      <option value="95">95</option>
      <option value="10">10</option>
    </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
    <tr id="non">  <td colspan="4">&nbsp;</td> </tr>
    <tr id="non">  <td colspan="4">&nbsp;</td> </tr>
  <tr>
    <td colspan="4">  
    
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr id="non">
	      <td><div align="center">
	      <input type="submit" value="Aceptar" class="boton" title="Presione este bot&oacute;n para guardar los cambios"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	      <input type="button" value="Cancelar" class="boton" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" onclick="javascript:window.location = '<?php echo $CFG->rootDirServ; ?>/<?php echo $_SESSION['pagina_inicio']; ?>/bienvenida.php'" />
	      </div>
		</td> 
	      </tr>
	    </table>
    </td>
   </tr>
  </table>
<br><br>
<table border="1" align="center" cellpadding="0" cellspacing="0" width="90%" title="Tabla que lista el personal con su respectivo movimiento">
  <tr>
    <td>&nbsp;</td> 
  </tr>
<?php 
	$consulta_id="SELECT id_mov, rfc, movimiento, e_iniciales, e_finales, status, max(cant_mov), f_registro from movimiento_personal group by rfc having cant_mov = max(cant_mov) order by status";
	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;
	$status_inicial = $registros->fields('status');

	if(!$registros->EOF)
		echo '<tr><td align="center" colspan = "11"><b>'.(($status_inicial==1)?"Activo":"Inactivo").'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			    <tr><th>Contador</th>
			    <th>RFC</th>
			    <th>Nombre</th>
			    <th width="20%">Movimiento</th>
			    <th>Efectos Iniciales</th>
			    <th>Efectos Finales</th>
				<th> Desactivar </th>
			    <th>Historial Movimiento</th>
			    <th>Eliminar</th>
			    <th>Plaza(s) Actual(es)</th>
			    <th>Historial Plaza(s)</th>
			  </tr>';
	else	echo '<tr align="center">
			<td> No existe personal registrado </th>
		  	</tr>';

		$aux_var = 2;

		while(!$registros->EOF){
			$contador = $contador + 1;
			$id_mov = $registros->fields('id_mov');
			$su_rfc = $registros->fields('rfc');
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$su_rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');

			$movimiento = $registros->fields('movimiento');
			$e_iniciales = $registros->fields('e_iniciales');
			$e_finales = $registros->fields('e_finales');
			$status = $registros->fields('status');

			if ($status_inicial != $status){
				$aux_var = 1; 
				echo '<tr><th colspan = "10">Total</th><th>'.(--$contador).'</th></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr><td>&nbsp;</td></tr>
					 <tr align="center"><td colspan = "11" ><b>'.(($status==1)?"Activo":"Inactivo").'</b></td></tr>
					 </table>
					<br><br>
					 <table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n"><tr><td>&nbsp;</td></tr>
					  	<tr align="center" width="100%">
						    <th>Contador</th>
						    <th>RFC</th>
						    <th>Nombre</th>
						    <th width="20%">Movimiento Actual</th>
						    <th>Efectos Iniciales</th>
						    <th>Efectos Finales</th>
						    <th>Historial Movimiento</th>
						    <th>Plaza(s) Actual(es)</th>

			    			    <th>Historial Plaza(s)</th>
					  </tr>';
					$status_inicial = $status;
					$contador = 1;
				}

			echo "<tr id='$id'>
				 <td align='center'>".$contador."</td>
				 <td align='center'>".$su_rfc."</td>
				 <td>".$nombre."</td>
				 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
				 <td align='center'>".$e_iniciales."</td>
				 <td align='center'>".$e_finales."</td>";
if ($aux_var == 2){
			?>

	     <td align='center'> <a href="efecto_movimiento_personal.php?idm=<?php echo $registros->fields('id_mov'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea desactivar este movimiento? Operaci&oacute;n irreversible.');"> <img border="0" src="../img/rechazar.png" width='25' height='25' title="Presione este bot&oacute;n para desactivar este movimiento"/></a></td>

<?php } ?>

				<td align='center'>
					<a href="efecto_movimiento_personal_historial.php?idp=<?php echo $registros->fields('rfc'); ?>" > <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver el historial de movimientos"/></a><?php echo"</td>";

if ($aux_var == 2){ 	?>
				<td align='center'>
					<a href="borrar_movimiento_de_personal.php?idm=<?php echo $registros->fields('id_mov'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este movimiento para este trabajador?');"> <img border="0" src="../../../img/eliminar.gif" title="Presione aqu&iacute; para eliminar este registro"/></a>
		     		</td>
<?php } ?>
				<td align='center'>
					<a href="agrega_personal_a_plantilla.php?idm=<?php echo $registros->fields('id_mov'); ?>&a=<?php echo $aux_var; ?>" > <img border="0" src="../img/ver.png" title="Presione aqu&iacute; para ver las plazas" width="25" height="25" /></a>
		     		</td>
				<td align='center'>
					<a href="plazas_personal_historial.php?idp=<?php echo $registros->fields('rfc'); ?>" > <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver el historial de plazas"/></a>
				</td>
	<?php
			echo "</tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}

	if ($aux_var == 2)
		echo "<tr><th colspan = '10'>Total</th><th>".$contador."</th></tr>";
	else
		echo "<tr><th colspan = '8'>Total</th><th>".$contador."</th></tr>";

	?>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
</table>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Clave_presupuestal.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Movimiento"/></a>
	</div>
	<? } ?>
