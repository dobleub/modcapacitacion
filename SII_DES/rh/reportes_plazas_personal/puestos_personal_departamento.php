<?php
	/********************************************************
		Puestos de Personal de acuerdo a Estructura

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 60);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Puestos Personal - Departamento ::.</title>
	</head>
<body>
<h2 align="center">Personal-Departamento de acuerdo a Puesto (Licencia/Activos)</h2>

  <?php
	$consulta_registros="select PE.rfc, apellidos_empleado, nombre_empleado, clave_area, descripcion_puesto, status_empleado from personal PE, puestos_personal PP, puestos P where (status_empleado = '01' or status_empleado = '02') and PP.rfc = PE.rfc and PP.clave_puesto = P.clave_puesto order by descripcion_puesto, clave_area, apellidos_empleado, nombre_empleado";  // , P.clave_puesto
	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	//Primer registro
	$puesto_inicial = $registros->fields('descripcion_puesto');
	
	if(!$registros->EOF)
		echo "<table align='center' width='70%' title='Esta secci&oacute;n muestra a los Trabajadores agrupados de acuerdo al puesto que ocupa y su respectivo departamento'><tr><td>&nbsp;</td></tr>
		 <tr>
		 <td colspan = '5' align='center'><b>".strtoupper($puesto_inicial)."</b></td>
		 </tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr width='100%'>
			<th align='center'> No. </th>
			<th width='40%'> Nombre completo </th>
			<th width='60%'> Departamento </th>
		 </tr>";
	else	echo '<table align="center" title="No existe personal asociado a Puesto - Estructura">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';
	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		$contador = $contador + 1;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$clave = $registros->fields('clave_area');
			$area="select descripcion_area from organigrama where clave_area='$clave'";
			$res_area=ejecutar_sql($area);
			$descripcion_area = $res_area->fields('descripcion_area');
		$puesto = $registros->fields('descripcion_puesto');
		
		if ($puesto_inicial != $puesto){
			$puesto_inicial = $puesto;
			$contador = 1;
			echo '<tr><td>&nbsp;</td></tr>
			 <tr>
			 <td colspan = "5" align="center"><b>'.strtoupper($puesto).'</b></td>
			 </tr>
			 <tr><td>&nbsp;</td></tr>
			 </table>
			 <table align="center" width="70%" title="Esta secci&oacute;n muestra a los Trabajadores agrupados de acuerdo al puesto que ocupa y su respectivo departamento">
				  <tr width="100%">
					<th align="center"> No. </th>
					<th width="40%"> Nombre completo </th>
					<th width="60%"> Departamento </th>
				  </tr>';
		}
		if ( $registros->fields('status_empleado') == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo			
		echo "<tr bgcolor='#FAAC58'>
		 <td  align='center'> $contador </td>
		 <td> $ae $ne </td>		
		 <td> $descripcion_area </td>
		 </tr>";
		$activar_flag = 1;
		} else {
		echo "<tr id='$id'>
		 <td  align='center'> $contador </td>
		 <td> $ae $ne </td>		
		 <td> $descripcion_area </td>
		 </tr>";
		}

		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=2 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
		</div>
	</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Personal_de_acuerdo_a_departamento.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Puestos de acuerdo a departamento" /></a>
	</div>
	<? } ?>
</body>
</html>
