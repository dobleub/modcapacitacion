<?php
	/********************************************************
		Efecto/Movimiento Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 

	// Recibir_parametros para el nuevo movimiento
	 $id_mov=$_POST['id_mov'];
	 $rfc=$_POST['rfc'];
	 $mov=$_POST['movimiento'];
	 $cantidad_de_movimientos= $_POST['cant_de_movimientos'];
	 $insertar=$_POST['insertar'];

	 $f_registro = date('d/m/Y');
	 $h_registro = date('h:i:s a');

	// INSERTAR UN NUEVO MOVIMIENTO PARA UN TRABAJADOR
if ($insertar == 'insertar')
	{
		// Verifica si tiene algun movimiento activo para proceder a desactivarlo e inserta un nuevo movimiento activo
		$consulta_select_sql_1= "select count(estatus) as cant from movimiento__personal where estatus = 1 and movimiento = $mov and rfc ='$rfc'";
        	$sql_consulta_1 = ejecutar_sql($consulta_select_sql_1);
		$cant_de_mov =$sql_consulta_1 ->fields('cant');

		// Si tiene algún movimiento activo entonces
		if($cant_de_mov != null){

			// Identificar el movimiento al que se deben desactivar sus plazas
			$consulta_movimiento="select id_mov from movimiento__personal where estatus = 1 and movimiento = $mov and rfc ='$rfc'";
				$registro_movimiento=ejecutar_sql($consulta_movimiento);
				$id_movimiento_a_desactivar = $registro_movimiento->fields('id_mov');

			// Identificar si este movimiento tiene alguna(s) plaza(s) asignada(s) para proceder a desactivarla(s)
			$consulta_si_existen_plazas="select id_clave_presupuestal_personal as id from claves_presupuestales_personal where id_movimiento = $id_movimiento_a_desactivar";
			$respusta_si_existen_plazas=ejecutar_sql($consulta_si_existen_plazas);
			$cant_de_plazas = $respusta_si_existen_plazas ->fields('id');

			// Si tiene plazas asociadas este movimiento entonces
				if($cant_de_plazas != null){
				// Desactivar las plazas del movimiento anterior
				$update_status_plaza= "update claves_presupuestales_personal set estatus = 2 where id_movimiento =$id_movimiento_a_desactivar";
				$desactivar_plaza = ejecutar_sql($update_status_plaza);
				}

			// Desactivar el movimiento anterior
			$consulta_update_sql= "update movimiento__personal set estatus = 2 where id_mov = $id_movimiento_a_desactivar";
			$sql_update = ejecutar_sql($consulta_update_sql);
			}

		// Insertar el nuevo movimiento
		$ejecutar_sql="insert into movimiento__personal (id_mov, rfc, movimiento, estatus, cant_mov, fecha_registro, hora_registro) values ($id_mov,'$rfc',$mov,1,$cantidad_de_movimientos,'$f_registro','$h_registro')";
		$insert_sql=ejecutar_sql($ejecutar_sql);

        echo '<script language="javascript" type="text/javascript"> alert("Registro guardado con exito") </script>';
	}

	//DESACTIVAR UN MOVIMIENTO CON SU(S) PLAZA(S)
	if (isset($_GET['idm']))
	{
	// Identificar si este movimiento tiene alguna(s) plaza(s) asignada(s) para proceder a desactivarla(s)
		$consulta_si_existen_plazas="select id_clave_presupuestal_personal as id from claves_presupuestales_personal where id_movimiento = ".($_GET['idm']);
		$respusta_si_existen_plazas=ejecutar_sql($consulta_si_existen_plazas);
		$cant_de_plazas = $respusta_si_existen_plazas ->fields('id');

	// Si tiene plazas asociadas este movimiento entonces
		if($cant_de_plazas != null){
			// Desactivar las plazas del movimiento anterior
			$update_status_plaza= "update claves_presupuestales_personal set estatus = 2 where id_movimiento =".($_GET['idm']);
			$desactivar_plaza = ejecutar_sql($update_status_plaza);
			}
	// Ahora se puede desactivar el movimiento
		$update_sql= "update movimiento__personal set estatus=2 where id_mov=".($_GET['idm']);
		$sql_update = ejecutar_sql($update_sql);
		echo "<script>alert('El movimiento ha sido desactivada con exito');</script>";
	}
?>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />



<?php
	$consulta_select_sql= "select count(id_clave) as Total from claves_presupuestales";
	$sql_consulta = ejecutar_sql($consulta_select_sql) ;
	$cantidad= $sql_consulta->fields('Total');
	if($cantidad != null){ //   $cantidad != null
?>
<form method="post" action="efecto_movimiento_personal_listado.php" name="datos" >

		<h2 align="center">Movimientos del Personal</h2>

	<table border="0" align="center" cellpadding="0" cellspacing="0" title="Seleccione el personal y el movimiento que desea asignarle">
	  <tr>
	    <th>&nbsp;</th>
	    <th>Personal</th>
	    <th>Movimiento</th>
	  </tr>
	  <tr id="non">
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr id="non">
	    <td>&nbsp;</td>
	    <td align="center">
		<select name="rfc" title="Presione aqu&iacute; para eligir un trabajador">
		<?php
			$consulta_rfc="select P.rfc, P.apellidos_empleado, P.nombre_empleado from personal P where P.status_empleado = '02' and (P.tipo_personal != 'H' and P.tipo_personal != 'Z' and nombramiento != 'Z') order by P.apellidos_empleado, P.nombre_empleado, P.rfc";
			$datos_rfc=ejecutar_sql($consulta_rfc);

			if(!$datos_rfc->rowcount()){ echo "<option value='0' selected> No hay personal dado de alta </option>"; }
			else {
				echo "<option value='0' selected> -- Seleccione Personal -- </option>";
				while(!$datos_rfc->EOF){
					echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
					$datos_rfc->MoveNext();
				}
			     }
		?>
		</select>
	    </td>
	    <td align="center">&nbsp;&nbsp;<select name="mov" title="Presione aqu&iacute; para eligir un tipo de movimiento">
			<!-- <option value="0" selected="selected"> -- Seleccione -- </option> -->
			<option value="20">20</option>
			<option value="95">95</option>
			<option value="10">10</option>
		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  </tr>
	  <tr id="non">  <td colspan="4">&nbsp;</td> </tr>
	  <tr id="non">  <td colspan="4">&nbsp;</td> </tr>
	  <tr>
	    <td colspan="4">  
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		      <tr id="non">
		      <td><div align="center">
		      <input type="submit" value="Aceptar" class="boton" title="Presione este bot&oacute;n para guardar los cambios"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		      <input type="button" value="Cancelar" class="boton" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" onclick="javascript:window.location = '<?php echo $CFG->rootDirServ; ?>/<?php echo $_SESSION['pagina_inicio']; ?>/bienvenida.php'" />
		      </div>
			</td> 
		      </tr>
		    </table>
	    </td>
	  </tr>
	</table>
	<br><br>
</form>

<?php } else { 
	$ses = $_SESSION['pagina_inicio'];
?>
<script>
alert("Por favor, registre primero al menos una Clave presupuestal");
window.location = '<?php echo $CFG->rootDirServ; ?>/<?php echo $ses; ?>/bienvenida.php'
</script>
<?php } ?>

	<!--  ACTIVOS -->
	<table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" title="Tabla que lista el personal con su respectivo movimiento">
  	<tr><td>&nbsp;</td> </tr>
<?php 
	$consulta_id="select id_mov, rfc, movimiento, estatus, cant_mov, fecha_registro, hora_registro from movimiento__personal group by rfc having estatus=1 order by rfc";
	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;
	$status_inicial = $registros->fields('estatus');

	if(!$registros->EOF)
		echo '<tr><td align="center" colspan = "9"><b>'.(($status_inicial==1)?"Activo":"Inactivo").'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			    <tr>
			    <th width="6%">Contador</th>
			    <th width="9%">RFC</th>
			    <th width="37%" >Nombre</th>
			    <th width="10%">Movimiento</th>
			    <th width="5%"> Desactivar </th>
			    <th width="5%">Historial Movimiento</th>
			    <th width="4%">Eliminar</th>
			    <th width="5%">Agregar/Consultar Plaza(s)</th>
			    <th width="4%">Historial Plaza(s)</th>
			  </tr>';
	else	echo '<tr align="center">
			<td> No existe claves presupuestales asignadas al personal </th>
		  	</tr>';

		while(!$registros->EOF){

			$aux_var = 2;  // El valor 2 indica que el movimiento esta activo

			$contador = $contador + 1;
			$id_mov = $registros->fields('id_mov');
			$su_rfc = $registros->fields('rfc');
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$su_rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');

			$movimiento = $registros->fields('movimiento');

			echo "<tr id='$id'>
				 <td align='center'> $contador </td>
				 <td align='center'> $su_rfc </td>
				 <td> $nombre </td>
				 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>";
			?>
				<!-- FUNCIONA -->
	     			<td align='center'> <a href="efecto_movimiento_personal.php?idm=<?php echo $registros->fields('id_mov'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea desactivar este movimiento? Operaci&oacute;n irreversible.');"> <img border="0" src="../img/rechazar.png" width='25' height='25' title="Presione este bot&oacute;n para desactivar este movimiento"/></a></td>
				<!-- FUNCIONA -->
				<td align='center'>
					<a href="efecto_movimiento_personal_historial.php?idp=<?php echo $registros->fields('rfc'); ?>&m=<?php echo $movimiento; ?>&a=<?php echo $aux_var; ?>" > <img border="0" src="../img/lista.gif" title="Presione este bot&oacute;n para ver el historial de movimientos"/></a><?php echo"</td>";?>
				<!-- FUNCIONA -->
				<td align='center'>
					<a href="borrar_movimiento_de_personal.php?idm=<?php echo $registros->fields('id_mov'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este movimiento para este trabajador?');"> <img border="0" src="../img/eliminar.gif" title="Presione aqu&iacute; para eliminar este registro"/></a>
		     		</td>
				<!-- FUNCIONA -->
				<td align='center'>
					<a href="agrega_clave_presupuestal_a_personal.php?idm=<?php echo $registros->fields('id_mov'); ?>&a=<?php echo $aux_var; ?>&m=<?php echo $movimiento; ?>" > <img border="0" src="../img/ver.png" title="Presione aqu&iacute; para ver las plazas actuales" width="25" height="25" /></a>
		     		</td>
				<!-- FUNCIONA -->
				<td align='center'>
					<a href="plazas_personal_historial.php?idp=<?php echo $registros->fields('rfc'); ?>&a=<?php echo $aux_var; ?>&m=<?php echo $movimiento; ?>" > <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver el historial de plazas"/></a>
				</td>
	<?php
			echo "</tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}

		echo "<tr><th colspan = '3'>Total</th><th colspan = '6'>".$contador."</th></tr>";
	?>
		  <tr><td>&nbsp;</td></tr>
		</table>

	<!--  INACTIVOS -->
	<table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" title="Tabla que lista el personal que ha tenido movimiento(s)">
<?php 
	$consulta_id="select  distinct(rfc) from movimiento__personal having rfc not in (select rfc from movimiento__personal having estatus = 1) order by rfc";

	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<tr><td align="center" colspan = "9"><b>'.(($status_inicial==1)?"Activo":"Inactivo").'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			    <tr>
				<td width="6%">&nbsp;</td>
				<th width="6%">Contador</th>
				<th width="9%">RFC</th>
				<th width="37%">Nombre</th>
				<th width="20%">Ultimo(s) Movimiento(s)</th>
				<th width="6%">Historial Movimiento(s)</th>

			    	<th width="4%">Historial de Plaza(s)</th>
				<td width="4%">&nbsp;</td>
			  </tr>';// 				<th width="5%">Ultima(s) Plaza(s)</th>
	else	echo '<tr align="center">
			<td> No existe personal dados de baja</th>
		  	</tr>';

		$var_reg = 0;
		while(!$registros->EOF){

			$aux_var = 1;  // El valor 1 indica que el movimiento esta desactivo

			$contador = $contador + 1;
			$su_rfc = $registros->fields('rfc');
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$su_rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');

			echo "<tr><td>&nbsp;</td>
				 <td id='$id' align='center'> $contador </td>
				 <td id='$id' align='center'> $su_rfc </td>
				 <td id='$id'> $nombre </td>
				 <td id='$id' align='center'>";
		// Ultimos movimientos
			$consulta_movimientos="select movimiento from movimiento__personal group by rfc having rfc not in (select rfc from movimiento__personal having estatus = 1) and
estatus = 2 and  (
( (movimiento = 20 and cant_mov = max(cant_mov)) or (movimiento = 20 and cant_mov = max(cant_mov)-1) or (movimiento = 20 and cant_mov = max(cant_mov)-2) ) or
( (movimiento = 95 and cant_mov = max(cant_mov)) or (movimiento = 95 and cant_mov = max(cant_mov)-1) or (movimiento = 95 and cant_mov = max(cant_mov)-2) ) or
( (movimiento = 10 and cant_mov = max(cant_mov)) or (movimiento = 10 and cant_mov = max(cant_mov)-1) or (movimiento = 10 and cant_mov = max(cant_mov)-2) ) )
and rfc = '$su_rfc'
order by rfc, cant_mov";
			$registros_movimientos=ejecutar_sql($consulta_movimientos);

			$var_reg = 0;

			while(!$registros_movimientos->EOF){

			$movimiento= $registros_movimientos->fields('movimiento');

			$registros_movimientos->MoveNext();

			if ($movimiento != $movimiento_pasado)
				echo '-'.$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."<br>";

			$movimiento_pasado = $movimiento;

			//$var_reg = $var_reg + 1;
			}

			//if ($var_reg == 1)
				//echo '-'.$movimiento_pasado.(($movimiento_pasado==10)?' (Base)':(($movimiento_pasado==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."<br>";

		// Fin ultimos movimientos

			echo "   </td>"; ?>

				<td id="<?php echo $id;?>" align="center">
					<a href="efecto_movimiento_personal_historial.php?idp=<?php echo $registros->fields('rfc'); ?>&a=<?php echo $aux_var; ?>" > <img border="0" src="../img/lista.gif" title="Presione este bot&oacute;n para ver el historial de movimientos"/></a><?php echo"</td>"; ?>

				<!--<td id="<?php echo $id;?>" align="center">
					<a href="agrega_personal_a_plantilla.php?idm=<?php echo $registros->fields('id_mov'); ?>&a=<?php echo $aux_var; ?>" > <img border="0" src="../img/ver.png" title="Presione aqu&iacute; para ver las ultimas plazas" width="25" height="25" /></a>
		     		</td> -->
				<td id="<?php echo $id;?>" align="center">
					<a href="plazas_personal_historial.php?idp=<?php echo $registros->fields('rfc');?>" > <img border="0" src="../img/lista.gif" title="Presione este bot&oacute;n para ver el historial de plazas"/></a>
				</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<?php
			echo "</tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}

		echo "<tr><td>&nbsp;</td><th colspan = '3'>Total</th><th colspan = '3'>".$contador."</th></tr>";
	?>
		  <tr><td>&nbsp;</td></tr>
	</table>

	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">
	<a href="../reportes/Movimientos.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Movimiento"/></a>
	</div>
	<? } ?>
