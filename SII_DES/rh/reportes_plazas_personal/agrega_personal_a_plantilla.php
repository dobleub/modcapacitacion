<?php
	/********************************************************
		Agregar Plaza(s) Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011

	Antes en personal
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);

	setlocale(LC_CTYPE, 'es');

	//$id_movimient = $_GET['idm'];
	if (isset($_GET['idm']))
	{ $_SESSION['idm']= $_GET['idm']; }

	// Insertar una nueva plaza
	$Crear = $_POST['Crear'];
	$rfc = $_POST['rfc_personal'];
	$clave_presupuestal = $_POST['clave_presupuestal'];
	$categoria_elegida = $_POST['categorias'];

	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$regresar = "javascript: document.location = 'efecto_movimiento_personal.php'";
 
if($Crear=="Crear"){
	if(($rfc=='0')or($clave_presupuestal=='0')or($categoria_elegida=='0')){
		echo "<script>alert('Seleccione el personal, la clave y la categoria que desea asignar');</script>";
		//echo "<script language='javascript' type='text/javascript'> window.location='efecto_movimiento_personal.php'</script>";
	    }
	else{
	$consulta_select_sql= "select count(id_plantilla_personal) as Total from plantilla_personal where rfc='$rfc' and id_clave=$clave_presupuestal and categoria='$categoria_elegida' and estatus=1 and fecha_registro='".date('d/m/Y')."'";
        $sql_consulta = ejecutar_sql($consulta_select_sql) ;
        $cantidad= $sql_consulta->fields('Total'); 

        if ($cantidad==1){
		$msg = "No Puede duplicar una Clave Presupuestal a un mismo trabajador";
		//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	?>
		<script language="javascript" type="text/javascript">
			msg = '<?php echo $msg; ?>'
			alert(msg)
			<?php echo $regresar; ?>
		</script> 
	<?php
		}
        else	{

		$qry_maximo_puesto_personal="select count(id_plantilla_personal) as maximo from plantilla_personal";
		$res_maximo_puesto_personal=ejecutar_sql($qry_maximo_puesto_personal);
		$maximo=$res_maximo_puesto_personal->fields('maximo');	// id_plantilla_personal

		$consulta_cant_mov_sql="select count(cantidad_plazas) as cantidad from plantilla_personal where rfc ='$rfc'";
		$registros_cant_mov=ejecutar_sql($consulta_cant_mov_sql);
		$cantidad_de_movimientos = $registros_cant_mov->fields('cantidad')+1;	// cantidad_plazas

		// Verificar que no excedan las 40 horas
		$consulta_horas_sql="select sum(horas_clave_plaza) as horas from plantilla_personal PP, registrar_claves RC where PP.id_clave = RC.id_clave and estatus = 1 and PP.rfc ='$rfc'";
		$registros_horas=ejecutar_sql($consulta_horas_sql);
		$horas_acumuladas = $registros_horas->fields('horas');	// horas_acumuladas
		//echo "acumuladas <br> + nuevas";
		$consulta_h_sql = "select horas_clave_plaza from registrar_claves where id_clave = $clave_presupuestal";
		$registros_h = ejecutar_sql($consulta_h_sql);
		$h_acumuladas = $registros_h->fields('horas_clave_plaza');	// horas_acumuladas

		if (($horas_acumuladas + $h_acumuladas) > 40){
		$msg = "Excede 40 hrs. Para agregar una plaza m&aacute;s es necesario que desactive otra!";
		//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	?>
		<script language="javascript" type="text/javascript">
			msg = '<?php echo $msg; ?>'
			alert(msg)
			<?php echo $regresar; ?>
		</script> 
	<?php
			}
		else	{
		$qry_insert_personal_a_lista="insert into plantilla_personal (id_plantilla_personal, rfc, id_clave, categoria, estatus, fecha_registro, fecha_actualizacion, cantidad_plazas, id_movimiento) values (".($maximo+1).",'$rfc', $clave_presupuestal,'$categoria_elegida',1,'".date('d/m/Y')."','".date('d/m/Y')."',$cantidad_de_movimientos,".$_SESSION['idm'].")";
		$res_insert_registro_personal=ejecutar_sql($qry_insert_personal_a_lista);
		$var_activa=1;
		echo "<script>alert('La nueva plaza se ha agregado correctamente');</script>";
			}
		}
	    }
	}

	//Desactivar una plaza
	$Crear = $_GET['idp'];
	if (isset($_GET['idp']))
	{
		$update_sql= "update plantilla_personal set estatus=2 where id_plantilla_personal=".($_GET['idp']);
		$sql_update = ejecutar_sql($update_sql) ;
		echo "<script>alert('La plaza ha sido desactivada con &eacute;xito');</script>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal ::.</title>
	</head>
 <body>
  <h2 align="center"> Plazas del Empleado <?php echo $rfc; ?></h2>
  <form name="personal" action="agrega_personal_a_plantilla.php" method="post">
   <input name="clave_puesto" type="hidden" id="id_clave" value="<?php echo ($maximo+1); ?>" size="20" maxlength="4" />
<?php if ( ( isset($_GET['a']) and ($_GET['a']==2) ) or $Crear=="Crear" or $var_activa==1) {?>
    <table align="center" title="Secci&oacute;n para agregar claves presupuestales al personal">
     <tr>
      <th>Personal</th>
      <th>Clave Presupuestal</th>
      <th>Categoria</th>
      </tr>
     <tr>
      <td id="non">
	<select name="rfc_personal" title="Presione aqu&iacute; para seleccionar el trabajador" onChange="javascript:document.solicitud_ver.submit();" title="Seleccione el rfc del personal al que desea asignarle clave presupuestal">
		<?php
	$consulta_rfc="select P.rfc, P.apellidos_empleado, P.nombre_empleado from personal P, movimiento_personal MP where MP.rfc = P.rfc and MP.id_mov =".$_SESSION['idm']."";
			$datos_rfc=ejecutar_sql($consulta_rfc);

			if(!$datos_rfc->rowcount())
			     echo "<option value='0' selected> No hay personal dado de alta </option>";
			else
			   { //echo "<option value='0' selected> -- Seleccione Personal -- </option>";
				while(!$datos_rfc->EOF){
				echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
				$datos_rfc->MoveNext();
				}
			    }
		?>
	</select>
<!-- 	<select name="rfc_personal" title="Presione aqu&iacute; para seleccionar el trabajador" onChange="javascript:document.solicitud_ver.submit();" title="Seleccione el rfc del personal al que desea asignarle clave presupuestal">
		<?php
			$consulta_rfc="select distinct (P.rfc), P.apellidos_empleado, P.nombre_empleado from personal P, movimiento_personal MP where MP.rfc = P.rfc and MP.status = 1 and P.status_empleado = '02' order by P.apellidos_empleado, P.nombre_empleado, P.rfc";  // and (MP.movimiento=10 or MP.movimiento=95)
			$datos_rfc=ejecutar_sql($consulta_rfc);

			if(!$datos_rfc->rowcount())
			     echo "<option value='0' selected> No hay personal dado de alta </option>";
			else
			   { echo "<option value='0' selected> -- Seleccione Personal -- </option>";
				while(!$datos_rfc->EOF){
				echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
				$datos_rfc->MoveNext();
				}
			    }
		?>
	</select>-->
      </td>
      <td id="non">
		<select name="clave_presupuestal" onChange="MM_jumpMenuCategorias('parent',this,0)" title="Presione aqu&iacute; para asignar una clave al trabajador">		
			<?php
			$sql_clave_presupuestal="select * from registrar_claves";
			$clave_presupuestal=ejecutar_sql($sql_clave_presupuestal);
			if(!$clave_presupuestal->rowcount())
				echo "<option value='0' selected> No hay claves Presupuestales Registradas </option>"; 
			else
			{       echo "<option value='0' selected> -- Seleccione Clave Presupuestal -- </option>";
				/**/while(!$clave_presupuestal->EOF){
			if($clave_presupuestal->fields('id_clave')==$post_id_clave){
				$selecciona=" selected";}
			else{ $selecciona="";} 
				echo '<option value="'.$clave_presupuestal->fields('id_clave').'"'.$selecciona.'>'.$clave_presupuestal->fields('clave_antes_punto').'.'.$clave_presupuestal->fields('clave_despues_punto').' C/'.($clave_presupuestal->fields('horas_clave_plaza') <= 9?'0':'').$clave_presupuestal->fields('horas_clave_plaza').' HRS. </option>';
				$clave_presupuestal->MoveNext();
						} 
			}
			?>
		</select> 
      </td>
      <td id="non"> 
		<select name="categorias" onChange="MM_jumpMenuCategorias('parent',this,0)" title="Presione aqu&iacute; para asignar una categoria al trabajador">		
			<?php
			$sql_categorias="select distinct categoria,descripcion_categoria from categorias";
			$categoria=ejecutar_sql($sql_categorias);

			if(!$categoria->rowcount())
				echo "<option value='0' selected> No hay Categorias Registradas </option>"; 
			else
				{       echo "<option value='0' selected> -- Seleccione una Categoria -- </option>";
		 		while(!$categoria->EOF){
					if($categoria->fields('categoria')==$post_categoria){ $selecciona=" selected";}
					else{ $selecciona="";}
					echo '<option value="'.$categoria->fields('categoria').'"'.$selecciona.'>'.$categoria->fields('descripcion_categoria').'</option>';
					$categoria->MoveNext();
					} 
				}
			?>
		</select>
      </td>
     </tr>
    </table>
	<?php } ?>
    <br><br>
    <div align="center">
<?php if ( ( isset($_GET['a']) and ($_GET['a']==2)) or $var_activa==1 ) {?>
     <input type="submit" name="Crear" class="boton" value="Crear" title="Presione este bot&oacute;n para guardar los cambios"/>
<?php } 
	$consulta_antiguedad="SELECT ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel from personal PE, plantilla_personal PP, movimiento_personal MP having PE.rfc = PP.rfc and PP.id_movimiento = MP.id_mov and MP.id_mov =". $_SESSION['idm'];
	
	$registros_antiguedad=ejecutar_sql($consulta_antiguedad);
	if(!$registros_antiguedad->EOF) $badera =1;

		$ingreso_rama= $registros_antiguedad->fields('ingreso_rama');
		$inicio_gobierno = $registros_antiguedad->fields('inicio_gobierno');
		$inicio_sep = $registros_antiguedad->fields('inicio_sep');
		$inicio_plantel= $registros_antiguedad->fields('inicio_plantel');
?>
     <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
    </div>
    <br><br>
    <table border="0" align="center" width='90%' title="Secci&oacute;n para mostrar las claves presupuestales de este trabajador" />

<?php if ( $badera == 1) {?>
	  <tr align="center"><td colspan = "7"><b>ANTIGUEDAD</b></td></tr>
	  <tr><td>&nbsp;</td></tr>
	  <tr><td>&nbsp;</td></tr>	
	  <tr align="center">
	    <td colspan = "2"> Gob : <b><?php echo $inicio_gobierno;?></b></td> 
	    <td> Sep : <b><?php echo $inicio_sep;?></b></td> 
	    <td colspan = "2"> Rama : <b><?php echo $ingreso_rama;?></b></td> 
	    <td colspan = "2"> ITT : <b><?php echo $inicio_plantel;?></b></td>   
	  </tr>
	  <tr><td>&nbsp;</td></tr>
	  <tr><td>&nbsp;</td></tr>	
  <?php
	}

	$consulta_registros="SELECT id_plantilla_personal, PP.rfc, id_clave, categoria, estatus, cantidad_plazas, apellidos_empleado, nombre_empleado, ingreso_rama, inicio_gobierno, inicio_sep, inicio_plantel from personal PE, plantilla_personal PP, movimiento_personal MP group by PP.rfc having PE.rfc = PP.rfc and PP.id_movimiento = MP.id_mov and MP.id_mov =". $_SESSION['idm']." order by estatus, id_plantilla_personal";
	
	$registros=ejecutar_sql($consulta_registros);
	$status_inicial = $registros->fields('estatus');
	$id = "non";
	$contador = 0;
	$total_horas = 0;
	if(!$registros->EOF)
		echo '<tr><td align="center" colspan = "7"><b>'.(($status_inicial==1)?"ACTIVO":"INACTIVO").'</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center">
				<th> No. </th>
				<th> Nombre Completo </th>
				<th> Clave </th>
				<th> Categoria </th>
				<th> Acci&oacute;n </th>
				<th> Eliminar </th>
				<th> Modificar </th>

		     </tr>';
	else{	echo '<tr align="center">';
		if ( isset($_GET['a']) and ($_GET['a']==2) )
			echo	'<td> Este trabajador no tiene plazas, proceda a realizar la asignaci&oacute;n </th>';
		else
			echo	'<td> Movimiento Inactivo. Para asignarle una nueva plaza a este trabajador primero debe asignarle un nuevo movimiento... </th>';
		echo '</tr>';
	    }

	while(!$registros->EOF){

		++$contador;
		$rfc = $registros->fields('rfc');
		$nombre_completo = $registros->fields('apellidos_empleado').' '.$registros->fields('nombre_empleado');;
		$ingreso_rama= $registros->fields('ingreso_rama');
		$inicio_gobierno = $registros->fields('inicio_gobierno');
		$inicio_sep = $registros->fields('inicio_sep');
		$inicio_plantel= $registros->fields('inicio_plantel');

		$idp=$registros->fields('id_plantilla_personal');

		$clave_presupuesto= $registros->fields('id_clave');
		  $qry_clave_presupuesto="select * from registrar_claves where id_clave =$clave_presupuesto";
		  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
		  $clave_ap=$res_clave_p->fields('clave_antes_punto');
		  $clave_dp=$res_clave_p->fields('clave_despues_punto');
		  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

		$total_horas = $total_horas + $horas_cp;

		$categoria= $registros->fields('categoria');
		  $qry_categoria="select distinct categoria,descripcion_categoria from categorias where categoria ='$categoria'";
		  $res_categoria=ejecutar_sql($qry_categoria);
		  $d_categoria=$res_categoria->fields('descripcion_categoria');

		$status = $registros->fields('estatus');

			if ($status_inicial != $status){
				$aux_var = 1;
				$total_horas = $total_horas - $horas_cp;

				echo "<tr align='center'><th colspan = '2' >Total de Horas</th><th>".$total_horas."</th><th colspan = '2'>Total de plazas</th><th  colspan = '2'>".--$contador."</th></tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
				<tr><td colspan = '7' align='center'><b>".(($status==1)?"ACTIVO":"INACTIVO")." </b></td></tr>
			</table>
			<table align='center' width='90%' title='Tabla que muestra la lista de trabajadores que actualmente no tienen plaza(s)'>
				<tr><td>&nbsp;</td></tr>
					<tr align='center' width='100%'>
						<th> No. </th>
						<th> Nombre Completo </th>
						<th> Clave </th>
						<th> Categoria </th>			
					  </tr>";// <th> Acci&oacute;n </th>
					$status_inicial = $status;
					$contador = 1;
				}

	echo "<tr id='$id'>
		 <td align='center'>".$contador."</td>
		 <td>".$nombre_completo."</td>
		 <td> ".$clave_ap.'.'.$clave_dp.' C/'.$horas_cp." HRS. </td>
		 <td align='center'>".$d_categoria."</td>";

	if ($status_inicial == 1){
			?>

	     <td align='center'> <a href="agrega_personal_a_plantilla.php?idp=<?php echo $registros->fields('id_plantilla_personal'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea desactivar esta plaza para este trabajador?');"> <img border="0" src="../img/rechazar.png" width='25' height='25' title="Presione este bot&oacute;n para desactivar esta plaza"/></a></td>

	     <td align='center'> <a href="borrar_personal_de_plantilla.php?idp=<?php echo $registros->fields('id_plantilla_personal'); ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar esta plaza para este trabajador?');"> <img border="0" src="../../../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar este registro"/></a></td>

             <td align='center'> <a href="modificar_clave_a_personal.php?idp=<?php echo $idp; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea modificar esta clave para este trabajador?');"> <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para modificar este registro"/></a></td>	     
	<?php
	}
	echo "</tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}


	if ($status_inicial==1) {echo "<tr align='center'><th colspan = '2' >Total de Horas</th><th>".$total_horas."</th><th colspan = '2'>Total de plazas</th><th colspan = '2'>".$contador."</th></tr>"; }
	else if ($status_inicial==2)	{echo "<tr align='center'><th colspan = '3'>Total de plazas </th><th>".$contador."</th>";}

	?>
<!--	-->
</table>
    <br><br>
	<?php if ($contador > 0) { ?>
		<br><br><br>
		<div align="center">  <a href="../reportes/Plazas_del_personal.pdf.php?idp=<?php echo $rfc; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Plazas Actuales-Personal"/></a>
		</div>
	<?php } ?>
   </form>
  </body>
 </html>
