<?php
	/********************************************************
		Efecto/Movimiento Historial

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 

	$volver = "efecto_movimiento_personal.php";
	$rfc=$_GET['idp'];
	$mov = $_GET['m'];  //parametro de efecto_movimiento_personal.php que indica el movmiento actual
	$flag = $_GET['a']; //parametro que indica si los movimientos seran activos
?>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />

		<h2 align="center">Historial de Movimientos del Empleado: <?php echo $rfc; ?></h2>

<form method="post" action="efecto_movimiento_personal_listado.php" name="datos" >
<br><br>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="90%" title="Tabla que lista el personal con su respectivo movimiento desde el &uacute;ltimo (m&aacute;s actual) hasta el primero que tuvo (m&aacute;s antiguo)">
  <tr>
    <td>&nbsp;</td> 
  </tr>
<?php 
	$consulta_id="select * from movimiento__personal where rfc ='$rfc' order by id_mov desc";
	$registros=ejecutar_sql($consulta_id);
	$id = "non";
	$contador = 0;
	if(!$registros->EOF)
		echo '<tr><td>&nbsp;</td></tr><tr>
			    <th>Contador</th>
			    <th>RFC</th>
			    <th>Nombre</th>
			    <th width="20%">Movimiento</th>
			    <th>Fecha de registro</th>
			    <th>Hora de registro</th>
			    <th>Status Mov.</th>
			  </tr>';
	else	echo '<tr align="center">
			<td> Este es su primer movimiento, no tiene historial </th>
		  	</tr>';
	$cont = 1;
		while(!$registros->EOF){
			$contador = $contador + 1;
			$id_mov = $registros->fields('id_mov');
			$su_rfc = $registros->fields('rfc');
				$consulta_nombre="select apellidos_empleado, nombre_empleado from personal where rfc ='$su_rfc'";
				$reg=ejecutar_sql($consulta_nombre);
				$nombre = $reg->fields('apellidos_empleado').' '.$reg->fields('nombre_empleado');

			$movimiento = $registros->fields('movimiento');
			$e_iniciales = $registros->fields('fecha_registro');
			$e_finales = $registros->fields('hora_registro');

			$status = $registros->fields('estatus');
		// Para ultimos movimientos activos
			if ($status == 1)
				if ($movimiento == $mov)
				echo "<tr bgcolor='D42A2A'>
					 <td align='center'> $contador </td>
					 <td align='center'> $su_rfc </td>
					 <td> $nombre </td>
					 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
					 <td align='center'> $e_iniciales </td>
					 <td align='center'> $e_finales </td>
					 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
				     </tr>";
				else
				echo "<tr>
					 <td align='center'> $contador </td>
					 <td align='center'> $su_rfc </td>
					 <td> $nombre </td>
					 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
					 <td align='center'> $e_iniciales </td>
					 <td align='center'> $e_finales </td>
					 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
				     </tr>";
		// Para ultimos movimientos inactivos
			else if ($flag == 1 and $cont < 4 and ($movimiento != $movimiento_pasado) ){
				echo "<tr bgcolor='F4F130'>
					 <td align='center'> $contador </td>
					 <td align='center'> $su_rfc </td>
					 <td> $nombre </td>
					 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
					 <td align='center'> $e_iniciales </td>
					 <td align='center'> $e_finales </td>
					 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
				     </tr>";
			$movimiento_pasado = $movimiento;
			$cont = $cont +1;
			}

		// Para movimientos inactivos del resto del historial
			else
			echo "<tr id='$id'>
				 <td align='center'> $contador </td>
				 <td align='center'> $su_rfc </td>
				 <td> $nombre </td>
				 <td align='center'>".$movimiento.(($movimiento==10)?' (Base)':(($movimiento==20)?' (Int. Limitado)':' (Int. Ilimitado)'))."</td>
				 <td align='center'> $e_iniciales </td>
				 <td align='center'> $e_finales </td>
				 <td align='center'>".(($status==1)?'Activo':'Inactivo')."</td>
			     </tr>";

			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}

 	if ($contador > 0) { 
		echo '<tr><td>&nbsp;</td></tr>';
		if ($flag == 2){
			echo '<tr><td colspan = "7"><i> * La filas blancas indican los movimientos activos </i></td></tr>';
			echo '<tr><td colspan = "7"><i> * La fila roja indica el movimiento actual activo</i></td></tr>';
		}
		if ($flag == 1){
			echo '<tr><td colspan = "7"><i> * La filas amarillas indican los &uacute;ltimos movimientos activos </i></td></tr>';
		}
		echo '<tr><td colspan = "7"><i> * El orden de esta lista es del &uacute;ltimo movimiento registrado al m&aacute;s antiguo </i></td></tr>';
		}
	?>
</table>
	<br><br>
	<div align="center">
		<input type="button" value="Cancelar" class="boton" onclick="javascript:window.location = '<?php echo $volver; ?>'" />
	</div>

</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Clave_presupuestal_historial.pdf.php?idp=<?php echo $rfc; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Historial Movimiento"/></a>
	</div>
	<? } ?>
