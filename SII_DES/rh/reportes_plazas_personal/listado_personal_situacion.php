<?php
	/********************************************************
		Listado de Pesonal seg�n su Situaci�n

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnol�gico de Minatitl�n

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 59);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Listado de Personal seg&uacute;n su Situaci&oacute;n ::.</title>
	</head>
<body>
	<h2 align="center">Listado de Personal seg&uacute;n su Situaci&oacute;n (Licencia/Activos)</h2>
 
  <?php
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, clave_area, status_empleado from personal where (status_empleado = '01' or status_empleado = '02')   order by apellidos_empleado, nombre_empleado, rfc";  //  nivel_estudios,
	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0;
	$clave_inicial = $registros->fields('clave_area');

	if(!$registros->EOF)
		echo '<table align="center" width="98%" title="Tabla que muestra la lista del personal seg&uacute;n su situaci&oacute;n"><tr><td>&nbsp;</td></tr>
		 <tr align="center">
			<th width="2%"> No. </th>
			<th width="7%"> RFC </th>
			<th width="20%"> Nombre completo </th>			
			<th width="25%"> Departamento </th>
			<th > Puesto </th>
			<th width="7%"> Nivel maximo de estudios</th>			
			<th width="19%"> Plaza(s) </th>			
		  </tr>';
	else	echo '<table align="center" title="No hay personal que mostrar seg&uacute;n su situaci&oacute;n">
		  <tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		$contador = $contador + 1;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$estatus = $registros->fields('status_empleado');

			$qry_estudios="select nivel_estudios, grado_maximo_estudios from estudios_personal where rfc = '$rfc'";
			$res_estudios=ejecutar_sql($qry_estudios);
			$nivel = $res_estudios->fields('nivel_estudios');
			$grado = $res_estudios->fields('grado_maximo_estudios');

			if (($nivel == null) and ($grado == null)){
				/*personal		--> nivel_estudios, grado_maximo_estudios
				estudios_personal	--> nivel_estudios, grado_maximo_estudios
				nivel_de_estudios	--> nivel_estudios, descripcion_nivel_estudios
				nivel_escolar		--> nivel_escolar, descripcion_nivel*/
				$descripcion_nivel_estudios = 'Sin Registro';
			}
			else {	$qry_nivel_estudios = "select descripcion_nivel_estudios from nivel_de_estudios where nivel_estudios = '$nivel'";
				$res_nivel_estudios = ejecutar_sql($qry_nivel_estudios);
				if ($res_nivel->EOF){
					$qry_nivel_escolar="select descripcion_nivel from nivel_escolar where nivel_escolar = '$grado'";
					$res_nivel_escolar=ejecutar_sql($qry_nivel_escolar);
					$descripcion_nivel_estudios = $res_nivel_escolar->fields('descripcion_nivel');
					}
				else{ $descripcion_nivel_estudios = $res_nivel_estudios->fields('descripcion_nivel_estudios'); }
			}
		$clave = $registros->fields('clave_area');
		
		$area="select descripcion_area from organigrama where clave_area='$clave'";
		$res_area=ejecutar_sql($area);
		$descripcion_area = $res_area->fields('descripcion_area');
		
		$qry_puesto = "select P.descripcion_puesto from puestos_personal PP, puestos P where PP.rfc = '$rfc' and PP.clave_puesto = P.clave_puesto";
		$res_puesto = ejecutar_sql($qry_puesto);
		$puesto = $res_puesto->fields('descripcion_puesto');
		
		if ( $estatus == 1){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
		echo "<tr bgcolor='#FAAC58'>
		 <td align='center'> $contador </td>
		 <td align='center'> $rfc </td>
		 <td> $ae $ne </td>		
		 <td> $descripcion_area </td>
		 <td> ".(($puesto == null)?'Sin Puesto':$puesto)."</td>
		 <td> $descripcion_nivel_estudios </td>
		 <td>";		
			$consulta_clave="select CPP.id_clave from personal PE, claves_presupuestales_personal CPP, movimiento__personal MP where PE.rfc = MP.rfc and MP.rfc ='$rfc' and id_mov = id_movimiento and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){

			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}
			if ($registros_clave->EOF) echo 'Sin plaza(s)';		
		echo "</td>		
		 </tr>";
		} else {
		echo "<tr id='$id'>
		 <td align='center'> $contador </td>
		 <td align='center'> $rfc </td>
		 <td> $ae $ne </td>		
		 <td> $descripcion_area </td>
		 <td>".(($puesto == null)?'Sin Puesto':$puesto)."</td>
		 <td> $descripcion_nivel_estudios </td>
		 <td>";		
			$consulta_clave="select CPP.id_clave from claves_presupuestales_personal CPP, movimiento__personal MP where MP.rfc = '$rfc' and id_mov = id_movimiento and CPP.estatus = 1 order by id_clave_presupuestal_personal desc";
			$registros_clave=ejecutar_sql($consulta_clave);
			while(!$registros_clave->EOF){

			$clave_presupuesto= $registros_clave->fields('id_clave');
				  $qry_clave_presupuesto="select * from claves_presupuestales where id_clave =$clave_presupuesto";
				  $res_clave_p=ejecutar_sql($qry_clave_presupuesto);
				  $clave_ap=$res_clave_p->fields('clave_antes_punto');
				  $clave_dp=$res_clave_p->fields('clave_despues_punto');
				  $horas_cp=$res_clave_p->fields('horas_clave_plaza');

				$registros_clave->MoveNext();
				echo "$clave_ap . $clave_dp C/$horas_cp HRS. <br>";
			}
			if ($registros_clave->EOF) echo 'Sin plaza(s)';				
		echo "</td>		
		 </tr>";
		}
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=6 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>	
</table>

	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	  </div>
	</form>
</body>
</html>
