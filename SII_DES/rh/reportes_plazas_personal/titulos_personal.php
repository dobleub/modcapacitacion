<?php
	/********************************************************
		Registro de títulos

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	seguridad('DRH');
	$web->Seguridad($_SESSION['susr'],8); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 65);

	$Crear = $_POST['Crear'];
	$titulo = strtoupper($_POST['titulo']);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

	$qry_maximo_titulo="select max(id_titulo) as maximo from titulos";
	$res_maximo_titulo=ejecutar_sql($qry_maximo_titulo);
	$maximo=$res_maximo_titulo->fields('maximo'); 	

	if($Crear=="Crear"){ 
		if($titulo == "")
			echo "<script>alert('Escriba el titulo que desea agregar');</script>";
		else{  
		$consulta_select_sql= "select count(id_titulo) as Total from titulos where titulo='$titulo'";
		$sql_consulta = ejecutar_sql($consulta_select_sql) ;
		$cantidad= $sql_consulta->fields('Total'); 

			if ($cantidad == 0){
				$qry_inserta_titulo="insert into titulos (id_titulo, titulo) values (".($maximo+1).",'$titulo')"; 
				$res_inserta_titulo=ejecutar_sql($qry_inserta_titulo);
				echo "<script>alert('El titulo ha sido agregado correctamente');</script>";
			}
			else {
				$msg = "No Puede duplicar el registro de un titulo";
				$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
			?>
		<script language="javascript" type="text/javascript">
			msg = '<?php echo $msg; ?>'
			alert(msg)
			<?php echo $regresar; ?>
		</script>
	<?php
			}
		}
	}

	//eliminar un titulo
	if (isset($_GET['t']))
	{	$delete_sql= "delete from titulos where id_titulo = ".($_GET['t']);
		$sql_delete = ejecutar_sql($delete_sql) ;
		echo "<script>alert('El titulo ha sido removido con exito');</script>";
	}
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>

		<title>.:: Registro de T&iacute;itulos ::.</title>
	</head>
<body>
	<h2 align="center"> Registro de T&iacute;itulos </h2>

<form name="personal" action="titulos_personal.php" method="post">

<input name="id_clave" type="hidden" id="id_clave" value="<?php echo ($maximo+1); ?>" />

	<table align="center" title="Secci&oacute;n para capturar claves presupuestales">
		<tr> 
			<th width="80" align="center"> T&iacute;itulo: </th>
			<td id="non">
				<input name="titulo" type="text" size="10" maxlength="15" tabindex="1" onkeyup="checkLen(this, this.value)" onblur="this.value = this.value.toUpperCase()" title="Ingrese el t&iacute;tulo que desea registrar">
			</td>
		</tr>
	</table>
<br><br>
  <div align="center">
    <input type="submit" name="Crear" class="boton" value="Crear" title="Presione este bot&oacute;n para guardar los cambios" />
    <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de Claves Presupuestales existentes en el instituto">
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		<?php
		$consulta_registros="select * from titulos order by id_titulo"; // order by descripcion_puesto
		$registros=ejecutar_sql($consulta_registros);
		$id = "non";
		$contador = 0;
	if(!$registros->EOF)
		echo "<tr align='center'>
			    		<th> No. </th>
			    		<th align='center'> T&iacute;tulo </th>
					<th> Eliminar </th>
		      </tr>";
	else	echo '<tr align="center">
			 <td> No exite alg&uacute;n t&iacute;tulo registrado </td>
		      </tr>';

		while(!$registros->EOF){
			++$contador;
			$id_title = $registros->fields('id_titulo');
			$title = $registros->fields('titulo');
			echo "<tr id='$id'>";
			echo "<td align='center'> $id_title </td>
			 <td> $title </td>
			 <td align='center'>";?>
	  <a href="titulos_personal.php?t=<?php echo $id_title; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este titulo?');"> <img border="0" src="../../../img/eliminar.gif" title="Presione aqu&iacute; para eliminar este registro" /></a><?php echo"</td></tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
			}
		?>
	</table>
</form>
</body>
</html>
