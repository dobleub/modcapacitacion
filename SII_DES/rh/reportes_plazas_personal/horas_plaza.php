<?php
	/********************************************************
		Horas de plaza del Personal

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Actualizado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	//require_once("../includes/config.inc.php");
	seguridad('DRH'); 
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$h = $_POST['h'];
	$Consultar = $_POST['Consultar'];
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<title>.:: Horas de plaza del Personal ::.</title>
</head>

<body>

<form name="form1" method="post" action="horas_plaza.php">
<br><br>
<table border="0" align="center" title="Secci&oacute;n para consultar el total de horas de un trabajador">
<tr>
<th>Seleccione el n&uacute;mero de horas</th>
    <td>
	<select name="h" title="Presione aqu&iacute; para seleccionar las horas">
	      <?php for($s=1;$s<=40;$s++){?>
	      <option value="<?php echo $s; ?>"><?php echo $s; ?></option>
	      <?php }?>
	</select>
     </td>

    <td>&nbsp;<input type="submit" value="Consultar" name="Consultar" class="boton" tabindex="5" title="Presione este bot&oacute;n para realizar la consulta"/></td>
  </tr>
</table>

  <?php	if ($Consultar=="Consultar"){	$titulo = "Personal con plaza docente de maximo $h horas";	?>

		<h2 align="center"><?php echo $titulo;?></h2>

		  <?php
			$consulta_registros="select distinct (P.rfc), nombre_empleado, apellidos_empleado, nombramiento  from personal P, claves_presupuestales_personal CPP, movimiento__personal MP where (P.status_empleado = '02') and (P.tipo_personal != 'H' or P.tipo_personal != 'Z')  and MP.rfc = P.rfc and id_movimiento = id_mov and CPP.estatus = 1 order by P.rfc, apellidos_empleado, nombre_empleado";

			$registros=ejecutar_sql($consulta_registros);
	
			$id = "non";
	
			$contador = 0;

			if(!$registros->EOF)
				echo '<table align="center" width="60%" title="Relaci&oacute;n de Personal con el total de horas"><tr><td>&nbsp;</td></tr>
				  <tr align="center" width="100%">
					<th> No. </th>
					<th> RFC </th>
					<th> Nombre completo </th>
					<th> Horas </th>
					<th> Plaza(s) </th>
					<th> Nombramiento </th>			
				  </tr>';
			else	echo ' <table align="center" title="No exite personal con plazas registradas">
				  <tr align="center">
					<td> No exite personal con plazas registradas </th>
				  </tr>';

			//Fin primer registro
			$registros->MoveFirst();
	
			while(!$registros->EOF){
				$rfc = $registros->fields('rfc');
				$ae = $registros->fields('apellidos_empleado');
				$ne = $registros->fields('nombre_empleado');
				$nom = $registros->fields('nombramiento');
				
				//$qry_horas ="select sum(horas_clave_plaza) as horas,count(horas_clave_plaza) as plazas from registrar_claves RC, plantilla_personal PP where RC.id_clave = PP.id_clave and rfc='$rfc' ";  // having sum(horas_clave_plaza)=$h
				$qry_horas ="select sum(horas_clave_plaza) as horas,count(horas_clave_plaza) as plazas from claves_presupuestales CP, claves_presupuestales_personal CPP, movimiento__personal MP where CP.id_clave = CPP.id_clave and id_movimiento=id_mov and MP.rfc='$rfc' and CPP.estatus = 1";  
					$plazas_por_persona = ejecutar_sql($qry_horas); 
					$horastot = $plazas_por_persona->fields("horas");		
					$plazastot = $plazas_por_persona->fields("plazas");
		
				if ($horastot <=$h)
						if(!$nombramiento->EOF){
							++$contador;
							$id = ($id=="non")?"par":"non";
							echo "<tr id='$id'>
							 <td align='center'> $contador </td>
							 <td align='center'> $rfc </td>
							 <td> $ae $ne </td>
							 <td align='center'> $horastot </td>
							 <td align='center'> $plazastot plaza".(($plazastot==1)?'':'s')."</td>		
							 <td>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
							 </tr>";
						}			
				$registros->MoveNext(); 
			}//Fin whil
			?>
		</table>
	<?php }//Fin consultar?>
  <br />
	<div align="center">
   	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
</form>
	<?php if($contador > 0){ ?>  
	<br><br><br>
	<div align="center">  <a href="../reportes/Horas_plaza.pdf.php?h=<?php echo $h; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal con est&eacute; total de horas"/></a>
	</div>
	<?php } ?>
</body>
</html>
