<?php
	/********************************************************
		Personal con Honorarios

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 58);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

		function fecha_ingreso ($quincena, $anyo)
			{
				$mes= intval ($quincena / 2);
			 	if ($quincena%2==1)	{$semana = 'primera'; $mes = $mes+1;}	else $semana = 'segunda';  
				if ($mes==1) $mes="Enero";
				else if ($mes==2) $mes="Febrero";
				else if ($mes==3) $mes="Marzo";
				else if ($mes==4) $mes="Abril";
				else if ($mes==5) $mes="Mayo";
				else if ($mes==6) $mes="Junio";
				else if ($mes==7) $mes="Julio";
				else if ($mes==8) $mes="Agosto";
				else if ($mes==9) $mes="Septiembre";
				else if ($mes==10) $mes="Octubre";
				else if ($mes==11) $mes="Noviembre";
				else if ($mes==12) $mes="Diciembre";
			return 'La '.$semana.' quincena de '.$mes.' de '.$anyo;
			}
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Listado Personal por Honorarios::.</title>
	</head>

<body>
		<h2 align="center">Personal con Honorarios</h2>

  <?php
	//$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, clave_area, curp_empleado, no_tarjeta, substring(inicio_sep,1,4) as ano_sep, substring(inicio_sep,5,6) as sem_sep, status_empleado from personal where (status_empleado = '01' or status_empleado = '02') order by status_empleado, apellidos_empleado, nombre_empleado";

$consulta_registros="select distinct(P.rfc), P.apellidos_empleado, P.nombre_empleado, no_tarjeta, curp_empleado, substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel , horas_honorarios, nombramiento from personal P where status_empleado = '02' 
	    	 and P.tipo_personal = 'H' and horas_honorarios != 0 order by apellidos_empleado, nombre_empleado";

	$registro=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	$clave_inicial = $registro->fields('clave_area');

	if(!$registro->EOF)
		echo '<table align="center" width="80%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n"><tr><td>&nbsp;</td></tr>
		 <tr align="center" width="100%">
			<th width="4%"> No. </th>
			<th width="6%"> No. tarjeta </th>
			<th width="9%"> RFC </th>
			<th> Nombre Completo </th>
			<th width="14%"> CURP </th>
			<th width="10%"> Nombramiento </th>
			<th width="4%"> Horas </th>
			<th width="18%"> Ingreso Plantel </th>				
		  </tr> ';
	else	echo '<table align="center">
		  	<tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registro->MoveFirst();

	while(!$registro->EOF){
		$contador = $contador + 1;
		$nt = $registro->fields('no_tarjeta');
		$rfc = $registro->fields('rfc');
		$ae = $registro->fields('apellidos_empleado');
		$ne = $registro->fields('nombre_empleado');
		$clave = $registro->fields('clave_area');
		$curp = $registro->fields('curp_empleado');
		$ap = $registro->fields('ano_plantel');
		$sp = $registro->fields('sem_plantel');
		//$status = $registro->fields('status_empleado');
		$horas_honorarios = $registro->fields('horas_honorarios');
		$nom = $registro->fields('nombramiento');
		
		echo "<tr id='$id'>
			<td align='center'> $contador </td>
			 <td align='center'>".$registro->fields('no_tarjeta')."</td>
			 <td align='center'> $rfc </td>
			 <td> $ae $ne </td>		
			 <td align='center'> $curp </td>
			 <td align='center'>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z')?'Sin Tipo':'Sin Registro')))."</td>
			 <td align='center'> $horas_honorarios </td>
			 <td align='center'> $ap$sp </td>
			</tr>"; // fecha_ingreso ($sp, $ap)
		
		$id = ($id=="non")?"par":"non";
		$registro->MoveNext();
		}
	?>
	
</table>

	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	  </div>
	</form>
	<? if ($contador > 0) { ?>
	<br><br>
	<div align="center">  <a href="../reportes/Personal_honorarios.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Personal-Honorarios"/></a>
	</div>
	<? } ?>
</body>
</html>
