<?php
	/********************************************************
		Personal Detalle
		Muestra Jefes, Administrativos, Docentes con o sin plaza

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 62);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$tipo= $_GET['tipo']; // <td>".$CFG->instituto."</td>
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal Detalle::.</title>
	</head>
<body>
<?php	// Condición: Activo, Inactivo, Administrativo, Docente, Jefe   de odb.personal, odb.jefes, odb.organigrama
	

	if ($tipo=='J'){
	$titulo = "Jefes de personal";
	$consulta_inicial="and P.rfc = J.rfc";
	}
	else 
		if ($tipo=='D'){ 
		$titulo = "Personal Docente";
		$consulta_inicial="and P.rfc not in (select rfc from jefes where rfc is not null) 
				   and nombramiento = 'D'";
		}
		else{ 
		$titulo = "Personal Administrativo";
		$consulta_inicial="and nombramiento = 'A'
				   and P.rfc not in (select rfc from jefes where rfc is not null)";
		}//fin else

	$consulta_registros="select distinct(P.rfc), apellidos_empleado, nombre_empleado, inicio_sep, curp_empleado, sexo_empleado, P.status_empleado, O.descripcion_area, P.nombramiento from personal P, jefes J, organigrama O
	where (status_empleado = '01' or status_empleado = '02') $consulta_inicial
	 and O.clave_area = P.clave_area
	order by status_empleado, apellidos_empleado, nombre_empleado";

	//, PU.nivel_puesto, PU.descripcion_puesto	, puestos_personal PUP, puestos PU		and PUP.rfc = P.rfc and PUP.clave_puesto = PU.clave_puesto
?>
	<h2 align="center"><?php echo $titulo;?> (Licencia/Activos)</h2>

  <?php
	$registros=ejecutar_sql($consulta_registros);
	$status_inicial = $registros->fields('status_empleado');
	
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<table align="center" width="99%" title="Tabla que muestra la lista de trabajadores con o sin plaza(s), con o sin movimiento(s)">
			<tr><td colspan = "9" align="center"><b>'.(($status_inicial=="02")?"Activo":(($status_inicial=="00")?"Baja por renuncia":(($status_inicial=="01")?"Licencia":(($status_inicial=="03")?"Baja por jubilaci&oacute;n":(($status_inicial=="04")?"Inactivo por cambio de RFC":(($status_inicial=="05")?"Baja por fallecimiento":(($status_inicial=="06")?"Inactivo":" "))))))).'</b></td></tr>
		  	<tr>
				<td>&nbsp;</td>
		  	</tr>
			  	<tr align="center" width="100%">
				<th	width="2%"> No. </th>
				<th width="7%"> RFC </th>
				<th width="10%"> CURP </th>
				<th width="4%"> G&eacute;nero </th>
				<th width="20%"> Nombre </th>
				<th> &Aacute;rea </th>'
				.(($tipo !='J')?'<th width="10%"> Rango Puesto </th>':'').
				'<th width="16%"> Puesto </th>'
				.(($tipo =='J')?'<th width="6%"> Nombramiento </th>':'')
				.'<th width="5%"> Ingreso SEP </th> </tr>';
	else	echo '<table align="center" title="No existe personal registrado">
		  	<tr align="center">
				<td> No exite personal registrado </th>
		  	</tr>' ;

	//Fin primer registro
	$registros->MoveFirst();

	while(!$registros->EOF){
		$contador = $contador + 1; 
	//$tmd = "Nombramiento";
	$band = 0;

		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		$nom = $registros->fields('nombramiento');
		$status = $registros->fields('status_empleado');
		$sexo = $registros->fields('sexo_empleado');
		$curp = $registros->fields('curp_empleado');
		$descripcion_area = $registros->fields('descripcion_area');
		//$descripcion_puesto = $registros->fields('descripcion_puesto');

			/**/$qry_nivel="select clave_puesto from puestos_personal where rfc='$rfc'";
			$res_nivel=ejecutar_sql($qry_nivel);

			if (!$res_nivel->EOF){
				$idpuesto = $res_nivel->fields('clave_puesto');
				$qry_nivel2="select descripcion_puesto, nivel_puesto from puestos where clave_puesto=$idpuesto";
				$res_nivel2=ejecutar_sql($qry_nivel2);
				$descripcion_puesto = $res_nivel2->fields('descripcion_puesto');
				$se = $res_nivel2->fields('nivel_puesto');
			}
			else {$descripcion_puesto = "Sin Puesto";
			      $se = 8;
			}

		//if ($descripcion_puesto == null)	$descripcion_puesto = "Sin Puesto";

			if ($status_inicial != $status){ 
				echo '<tr><th colspan = "8">Total</th><th>'.(--$contador).'</th></tr>
				      <tr><td>&nbsp;</td></tr>
				      <tr><td>&nbsp;</td></tr>
				      <tr>
					 <td colspan = "9" align="center"><b>'.(($status=="02")?"Activo":(($status=="00")?"Baja por renuncia":(($status=="01")?"Licencia":(($status=="03")?"Baja por jubilaci&oacute;n":(($status=="04")?"Inactivo por cambio de RFC":(($status=="05")?"Baja por fallecimiento":(($status=="06")?"Inactivo":" "))))))).'</b></td>
				      </tr>
				</table>
				<table border="0" align="center" width="99%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n">
				      <tr><td>&nbsp;</td></tr>
				      <tr align="center" width="100%">
					 <th width="2%"> No. </th>
					 <th width="7%"> RFC </th>
					 <th width="10%"> CURP </th>
					 <th width="4%"> G&eacute;nero </th>
					 <th width="20%"> Nombre </th>
					 <th> &Aacute;rea </th>'
					 .(($tipo !='J')?'<th width="10%"> Rango Puesto </th>':'').
					 '<th width="16%"> Puesto </th>'
					 .(($tipo =='J')?'<th width="6%"> Nombramiento </th>':'')
					 .'<th width="5%"> Fecha ingreso </th>	
				       </tr>';
				$status_inicial = $status;
				$contador = 1;
				}
		
		echo "<tr id='$id'>
		 	<td align='center'> $contador </td>
		 	<td align='center'> $rfc </td>					
		 	<td align='center'> $curp </td>
		 	<td align='center'>".(($sexo == "F" )?'Mujer':'Hombre')."</td>
		 	<td> $ne $ae </td>
		 	<td> $descripcion_area </td>";

		if ($tipo == "A" or $tipo == "D") {	
		echo "<td align='center'>".(($se=="1")?"DIRECCI&Oacute;N":(($se=="2")?"SUBDIRECCI&Oacute;N":(($se=="3")?"JEFES DE DEPARTAMENTO":(($se=="4")?"JEFES DE AREA, COORDINADORES":(($se=="5")?"JEFES DE PROYECTO Y LABORATORIOS":(($se=="6")?"SECRETARIA":(($se=="7")?"AUXILIAR":"Sin Nivel")))))))."</td>";} 

		echo "<td>".$descripcion_puesto."</td>";

		if ($tipo =='J') { 
		echo "<td align='center'>".(($nom=='D')?'Docente':(($nom == 'A')?'Administrativo':(($nom == 'Z' )?'Sin Tipo':'Sin Reg.')))."</td>";}
	
		$sep  = $registros->fields('inicio_sep');
		$anho = substr($registros->fields('inicio_sep'), 0,4 );
		$quin = substr($registros->fields('inicio_sep'), 4,6 );
			switch($quin){
					case "01": $mes = "01"; $dia = "01"; break;
					case "02": $mes = "01"; $dia = "15"; break;
					case "03": $mes = "02"; $dia = "01"; break;
					case "04": $mes = "02"; $dia = "15"; break;
					case "05": $mes = "03"; $dia = "01"; break;
					case "06": $mes = "03"; $dia = "15"; break;
					case "07": $mes = "04"; $dia = "01"; break;
					case "08": $mes = "04"; $dia = "15"; break;
					case "09": $mes = "05"; $dia = "01"; break;
					case "10": $mes = "05"; $dia = "15"; break;
					case "11": $mes = "06"; $dia = "01"; break;
					case "12": $mes = "06"; $dia = "15"; break;
					case "13": $mes = "07"; $dia = "01"; break;
					case "14": $mes = "07"; $dia = "15"; break;
					case "15": $mes = "08"; $dia = "01"; break;
					case "16": $mes = "08"; $dia = "15"; break;
					case "17": $mes = "09"; $dia = "01"; break;
					case "18": $mes = "09"; $dia = "15"; break;
					case "19": $mes = "10"; $dia = "01"; break;
					case "20": $mes = "10"; $dia = "15"; break;
					case "21": $mes = "11"; $dia = "01"; break;
					case "22": $mes = "11"; $dia = "15"; break;
					case "23": $mes = "12"; $dia = "01"; break;
					case "24": $mes = "12"; $dia = "15"; break;
				}//fin switch

		echo "<td align='center'> $dia/$mes/$anho </td>		
		</tr>"; // 

		//Fin mostrar las plazas		
		$registros->MoveNext();
	}
	echo "<tr><th colspan = '8'>Total</th><th>".$contador."</th></tr>";
	?>
</table>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Jefes_docentes_admin.pdf.php?t=<?php echo $tipo;?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: <?php echo (($tipo=='J')?'Jefes':(($tipo=='A')?'Administrativos':'Docentes')); ?> " /></a>
	</div>
	<? } ?>
</body>
</html>
