<?php
	/********************************************************
		Plantilla de personal

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Plantilla de Personal ::.</title>
	</head>

<body>
<?php	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";	?>
<h2 align="center">Plantilla de Personal</h2>
  <?php
	$id = "non";
	$contador = 1;
	$consulta_registros="select clave_area, descripcion_area from organigrama order by clave_area";//descripcion_area";
	$registros=ejecutar_sql($consulta_registros);

	if(!$registros->EOF){
		echo "<table align='center' title='Lista ordenada del personal incorporado en cada departamento'>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		  	<tr align='center'>
			<th> No. </th>
			<th> Departamento</th>
			<th> Ver personal</th>
			</tr>
			<tr id='$id'>
			 <td> $contador </td>
			 <td>TODOS LOS DEPARTAMENTOS</td>
			 <td align='center'>";?>

  <a href="plantilla_personal_empleados.php?depto=TODOS&descrip=<?php echo $descrip; ?>" target = "_self"> <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver la plantilla de todos los departamentos" /></a><?php echo"</td>";
		echo "</tr>";	
	}
	else	echo ' <table align="center" title="Esta tabla muestra en orden n&uacute;mero el personal incorporado en cada departamento">
		  	<tr align="center">
			<td> No exite personal registrado </th>
		  	</tr>';
	$id = "par";
	while(!$registros->EOF){
		$contador = $contador + 1;
		$clave = $registros->fields('clave_area');
		$descrip = $registros->fields('descripcion_area');
		echo "<tr id='$id'>
		 <td> $contador </td>
		 <td> $descrip </td>
		 <td align='center'>";?>
  <a href="plantilla_personal_empleados.php?depto=<?php echo $clave; ?>&descrip=<?php echo $descrip; ?>" target = "_self"> <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver s&oacute;lo la plantilla de este departamento" /></a><?php echo"</td>";
		echo "</tr>		
		 </tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
</table>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
  </div>
</form>
</body>
</html>
