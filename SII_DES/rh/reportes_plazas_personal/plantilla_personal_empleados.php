<?php
	/********************************************************
		Plantilla de personal Por departamento

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnológico de Minatitlán

		Modificado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Plantilla - Personal -Empleados ::.</title>
	</head>
<body>
<?php
	$depto = $_GET['depto'];
	$descrip = $_GET['descrip'];
	$regresar = "javascript:history.back();";
	$total = 0;
?>
<h2 align="center">Plantilla de Personal (Licencia/Activos)</h2>

  <?php
	if($depto=="TODOS")
		$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, clave_area, status_empleado from personal where (status_empleado = '01' or status_empleado = '02')  order by clave_area, apellidos_empleado, nombre_empleado";
	else
		$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, clave_area, status_empleado from personal where (status_empleado = '01' or status_empleado = '02')  and clave_area = '$depto' order by apellidos_empleado, nombre_empleado";

	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0;
	$clave_inicial = $registros->fields('clave_area');

	//Primer registro
	$area="select descripcion_area from organigrama where clave_area='$clave_inicial'";
	$res_area=ejecutar_sql($area);
	$descripcion_area = $res_area->fields('descripcion_area');

	if(!$registros->EOF)
		echo "<table align='center' width='60%' title='Lista de trabajadores de este departamento'><tr><td>&nbsp;</td></tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		 <tr>
		 <td colspan = '5' align='center'><b> $descripcion_area </b></td>
		 </tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr align='center' width='100%'>
				<th width='5%'> No. </th>
				<th width='20%'> RFC </th>
				<th width='45%'> Nombre completo </th>
				<th width='30%'> Cant. Plaza(s) </th>			
		  </tr>";
	else   echo ' <table align="center" title="No hay trabajadores en este departamento">
		  	<tr align="center">
				<td> No exite personal de este departamento </th>
		  	</tr>';
	//Fin primer registro
	$total_de_plazas = 0;
	if(!$registros->EOF){
		$registros->MoveFirst();
		while(!$registros->EOF){
			++$contador;
			//$nt = $registros->fields('no_tarjeta');
			$rfc = $registros->fields('rfc');
			$ae = $registros->fields('apellidos_empleado');
			$ne = $registros->fields('nombre_empleado');
			$clave = $registros->fields('clave_area');
			
			$area="select descripcion_area from organigrama where clave_area='$clave'";
			$res_area=ejecutar_sql($area);
			$descripcion_area = $res_area->fields('descripcion_area');
			
			if ($clave_inicial != $clave){ //   (--$contador)
				echo '<tr><th colspan = "3">Total</th><th>'.$total_de_plazas.'</th></tr>
				 <tr><td>&nbsp;</td></tr>
				 <tr>
				 <td colspan = "5" align="center"><b>'.$descripcion_area.'</b></td>
				 </tr>
				 <tr><td>&nbsp;</td></tr>
				 </table>
				 <table align="center" width="60%" title="Lista de trabajadores de este departamento">
				  	<tr align="center">
						<th width="5%"> No. </th>
						<th width="20%"> RFC </th>
						<th width="45%"> Nombre completo </th>
						<th width="30%"> Cant. Plaza(s) </th>				
				  	</tr> ';
				//$total = $total + (--$contador); // 
				$clave_inicial = $clave;
				$contador = 1;
				$total_de_plazas = 0;
			}
	
			if ($registros->fields('status_empleado') == '01'){
				echo "<tr bgcolor='#FAAC58'>
				<td align='center'> $contador </td>
				<td align='center'> $rfc </td>
				<td> $ae $ne </td>
				<td align='center'>";
					$qry_horas ="select count(horas_clave_plaza) as plazas from claves_presupuestales CP, claves_presupuestales_personal CPP, movimiento__personal MP where CP.id_clave = CPP.id_clave and id_mov = id_movimiento and MP.rfc='$rfc' and CPP.estatus = 1";
			$plazas_por_persona = ejecutar_sql($qry_horas); 		
			$plazastot = $plazas_por_persona->fields("plazas");
				echo $plazastot.' plaza'.(($plazastot>1)?'s':'.').	
				"</td></tr>";
			$activar_flag = 1;
			} else {
				echo "<tr id='$id'>
				<td align='center'> $contador </td>
				<td align='center'> $rfc </td>
				<td> $ae $ne </td>
				<td align='center'>";
					$qry_horas ="select count(horas_clave_plaza) as plazas from claves_presupuestales CP, claves_presupuestales_personal CPP, movimiento__personal MP where CP.id_clave = CPP.id_clave and id_mov = id_movimiento and MP.rfc='$rfc' and CPP.estatus = 1";
			$plazas_por_persona = ejecutar_sql($qry_horas); 		
			$plazastot = $plazas_por_persona->fields("plazas");
				echo $plazastot.' plaza'.(($plazastot>1)?'s':'.').	
				"</td></tr>";
			}

		$total_de_plazas = $plazastot + $total_de_plazas;
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		echo "<tr><th colspan = '3'>Total</th><th> $total_de_plazas </th></tr>"; //  $contador
		//$total = $total + $contador; // 
if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=3 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
	</table>
	<?php
	
	}//Fin if
	?>
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
		</div>
	</form>
	<?php if($contador > 0){ ?>
	<br><br><br>
	<!--<div align="center">  <a href="../reportes/Plantilla_de_personal.pdf.php?depto=<?php echo $depto; ?>" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Plantilla de Personal"/></a>
	</div>	-->
        <?php } ?>
</body>
</html>
