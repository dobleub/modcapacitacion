<?php
	/********************************************************
		Efecto Movimiento Personal Listado

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH');

// FUNCIONES 301111

	function num_quincena ($dia, $mes)
	{  if ($dia <= 15){ $quincena = (($mes-1)*2)+1;} else {if ($dia > 15){ $quincena = $mes*2;}}
   	  return $quincena;
	}

	function antiguedad_en_anos_quincenas ($parametro_quincena, $parametro_ano, $quincena_actual, $ano_actual)
	{
		if ($parametro_ano <= $ano_actual) {
		  if (($ano_actual - $parametro_ano) >= 1){   //2012-2008 = 4  16
		    if ($parametro_quincena < $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = $quincena_actual-$parametro_quincena;} //
	   	    if ($parametro_quincena == $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = 0;}
		    if ($parametro_quincena > $quincena_actual){ $anos = $ano_actual-$parametro_ano-1; $quincenas = 24-($parametro_quincena-$quincena_actual);}
			}
		  else {$anos = 0; $quincenas = $quincena_actual - $parametro_quincena;}
		  return $anos." a&ntilde;o(s) y ".$quincenas." quincena(s)";		
		}
		else return "Datos erroneos";
	}  

	$quincena_actual =  num_quincena (date ('d'), date ('m'));

// FIN FUNCIONES 
	$rfc=$_POST['rfc'];
	$mov=$_POST['mov'];

	$regresar = "window.location = 'efecto_movimiento_personal.php'";
	$volver = "efecto_movimiento_personal.php";

	// Si no hay datos retorna a solicitarlos
	if(($mov=='0')or($rfc=="0")){
		echo "<script>alert('Seleccione el personal y el movimiento que desea asignarle');</script>";
		echo "<script>".$regresar."</script>";
	}

	// Antiguedad en el plantel
	$consulta_registros="select apellidos_empleado, nombre_empleado, status_empleado, substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel from personal where ( rfc = '".$rfc."' )";
	$registros1=ejecutar_sql($consulta_registros);
		$ap = $registros1->fields('ano_plantel');
		$sp = $registros1->fields('sem_plantel');

	//Aquí inicia el cálculo del efecto inicial de cada movimiento y la cantidad de movimientos
	//Contar cuantos movimientos lleva en total este trabajador
	$consulta_cant_mov_sql="select count(cant_mov) as cantidad from movimiento__personal where rfc ='$rfc'";
	$registros_cant_mov=ejecutar_sql($consulta_cant_mov_sql);
	$cantidad_de_movimientos = $registros_cant_mov->fields('cantidad');

	// Calcula la quincena y el año final
if (($mov != '0') and ($rfc != "0"))
		{
		++$cantidad_de_movimientos;

		$consulta_select_sql= "select max(id_mov) as Total from movimiento__personal";
        	$sql_consulta = ejecutar_sql($consulta_select_sql) ;

		if ( $sql_consulta->fields('Total') == null)	$id_mov= 1;
		if ( $sql_consulta->fields('Total') != null)	$id_mov= $sql_consulta->fields('Total')+1;
		}
?>
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/popcalendar.js"></script>

		<h2 align="center"> Asignar Movimiento <?php echo $mov;?> al Empleado </h2>

<form method="post" action="efecto_movimiento_personal.php" name="datos" >
 <table border="0" align="center" cellpadding="0" cellspacing="0" width="20%">
  <tr> <td>&nbsp;</td> </tr>
  <tr> <th>Antiguedad(Plantel)</th> </tr>
  <tr id="non"> <td>&nbsp;</td> </tr>
  <tr id="non">
    <?php echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sp, $ap, $quincena_actual, date ('Y'))."</td>"; ?>
  </tr>
    <tr id="non">
      <td>&nbsp;</td>
	  <input type="hidden" name="id_mov" value="<?php echo $id_mov; ?>" >
	  <input type="hidden" name="rfc" value="<?php echo $rfc; ?>" >
	  <input type="hidden" name="movimiento" value="<?php echo $mov; ?>" >
	  <input type="hidden" name="cant_de_movimientos" value="<?php echo $cantidad_de_movimientos; ?>">
	  <input type="hidden" name="insertar" value="insertar" >
    </tr>
  </table>

	<br/><br/>
	<div align="center">
		<input type="submit" value="Aceptar" class="boton" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="Cancelar" class="boton" onclick="javascript:window.location = '<?php echo $volver; ?>'" />
	</div>

</form>
