<?php
	/********************************************************
		Informacion

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		29 de Noviembre de 2011
	********************************************************/
	//Librerias y funciones
	  require_once("../../includes/config.inc.php");
	  seguridad('DRH'); 
	  $web->Seguridad($_SESSION['susr'],8);
?>
<html>
	<head>
		<title>SIIT :: Bienvenida RH</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	</head>
<body>
<br>
<br> 
<?php
$er = 'N';
if(isset($_SESSION["autentificado"]) && $_SESSION["autentificado"] == "SI")
{
	$usuario=$_SESSION["susr"];
	$qry_usuario = "select * from acceso where usuario = '$usuario'";
	$res_usuario = ejecutar_sql($qry_usuario);
} 
?>

	<title>DEPARTAMENTO DE RECURSOS HUMANOS</title>

	<center>
			<b><font face="Arial, Helvetica, sans-serif" size="3">DEPARTAMENTO DE RECURSOS HUMANOS</font></b>
		<br>
		<br>
			<b>JEFE : <?php echo $res_usuario->fields('nombre_usuario') ?> </b>
		<br>
			recursoshumanos@ittlaxiaco.edu.mx
	</center>
	<br>

	<hr>

	<strong><font color="#000066">CONVOCATORIAS.</font></strong>
	<br>
	<br>
	<a href="informacion/convocatorias/Audio_LT.zip" target="_blank">Convocatorias ....</a>
	<br>

	<hr>

	<strong><font color="#000066">LEYES Y REGLAMENTOS TRABAJO.</font></strong>
	<br>
	<br>
	<a href="informacion/recursos_humanos/REGLAMENTO-1.pdf" target="_blank">A.- Reglamento interior del personal docente.</a>
	<br>
	<a href="informacion/recursos_humanos/REGLAMENTO-2.pdf" target="_blank">B.- Reglamento interior del personal no docente.</a>
	<br>
	<a href="informacion/recursos_humanos/BASE LEGAL Y NORMATIVA RH208.zip" target="_blank">C.- Otras leyes y reglamentos.</a>
	<br>
	<hr/>

	<strong><font color="#000066">REGLAMENTOS DEL ISSSTE.</font></strong>
	<br>
	<br>
	<a href="informacion/recursos_humanos/xxx.pdf" target="_blank">A.- Doc.</a>
	<br>
	<a href="informacion/recursos_humanos/xxx.zip" target="_blank">B.- Puede ser zip u otro</a>
	<br>
	<a href="informacion/recursos_humanos/xxx.zip" target="_blank">C.-Presentaci&oacute;n ley de ISSSTE.</a>
	<br>
	<br>
	<hr/>

	<strong><font color="#000066">ENLACES IMPORTANTES.</font></strong>
	<br>
	<br>

	<hr>
	<strong><font color="#000066">ISSSTE</font></strong>
	<br>
	<br>
	<a href="http://www.issste.gob.mx" target="_blank">Enlace al portal del ISSSTE</a>
	<br>
	<hr>
	<strong><font color="#000066">FOVISSSTE</font></strong>
	<br>
	<br>
	<a href="http://www.fovissste.gob.mx" target="_blank">Enlace al portal de FOVISSSTE</a>
	<br>
	<hr>
	<strong><font color="#000066">Seguro de vida</font></strong>
	<br>
	<br>
	<a href="informacion/recursos_humanos/xxx.pdf" target="_blank">Seguro de vida Institucional  </a>
	<br>
	<hr>	
	<strong><font color="#000066">SEGUIMIENTO Y GESTORIA</font></strong><br><div align="justify">
	<br>
	<a href="informacion/recursos_humanos/xxx.pdf" target="_blank">Seguimiento</a>
	<br>
	<a href="informacion/recursos_humanos/xxx.pdf" target="_blank">Gestoria</a>
	<br>
	<hr>
	<strong><font color="#000066">SERVICIOS AL PERSONAL</font></strong>
	<br>
	<hr>
	<strong><font color="#000066">REGISTROS Y CONTROLES</font></strong>

</body>
</html>
