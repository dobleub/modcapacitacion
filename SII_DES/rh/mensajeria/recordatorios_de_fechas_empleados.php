<?php
	/********************************************************
		Antiguedad Cumplida

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		23 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";

	function la_fecha ($quincena)
	{$dia = '01';
		for ($i=1; $i<=24; $i++){
			if ( $quincena == $i) 
				{
				$dia = ( (($quincena%2)==0)?'15':'01' );
				$mes = ceil($quincena / 2);
				$mes = ( ($mes < 10)?'0'.$mes:$mes );
				}
		}
   	  return "/$mes/$dia";
	}

?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Presonal que puede realizar tr&aacute;mites Antiguedad ::.</title>
	</head>
<body>

<?php
	$depto = $_GET['depto'];
	$descrip = $_GET['descrip'];
	$get_fecha = $_GET['registros'];
	$regresar = "javascript:history.back();";
	$total = 0;
	echo "<h2 align='center'>Personal que puede realizar tramites de antiguedad de $get_fecha a�os </h2>";
?>


  <?php
	$ano = date ('Y');
	$mes = date ('m');
	$dia = date ('d');

  // (tipo_personal ='B' or tipo_personal = 'X') and
	if($get_fecha=="Todos"){    
	}
	else{	
		$delete_inicio="delete from  temporal_antiguedad_rh";
		$d_inicio_=ejecutar_sql($delete_inicio);  // (substring(rfc,7,2)='".date('m')."')
		//$extraer_registros="select rfc, substring(ingreso_rama,1,4) as ano_rama, substring(ingreso_rama,5,6) as sem_rama, substring(inicio_gobierno,1,4) as ano_gob, substring(inicio_gobierno,5,6) as sem_gob, substring(inicio_sep,1,4) as ano_sep, substring(inicio_sep,5,6) as sem_sep, substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel from personal where status_empleado='02' and (tipo_personal != 'Z' and nombramiento != 'Z') order by apellidos_empleado, nombre_empleado";

		$extraer_registros="select rfc, substring(ingreso_rama,1,4) as ano_rama, substring(ingreso_rama,5,6) as sem_rama, substring(inicio_gobierno,1,4) as ano_gob, substring(inicio_gobierno,5,6) as sem_gob, substring(inicio_sep,1,4) as ano_sep, substring(inicio_sep,5,6) as sem_sep, substring(inicio_plantel,1,4) as ano_plantel, substring(inicio_plantel,5,6) as sem_plantel from personal where status_empleado='02' order by apellidos_empleado, nombre_empleado";
		$extraer_=ejecutar_sql($extraer_registros);
		while(!$extraer_->EOF){
				$rfc = $extraer_->fields('rfc');
				$ar = $extraer_->fields('ano_rama');
				$qr = $extraer_->fields('sem_rama');
				$ag = $extraer_->fields('ano_gob');
				$qg = $extraer_->fields('sem_gob');
				$as = $extraer_->fields('ano_sep');
				$qs = $extraer_->fields('sem_sep');
				$ap = $extraer_->fields('ano_plantel');
				$qp = $extraer_->fields('sem_plantel');

				$fr = $ar.la_fecha ($qr);
				$fs = $as.la_fecha ($qs);
				$fg = $ag.la_fecha ($qg);
				$fp = $ap.la_fecha ($qp);

			$insert_temp="insert into temporal_antiguedad_rh values ('$rfc', '$fr', '$fs', '$fg', '$fp')";
			$insert_t_=ejecutar_sql($insert_temp);

			$extraer_->MoveNext();
		}

	$f_limite = ($ano - $get_fecha).'/'.date ('m').'/'.date ('d');
	$f_inicia = ($ano - ($get_fecha - 1)).'/'.date ('m').'/'.date ('d');

	// ESTE ES OTRA COSA

		$consulta_registros="select T.rfc, apellidos_empleado, nombre_empleado, clave_area, status_empleado from personal, temporal_antiguedad_rh T where T.rfc = personal.rfc and (
(f_rama between '$f_limite' and '$f_inicia' ) or
(f_sep between '$f_limite' and '$f_inicia' ) or
(f_gob between '$f_limite' and '$f_inicia' ) or
(f_plantel between '$f_limite' and '$f_inicia' )
) order by apellidos_empleado, nombre_empleado"; 

		$registros=ejecutar_sql($consulta_registros);
	}

	$id = "non";
	$contador = 0;
	$clave_inicial = $registros->fields('clave_area');

	//Primer registro
	$area="select descripcion_area from organigrama where clave_area='$clave_inicial'";
	$res_area=ejecutar_sql($area);
	$descripcion_area = $res_area->fields('descripcion_area');

	if(!$registros->EOF)
		echo '<table align="center" width="50%" title="Plantilla de personal para realizar tramites de antiguedad">
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			<tr>
			<td colspan =3 align="center"><b>'.$descripcion_area.'</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<th align="center" width="5%"> No. </th>
				<th align="center" width="20%"> RFC </th>
				<th align="center"> Nombre completo </th>
			  </tr>';
	else	echo '<table align="center" title="No hay personal registrado">
			  <tr align="center">
				<td> No exiten recordatorios para estas fechas </th>
			  </tr>';
	//Fin primer registro

	if(!$registros->EOF){

		$registros->MoveFirst();
		while(!$registros->EOF){
	
			$contador = $contador + 1; 
			$rfc = $registros->fields('rfc');
			$ae = $registros->fields('apellidos_empleado');
			$ne = $registros->fields('nombre_empleado');
			$clave = $registros->fields('clave_area');
			$estatus = $registros->fields('status_empleado');
 			
			$area="select descripcion_area from organigrama where clave_area='$clave'";
			$res_area=ejecutar_sql($area);
			$descripcion_area = $res_area->fields('descripcion_area');
			
			if ($clave_inicial != $clave){
				echo "<tr><th colspan = '2'>Total</th><th>".($contador-1)."</th></tr>";
				$total = $total + ($contador-1);
				echo "<tr><td>&nbsp;</td></tr>
					<tr>
					<td colspan = '3' align='center'><b>".$descripcion_area."</b></td>
					</tr>
					<tr><td>&nbsp;</td></tr>";
				$clave_inicial = $clave;
				$contador = 1;
				echo'</table>
				<table align="center" width="50%">
				  <tr>
				    <th align="center" width="5%"> No. </th>
				    <th align="center" width="20%"> RFC </th>
				    <th align="center"> Nombre completo </th>			
				  </tr>';
			}

		if ( $estatus == 1){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
				<td align='center' width='5%'>".$contador."</td>
				<td align='center' width='20%'>".$rfc."</td>
				<td>".$ae." ".$ne."</td>
			      </tr>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>
				<td align='center' width='5%'>".$contador."</td>
				<td align='center' width='20%'>".$rfc."</td>
				<td>".$ae." ".$ne."</td>
			      </tr>";
		}

			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		echo "<tr><th colspan = '2'>Total</th><th>".$contador."</th></tr>";
		$total = $total + $contador;

if ($activar_flag == 1){
		?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=2 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>

	<?php
		echo "<tr><td>&nbsp;</td></tr>
			<tr>
			<td colspan = '3' align='center'><b>Para mayor informaci&oacute;n consulte la secci&oacute;n Mensajer&iacute;a -> Antiguedad </b></td>
			</tr></table>";	
	}//Fin if
	?>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
</body>
</html>
