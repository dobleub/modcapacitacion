<?php
/********************************************************
		�Qui�n cumplea a�os hoy?

		Desarrollado por: Ing. Ruben Dario Rodriguez Samado
		Instituto Tecnol�gico de Minatitl�n

		Modificado por:	Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 58);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Listado General de Personal ::.</title>
	</head>

<body>
		<h2 align="center">Personal que cumplean a�os este mes</h2>

  <?php
	$consulta_registros="select rfc, apellidos_empleado, nombre_empleado, status_empleado from personal where (status_empleado = '01' or status_empleado = '02') and (substring(rfc,7,2)='".date('m')."') order by fecha_nacimiento, status_empleado, apellidos_empleado, nombre_empleado";
	$registros=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	$clave_inicial = $registros->fields('clave_area');

	/*if(!$registros->EOF)
		echo '<table align="center" width="40%" title="Relaci&oacute;n de todo el personal de la instituci&oacute;n"><tr><td>&nbsp;</td></tr>
		 <tr align="center" width="100%">
			<th> No. </th>
			<th> Nombre completo </th>
			<th> Fecha Nacimiento </th>
			<th> Edad </th> 
			<th> Status </th>			
		  </tr> ';
	else	echo '<table align="center">
		  	<tr align="center">
			<td> No exite personal registrado </th>
		  </tr>';

	//Fin primer registro
	$registros->MoveFirst();*/

$aux_mes = 1;
$aux_mes1 = 1;
	while(!$registros->EOF){
		$contador = $contador + 1; 
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');  
		$se = $registros->fields('status_empleado');
		$estatus = $registros->fields('status_empleado');

		$rfc_sin=substr($rfc,-9);  // ejemplo cusm890901 asc
		$fecha=substr($rfc_sin,0,6);
		
		if (substr($rfc_sin,0,1) > 1)	$anio=intval(substr($fecha,0,2))+1900;
		else	$anio=intval(substr($fecha,0,2))+2000;

		$mes = substr($fecha,2,2);
		$dia = substr($fecha,4,2);
		$edad=date("Y",time())-$anio;
		$aux = 0;
		$fecha_antes = $dia."/".$mes;

		if (date('m')== $mes)
			if (date('d/m')== $fecha_antes){	//Si cumple a�os el d�a de hoy
				if ($aux_mes1 == 1)
					echo '<table align="center" width="40%" title="Relaci&oacute;n del personal de la instituci&oacute;n que cumple a�os hoy "><tr><td>&nbsp;</td></tr>					<tr><td colspan=5 align="center">Hoy</td></tr>
					<tr><td>&nbsp;</td></tr>
					 <tr align="center" width="100%">
						<th> No. </th>
						<th width="50%"> Nombre completo </th>
						<th width="30%"> Fecha Nacimiento </th>
						<th> Edad </th> 
					  </tr> ';

				if ( $estatus == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
					echo "<tr bgcolor='#FAAC58'>
						 <td align='center'> $contador </td> 	
						 <td> $ae $ne </td>
						 <td align='center'> $dia / $mes / $anio</td>
						 <td align='center'> $edad </td>
						</tr>";
				$activar_flag = 1;
		
				}else {
					echo "<tr id='$id'>
						 <td align='center'> $contador </td> 	
						 <td> $ae $ne </td>
						 <td align='center'> $dia / $mes / $anio </td>
						 <td align='center'> $edad </td>
						</tr>";
				}
			$aux = 1;
		}
		else {	// Si cumple a�os en este mes
			if ($aux_mes == 1)
				echo '<table align="center" width="40%" title="Relaci&oacute;n del personal de la instituci&oacute;n que cumple a�os este mes"><tr><td>&nbsp;</td></tr>				<tr><td colspan=5 align="center">Mes</td></tr>
				<tr><td>&nbsp;</td></tr>
				 <tr align="center" width="100%">
					<th> No. </th>
					<th width="50%"> Nombre completo </th>
					<th width="30%"> Fecha Nacimiento </th>
					<th> Edad </th>		
				  </tr> ';

			$aux_mes = $aux_mes + 1;

			if ( $estatus == '01'){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
				echo "<tr bgcolor='#FAAC58'>
					 <td align='center'> $contador </td> 	
					 <td> $ae $ne </td>
					 <td align='center'> $dia / $mes / $anio</td>
					 <td align='center'> $edad </td>
					</tr>";
			$activar_flag = 1;
		
			}else {
				echo "<tr id='$id'>
					 <td align='center'> $contador </td> 	
					 <td> $ae $ne </td>
					 <td align='center'> $dia / $mes / $anio </td>
					 <td align='center'> $edad </td>
					</tr>";
			}

		$aux = 1;
		}
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
		}

if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=4 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>

	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	  </div>
	</form>
	<? if ($aux == 1) { ?>
	<!-- <br><br>
	<div align="center">  <a href="../reportes/Cumpleano.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n:  Personal Cumpleanos"/></a>
	</div>-->
	<? } ?>
</body>
</html>
