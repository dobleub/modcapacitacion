<?php
	/********************************************************
		Listado General de Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		22 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Personal ::.</title>
	</head>
<body>
<?php $regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";?>
	<h2 align="center">Plantilla de Antiguedad del Personal</h2>
  <?php 
		echo '<table align="center">
		  <tr align="center">
			<th> No. </th>  
		 <th> I.T.T./ RAMA/ SEP/ GOB. </th>
			<th> Ver personal</th>
		  </tr>'; 
	$id = "non";
	$contador = 1;
	$registros=5;
	while($registros <=40){
		echo "<tr id='$id'>
		 <td>".$contador."</td>
		 <td align='center'>".$registros." a&ntilde;os de Antiguedad</td>
		 <td align='center'>";?>
  <a href="recordatorios_de_fechas_empleados.php?registros=<?php echo $registros;  ?>" target = "_self"> <img border="0" src="../../../img/iconos/lista.gif" title="Presione este bot&oacute;n para ver el personal que cumple con esta antiguedad"/></a><?php echo"</td></tr>";
		$id = ($id=="non")?"par":"non";
		$registros=$registros+5;
		$contador = $contador + 1;
		}  
	?>
</table>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
</body>
</html>
