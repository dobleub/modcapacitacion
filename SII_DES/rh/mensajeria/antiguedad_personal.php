<?php
	/********************************************************
		Antiguedad de Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 

if (isset($_GET['todos']) and !isset($_POST['todos']))	$todos = $_GET['todos'];	else   $todos = $_POST['todos'];
// FUNCIONES 241111

	function num_quincena ($dia, $mes)
	{  if ($dia <= 15){ $quincena = (($mes-1)*2)+1;} else {if ($dia > 15){ $quincena = $mes*2;}}
   	  return $quincena;
	}

	function antiguedad_en_anos_quincenas ($parametro_quincena, $parametro_ano, $quincena_actual, $ano_actual)
	{
		if ($parametro_ano <= $ano_actual) {
		  if (($ano_actual - $parametro_ano) >= 1){   //2012-2008 = 4  16
		    if ($parametro_quincena < $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = $quincena_actual-$parametro_quincena;} //
	   	    if ($parametro_quincena == $quincena_actual){ $anos = $ano_actual-$parametro_ano; $quincenas = 0;}
		    if ($parametro_quincena > $quincena_actual){ $anos = $ano_actual-$parametro_ano-1; $quincenas = 24-($parametro_quincena-$quincena_actual);}
			}
		  else {$anos = 0; $quincenas = $quincena_actual - $parametro_quincena;}
		  return $anos." a&ntilde;o(s) y ".$quincenas." quincena(s)";		
		}
		else return "Datos erroneos";
	}  

	$quincena_actual =  num_quincena (date ('d'), date ('m'));

	function fecha_ingreso ($quincena, $anyo)
	{
		$mes= intval ($quincena / 2);
	 	if ($quincena%2==1)	{$semana = 'primera'; $mes = $mes+1;}	else $semana = 'segunda';  
		if ($mes==1) $mes="Enero";
		else if ($mes==2) $mes="Febrero";
		else if ($mes==3) $mes="Marzo";
		else if ($mes==4) $mes="Abril";
		else if ($mes==5) $mes="Mayo";
		else if ($mes==6) $mes="Junio";
		else if ($mes==7) $mes="Julio";
		else if ($mes==8) $mes="Agosto";
		else if ($mes==9) $mes="Septiembre";
		else if ($mes==10) $mes="Octubre";
		else if ($mes==11) $mes="Noviembre";
		else if ($mes==12) $mes="Diciembre";
	return 'La '.$semana.' quincena de '.$mes.' de '.$anyo;
	}
// FIN FUNCIONES 
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Antiguedad del Empleado ::.</title>
	</head>
<body>

	<?php   $rfc_personal=$_POST['rfc_personal'];
		$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	?>
	<h2 align="center"> Antiguedad del Empleado (Licencia - Activos)</h2>

	<form name="solicitud_ver" method="post" action="">
	<h3 align="center"> Opciones de Visualizaci&oacute;n: </h3>
	<table width="30%" align="center">
		<tr align="center"> <th>Ver: </th> </tr>
		<tr align="center">
		  <th>
			<select name="rfc_personal" title="Seleccione el nivel de estudios" onChange="javascript:document.solicitud_ver.submit();">
			<?php
			$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where (status_empleado = '02' or status_empleado = '01')order by apellidos_empleado, nombre_empleado, rfc";
			$datos_rfc=ejecutar_sql($consulta_rfc);
			if(!$datos_rfc->rowcount()) echo "<option value='0' selected> No hay personal dado de alta </option>"; 
			else
			   { echo "<option value='0' selected> -- Seleccione Personal -- </option>";
			     while(!$datos_rfc->EOF){
				echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
				$datos_rfc->MoveNext();
				}
			   }
			?>
			</select>
		   </th>
		</tr>
	</table>
	<br>

 <input type='hidden' name='todos' value='1'>

<table border=0 align="center" title="Esta secci&oacute;n muestra la antiguedad del personal">
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr align="center">
   	<th  align="center"> No. </th>
	<th> RFC </th>
	<th align="center"> Nombre completo       </th>
	<!-- <th align="center"> Status </th>-->
	<th> Antiguedad/Ingreso (RAMA)     </th>
	<th> Antiguedad/Ingreso (GOB)      </th>
	<th> Antiguedad/Ingreso (SEP)      </th>
	<th> Antiguedad/Ingreso (PLANTEL)  </th>
  </tr>
  <?php
	//

        if ($rfc_personal == '0' or $todos == 't')
	$consulta_registros="select P.rfc,P.apellidos_empleado, P.nombre_empleado, P.status_empleado, substring(P.ingreso_rama,1,4) as ano_rama, substring(P.ingreso_rama,5,6) as sem_rama,substring(P.inicio_gobierno,1,4) as ano_gob,substring(P.inicio_gobierno,5,6) as sem_gob,substring(P.inicio_sep,1,4) as ano_sep, substring(P.inicio_sep,5,6) as sem_sep,substring(P.inicio_plantel,1,4) as ano_plantel, substring(P.inicio_plantel,5,6) as sem_plantel from personal P where (status_empleado='01' or status_empleado='02') order by status_empleado, apellidos_empleado, nombre_empleado"; 
        else if ($rfc_personal != '0')
	$consulta_registros="select P.rfc,P.apellidos_empleado, P.nombre_empleado, P.status_empleado, substring(P.ingreso_rama,1,4) as ano_rama, substring(P.ingreso_rama,5,6) as sem_rama,substring(P.inicio_gobierno,1,4) as ano_gob,substring(P.inicio_gobierno,5,6) as sem_gob,substring(P.inicio_sep,1,4) as ano_sep, substring(P.inicio_sep,5,6) as sem_sep,substring(P.inicio_plantel,1,4) as ano_plantel, substring(P.inicio_plantel,5,6) as sem_plantel from personal P where (status_empleado='01' or status_empleado='02')  and ( P.rfc = '".$rfc_personal."' )";

	$registros=ejecutar_sql($consulta_registros);

	$id = "non";
	$contador = 0;

	while(!$registros->EOF){
		$contador = $contador + 1;
		$rfc = $registros->fields('rfc');
		$ae = $registros->fields('apellidos_empleado');
		$ne = $registros->fields('nombre_empleado');
		//$se = $registros->fields('status_empleado');
		$ar = $registros->fields('ano_rama');
		$sr = $registros->fields('sem_rama');
		$ag = $registros->fields('ano_gob');
		$sg = $registros->fields('sem_gob');
		$as = $registros->fields('ano_sep');
		$ss = $registros->fields('sem_sep');
		$ap = $registros->fields('ano_plantel');
		$sp = $registros->fields('sem_plantel');
		$mov = $registros->fields('movimiento');

		$estatus = $registros->fields('status_empleado');

		if ( $estatus == 1){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>";
			echo "<td align='center'>".$contador."</td>";
			echo "<td align='center'>".$rfc."</td>";
			echo "<td>".$ae." ".$ne."</td>";
			//echo "<td align='center'>".(($se=="02")?"Activo":(($se=="00")?"Baja por renuncia":(($se=="01")?"Licencia":(($se=="03")?"Baja por jubilaci&oacute;n":(($se=="04")?"Inactivo por cambio de RFC":(($se=="05")?"Baja por fallecimiento":(($se=="06")?"Inactivo":" ")))))))."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sr, $ar, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sr, $ar)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sg, $ag, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sg, $ag)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($ss, $as, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($ss, $as)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sp, $ap, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sp, $ap)."</td>";
			echo "</tr>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>";
			echo "<td align='center'>".$contador."</td>";
			echo "<td align='center'>".$rfc."</td>";
			echo "<td>".$ae." ".$ne."</td>";
			//echo "<td align='center'>".(($se=="02")?"Activo":(($se=="00")?"Baja por renuncia":(($se=="01")?"Licencia":(($se=="03")?"Baja por jubilaci&oacute;n":(($se=="04")?"Inactivo por cambio de RFC":(($se=="05")?"Baja por fallecimiento":(($se=="06")?"Inactivo":" ")))))))."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sr, $ar, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sr, $ar)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sg, $ag, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sg, $ag)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($ss, $as, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($ss, $as)."</td>";
			echo "<td align='center'> ".antiguedad_en_anos_quincenas ($sp, $ap, $quincena_actual, date ('Y'))."<br>- - -<br>".fecha_ingreso ($sp, $ap)."</td>";
			echo "</tr>";
		}
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}

if ($activar_flag == 1){
	?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=7>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
</table>
</form>

<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
</body>
</html>
