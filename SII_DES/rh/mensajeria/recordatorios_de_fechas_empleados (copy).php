<?php
	/********************************************************
		Antiguedad Cumplida

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		23 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Antiguedad Cumplida ::.</title>
	</head>
<body>
<?php
	$depto = $_GET['depto'];
	$descrip = $_GET['descrip'];
	$get_fecha = $_GET['registros'];
	$regresar = "javascript:history.back();";
	$total = 0;
?>
<h2 align="center">Antiguedad Cumplida</h2>

  <?php
	$ano = date ('Y');
	$mes = date ('m');
	$dia = date ('d');
	function num_quincena ($dia, $mes)
		{  if ($dia <= 15){ $quincena = (($mes-1)*2)+1;}
	   	   else {if ($dia > 15){ $quincena = $mes*2;}}
	   	  return $quincena;
		}
	$quincena_actual =  num_quincena ($dia, $mes);

	$aux_05_anos = $ano -  5; // 2006
	$aux_10_anos = $ano - 10; // 2001
	$aux_15_anos = $ano - 15; // 1996
	$aux_20_anos = $ano - 20; // 1991
	$aux_25_anos = $ano - 25; // 1986
	$aux_30_anos = $ano - 30; // 1981
	$aux_35_anos = $ano - 35; // 1976
	$aux_40_anos = $ano - 40; // 1971
	$aux_fecha_anos = $ano - $get_fecha; // Fecha especifica

  // (tipo_personal ='B' or tipo_personal = 'X') and
	if($get_fecha=="Todos"){    
		$consulta_registros="select substring(ingreso_rama,1,4) as rama,substring(inicio_gobierno,1,4) as gob,substring(inicio_sep,1,4) as sep,substring(inicio_plantel,1,4) as plantel, * 
from personal where (status_empleado='01' or status_empleado='02') 
                      and (
                    (((substring(ingreso_rama,1,4)  = '$aux_05_anos') or
                    (substring(ingreso_rama,1,4)  = '$aux_10_anos') or 
                    (substring(ingreso_rama,1,4)  = '$aux_15_anos') or 
                    (substring(ingreso_rama,1,4)  = '$aux_20_anos') or  
                    (substring(ingreso_rama,1,4)  = '$aux_25_anos') or 
                    (substring(ingreso_rama,1,4)  = '$aux_30_anos') or  
                    (substring(ingreso_rama,1,4)  = '$aux_35_anos') or 
                    (substring(ingreso_rama,1,4)  = '$aux_40_anos')) and (substring(ingreso_rama,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or 
                    (((substring(inicio_gobierno,1,4)  = '$aux_05_anos') or
                    (substring(inicio_gobierno,1,4)  = '$aux_10_anos') or 
                    (substring(inicio_gobierno,1,4)  = '$aux_15_anos') or 
                    (substring(inicio_gobierno,1,4)  = '$aux_20_anos') or  
                    (substring(inicio_gobierno,1,4)  = '$aux_25_anos') or 
                    (substring(inicio_gobierno,1,4)  = '$aux_30_anos') or  
                    (substring(inicio_gobierno,1,4)  = '$aux_35_anos') or 
                    (substring(inicio_gobierno,1,4)  = '$aux_40_anos')) and (substring(inicio_gobierno,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or
                    (((substring(inicio_sep,1,4)  = '$aux_05_anos') or
                    (substring(inicio_sep,1,4)  = '$aux_10_anos') or 
                    (substring(inicio_sep,1,4)  = '$aux_15_anos') or 
                    (substring(inicio_sep,1,4)  = '$aux_20_anos') or  
                    (substring(inicio_sep,1,4)  = '$aux_25_anos') or 
                    (substring(inicio_sep,1,4)  = '$aux_30_anos') or  
                    (substring(inicio_sep,1,4)  = '$aux_35_anos') or 
                    (substring(inicio_sep,1,4)  = '$aux_40_anos')) and (substring(inicio_sep,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or
                    (((substring(inicio_plantel,1,4)  = '$aux_05_anos') or
                    (substring(inicio_plantel,1,4)  = '$aux_10_anos') or 
                    (substring(inicio_plantel,1,4)  = '$aux_15_anos') or 
                    (substring(inicio_plantel,1,4)  = '$aux_20_anos') or  
                    (substring(inicio_plantel,1,4)  = '$aux_25_anos') or 
                    (substring(inicio_plantel,1,4)  = '$aux_30_anos') or  
                    (substring(inicio_plantel,1,4)  = '$aux_35_anos') or 
                    (substring(inicio_plantel,1,4)  = '$aux_40_anos')) and (substring(inicio_plantel,5,6) ='$quincena_actual') ))
order by gob, sep, rama, plantel, apellidos_empleado, nombre_empleado";
		$registros=ejecutar_sql($consulta_registros);
	}
	else{	$consulta_registros="select substring(ingreso_rama,1,4) as rama,substring(inicio_gobierno,1,4) as gob,substring(inicio_sep,1,4) as sep,substring(inicio_plantel,1,4) as plantel, * 
from personal where (status_empleado='01' or status_empleado='02') and 
                    (tipo_personal ='B' or tipo_personal = 'X') and  (
                    ((substring(ingreso_rama,1,4)  = '$aux_fecha_anos') and (substring(ingreso_rama,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or 
                    ((substring(inicio_gobierno,1,4)  = '$aux_fecha_anos') and (substring(inicio_gobierno,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or
                    ((substring(inicio_sep,1,4)  = '$aux_fecha_anos') and (substring(inicio_sep,5,6) ='$quincena_actual') and (tipo_personal != 'Z' and nombramiento != 'Z')) or
                    ((substring(inicio_plantel,1,4)  = '$aux_fecha_anos') and (substring(inicio_plantel,5,6) ='$quincena_actual')))
order by gob, sep, rama, plantel, apellidos_empleado, nombre_empleado";
		$registros=ejecutar_sql($consulta_registros);
	}

	$id = "non";
	$contador = 0;
	$clave_inicial = $registros->fields('clave_area');

	//Primer registro
	$area="select descripcion_area from organigrama where clave_area='$clave_inicial'";
	$res_area=ejecutar_sql($area);
	$descripcion_area = $res_area->fields('descripcion_area');

	if(!$registros->EOF)
		echo '<table align="center" width="100%" title="Plantilla de personal que cumple esta antiguedad en la quincena actual">
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			<tr>
			<td colspan =5 align="center"><b>'.$descripcion_area.'</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center" width="100%">
				<th width="10%"> No. </th>
				<th width="20%"> RFC </th>
				<th width="40%"> Nombre completo </th>
				<th width="30%"> Sector </th>			
			  </tr>';
	else	echo '<table align="center" title="No hay personal registrado">
			  <tr align="center">
				<td> No exiten recordatorios para estas fechas </th>
			  </tr>';
	//Fin primer registro

	if(!$registros->EOF){

		$registros->MoveFirst();
		while(!$registros->EOF){
	
			$contador = $contador + 1; 
			$rfc = $registros->fields('rfc');
			$ae = $registros->fields('apellidos_empleado');
			$ne = $registros->fields('nombre_empleado');
			$clave = $registros->fields('clave_area');
			$estatus = $registros->fields('status_empleado');

                        if ((substr($registros->fields('ingreso_rama'),0,4) == $aux_fecha_anos) and
			    (substr($registros->fields('ingreso_rama'),4,5) == $quincena_actual)) $_rama_anos = "RAMA"; else $_rama_anos = " ";
                        if ((substr($registros->fields('inicio_gobierno'),0,4) == $aux_fecha_anos) and
			    (substr($registros->fields('inicio_gobierno'),4,5) == $quincena_actual)) $_gob_anos = "GOB"; else $_gob_anos = " ";
                        if ((substr($registros->fields('inicio_sep'),0,4) == $aux_fecha_anos) and
			    (substr($registros->fields('inicio_sep'),4,5) == $quincena_actual)) $_sep_anos = "SEP"; else $_sep_anos = " ";
                        if ((substr($registros->fields('inicio_plantel'),0,4) == $aux_fecha_anos) and
			    (substr($registros->fields('inicio_plantel'),4,5) == $quincena_actual)) $_plantel_anos = "PLANTEL"; else $_plantel_anos = " ";
 			
			$area="select descripcion_area from organigrama where clave_area='$clave'";
			$res_area=ejecutar_sql($area);
			$descripcion_area = $res_area->fields('descripcion_area');
			
			if ($clave_inicial != $clave){
				echo "<tr><th colspan = '3'>Total</th><th>".($contador-1)."</th></tr>";
				$total = $total + ($contador-1);
				echo "<tr><td>&nbsp;</td></tr>
					<tr>
					<td colspan = '5' align='center'><b>".$descripcion_area."</b></td>
					</tr>
					<tr><td>&nbsp;</td></tr>";
				$clave_inicial = $clave;
				$contador = 1;
				echo'</table>
				<table align="center" width="100%">
				  <tr align="center">
					<th width="10%"> No. </th>
					<th width="20%"> RFC </th>
					<th width="40%"> Nombre completo </th>
					<th width="30%"> Sector </th>				
				  </tr>';
			}


		if ( $estatus == 1){  // estaus = 1 : Licencia ; estaus = 2 : Activo; estaus = 6 : Inactivo
			echo "<tr bgcolor='#FAAC58'>
				<td>".$contador."</td>
				<td>".$rfc."</td>
				<td>".$ae." ".$ne."</td>";
		$activar_flag = 1;
		} else {
			echo "<tr id='$id'>
				<td>".$contador."</td>
				<td>".$rfc."</td>
				<td>".$ae." ".$ne."</td>";
		}





			
	$id = ($id=="non")?"par":"non";
			
			echo "<td>".(($_rama_anos!=' ')?" - ":"").$_rama_anos.(($_gob_anos!=' ')?" - ":"").$_gob_anos.(($_sep_anos!=' ')?" - ":"").$_sep_anos.(($_plantel_anos!=' ')?" - ":"").$_plantel_anos."</td>";
			//Fin mostrar las plazas
		
			echo "</tr>";
			$id = ($id=="non")?"par":"non";
			$registros->MoveNext();
		}
		echo "<tr><th colspan = '3'>Total</th><th>".$contador."</th></tr>";
		$total = $total + $contador;

if ($activar_flag == 1){
		?>
 <tr><td>&nbsp;</td></tr>
 <tr><td bgcolor='#FAAC58'></td><td colspan=2 align='center'>* Las filas en anaranjado indican que el trabajador tiene licencia </td></tr>
<?php } ?>
	</table>
	<?php
		echo "<table align='center' width='100%'><tr><td>&nbsp;</td></tr>
			<tr>
			<td colspan = '5' align='center'><b>Para mayor informaci&oacute;n consulte la secci&oacute;n Mensajer&iacute;a -> Antiguedad Personal </b></td>
			</tr></table>";	
	}//Fin if
	?>
<form name="personal" method="post" >
  <br />
	<div align="center">
	  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
  </div>
</form>
</body>
</html>
