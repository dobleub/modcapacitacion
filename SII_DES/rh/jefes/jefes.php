<?php
	/********************************************************
		Jefes

		Modificado por: Miguel Angel Cruz Sandoval
		Instituto Tecnol�gico de Tlaxiaco
		01 de Diciembre de 2011
	********************************************************/

	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require_once($CFG->funciones_phpDir."/funciones_combos.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 54);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
	<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
	<title>.:: Recursos Humanos &raquo; Jefes ::.</title>
</head>
<body>
<h3 align="center"> Jefes de Departamentos </h3>
<?php
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	function deptos($depto)
	{
	  $qry_deptos = "select clave_area, descripcion_area from organigrama where substring(clave_area, 5, 2) = '00'"; //  and nivel='4'
		$res_deptos = ejecutar_sql($qry_deptos);
		echo '<option value="0"> -- Seleccione el area -- </option>';
		while($deptos = $res_deptos->fetchrow())
		{
			$clave_area = $deptos['clave_area'];
			$descripcion_area = $deptos['descripcion_area'];
			$selected = ($depto == $clave_area)?" selected":"";
			$combo_deptos.= "<option value='".$clave_area."'".$selected.">".$descripcion_area."</option>";
		}
	
		return $combo_deptos;
	}

	$qry_jefes = "exec pac_jefes '$clave_area'";
	$res_jefes = ejecutar_sql($qry_jefes);
	$clave_area = $res_jefes->fields('clave_area');
	$jefe_area = $res_jefes->fields('jefe_area');
	$rfc_jefe = $res_jefes->fields('rfc');
//echo $jefe_area;
?>
<form name="jefes" action="jefes.php" method="post">
	<table align="center" width="500" title="Secci&oacute;n para actualizar la jefatura de un departamento">
		<tr>
			<th width="200"> �rea: </th>
			<td id="non"> <select name="clave_area" onchange="document.jefes.submit()" title="Primero debe elegir el departamento al que agregar&aacute; un nuevo jefe"> <?php echo deptos($clave_area); ?>	</select> </td>
		</tr>
		<tr>
			<th> Jefe: </th>
			<td id="non"> 
				
				<select name="rfc_jefe_area" onchange="cambio();" title="Presione aqu&iacute; para asignar al nuevo jefe">
					<option value="0"> Seleccione Personal </option>
					<?php

					$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
					$datos_rfc=ejecutar_sql($consulta_rfc);
					
					while(!$datos_rfc->EOF){
						$selecciona=(($datos_rfc->fields('rfc')==$rfc_jefe)?" selected":"");
						echo '<option value="'.$datos_rfc->fields('rfc').'"'.$selecciona.'>'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>\n';
						$datos_rfc->MoveNext();
					}

					?>
				</select> 	
			</td>
		</tr>
	</table>
	<br />
	<div align="center">
		<input type="button" class="boton" value="Actualizar" onclick="javascript: document.jefes.action = 'jefes_bd.php'; document.jefes.submit()" title="Presione este bot&oacute;n para guardar los cambios">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" class="boton" value="Regresar" onclick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n">

	</div>
<!-- MODIFICACION 101111-->
<br><br>
	<table align="center" title="Tabla que muestra la relaci&oacute;n de jefaturas en los distintos departamentos del instituto">
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
  
  <?php
	$consulta_registros="select clave_area, J.rfc, jefe_area, descripcion_area from jefes J order by jefe_area, J.rfc";
	$registros=ejecutar_sql($consulta_registros);
	$id = "non";
	$contador = 0;

	if(!$registros->EOF)
		echo '<tr align="center">
			    <th> No. </th>
			    <th> RFC </th>
			    <th> T&iacute;tulo </th>
			    <th> Nombre completo </th>
			    <th> Departamento </th>
			    <th> Eliminar </th>
		      </tr>';
	else	echo '<tr align="center">
			 <td> No exite personal registrado </th>
		      </tr>';

	while(!$registros->EOF){
		$contador = $contador + 1;
		$rfc = $registros->fields('rfc');

		$consulta_titulo = "select titulo from estudios_personal EP, titulos T where T.id_titulo = EP.id_titulo and rfc = '$rfc'";
		$registro = ejecutar_sql($consulta_titulo);
		$titulo = $registro->fields('titulo');

		$jefe = $registros->fields('jefe_area');
		$des = $registros->fields('descripcion_area');

		echo "<tr id='$id'>
		 <td> $contador </td>
		 <td> $rfc </td>
		 <td> $titulo </td>
		 <td> $jefe </td>
		 <td> $des </td>
		 <td align='center'>";?>
  <a href="borrar_puesto.php?rfc=<?php echo $rfc; ?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este dato ?');"> <img border="0" src="../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar este registro"/></a><?php echo"</td>
		</tr>";
		$id = ($id=="non")?"par":"non";
		$registros->MoveNext();
	}
	?>
</table>
<!-- FIN MODIFICACION -->
</form>
	<? if ($contador > 0) { ?>
	<br><br><br>
	<div align="center">  <a href="../reportes/Jefes.pdf.php" target="_blank"> <img src="../img/pdf.png" width="68" height="68" border="0" title="Presione aqu&iacute; para imprimir la relaci&oacute;n: Jefes"/></a>
	</div>
	<? } ?>
</body>
</html>
