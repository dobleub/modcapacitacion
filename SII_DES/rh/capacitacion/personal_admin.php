<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/personal_admin.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	
	if($_GET["exist"] || $_GET["noexist"]){
		$exist = $_GET["exist"];
		$noexist = $_GET["noexist"];
		$rfc = $_GET["rfc"];
		$puesto = $_GET["idpuesto"];
		$departamento = $_GET["iddepto"];
		$subdireccion = $_GET["idsub"];
		$subid = $_GET["subdirid"];
		$accion = "update";
	} else {
		$rfc = $_GET["rfc"];
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		<script type="text/javascript">
			window.onload = function() {
				// code to execute here
				//activartextbox();
				<?php if($exist=="id") { ?>
					cargarSubgrupoID('carga_puesto_per.php','txtHint','depto','<?php echo $departamento ?>','puesto','<?php echo $puesto ?>','reload_puestou_per');
					reload_puestou_per();
				<?php } else {?>
					cargarSubgrupoID('carga_puesto_per.php','txtHint','subid','<?php echo $subid ?>','puesto','<?php echo $puesto ?>','reload_puestou_per');
					reload_puestou_per();
				<?php } ?>
			}
		</script>
		
		<title>.:: Administraci&oacute;n Personal - Puesto - Departamento - Subdirecci&oacute;n - Escolaridad ::.</title>
		
	</head>
<body>
	
	<h2 align="center">Administraci&oacute;n de los detalles del Personal</h2>
<?php
	if($accion == "update"){
		$titulo = '<h3 align="center">ACTUALIZAR DATOS DEL PERSONAL</h3>';
		$form = '<form name="u_capacidad" id="u_capacidad" method="get" action="ime_puestos.php" onSubmit="return validarcampos_uc()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar la Datos del Personal"/>';
		$funcionreload = 'reload_puestou_per';
	} else {
		$titulo = '<h3 align="center">ALTA DE DATOS DEL PERSONAL</h3>';
		$form = '<form name="i_capacidad" id="i_capacidad" method="get" action="ime_puestos.php" onSubmit="return validarcampos_ic()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Alta de datos del Personal"/>';
		$funcionreload = 'reload_puestoi_per';
	}

?>

  <!-- Contenido -->
	<?php
		echo '<br/>';
		echo $titulo;

		echo $form;
	?>
		<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">
			<tr>
				<th width="150"> Nombre: </th>
				<td width="350">
					<select name="dp0" title="Presione aqu&iacute; para seleccionar el Personal">
						<?php
						$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> No hay personal dado de alta </option>";
						else {
							if($rfc){
								while(!$datos->EOF){
									if($rfc == $datos->fields('rfc') )
										echo '<option value="'.$datos->fields('rfc').'" selected>'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
									else 
										echo '<option value="'.$datos->fields('rfc').'">'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
									$datos->MoveNext();
								}
							} else {
								echo "<option value='-1' selected> - Selecciona Personal - </option>";
								while(!$datos->EOF){
								echo '<option value="'.$datos->fields('rfc').'">'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
								$datos->MoveNext();
								}
							}
						}
						?>
					</select>
				</td>
				
			</tr>
			<tr>
				<th width="100"> Subdireccion: </th>
				<td>
					<select name="dpt1" id="dpt1" title="Seleccione la Subdirecci&oacute;n" onchange="cargarSubgrupos('carga_puesto_per.php','txtHint','subid',this.value,'<?php echo $funcionreload ?>');'<?php echo $funcionreload ?>()';" >
					<?php
						$consulta="select idsub, descripcion from cp_subdireccion where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Subdirecci&oacute;n faltantes </option>";
						else { 
							if($subid){
								while(!$datos->EOF){
									if($subid == $datos->fields('idsub'))
										echo '<option value="'.$datos->fields('idsub').'" selected>'.$datos->fields('descripcion').'</option>';
									else
										echo '<option value="'.$datos->fields('idsub').'">'.$datos->fields('descripcion').'</option>';
									$datos->MoveNext();
								}
							} else {
								echo "<option value='-1' selected> - Selecciona Subdirecci&oacute;n - </option>";
								while(!$datos->EOF){
									echo '<option value="'.$datos->fields('idsub').'">'.$datos->fields('descripcion').'</option>';
									$datos->MoveNext();
								}
							}
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Departamento: </th>
				<td>
					<select name="dpt1" id="dpt1" title="Seleccione el Departamento" onchange="cargarSubgrupos('carga_puesto_per.php','txtHint','depto',this.value,'<?php echo $funcionreload ?>');'<?php echo $funcionreload ?>()';" >
					<?php
							$sql="select idsub, descripcion from cp_subdireccion where estado='1' ";
							$datosgral=ejecutar_sql($sql);
							
							if(!$datosgral->rowcount())	echo "<option value='-1' selected> Datos de Departamentos faltantes </option>";
							else {
								echo "<option value='-1' selected> - Selecciona Departamento - </option>";
								while(!$datosgral->EOF){
									echo '<optgroup label="SUB: '.$datosgral->fields('descripcion').'">';
									
										$consulta="select iddepto, descripcion from cp_departamento where estado='1' and idsub='".$datosgral->fields('idsub')."'";
										$datos=ejecutar_sql($consulta);
											if($departamento){
												while(!$datos->EOF){
													if($departamento == $datos->fields('iddepto'))
														echo '<option value="'.$datos->fields('iddepto').'" selected>'.$datos->fields('descripcion').'</option>';
													else
														echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
													$datos->MoveNext();
												}
											} else {
												while(!$datos->EOF){
													echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
													$datos->MoveNext();
												}
											}
									echo '</optgroup>';
									$datosgral->MoveNext();
								}
							}
						?>
						<?php
						//$consulta="select iddepto, descripcion from cp_departamento where estado = '1'";

						//$datos=ejecutar_sql($consulta);

						//if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Departamentos faltantes </option>";
						//else
						//{
							//if($departamento){
								//while(!$datos->EOF){
									//if($departamento == $datos->fields('iddepto'))
										//echo '<option value="'.$datos->fields('iddepto').'" selected>'.$datos->fields('descripcion').'</option>';
									//else
										//echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
									//$datos->MoveNext();
								//}
							//} else {
								//echo "<option value='-1' selected> - Selecciona Departamento - </option>";
								//while(!$datos->EOF){
									//echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
									//$datos->MoveNext();
								//}
							//}
						//}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Puesto: </th>
				<td>
					<!-- select de Seleccion de Puesto -->
					<div id="txtHint"></div>
				</td>
			</tr>
				
		</table>
		<br/>	
		
		<div align="center">
			<input type="hidden" name="deptoid">
			<input type="hidden" name="subid">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="exist" value="<?php echo $exist; ?>">
			<input type="hidden" name="noexist" value="<?php echo $noexist; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php 
				echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	


	<br/>
	<br/>
		<table align="center" title="Modificar / Eliminar Detalles del Personal">
			<tr>
				<th colspan="8"><center>DETALLES DEL PERSONAL</center></th>
			</tr>
			<tr>
				<th>No.</th>
				<th>RFC</th>
				<th>Nombre</th>
				<th>Puesto</th>
				<th>Departamento</th>
				<th>Subdirecci&oacute;n</th>
				<th colspan="2">Accion</th>
			</tr>
			<?php
			// Muestra todos los empleados que tienen puestos de Subdirectores
			$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, pt.idpuesto, pt.descripcion  as puesto, sb.idsub, sb.descripcion as subdir
			from personal as p, cp_personal_det as pd, cp_puesto as pt, cp_departamento as dp, cp_subdireccion as sb
			where p.status_empleado = '02' and pd.rfc = p.rfc and pd.idpuesto = pt.idpuesto 
            and pt.idsub = sb.idsub and pt.iddepto is NULL order by p.apellidos_empleado, p.nombre_empleado, p.rfc";

				$datos=ejecutar_sql($consulta);
				$i = 0;
				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dp" id="m_dp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.($i = $i + 1).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non">'.$datos->fields('puesto').'</td>';
						echo '<td id="non">'.$datos->fields('depto').'</td>';
						echo '<td id="non">'.$datos->fields('subdir').'</td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="idpuesto" value="'.$datos->fields('idpuesto').'">';
						echo '<input type="hidden" name="iddepto" value="'.$datos->fields('iddepto').'">';
						echo '<input type="hidden" name="subdirid" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="exist" value="none">';
					echo '</form>';
					echo '<form name="e_dp" id="e_dp" method="get" action="ime_puestos.php" onSubmit="return validarcampos()" >';
						//echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'" disabled/> </center></td>';	
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="exist" value="id">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
				
				
			// Muestra todos los empleados que tienen puestos
			$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, pt.idpuesto, pt.descripcion as puesto, dp.iddepto, dp.descripcion as depto, sb.idsub, sb.descripcion as subdir
			from personal as p, cp_personal_det as pd, cp_puesto as pt, cp_departamento as dp, cp_subdireccion as sb
			where p.status_empleado = '02' and pd.rfc = p.rfc and pd.idpuesto = pt.idpuesto and pt.iddepto = dp.iddepto and dp.idsub = sb.idsub
			order by p.apellidos_empleado, p.nombre_empleado, p.rfc";

				$datos=ejecutar_sql($consulta);
				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dp" id="m_dp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.($i = $i + 1).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non">'.$datos->fields('puesto').'</td>';
						echo '<td id="non">'.$datos->fields('depto').'</td>';
						echo '<td id="non">'.$datos->fields('subdir').'</td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="idpuesto" value="'.$datos->fields('idpuesto').'">';
						echo '<input type="hidden" name="iddepto" value="'.$datos->fields('iddepto').'">';
						echo '<input type="hidden" name="idsub" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="exist" value="id">';
					echo '</form>';
					echo '<form name="e_dp" id="e_dp" method="get" action="ime_puestos.php" onSubmit="return validarcampos()" >';
						//echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'" disabled/> </center></td>';	
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="exist" value="id">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			
			// Muestra todos los empleados que no estan en la lista de empleados con puestos
				//$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' and rfc not in (select distinct(rfc) from cp_personal_det where estado='1') order by apellidos_empleado, nombre_empleado, rfc";
				$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' and rfc not in (select distinct(rfc) from cp_personal_det) order by apellidos_empleado, nombre_empleado, rfc";
				
				$datos=ejecutar_sql($consulta);
				
				if(!$datos->rowcount())	echo "No hay personal dado de alta";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dp" id="m_dp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.($i = $i + 1).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non"> - </td>';
						echo '<td id="non"> - </td>';
						echo '<td id="non"> - </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="noexist" value="id">';
					echo '</form>';
					echo '<form name="e_dp" id="e_dp" method="get" action="ime_puestos.php" onSubmit="return validarcampos()" >';
						//echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';	
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
		</table>
	
  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
