	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/

function validarcampos_ic(){
	cap1 = document.i_capacidad.cap1.value
	cap0 = document.i_capacidad.cap0.value
	group = document.i_capacidad.gcap.options[document.i_capacidad.gcap.selectedIndex].value
	subg = document.i_capacidad.subcap.options[document.i_capacidad.subcap.selectedIndex].value
	
	if (document.getElementById('enableid').checked==true){
		cap0 = document.i_capacidad.cap0.value
		if(cap0 == " "){
		alert("Por favor escribe un Identificador para la nueva Capacidad");
		document.i_capacidad.cap0.focus();
		return false;
		}
	} else {
		cap0="-1"
	}
	if(cap1 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_capacidad.cap1.focus();
		return false;
	}
	if(group == "-1" || subg == "-1"){
		alert("Por favor seleccione un Grupo y Subgrupo para la Capacidad nueva");
		if(group == "-1"){
			document.i_capacidad.gcap.focus();
		} else {
			document.i_capacidad.subcap.focus();
		}
		return false;
	}
	document.i_capacidad.idcap.value = cap0
	document.i_capacidad.idgc.value = group
	document.i_capacidad.idsgc.value = subg
}

function validarcampos_ig(){
	grp0 = document.i_grupo.grp0.value
	
	if(grp0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_grupo.grp0.focus();
		return false;
	}
}

function validarcampos_isg(){
	subg0 = document.i_subgrupo.subg0.value
	grp1 = document.i_subgrupo.grp1.options[document.i_subgrupo.grp1.selectedIndex].value
	
	if(subg0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_subgrupo.subg0.focus();
		return false;
	}
	if(grp1 == "-1"){
		alert("Por favor seleccione un Grupo para el Subgrupo nuevo");
		document.i_subgrupo.grp1.focus();
		return false
	}
}

function validarcampos_ib(){
	barr0 = document.i_barrera.barr0.value
	
	if(barr0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_barrera.barr0.focus();
		return false;
	}
}

function validarcampos_ii(){
	inst0 = document.i_instructor.inst0.options[document.i_instructor.inst0.selectedIndex].value
	inst1 = document.i_instructor.inst1.value
	inst2 = document.i_instructor.inst2.value
	inst3 = document.i_instructor.inst3.value
	inst4 = document.i_instructor.inst4.value
	inst5 = document.i_instructor.inst5.options[document.i_instructor.inst5.selectedIndex].value
	
	if(inst1 == "" || inst2 == "" || inst3 == "" || inst4 == ""){
		alert("Por favor ingrese un dato antes de presionar el boton INSERTAR");
		if(inst1 == ""){ document.i_instructor.inst1.focus(); return false; }
		if(inst2 == ""){ document.i_instructor.inst2.focus(); return false; }
		if(inst3 == ""){ document.i_instructor.inst3.focus(); return false; }
		if(inst4 == ""){ document.i_instructor.inst4.focus(); return false; }
	}
	if(inst5 == "-1"){
		alert("Por favor seleccione la Empresa de donde proviene el Instructor");
		document.i_instructor.inst5.focus();
		return false;
	}
}

function validarcampos_isub(){
	sub0 = document.i_sub.sub0.value
	if(sub0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_sub.sub0.focus();
		return false;
	}
}

function validarcampos_idep(){
	dep0 = document.i_dep.dep0.value
	dep1 = document.i_dep.dep1.options[document.i_dep.dep1.selectedIndex].value
	
	if(dep0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_dep.dep0.focus();
		return false;
	}
	if(dep1 == "-1"){
		alert("Por favor seleccione la SUBDIRECCION a donde pertenece el nuevo DEPARTAMENTO");
		document.i_dep.dep1.focus();
		return false;
	}
}

function validarcampos_ipto(){
	pto0 = document.i_pto.pto0.value
	pto1 = document.i_pto.pto1.options[document.i_pto.pto1.selectedIndex].value
	pto2 = document.i_pto.pto2.options[document.i_pto.pto2.selectedIndex].value
	
	if(pto0 == ""){
		alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
		document.i_pto.pto0.focus();
		return false;
	}
}

function validarcampos_iemp(){
	emp0 = document.i_emp.emp0.value
	emp1 = document.i_emp.emp1.value
	emp2 = document.i_emp.emp2.value
	emp3 = document.i_emp.emp3.value
	emp4 = document.i_emp.emp4.value
	
	if(emp0 == "" || emp1 == "" || emp2 == "" || emp3 == "" || emp4 == ""){
		alert("Por favor ingrese un dato antes de presionar el boton INSERTAR");
		if(emp0 == ""){ document.i_emp.emp0.focus(); return false; }
		if(emp1 == ""){ document.i_emp.emp1.focus(); return false; }
		if(emp2 == ""){ document.i_emp.emp2.focus(); return false; }
		if(emp3 == ""){ document.i_emp.emp3.focus(); return false; }
		if(emp4 == ""){ document.i_emp.emp4.focus(); return false; }
	}
}
function activartextbox(){
	if (document.getElementById('enableid').checked==false)
	{
		document.getElementById('cap0').disabled=true;
		//document.getElementById('cap0').style.color='red';
	}
	if (document.getElementById('enableid').checked==true)
	{
		document.getElementById('cap0').disabled=false;
	}
}
function activarpuestos(){
	if (document.getElementById('enableid').checked==false)
	{
		document.getElementById('cap0').disabled=true;
		//document.getElementById('cap0').style.color='red';
	}
	if (document.getElementById('enableid').checked==true)
	{
		document.getElementById('cap0').disabled=false;
	}
}

function cargarSubgrupos(archivo,elemento,variable,valor,accion){
	if (valor==""){
		document.getElementById(elemento).innerHTML="";
		return;
	} 
	// código para IE7+, Firefox, Chrome, Opera, Safari
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	//código para IE6, IE5
	}else{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(elemento).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open('GET',archivo+'?'+variable+'='+valor+'&action='+accion,true);
	xmlhttp.send();
}

function cargarSubgrupoID(archivo,elemento,variable,valor,variable2,valor2,accion){
	if (valor=="" || valor2==""){
		document.getElementById(elemento).innerHTML="";
		return;
	} 
	// código para IE7+, Firefox, Chrome, Opera, Safari
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	//código para IE6, IE5
	}else{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(elemento).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open('GET',archivo+'?'+variable+'='+valor+'&'+variable2+'='+valor2+'&action='+accion,true);
	xmlhttp.send();
}

function reload_grupob_cap(){
	document.u_capacidad.idgc.value = document.u_capacidad.gcap.options[document.u_capacidad.gcap.selectedIndex].value
	document.u_capacidad.idsgc.value = document.u_capacidad.subcap.options[document.u_capacidad.subcap.selectedIndex].value
}			
function reload_grupo_cap(){
	document.i_capacidad.idgc.value = document.i_capacidad.gcap.options[document.i_capacidad.gcap.selectedIndex].value
	document.i_capacidad.idsgc.value = document.i_capacidad.subcap.options[document.i_capacidad.subcap.selectedIndex].value
}
function reload_puestou_per(){
	document.u_capacidad.deptoid.value = document.u_capacidad.dpt1.options[document.u_capacidad.dpt1.selectedIndex].value
	document.u_capacidad.subid.value = document.u_capacidad.pst1.options[document.u_capacidad.pst1.selectedIndex].value
}			
function reload_puestoi_per(){
	document.i_capacidad.deptoid.value = document.i_capacidad.dpt1.options[document.i_capacidad.dpt1.selectedIndex].value
	document.i_capacidad.subid.value = document.i_capacidad.pst1.options[document.i_capacidad.pst1.selectedIndex].value
}
function reload_cedulai_per(){
	document.i_capacidad.personal.value = document.i_capacidad.dp0.options[document.i_capacidad.dp0.selectedIndex].value
}


function delete_cap(){
	alert("Por favor ingrese un nombre antes de presionar el boton INSERTAR");
}

function delete_confirm(forms){
	var answer = confirm("¿Está seguro que quiere eliminar los datos?")
	if (answer){
		this.forms.Submit();
	}
}



function addRow(table,fecha,input){
	var dia = "";
	
	if(fecha.getDay()==0)
		dia = "Domingo";
	else if(fecha.getDay()==1)
		dia = "Lunes";
	else if(fecha.getDay()==2)
		dia = "Martes";
	else if(fecha.getDay()==3)
		dia = "Miercoles";
	else if(fecha.getDay()==4)
		dia = "Jueves";
	else if(fecha.getDay()==5)
		dia = "Viernes";
	else if(fecha.getDay()==6)
		dia = "Sabado";
	
	var newRow = document.createElement("tr");
	var newCol = document.createElement("td");
	var newCol2 = document.createElement("td");
	var newCol3 = document.createElement("td");
	var newInput = document.createElement('input');
	var newInput2 = document.createElement('input');
	var newTxt = document.createTextNode(dia);
	var newFecha = document.createTextNode((fecha.getMonth()+1) + "/" + (fecha.getDate()) + "/" + (fecha.getFullYear()));
	
	newInput.type = 'text';
	newInput.name = 'hr' + input;
	newInput.setAttribute('title','Anote el horario para cada dia en este formato: 10:00-13:00');
	//newInput.setAttribute("onblur","cargarhorario(document.getElementById('horarios').value = this.value)");
	newInput2.type = 'hidden';
	newInput2.name = 'hrid' + input;
	newInput2.value = (fecha.getMonth()+1) + "/" + (fecha.getDate()) + "/" + (fecha.getFullYear());
	newCol.setAttribute("id","non");
	newCol2.setAttribute("id","non");
	newCol3.setAttribute("id","non");
	newCol.appendChild(newFecha);
	newCol2.appendChild(newTxt);
	newCol3.appendChild(newInput);
	newCol3.appendChild(newInput2);
	newRow.appendChild(newCol);
	newRow.appendChild(newCol2);
	newRow.appendChild(newCol3);
	table.getElementsByTagName("tbody")[0].appendChild(newRow); // appends it to the first tbody eleme
}

function deltbody(elemento){
	// Obtenemos el elemento
	// var del = document.getElementById('tablahorarios').getElementsByTagName('tbody');
	// Obtenemos el elemento
	var el = document.getElementById(elemento);
	// Obtenemos el padre de dicho elemento
	// con la propiedad “parentNode”
	var padre = el.parentNode;
	// Eliminamos el hijo (el) del elemento padre
	padre.removeChild(el); 
}


function seeline(){
	deltbody('tablebody');
	var f1 = new Date(document.getElementById('fechainicio').value);
	var f2 = new Date(document.getElementById('fechafin').value);
	diasdiff = (f2 - f1) / (24*3600*1000);
	document.getElementById('diasdiff').value=(diasdiff + 1);
	
	var newbody = document.createElement("tbody");
	newbody.setAttribute("id","tablebody");
	document.getElementById('tablahorarios').appendChild(newbody);

	for (var i=0; i<=diasdiff; i++){
		diaact = new Date(f1.getTime() + ((i) * 24 * 3600 * 1000));
		addRow(document.getElementById('tablahorarios'), diaact, i);
	}
	
	//var fecha1 = f1.split("/");
	//for (var i=0; i<fecha1.length; i++){
		//document.getElementById('seeline').innerHTML=fecha1[i];
	//}
}

function cargarhorario(){
	var div1 = document.getElementById("tablahorarios");
	var div1Paras = div1.getElementsByTagName("input");
	var num = div1Paras.length;
	var str = "";
	for(var i=0; i<num; i++){
		str = str.concat(div1Paras[i].value);
	}
	
	document.getElementById('horarios').innerHTML = str;
}


function addRowPersonal(table,valor,input){

	var newRow = document.createElement("tr");
	var newCol = document.createElement("td");
	var newCol2 = document.createElement("td");
	var newSelect = document.createElement('select');
	var newTxt = document.createTextNode(input);
	var newTxt2 = document.createTextNode(valor);
	
	newSelect.name = 'per' + input;
	newSelect.setAttribute("value",newTxt2);
	newCol.setAttribute("id","non");
	newCol2.setAttribute("id","non");
	newCol.appendChild(newTxt);
	newCol2.appendChild(newSelect);
	newRow.appendChild(newCol);
	newRow.appendChild(newCol2);
	table.getElementsByTagName("tbody")[0].appendChild(newRow); // appends it to the first tbody eleme
}

function seelinepersonal(numero){
	deltbody('tablebp');
	var newbody = document.createElement("tbody");
	newbody.setAttribute("id","tablebp");
	document.getElementById('tablapersonal').appendChild(newbody);
	
	//var array = (document.getElementById('arreglo').value).split(';');
	
	for (var i=0; i<numero; i++){
		addRowPersonal(document.getElementById('tablapersonal'), 0, (i+1));
	}
	
	//var fecha1 = f1.split("/");
	//for (var i=0; i<fecha1.length; i++){
		//document.getElementById('seeline').innerHTML=fecha1[i];
	//}
}
