	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/


function mostrar(valor,elemento)
{
    document.getElementById(elemento).innerHTML=valor;
}

function valor_seleccionado_rb(grupo,div,input){
	//document.write(div);
	for(i=0;i<grupo.length;i++){
		if(grupo[i].checked) {
		marcado=i;
		}
	}
	//alert("El valor seleccionado es: "+document.e_instructor.group1[marcado].value);
	mostrar(grupo[marcado].value,div);
	input.value=grupo[marcado].value;
	
	valor_total_gral();
}

function valor_seleccionado(grupo,div){
	//document.write(div);
	for(i=0;i<grupo.length;i++){
		if(grupo[i].checked) {
		marcado=i;
		}
	}
	//alert("El valor seleccionado es: "+document.e_instructor.group1[marcado].value);
	mostrar(grupo[marcado].value,div);	
}

function valor_total(grupo){
	cont = 0;
	for(i=0;i<grupo.length;i++){
		if(grupo[i].checked) {
		marcado=i;
		cont += 1;
		}
	}
	if(cont == 0){
		return "0";
	} else {
		return grupo[marcado].value
	}
}
function valor_total_gral(){
	total1 = valor_total(e_instructor.g1);
	total2 = valor_total(e_instructor.g2);
	total3 = valor_total(e_instructor.g3);
	total4 = valor_total(e_instructor.g4);
	total5 = valor_total(e_instructor.g5);
	
	total = parseInt(total1) + parseInt(total2) + parseInt(total3) + parseInt(total4) + parseInt(total5);
	
	document.getElementById("totalc").innerHTML=total;
	document.e_instructor.tcriterios.value = total;
}
