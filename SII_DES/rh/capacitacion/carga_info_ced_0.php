<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";

/*
 * carga_subgrupos_cap.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
 
if ($_GET["rfc"]){
	$rfc = $_GET["rfc"];
	
	//$consulta="select *	from personal where status_empleado = '02' and rfc = '".$rfc."' order by apellidos_empleado, nombre_empleado, rfc";
	$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.telefono_empleado, p.sexo_empleado, p.correo_electronico, p.estudios
				from personal as p
				where p.status_empleado = '02' and p.rfc = '$rfc' order by p.apellidos_empleado, p.nombre_empleado, p.rfc";
} else {
	$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
}

	$accion=$_GET["action"];

	$datos=ejecutar_sql($consulta);
	//echo array_count_values($datos);
	//var_dump($datos->fields);
	//echo $consulta;
	
	
	if(!$datos->rowcount())	echo "Datos faltantes";
		else {			
				while(!$datos->EOF){
					if($datos->fields('sexo_empleado')=="M")
						$genero = "HOMBRE";
					else
						$genero = "MUJER";
					
				echo '<table align="center" title="Por favor rellene los siguientes datos faltantes del Personal">';
					echo '<tr>
						<th colspan="2"> Nombre: </th>
						<td id="non" colspan="2">
							<input name="per0" type="text" size="50" maxlength="100" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('apellidos_empleado')).' '.utf8_encode($datos->fields('nombre_empleado')).'" title="Nombre del Personal seleccionado">
						</td>
					</tr>';
					echo '<tr>
						<th> R.F.C.: </th>
						<td id="non">
							<input name="per1" type="text" size="25" maxlength="25" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('rfc').'" title="R.F.C. del Personal seleccionado" disabled>
						</td>
						<th> G&eacute;nero: </th>
						<td id="non">
							<input name="per2" type="text" size="25" maxlength="25" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$genero.'" title="R.F.C. del Personal seleccionado" disabled>
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Clave Presupuestal: </th>
						<td id="non" colspan="2">
							<input name="per3" type="text" size="20" maxlength="20" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('clave_presupuestal').'" title="Clave Presupuestal">
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Tel&eacute;fono Oficial: </th>
						<td id="non" colspan="2">
							<input name="per4" type="text" size="25" maxlength="20" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('telefono_empleado').'" title="Tel&eacute;fono del Personal" >
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Correo Electr&oacute;nico: </th>
						<td id="non" colspan="2">
							<input name="per5" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('correo_electronico').'" title="Correo Electr&oacute;nico del Personal" >
						</td>
					</tr>';
				echo '</table>';
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">DATOS ESCOLARES</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
					
					$sql="select idescolaridad, descripcion from cp_escolaridad_p where estado = '1'";
					$datos2=ejecutar_sql($sql);
					//var_dump($datos2-);
					echo '<tr>
						<th>GRADO</th>
						<th>CONCLUIDA</th>
						<th>INCONCLUSA</th>
						<th width="70"></th>
					</tr>';
						while(!$datos2->EOF){
							echo '<tr>
								<td id="non"> '.utf8_encode($datos2->fields('descripcion')).' </td>
								<td id="non"> <input name=e'.$datos2->fields('idescolaridad').' type="radio" value="1"> </td>
								<td id="non"> <input name=e'.$datos2->fields('idescolaridad').' type="radio" value="2"> </td>
								<td id="non"></td>
							</tr>';
						$datos2->MoveNext();
						}
					echo '<tr>
						<th>Carrera: </th>
						<td id="non" colspan="3">
							<input name="per6" type="text" size="60" maxlength="60" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('estudios')).'" title="Nombre de la Carrera" >
						</td>
					</tr>';
					
				echo '</table>';
				
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">DATOS LABORALES</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
					
					$sql="select idescolaridad, descripcion from cp_escolaridad_p where estado = '1'";
					$datos2=ejecutar_sql($sql);
					//var_dump($datos2-);
					echo '<tr>
						<th>GRADO</th>
						<th>CONCLUIDA</th>
						<th>INCONCLUSA</th>
						<th width="70"></th>
					</tr>';
						while(!$datos2->EOF){
							echo '<tr>
								<td id="non"> '.utf8_encode($datos2->fields('descripcion')).' </td>
								<td id="non"> <input name=e'.$datos2->fields('idescolaridad').' type="radio" value="1"> </td>
								<td id="non"> <input name=e'.$datos2->fields('idescolaridad').' type="radio" value="2"> </td>
								<td id="non"></td>
							</tr>';
						$datos2->MoveNext();
						}
					echo '<tr>
						<th>Carrera: </th>
						<td id="non" colspan="3">
							<input name="per6" type="text" size="60" maxlength="60" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('estudios')).'" title="Nombre de la Carrera" >
						</td>
					</tr>';
					
				echo '</table>';
				
				$datos->MoveNext();
				}
		}
	
?>
