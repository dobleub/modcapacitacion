<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/capacidades_personal.php'";
	$regresarInicio = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	

	if($_GET['clave']){
		$clave = $_GET["clave"];
		$capacidad = $_GET["capacidad"];
		$instr = $_GET["instructor"];
		$cupo = $_GET["cupo"];
		$fecha = $_GET["fecha"];
		$horario = $_GET["horario"];
		$fechainicio = $_GET["fechainicio"];
		$fechafin = $_GET["fechafin"];
		$totalhoras = $_GET["totalhoras"];
		$otro = $_GET["otro"];
		
		$accion = "update";
	} else {
		$accion = "insert";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<script type="text/javascript">
			window.onload = function() {
				// code to execute here
				//<?php if($cupo) { ?>
					//seelinepersonal(<?php echo $cupo; ?>);
				//<?php } ?>
			}
		</script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
	
<body>

	<h2 align="center">Administraci&oacute;n de INSCRIPCIONES</h2>

<?php
	if ($accion == "update"){			
		$titulo = '<h3 align="center">Inscripci&oacute;n de Personal a Curso ya Autorizado</h3>';
		$form = '<form name="u_inscripcion" id="u_inscripcion" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos_uc()" >';
		
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()" disabled>Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Actualizaci&oacute; de la Inscripcion del personal"/>';
		$funcionreload = 'reload_grupob_cap';
		
	} else{
		$titulo = '<h3 align="center">Inscripci&oacute;n de Personal a Curso ya Autorizado</h3>';
		$form = '<form name="i_inscripcion" id="i_inscripcion" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos_ic()">';
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()">Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Inscripci&oacute;n de Personal a Curso Autorizado Previamente"/>';
		$funcionreload = 'reload_grupo_cap';
	}
?>
	<!-- Contenido -->  
<?php
	echo '<br/>';
	echo $titulo;

	echo $form;
?>
		<table align="center" title="Inscripci&oacute;n de Personal a Curso Autorizado Previamente">
			<tr>
				<th> CLAVE DE CAPACIDAD/CURSO AUTORIZADO: </th>
				<td id="non">

					<input name="cap0" type="text" size="25" maxlength="30" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $clave ?>" title="Ingrese una CLAVE de Capacitaci&oacute;n/curso. Ejemplo: CP-A-01-119-119-12" disabled>

				</td>
			</tr>
			<tr>
				<th> Nombre de CAPACIDAD/curso: </th>
				<td id="non">

					<input name="cap2" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $capacidad ?>" title="Muestra el nombre de Capacitaci&oacute;n/curso actual. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS" disabled>

				</td>
			</tr>
			<tr>
				<th> Nombre del Instructor: </th>
				<td id="non">

					<input name="cap4" type="text" size="60" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $instr ?>" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: CAPACIDAD T&Eacute;CNICA: ADMINISTRACI&Oacute;N DE BASES DE DATOS" disabled>

				</td>
			</tr>
			<tr>
				<th> FECHA de registro de la capacidad/curso: </th>
				<td id="non">
					<input id="registro" name="cap5" type="text" maxlength="10" style="padding-right: 22px;" value="<?php echo $fecha ?>" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013" disabled/>
				</td>
			</tr>
			<tr>
				<th> Total de horas: </th>
				<td id="non">
					<input id="totalh" name="cap8" type="text" maxlength="10" style="padding-right: 22px;" value="<?php echo $totalhoras ?>" title="Anote total de horas que tomara completar el Curso" disabled/>
				</td>
			</tr>
			<tr>
				<th> Cupo l&iacute;mite: </th>
				<td id="non">
					<input id="totalp" name="cap9" type="text" maxlength="10" style="padding-right: 22px;" value="<?php echo $cupo ?>" title="Anote total de horas que tomara completar el Curso" disabled/>
				</td>
			</tr>
			<tr>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<th> FECHA de INICIO: </th>
				<th> FECHA de T&Eacute;RMINO de la capacidad/curso: </th>
			</tr>
			<tr>
				<td id="non"> <center><input id="fechainicio" name="cap6" type="text" size="10" maxlength="10" value="<?php echo $fechainicio ?>" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013" disabled/></center> </td>
				<td id="non"><input id="fechafin" name="cap7" type="text" size="10" maxlength="10" value="<?php echo $fechafin ?>" title="Fecha de Registro de TERMINO de Curso. Ejemplo: 12/06/2013" disabled/>
			</tr>
		</table>
		<table align="center" title="Datos de la Nueva Capacidad/curso ya Autorizada, por favor seleccione una capacidad de la lista anexa en la parte de abajo">
			<tr>
				<table id="tablahorarios" align="center">
					<?php
					$consulta="select h.idhorario, h.totalhoras, hd.dia, hd.fecha_dia, hd.horainicio 
					from cp_horario_gral as h, cp_horario_det as hd, cp_horario_gral_det as hgd 
					where hgd.idhorario = h.idhorario and hgd.idhd = hd.idhd ";
					$consulta="select h.idhorario, h.totalhoras from cp_horario_gral as h where h.idhorario = '$horario'";
					
					$datos=ejecutar_sql($consulta);
					
					if(!$datos->rowcount())	echo "<table align='center'> <tr><th>HORARIO</th></tr> <tr><td>Capacidad Autorizada sin Horario</td></tr> </table>";
					else {
						while(!$datos->EOF){
							echo '<tr>';
							echo '<th> HORARIO </th>';
							echo '<td id="non" align="right"> Total de Horas: ';
							echo ' <strong> '.$datos->fields('totalhoras').' </strong> </td>';
							echo '</tr>';
							
								$consulta2 = "select hd.dia, hd.fecha_dia, hd.horainicio 
								from cp_horario_det as hd, cp_horario_gral_det as hgd 
								where hgd.idhorario ='".$datos->fields('idhorario')."' and hgd.idhd = hd.idhd ";
								$datos2=ejecutar_sql($consulta2);
								
								echo '<tr>';
									if(!$datos2->rowcount())	echo "<tr><td>Capacidad Autorizada sin Horario</td></tr>";
									else {
										while(!$datos2->EOF){
											echo '<td>';
												echo '<table>';
												echo '<tr>';
													echo '<td id="non" align="center"> '.$datos2->fields('fecha_dia').' </td>';
												echo '</tr>';
												echo '<tr>';
													echo '<th align="center"> '.$datos2->fields('dia').' </th>';
												echo '</tr>';
												echo '<tr>';
													echo '<td id="non" align="center"> '.$datos2->fields('horainicio').' </td>';
												echo '</tr>';
												echo '</table>';
											echo '</td>';
										$datos2->MoveNext();
										}
									}
								echo '</tr>';
						$datos->MoveNext();
						}
					}
					?>
				</table>
			</tr>
		</table>
		<table id="tablapersonal" align="center" title="Por favor seleccione las personas que van a participar en el Curso">
			<tbody id="tablebp">
			<tr>
				<th width="300" colspan="2"> PERSONAL Administrativo y Docente: </th>
			</tr>
						<?php
						$consultaper="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
						$datosper=ejecutar_sql($consultaper);
						
						if($cupo){
							for($i=0;$i<$cupo;$i++){
								echo '<tr>';
								echo '<td id = "non" align="right" width="25">'.($i + 1).'</td>';
								echo '<td id="non">';
								echo '<select name="sp'.$i.'" title="Presione aqu&iacute; para seleccionar el Personal">';
								if(!$datosper->rowcount())	echo "<option value='-1' selected> No hay personal dado de alta </option>";
								else { echo "<option value='-1' selected> - Selecciona Personal - </option>";
									  while(!$datosper->EOF){
									  echo '<option value="'.$datosper->fields('rfc').'">'.$datosper->fields('rfc').' - '.$datosper->fields('apellidos_empleado').' '.$datosper->fields('nombre_empleado').'</option>';
									  $datosper->MoveNext();
									  }
									}
								echo '</select>';
								echo '</td>';
								echo '</tr>';
								$datosper->MoveFirst();
							}
						} else {
							echo '<tr>';
							echo '<td id = "non" align="right" width="25" >';
							echo '<select name="spa" title="Presione aqu&iacute; para seleccionar el Personal">';
								if(!$datosper->rowcount())	echo "<option value='-1' selected> No hay personal dado de alta </option>";
								else    { echo "<option value='-1' selected> - Selecciona Personal - </option>";
									  while(!$datosper->EOF){
									  echo '<option value="'.$datosper->fields('rfc').'">'.$datosper->fields('rfc').' - '.$datosper->fields('apellidos_empleado').' '.$datosper->fields('nombre_empleado').'</option>';
									  $datosper->MoveNext();
									  }
									}
							echo '</td>';
							echo '</tr>';
							echo '</select>';
						}
						?>
			</tbody>
		</table>
		<div align="center">
			<input type="hidden" name="clave" value="<?php echo $clave ?>">
			<input type="hidden" name="otro" value="<?php echo $otro ?>">
			<input type="hidden" name="cupo" value="<?php echo $cupo ?>">
			
			<input type="hidden" name="accion" value="<?php echo $accion ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar ?>">
			
			<?php
			echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>


	<!-- Modificar Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">CAPACIDADES AUTORIZADAS</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
<!--
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
-->
		<tr>
			<th>CLAVE</th>
			<th>Capacidad</th>
			<th>Instructor</th>
			<th>Fecha de Inicio </th>
			<th>Fecha de T&eacute;rmino</th>
			<th>Cupo limite</th>
			<th>Total de horas</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
			<?php
		
				//$consulta="select ca.clavecurso, c.idcapacidad as idc, c.descripcion as des, g.idgrupo as idgrup, g.descripcion as desg, s.idsubgrupo as idsub, s.descripcion as dess from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				$consulta="select ca.clavecurso, ca.fecha, c.descripcion, i.titulo, i.nombre, ca.limite_personal, h.idhorario, h.fechainicio,h.fechafin,h.totalhoras
							from cp_capacidad_autorizada as ca, cp_capacidades as c, cp_instructor_cap as i, cp_horario_gral as h
							where ca.idcapacidad = c.idcapacidad and ca.idinstructor = i.idinstructor and ca.idhorario = h.idhorario and ca.estado='1' order by descripcion";
				//echo $consulta;
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Capacidades Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('clavecurso').'</td>';
						echo '<td id="non">'.$datos->fields('descripcion').' </td>';
						echo '<td id="non"> '.$datos->fields('titulo').' '.$datos->fields('nombre').' </td>';
						echo '<td id="non"> '.$datos->fields('fechainicio').' </td>';
						echo '<td id="non"> '.$datos->fields('fechafin').' </td>';
						echo '<td id="non" align="center"> '.$datos->fields('limite_personal').' </td>';
						echo '<td id="non" align="center"> '.$datos->fields('totalhoras').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_ins" value="" title = "Inscribir en '.$datos->fields('descripcion').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('descripcion').'">';
						echo '<input type="hidden" name="instructor" value="'.$datos->fields('nombre').'">';
						echo '<input type="hidden" name="cupo" value="'.$datos->fields('limite_personal').'">';
						echo '<input type="hidden" name="horario" value="'.$datos->fields('idhorario').'">';
						echo '<input type="hidden" name="fecha" value="'.$datos->fields('fecha').'">';
						echo '<input type="hidden" name="fechainicio" value="'.$datos->fields('fechainicio').'">';
						echo '<input type="hidden" name="fechafin" value="'.$datos->fields('fechafin').'">';
						echo '<input type="hidden" name="totalhoras" value="'.$datos->fields('totalhoras').'">';
					echo '</form>';
					echo '<form name="m_cp" id="m_cp" method="get" action="ver_capacidades_personal.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_stt" value="" title = "Ver estado de '.$datos->fields('descripcion').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						echo '<input type="hidden" name="cupo" value="'.$cupo.'">';
						echo '<input type="hidden" name="accion" value="ver">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
				
				$consulta="select ca.clavecurso, c.descripcion, i.titulo, i.nombre, ca.limite_personal, h.idhorario, h.fechainicio,h.fechafin,h.totalhoras
				from cp_capacidad_autorizada as ca, cp_otras_capacidades as c, cp_instructor_cap as i, cp_horario_gral as h
				where ca.idotracap = c.idotracap and ca.idinstructor = i.idinstructor and ca.idhorario = h.idhorario order by descripcion";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('clavecurso').'</td>';
						echo '<td id="non">'.$datos->fields('descripcion').' </td>';
						echo '<td id="non"> '.$datos->fields('titulo').' '.$datos->fields('nombre').' </td>';
						echo '<td id="non"> '.$datos->fields('fechainicio').' </td>';
						echo '<td id="non"> '.$datos->fields('fechafin').' </td>';
						echo '<td id="non" align="center"> '.$datos->fields('limite_personal').' </td>';
						echo '<td id="non" align="center"> '.$datos->fields('totalhoras').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_ins" value="" title = "Inscribir en '.$datos->fields('descripcion').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('descripcion').'">';
						echo '<input type="hidden" name="instructor" value="'.$datos->fields('nombre').'">';
						echo '<input type="hidden" name="cupo" value="'.$datos->fields('limite_personal').'">';
						echo '<input type="hidden" name="fecha" value="'.$datos->fields('fecha').'">';
						echo '<input type="hidden" name="horario" value="'.$datos->fields('idhorario').'">';
						echo '<input type="hidden" name="fechainicio" value="'.$datos->fields('fechainicio').'">';
						echo '<input type="hidden" name="fechafin" value="'.$datos->fields('fechafin').'">';
						echo '<input type="hidden" name="totalhoras" value="'.$datos->fields('totalhoras').'">';
						echo '<input type="hidden" name="otro" value="id">';
					echo '</form>';
					echo '<form name="m_cp" id="m_cp" method="get" action="ver_capacidades_personal.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_stt" value="" title = "Ver estado de '.$datos->fields('descripcion').' "/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						echo '<input type="hidden" name="cupo" value="'.$cupo.'">';
						echo '<input type="hidden" name="otro" value="id">';
						echo '<input type="hidden" name="accion" value="ver">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarInicio; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
