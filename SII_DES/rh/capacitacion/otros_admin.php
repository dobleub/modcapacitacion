<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/otros_admin.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	if($_GET['idbar']){
		$barid = $_GET['idbar'];
		$bardes = $_GET['barrera'];
		$accionbar = "update";
	} else {
		$accionbar = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Otras herramientas ::.</title>
	</head>

<body>	
	<h2 align="center">Administraci&oacute;n de Herramientas Varias</h2>

<?php
	if($accionbar == "update"){
		$form = '<form name="u_barrera" id="u_barrera" method="get" action="" onSubmit="return validarcampos_ub()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar la BARRERA"/>';
	} else {
		$form = '<form name="i_barrera" id="i_barrera" method="get" action="" onSubmit="return validarcampos_ib()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva BARRERA"/>';
	}

?>
	
  <!-- Contenido -->
	<br/>
	<h3 align="center">Ingresar/Modificar/Eliminar NUEVAS BARRERAS QUE IMPIDEN EL DESEMPE&Ntilde;O DEL PERSONAL</h3>
	
	<?php 
		echo $form;
	?>
		<table align="center" title="Ingresa un nombre descriptivo acerca de una nueva BARRERA que impide el buen desempe&ntilde;o del personal administrativo/docente">
			<tr> 
				<th width="150"> Ingrese un nombre: </th>
				<td id="non">
					<input name="barr0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $bardes ?>" title="Ingrese un nombre. Ejemplo: ESTR&Eacute;S LABORAL">
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="idbar" value="<?php echo $barid; ?>">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php
				echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	<br/>
	<br/>
	
	
		<table align="center" width="400" title="Modificar / Eliminar una BARRERA">
			<tr>
				<th colspan="2">Nombre de la Barrera que impiden el buen desempe&ntilde;o del Personal</th>
				<th colspan="2"> Acc&oacute;n</th>
				<th></th>
			</tr>
			<?php
			$consulta="select idbarrera, descripcion from cp_barreras_personal where estado='1'";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Datos de Barreras Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_bar" id="m_bar" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<tr> <td width="10"></td>';
						echo '<td id="non">'.$datos->fields('descripcion').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
						echo '<input type="hidden" name="idbar" value="'.$datos->fields('idbarrera').'">';
						echo '<input type="hidden" name="barrera" value="'.$datos->fields('descripcion').'">';
					echo '</form>';
					echo '<form name="e_bar" id="e_bar" method="get" action="ime_otros.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="button" class="button_del" value="" title = "Eliminar" onclick="delete_cap();submit();"/> </center></td>';	
						echo '<input type="hidden" name="barrera" value="'.$datos->fields('idbarrera').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
						echo '<td width="10"></td>';
					echo '</tr>';
					echo '</form>';
					$datos->MoveNext();
					}
				}
			?>
		</table>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
