<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_capacidades.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	$otro=$_GET["otro"];
	
	$enableid = $_GET["enableid"];
	$cap_id = $_GET["cap0"];
	$cap_name = $_GET["cap1"];
	$grupo_id = $_GET["gcap"];
	$subg_id = $_GET["subcap"];
	
	$capid = $_GET["idcap"];
	$grpid = $_GET["idgc"];
	$subgid = $_GET["idsgc"];
	
	$capacidad = $_GET["capacidad"];
	
		if($accion == "insert"){
			if($cap_name!="" && $subg_id!='-1'){
				if($enableid && $cap_id){
					$query = sybase_query("declare @echo char(1) exec i_otras_capacidades '$cap_id', '$cap_name', $subg_id, @echo output select @echo as echo");
				} else {
					$query = sybase_query("declare @echo char(1) exec i_otras_capacidades NULL, '$cap_name', $subg_id, @echo output select @echo as echo");
				}
				$mensaje = sybase_get_last_message();				
			} else {
				$mensaje = "No se pudo Insertar la capacidad por falta de informaci&oacute;n";
			}
		}
		if($accion == "update"){
			if($cap_name!="" && $grpid!='-1' && $subgid!='-1'){
				if($otro=="id")
					$query = sybase_query("update cp_otras_capacidades set descripcion='$cap_name',idsubgrupo=$subgid where idotracap='$capid'");
				else
					$query = sybase_query("update cp_capacidades set descripcion='$cap_name',idsubgrupo=$subgid where idcapacidad='$capid'");
					
				$mensaje = "Capacidad Actualizado CORRECTAMENTE";
			} else {
				$mensaje = "No se pudo Actualizar la capacidad por falta de informaci&oacute;n";
			}
		}
		if($accion == "delete"){
			if($otro=="id")
				$query = sybase_query("update cp_otras_capacidades set estado='0' where idotracap='$capacidad' ");
			else
				$query = sybase_query("update cp_capacidades set estado='0' where idcapacidad='$capacidad' ");
			
			$mensaje = "Capacidad Eliminado CORRECTAMENTE";
		}
		
		//echo $mensaje;
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
