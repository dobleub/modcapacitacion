<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_capacidades.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	$otro=$_GET["otro"];
	
	$clave = $_GET["cap0"];
	$cap_id = $_GET["idcap"];
	$grupo_id = $_GET["idgc"];
	$subg_id = $_GET["idsgc"];
	$insti_id = $_GET["insti0"];
	$instr_id = $_GET["instr0"];
	$fecha = $_GET["cap5"];
	$fecha_i = $_GET["cap6"];
	$fecha_f = $_GET["cap7"];
	$total_h = filter_input(INPUT_GET, 'cap8', FILTER_VALIDATE_INT);
	$total_p = filter_input(INPUT_GET, 'cap9', FILTER_VALIDATE_INT);
	$total_d = $_GET["diasdiff"];
	
	$h0 = $_GET["hr0"];
	$h1 = $_GET["hr1"];
	$h2 = $_GET["hr2"];
	$h3 = $_GET["hr3"];
	$h4 = $_GET["hr4"];
	$h5 = $_GET["hr5"];
	$h6 = $_GET["hr6"];
	$h7 = $_GET["hr7"];
	$h8 = $_GET["hr8"];
	$h9 = $_GET["hr9"];
	$h10 = $_GET["hr10"];
	$h11 = $_GET["hr11"];
	$h12 = $_GET["hr12"];
	
	$hr0 = $_GET["hrid0"];
	$hr1 = $_GET["hrid1"];
	$hr2 = $_GET["hrid2"];
	$hr3 = $_GET["hrid3"];
	$hr4 = $_GET["hrid4"];
	$hr5 = $_GET["hrid5"];
	$hr6 = $_GET["hrid6"];
	$hr7 = $_GET["hrid7"];
	$hr8 = $_GET["hrid8"];
	$hr9 = $_GET["hrid9"];
	$hr10 = $_GET["hrid10"];
	$hr11 = $_GET["hrid11"];
	$hr12 = $_GET["hrid12"];
	
	$capacidad = $_GET["capacidad"];
	
		if($accion == "insert"){
			$mensaje = "No se puede realizar la operaci&oacute;n";
		}
		if($accion == "update"){
			if($clave!="" && $cap_id!='-1' && $subg_id!='-1' && $insti_id!='-1' && $instr_id!='-1' && $fecha!="" && $fecha_i!="" && $fecha_f!=""){
				if($total_h !== null && $total_p !== false)
					$query = sybase_query("declare @echo char(1) exec i_horario_gral '$fecha_i', '$fecha_f', $total_h, @echo output select @echo as echo");
				else
					$query = sybase_query("declare @echo char(1) exec i_horario_gral '$fecha_i', '$fecha_f', 30, @echo output select @echo as echo");
				
				if($h0!=="" && $h1!==""){
					if($h0 && $hr0)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr0', '$h0', NULL, NULL, @echo output select @echo as echo");
					if($h1 && $hr1)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr1', '$h1', NULL, NULL, @echo output select @echo as echo");
					if($h2 && $hr2)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr2', '$h2', NULL, NULL, @echo output select @echo as echo");
					if($h3 && $hr3)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr3', '$h3', NULL, NULL, @echo output select @echo as echo");
					if($h4 && $hr4)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr4', '$h4', NULL, NULL, @echo output select @echo as echo");
					if($h5 && $hr5)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr5', '$h5', NULL, NULL, @echo output select @echo as echo");
					if($h6 && $hr6)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr6', '$h6', NULL, NULL, @echo output select @echo as echo");
					if($h7 && $hr7)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr7', '$h7', NULL, NULL, @echo output select @echo as echo");
					if($h8 && $hr8)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr8', '$h8', NULL, NULL, @echo output select @echo as echo");
					if($h9 && $hr9)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr9', '$h9', NULL, NULL, @echo output select @echo as echo");
					if($h10 && $hr10)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr10', '$h10', NULL, NULL, @echo output select @echo as echo");
					if($h11 && $hr11)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr11', '$h11', NULL, NULL, @echo output select @echo as echo");
					if($h12 && $hr12)
						sybase_query("declare @echo char(1) exec i_horario_det '$hr12', '$h12', NULL, NULL, @echo output select @echo as echo");
				} else {
					$mensaje = "Por favor de click en el botón CREAR HORARIO y rellene los datos que se piden";
				}
				
				$consulta="select idhorario from cp_horario_gral where estado = '2'";
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	$mensaje = "No se pudo completar la operacion";
				else { 
					while(!$datos->EOF){
						$hrid = $datos->fields('idhorario');
						$datos->MoveNext();
					}
					if($otro=="id")
						$query = sybase_query("declare @echo char(1) exec i_capacidad_autorizada '$clave', NULL, '$cap_id', '$instr_id', '$hrid', '$fecha', $total_p, '$insti_id', @echo output select @echo as echo");
					else
						$query = sybase_query("declare @echo char(1) exec i_capacidad_autorizada '$clave', '$cap_id', NULL, '$instr_id', '$hrid', '$fecha', $total_p, '$insti_id', @echo output select @echo as echo");
					
					$mensaje = "Capacidad AUTORIZADA CORRECTAMENTE";
				}
				//$mensaje = sybase_get_last_message();
			} else {
				$mensaje = "No se pudo AUTORIZAR la capacidad por falta de información";
			}
		}
		if($accion == "delete"){
			if($otro=="id")
				$query = sybase_query("update cp_otras_capacidades set estado='0' where idotracap='$capacidad' ");
			else
				$query = sybase_query("update cp_capacidades set estado='0' where idcapacidad='$capacidad' ");
			
			$mensaje = "Capacidad Eliminado CORRECTAMENTE";
		}
		
		//echo $mensaje;
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}
 else {

	echo '<script type="text/javascript">';
	echo 'alert("Datos incorrectos");';
	echo $regresar;
	echo '</script>';
		
}


?>
