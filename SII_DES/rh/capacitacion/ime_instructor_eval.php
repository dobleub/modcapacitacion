<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_instructor_emp.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	
	$institucion = $_GET["evali0"];
	$instructor = $_GET["evali1"];
	$fecha = $_GET["evali2"];
	$curso = $_GET["evali3"];
	
	$cri_1_id = $_GET["id1"];
	$cri_1 = $_GET["g1"];
	
	$cri_2_id = $_GET["id2"];
	$cri_2 = $_GET["g2"];
	
	$cri_3_id = $_GET["id3"];
	$cri_3 = $_GET["g3"];
	
	$cri_4_id = $_GET["id4"];
	$cri_4 = $_GET["g4"];
	
	$cri_5_id = $_GET["id5"];
	$cri_5 = $_GET["g5"];
	
	$aceptado = $_GET["insa"];
	
		if($accion == "insert"){
			if($institucion!='-1' && $instructor!='-1' && $curso!='-1' && $cri_1!="" && $cri_2!="" && $cri_3!="" && $cri_4!="" && $cri_5!="" && $aceptado!=""){
				$query = sybase_query("declare @echo char(1) exec i_evaluacion_ins '$instructor', '$fecha', '$curso', '$aceptado', '$institucion', $cri_1_id, $cri_1, @echo output select @echo as echo");
				$query = sybase_query("declare @echo char(1) exec i_evaluacion_ins '$instructor', '$fecha', '$curso', '$aceptado', '$institucion', $cri_2_id, $cri_2, @echo output select @echo as echo");
				$query = sybase_query("declare @echo char(1) exec i_evaluacion_ins '$instructor', '$fecha', '$curso', '$aceptado', '$institucion', $cri_3_id, $cri_3, @echo output select @echo as echo");
				$query = sybase_query("declare @echo char(1) exec i_evaluacion_ins '$instructor', '$fecha', '$curso', '$aceptado', '$institucion', $cri_4_id, $cri_4, @echo output select @echo as echo");
				$query = sybase_query("declare @echo char(1) exec i_evaluacion_ins '$instructor', '$fecha', '$curso', '$aceptado', '$institucion', $cri_5_id, $cri_5, @echo output select @echo as echo");
				$mensaje = sybase_get_last_message();
			} else {
				$mensaje = "No se pudo Insertar datos de Evaluacion de Instructor por falta de informaci&oacute;n";
			}
		}
		//if($accion == "update"){
			//if($emp_id!=""){
				//$query = sybase_query("update cp_instructor_emp set descripcion='$emp_name',pais='$emp_pais',estadoe='$emp_edo',ciudad='$emp_cd',direccion='$emp_dir',email='$emp_mail',telefonos='$emp_tel' where idempresa='$emp_id' ");	
				//$mensaje = "Datos de Empresa Actualizado CORRECTAMENTE";
			//} else {
				//$mensaje = "No se pudieron Actualizar los datos de Empresa por falta de informaci&oacute;n";
			//}
		//}
		//if($accion == "delete"){
			//$query = sybase_query("update cp_instructor_emp set estado='0' where idempresa='$empresa' ");
			//$mensaje = "Datos de Empresa Eliminado CORRECTAMENTE";
		//}
		
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
