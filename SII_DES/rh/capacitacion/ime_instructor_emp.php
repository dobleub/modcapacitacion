<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_instructor_emp.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	
	$emp_name = $_GET["emp0"];
	$emp_pais = $_GET["emp1"];
	$emp_edo = $_GET["emp2"];
	$emp_cd = $_GET["emp3"];
	$emp_dir = $_GET["emp4"];
	$emp_tel = $_GET["emp5"];
	$emp_mail = $_GET["emp6"];
	
	$emp_id = $_GET["idemp"];
	
	$empresa = $_GET["empresa"];
	
		if($accion == "insert"){
			if($emp_name!=""){
				$query = sybase_query("declare @echo char(1) exec i_instructor_emp '$emp_name','$emp_pais','$emp_edo','$emp_cd','$emp_dir','$emp_mail','$emp_tel', @echo output select @echo as echo");
				$mensaje = sybase_get_last_message();				
			} else {
				$mensaje = "No se pudo Insertar datos de Empresa por falta de informaci&oacute;n";
			}
		}
		if($accion == "update"){
			if($emp_id!=""){
				$query = sybase_query("update cp_instructor_emp set descripcion='$emp_name',pais='$emp_pais',estadoe='$emp_edo',ciudad='$emp_cd',direccion='$emp_dir',email='$emp_mail',telefonos='$emp_tel' where idempresa='$emp_id' ");	
				$mensaje = "Datos de Empresa Actualizado CORRECTAMENTE";
			} else {
				$mensaje = "No se pudieron Actualizar los datos de Empresa por falta de informaci&oacute;n";
			}
		}
		if($accion == "delete"){
			$query = sybase_query("update cp_instructor_emp set estado='0' where idempresa='$empresa' ");
			$mensaje = "Datos de Empresa Eliminado CORRECTAMENTE";
		}
		
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
