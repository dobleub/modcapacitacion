<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/instructor_eval.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	$accion = "insert";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		<script type="text/javascript" src="js/funciones_e_inst.js"></script>
		
	<script src="js/mootools-core.js" type="text/javascript"></script>
	<script src="js/mootools-more.js" type="text/javascript"></script>
	<script src="Source/Locale.es-ES-DatePicker.js" type="text/javascript"></script>
	<script src="Source/Picker.js" type="text/javascript"></script>
	<script src="Source/Picker.Attach.js" type="text/javascript"></script>
	<script src="Source/Picker.Date.js" type="text/javascript"></script>
	<link href="Source/datepicker.css" rel="stylesheet">
	
		<title>.:: Criterios para Seleccionar Instructor ::.</title>
	</head>
<body>
<?php ?>
	<h2 align="center">Criterios para Seleccionar el Personal Instructor del Curso</h2>

  <!-- Contenido -->
	<br/>
	<!--
	<h3 align="center">INGRESAR INSTRUCTOR</h3>
	-->
	
	<form name="e_instructor" id="e_instructor" method="get" action="ime_instructor_eval.php" onSubmit="return validarcampos_ei()" >
		<table align="center" title="Por favor rellene los siguientes datos para evaluar un Instructor disponible">
			<tr>
				<th width="150"> Instituto Tecnol&oacute;gico o Centro: </th>
				<td width="350">
					<select name="evali0" title="Seleccione el Tecnol&oacute;gico o Centro">
						<?php
						$consulta="select idinstituto,descripcion from cp_institucion";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Instituci&oacute;n faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Instituci&oacute;n - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idinstituto').'">'.$datos->fields('descripcion').' </option>';
								$datos->MoveNext();
							}							
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Nombre del Instructor: </th>
				<td>
					<select name="evali1" title="Seleccione el Instructor">
						<?php
						$consulta="select idinstructor,titulo,nombre from cp_instructor_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Instructor faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Instructor - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idinstructor').'">'.$datos->fields('titulo').' '.$datos->fields('nombre').'</option>';
								$datos->MoveNext();
							}							
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Fecha de Evaluaci&oacute;n: </th>
				<td>
					<input name="evali2" type="text" class="open" style="display: block;">
					<style type="text/css"></style>
					<script type="text/javascript">
						window.addEvent('domready', function(){
							myPicker = new Picker.Date($$('a.open, input'), {
								toggle: $$('.toggle'),
								positionOffset: {x: -10, y: 10}
							});
							$('open').addEvent('click', function(e){
								e.stop();
								myPicker.open();
							});
						});
					</script>
				</td>
			</tr>
			<tr> 
				<th width="100"> Nombre del curso: </th>
				<td>
					<select name="evali3" title="Seleccione el Nombre del Curso">
						<?php
						$consulta="select idsubgrupo,descripcion from cp_subgrupo_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Capacidades faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Capacidad - </option>";
							while(!$datos->EOF){
								echo '<optgroup label="'.$datos->fields('descripcion').'">';
									$consultacap="select idcapacidad,descripcion,idsubgrupo from cp_capacidades where estado='1' and idsubgrupo = cast('".$datos->fields('idsubgrupo')."' as integer)";
									$datoscap=ejecutar_sql($consultacap);
									while(!$datoscap->EOF){
										echo '<option value="'.$datoscap->fields('idcapacidad').'">'.$datoscap->fields('descripcion').'</option>';
										$datoscap->MoveNext();
									}
								echo '</optgroup>';
								$datos->MoveNext();
							}							
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
<!--
				<th> Nombre de la Empresa o Plantel: </th>
				<td>
					<select name="evali4" title="Seleccione la empresa de donde proviene el Instructor">
						<option value="-1" selected> -Seleccione una empresa- </option>
						<option value="1">Empresa 1</option>
						<option value="2">Empresa 2</option>
						<option value="3">Empresa 3</option>
					</select>
				</td>
-->
			</tr>
		</table>
		<br/>
		<table id="tc" align="center" title="Criterios de Evaluaci&ocute;n">
			<tr>
				<th>Criterio</th>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>Total</th>
			</tr>
			<div class="currentline">
				<?php
				$consulta="select idcriterio,descripcion from cp_evaluacion_criterios where estado = '1'";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);
				
				if(!$datos->rowcount())	echo "Criterios de evaluacion Faltantes";
				else { 
					while(!$datos->EOF){
						echo '<tr>';
						echo '<td> '.$datos->fields('descripcion').' </td>';
						echo '<td><input name=g'.$datos->fields('idcriterio').' type="radio" value="1" onClick=valor_seleccionado_rb(e_instructor.g'.$datos->fields('idcriterio').',"stc'.$datos->fields('idcriterio').'",e_instructor.stc'.$datos->fields('idcriterio').')></td>';
						echo '<td><input name=g'.$datos->fields('idcriterio').' type="radio" value="2" onClick=valor_seleccionado_rb(e_instructor.g'.$datos->fields('idcriterio').',"stc'.$datos->fields('idcriterio').'",e_instructor.stc'.$datos->fields('idcriterio').')></td>';
						echo '<td><input name=g'.$datos->fields('idcriterio').' type="radio" value="3" onClick=valor_seleccionado_rb(e_instructor.g'.$datos->fields('idcriterio').',"stc'.$datos->fields('idcriterio').'",e_instructor.stc'.$datos->fields('idcriterio').')></td>';
						echo '<td><input name=g'.$datos->fields('idcriterio').' type="radio" value="4" onClick=valor_seleccionado_rb(e_instructor.g'.$datos->fields('idcriterio').',"stc'.$datos->fields('idcriterio').'",e_instructor.stc'.$datos->fields('idcriterio').')></td>';
						echo '<td><input name=g'.$datos->fields('idcriterio').' type="radio" value="5" onClick=valor_seleccionado_rb(e_instructor.g'.$datos->fields('idcriterio').',"stc'.$datos->fields('idcriterio').'",e_instructor.stc'.$datos->fields('idcriterio').')></td>';
						echo '<td> <center><div id="stc'.$datos->fields('idcriterio').'"></div></center> <input name="stc'.$datos->fields('idcriterio').'" type="hidden"> <input name="id'.$datos->fields('idcriterio').'" type="hidden" value="'.$datos->fields('idcriterio').'"> </td>';
						echo '</tr>';
						$datos->MoveNext();
					}
				}
				?>
	<!--
			<tr>
				<td>Nombre del Criterio a evaluar</td>
			
				<td><input name="g0" type="radio" value="1" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)"></td>
				<td><input name="g0" type="radio" value="2" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)"></td>
				<td><input name="g0" type="radio" value="3" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)"></td>
				<td><input name="g0" type="radio" value="4" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)"></td>
				<td><input name="g0" type="radio" value="5" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)"></td>
				<td><center><div id="stc0"></div></center> <input name="stc0" type="hidden"></td>
			</tr>
	-->
			</div>
			<tr>
				<td></td>
				<th colspan="5">Total de Puntaje</th>
				<th><center><div id="totalc"></div></center> <input name="tcriterios" type="hidden"></th>
			</tr>
			<tr>
				<td></td>
				<th colspan="5">Aceptado:</th>
				<th><input name="insa" type="radio" value="1">SI &nbsp;&nbsp;&nbsp;<input name="insa" type="radio" value="0">NO </th>
			</tr>
		</table>
		
	<br />
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Dar de Alta la Evaluacion de Instructor"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">NOTA: Evaluar considerando la siguiente escala.</h3>
	<table align="center">
		<tr>
			<td>&nbsp;<strong>1</strong>-Malo</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>2</strong>-Regular</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>3</strong>-Bien</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>4</strong>-Muy bien</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>5</strong>-Excelente &nbsp;</td>
		</tr>
	</table>
	
  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
