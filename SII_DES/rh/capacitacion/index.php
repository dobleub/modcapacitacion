<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
<body>
<?php $regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";?>
	<h2 align="center">Administraci&oacute;n de Capacidades/Cursos</h2>

  <!-- Contenido -->   
  <br/>  
	<table align="center" title="Men&uacute; de opciones">
		<tr>
			<th width="120"> Capacidades </th>
			<th width="120"> Instructor </th>
			<th width="120"> Horarios </th>
			<th width="120"> Inscripciones </th>
		</tr>
		<tr>
			<th> Informe de cursos </th>
		</tr>
	</table>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
