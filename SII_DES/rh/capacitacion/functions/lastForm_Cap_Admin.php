	<?php
		if($_POST['capacidad']){
			$accion = 'and rfc = '.$_POST['capacidad'];
			$capacidad = $_POST['capacidad'];
		} else {
			$accion = "insert";
		}
		$consulta="select c.idcapacidad,c.descripcion,s.descripcion as subgrupo,g.descripcion as grupo from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo";
	?>
	<form name="i_capacidad" id="i_capacidad" method="post" action="" onSubmit="return validarcampos_ic()" >
		<table align="center" title="Ingresa un nombre descriptivo acerca de la Nueva Capacidad/curso posible a impartir">
			<tr>
				<th width="120"> Ingresar el ID de la Capacidad </th>
				<td id="non" colspan="2">
					<?php
					echo '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()" >Si';
					echo '<input type="text" id="cap0" name="cap0" size="20" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un ID de Capacitaci&oacute;n/curso. Ejemplo: SCADM02">';
					?>
				</td>
			</tr>
			<tr> 
				<th width="120"> Ingrese un nombre de CAPACIDAD/curso: </th>
				<td id="non">
					<?php
					echo '<input name="cap0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">';
					?>
				</td>
				<td id="non" width="100">
					<select name="gcap" id="gcap" title="Seleccione Grupo a que pertenecer&aacute; la capacidad" onchange="cargarSubgrupos('carga_subgrupos_cap.php','txtHint','gcap',this.value,'reload_grupo_cap');reload_grupo_cap();">
						<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
								$datos->MoveNext();
							}
						}
						?>
					</select>
					
					<!-- select de Seleccion de Subgrupo -->
					<div id="txtHint"></div>

				</td>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="idgc">
			<input type="hidden" name="idsgc">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la Capacidad/curso"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
