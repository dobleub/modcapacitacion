<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Administraci&oacute;n Personal - Puesto - Departamento - Subdirecci&oacute;n - Escolaridad ::.</title>
	</head>
<body>
<?php ?>
	<h3 align="center">Administraci&oacute;n de los detalles del Personal</h3>

  <!-- Contenido -->
	<br/>
	
	<form name="i_dp" id="myForm" method="post" action="" onSubmit="return validarcampos_idp()" >
		<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">
			<tr>
				<th width="150"> Nombre: </th>
				<td width="350">
					<select name="dp0" title="Presione aqu&iacute; para seleccionar el empleado">
						<?php
						$consulta_rfc="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";

						$datos_rfc=ejecutar_sql($consulta_rfc);

						if(!$datos_rfc->rowcount())	echo "<option value='-1' selected> No hay personal dado de alta </option>";
						else    { echo "<option value='-1' selected> -- Seleccione Personal -- </option>";
							  while(!$datos_rfc->EOF){
							  echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
							  $datos_rfc->MoveNext();
							  }
							}
						?>
				</td>
				
			</tr>
			<tr>
				<th width="100"> Departamento: </th>
				<td>
					<select name="dp1" title="Seleccione el Departamento">
						<option value="-1" selected> -Seleccione Departamento- </option>
						<option value="DPT1"> Departamento 1 </option>
						<option value="DPT2"> Departamento 2 </option>
						<option value="DPT3"> Departamento 3 </option>
						<option value="DPT4"> Departamento 4 </option>
						<option value="DPT5"> Departamento 5 </option>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Puesto: </th>
				<td>
					<select name="dp2" title="Seleccione el Puesto">
						<option value="-1" selected> -Seleccione Puesto- </option>
						<option value="PTO1"> Puesto 1 </option>
						<option value="PTO2"> Puesto 2 </option>
						<option value="PTO3"> Puesto 3 </option>
						<option value="PTO4"> Puesto 4 </option>
						<option value="PTO5"> Puesto 5 </option>
					</select>
				</td>
			</tr>
						
			<!-- <tr> 
				<th width="100"> Escolaridad: </th>
				<td>
				Concluida
				&nbsp;&nbsp;
				Inconclusa
				</td>
			</tr>
			<tr>
				<td id = "non">Nombre de Grado</td>
				<td>
				<table>
					<tr>
						<td width="80">
							<input name="g0" type="radio" value="1" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)">
						</td>
						<td>
							<input name="g0" type="radio" value="0" onClick="valor_seleccionado_rb(e_instructor.g0,'stc0',e_instructor.stc0)">
						</td>
						<td><center><div id="stc0"></div></center> <input name="stc0" type="hidden"></td>
					</tr>
				</table>
				</td>
			</tr> -->			
		</table>
		<br/>	
		
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva BARRERA"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<br/>
	<br/>
	<br/>
	<form name="m_dp" id="myForm" method="post" action="" onSubmit="return validarcampos()" >
		<table align="center" title="Modificar / Eliminar Detalles del Personal">
			<tr>
				<th colspan="6">Detalles del Personal</th>
			</tr>
			<tr>
				<th>Nombre</th>
				<th>Puesto</th>
				<th>Departamento</th>
				<th>Subdirecci&oacute;n</th>
				<th colspan="2">Accion</th>
			</tr>
			<tr>
				<td id="non">Nombre del Personal</td>
				<td id="non">Descripci&oacute;n del Puesto</td>
				<td id="non">Nombre del Departamento</td>
				<td id="non">Nombre de la Subdirecci&oacute;n</td>
				<td id="non">Modificar</td>
				<td id="non">Eliminar</td>
			</tr>
		</table>
	</form>
	
  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
