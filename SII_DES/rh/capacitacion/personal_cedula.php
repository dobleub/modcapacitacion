<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/personal_cedula.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	
	if($_GET["exist"] || $_GET["noexist"]){
		$exist = $_GET["exist"];
		$noexist = $_GET["noexist"];
		$rfc = $_GET["rfc"];
		$puesto = $_GET["idpuesto"];
		$departamento = $_GET["iddepto"];
		$subdireccion = $_GET["idsub"];
		$accion = "update";
	} else {
		$rfc = $_GET["rfc"];
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		<script type="text/javascript">
			//window.onload = function() {
				// code to execute here
				//activartextbox();
				//<?php if($exist) { ?>
					//cargarSubgrupoID('carga_puesto_per.php','txtHint','depto','<?php echo $departamento ?>','puesto','<?php echo $puesto ?>','reload_puestou_per');
					//reload_puestou_per();
				//<?php } ?>
			//}
		</script>
		
		<title>.:: Administraci&oacute;n Personal - Puesto - Departamento - Subdirecci&oacute;n - Escolaridad ::.</title>
		
	</head>
<body>
	
	<h2 align="center">C&Eacute;DULA DE DETECCI&Oacute;N DE NECESIDADES DE CAPACITACI&Oacute;N DEL PERSONAL DIRECTIVO</h2>
<?php
	if($accion == "update"){
		$titulo = '<h3 align="center">ACTUALIZAR DATOS DEL PERSONAL</h3>';
		$form = '<form name="u_capacidad" id="u_capacidad" method="get" action="" onSubmit="return validarcampos_uc()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar la Datos del Personal"/>';
		$funcionreload = 'reload_cedulau_per';
	} else {
		$titulo = '';
		$form = '<form name="i_capacidad" id="i_capacidad" method="get" action="" onSubmit="return validarcampos_ic()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Alta de datos del Personal"/>';
		$funcionreload = 'reload_cedulai_per';
	}

?>

  <!-- Contenido -->
	<?php
		echo '<br/>';
		//echo $titulo;

		echo $form;
	?>
		<h3 align="center"> DATOS PERSONALES </h3>
		<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">
			<tr>
				<th width="150"> Nombre: </th>
				<td width="350">
					<select name="per0" title="Presione aqu&iacute; para seleccionar el Personal" onchange="cargarSubgrupos('carga_info_ced.php','txtHint','rfc',this.value,'<?php echo $funcionreload ?>');'<?php echo $funcionreload ?>()';" >
						<?php
						$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> No hay personal dado de alta </option>";
						else {
							if($rfc){
								while(!$datos->EOF){
									if($rfc == $datos->fields('rfc') )
										echo '<option value="'.$datos->fields('rfc').'" selected>'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
									else 
										echo '<option value="'.$datos->fields('rfc').'">'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
									$datos->MoveNext();
								}
							} else {
								echo "<option value='-1' selected> - Selecciona Personal - </option>";
								while(!$datos->EOF){
								echo '<option value="'.$datos->fields('rfc').'">'.$datos->fields('rfc').' - '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</option>';
								$datos->MoveNext();
								}
							}
						}
						?>
					</select>
				</td>
				
			</tr>
		</table>
		
		<!-- Informaci'on del personal para completar la c'edula de inscripci'on -->
		<div id="txtHint"></div>
		
		
		<br/>
		<br/>
		<div align="center">
			<input type="hidden" name="personal">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php 
				echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<br/>
	<br/>
  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
