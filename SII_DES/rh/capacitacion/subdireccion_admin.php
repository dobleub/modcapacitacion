<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/subdireccion_admin.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	if($_GET["idsub"]){
		$sub_id = $_GET["idsub"];
		$sub_name = $_GET["subdireccion"];
		$accionsub = "update";
	} else {
		$accionsub = "insert";
	}
	if($_GET["iddepto"]){
		$depto_id = $_GET["iddepto"];
		$depto_name = $_GET["departamento"];
		$subid = $_GET["subdireccion"];
		$acciondepto = "update";
	} else {
		$acciondepto = "insert";
	}
	if($_GET["idpuesto"]){
		$puesto_id = $_GET["idpuesto"];
		$puesto_name = $_GET["puesto"];
		$depto = $_GET["departamento"];
		$subdir = $_GET["subdireccion"];
		$accionpuesto = "update";
	} else {
		$accionpuesto = "insert";
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Otras herramientas ::.</title>
	</head>
<body>

<?php
	if($accionsub == "update" || $acciondepto == "update" || $accionpuesto == "update"){
		if($accionsub == "update"){
			$titulosub = '<h3 align="center">ACTUALIZAR SUBDIRECCI&Oacute;N </h3>';
			$formsub = '<form name="u_sub" id="u_sub" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_isub()" >';
			$buttonsub = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar una SUBDIRECCI&Oacute;N"/>';
		} else {
			$titulosub = '<h3 align="center">INSERTAR SUBDIRECCI&Oacute;N </h3>';
			$formsub = '<form name="i_sub" id="i_sub" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_isub()" >';
			$buttonsub = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva SUBDIRECCI&Oacute;N"/>';
		}
		if($acciondepto == "update"){
			$titulodepto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR DEPARTAMENTOS</h3>';
			$formdepto = '<form name="u_dep" ud="i_dep" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_idep()" >';
			$buttondepto = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar un Departamento"/>';
		} else {
			$titulodepto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR DEPARTAMENTOS</h3>';
			$formdepto = '<form name="i_dep" id="i_dep" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_idep()" >';
			$buttondepto = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el nuevo Departamento"/>';
		}
		if($accionpuesto == "update"){
			$titulopuesto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR PUESTO</h3>';
			$formpuesto = '<form name="i_pto" id="i_pto" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_ipto()" >';
			$buttonpuesto = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar un Puesto"/>';
		} else {
			$titulopuesto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR PUESTO</h3>';
			$formpuesto = '<form name="i_pto" id="i_pto" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_ipto()" >';
			$buttonpuesto = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el nuevo Puesto"/>';
		}
	
	} else {
		$titulosub = '<h3 align="center">INSERTAR SUBDIRECCI&Oacute;N </h3>';
		$formsub = '<form name="i_sub" id="i_sub" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_isub()" >';
		$buttonsub = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva SUBDIRECCI&Oacute;N"/>';
		
		$titulodepto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR DEPARTAMENTOS</h3>';
		$formdepto = '<form name="i_dep" id="i_dep" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_idep()" >';
		$buttondepto = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el nuevo Departamento"/>';
		
		$titulopuesto = '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INSERTAR PUESTO</h3>';
		$formpuesto = '<form name="i_pto" id="i_pto" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos_ipto()" >';
		$buttonpuesto = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el nuevo Puesto"/>';
	}

?>


	<h2 align="center">Administraci&oacute;n de PUESTOS que ocupa el Personal</h2>

  <!-- Contenido -->
<?php 
	echo '<br/>';
	echo $titulosub;

	echo $formsub;
?>
		<table align="center" title="Ingresa un nombre descriptivo acerca de una nueva SUBDIRECCI&Oacute;N">
			<tr> 
				<th width="150"> Ingrese un Nombre de SUBDIRECCI&Oacute;N: </th>
				<td id="non">
					<input name="sub0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $sub_name; ?>" title="Ingrese un nombre. Ejemplo: SISTEMAS Y COMPUTACI&Oacute;N">
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="subdireccion" value="<?php echo $sub_id; ?>">
			<input type="hidden" name="accions" value="<?php echo $accionsub; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php 
				echo $buttonsub;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>



  <!-- Administar Departamentos -->
	<?php 
	echo '<br/>	<br/> <br/>';
	echo $titulodepto;
	
	echo $formdepto;
	?>
		<table align="center" title="Ingresa un nombre descriptivo de un DEPARTAMENTO">
			<tr> 
				<th width="150"> Ingrese un Nombre de DEPARTAMENTO: </th>
				<td id="non">
					<input name="dep0" type="text" size="50" maxlength="150" tabindex="0" value="<?php echo $depto_name; ?>" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre. Ejemplo: RECURSOS HUMANOS">
				</td>
				<td id="non">
					<select name="dep1" title="Seleccione Subdirecci&oacute;n a que pertenece el nuevo Departamento">
						<?php
						$consulta="select idsub, descripcion from cp_subdireccion where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Subdirecci&oacute;n faltantes </option>";
						else { 
							if($subid){
								while(!$datos->EOF){
									if($subid == $datos->fields('idsub'))
										echo '<option value="'.$datos->fields('idsub').'" selected>'.$datos->fields('descripcion').'</option>';
									else
										echo '<option value="'.$datos->fields('idsub').'">'.$datos->fields('descripcion').'</option>';
									$datos->MoveNext();
								}
							} else {
								echo "<option value='-1' selected> - Selecciona Subdirecci&oacute;n - </option>";
								while(!$datos->EOF){
									echo '<option value="'.$datos->fields('idsub').'">'.$datos->fields('descripcion').'</option>';
									$datos->MoveNext();
								}
							}
						}
						?>
					</select>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="departamento" value="<?php echo $depto_id; ?>">
			<input type="hidden" name="acciond" value="<?php echo $acciondepto; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php 
				echo $buttondepto;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	
	
	<!-- Administrar Puestos aunque en realidad todos tienen los mismos puestos, pero por si acaso --> 
	<?php 
	echo '<br/>	<br/> <br/>';
	echo $titulopuesto;
	
	echo $formpuesto;
	?>
		<table align="center" title="Ingresa un nombre descriptivo de un PUESTO">
			<tr> 
				<th width="150"> Ingrese un Nombre de PUESTO: </th>
				<td id="non">
					<input name="pto0" type="text" size="50" maxlength="150" tabindex="0" value="<?php echo $puesto_name; ?>" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre. Ejemplo: JEFE, AUXILIAR">
				</td>
				<td id="non">
					<select name="pto1" title="Seleccione Departamento a que pertenece el nuevo Puesto">
						<?php
							$sql="select idsub, descripcion from cp_subdireccion where estado='1' ";
							$datosgral=ejecutar_sql($sql);
							
							if(!$datosgral->rowcount())	echo "<option value='-1' selected> Datos de Departamentos faltantes </option>";
							else {
								echo "<option value='-1' selected> - Selecciona Departamento - </option>";
								while(!$datosgral->EOF){
									echo '<optgroup label="SUB: '.$datosgral->fields('descripcion').'">';
									
										$consulta="select iddepto, descripcion from cp_departamento where estado='1' and idsub='".$datosgral->fields('idsub')."'";
										$datos=ejecutar_sql($consulta);
											if($depto){
												while(!$datos->EOF){
													if($depto == $datos->fields('iddepto'))
														echo '<option value="'.$datos->fields('iddepto').'" selected>'.$datos->fields('descripcion').'</option>';
													else
														echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
													$datos->MoveNext();
												}
											} else {
												while(!$datos->EOF){
													echo '<option value="'.$datos->fields('iddepto').'">'.$datos->fields('descripcion').'</option>';
													$datos->MoveNext();
												}
											}
									echo '</optgroup>';
									$datosgral->MoveNext();
								}
							}
						?>
					</select>
				</td>
				<td id="non">
					<select name="pto2" title="Seleccione Subdireccion">
						<?php
							$sql="select idsub, descripcion from cp_subdireccion where estado='1' ";
							$datosgral=ejecutar_sql($sql);
							
							if(!$datosgral->rowcount())	echo "<option value='-1' selected> Datos de Subdirecci'on faltantes </option>";
							else {
								echo "<option value='-1' selected> - Selecciona Subdirecci&oacute;n - </option>";
								while(!$datosgral->EOF){
										if($subdir){
											if($subdir == $datosgral->fields('idsub'))
												echo '<option value="'.$datosgral->fields('idsub').'" selected>'.$datosgral->fields('descripcion').'</option>';
											else
												echo '<option value="'.$datosgral->fields('idsub').'">'.$datosgral->fields('descripcion').'</option>';
										} else {
											echo '<option value="'.$datosgral->fields('idsub').'">'.$datosgral->fields('descripcion').'</option>';
										}
									$datosgral->MoveNext();
								}
							}
						?>
					</select>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="puesto" value="<?php echo $puesto_id; ?>">
			<input type="hidden" name="accionp" value="<?php echo $accionpuesto; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php 
				echo $buttonpuesto;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp; 
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	
	
	<!-- Administrar Puestos --> 
	<br/>
	<br/>
	<br/>
	<br/>
	<form name="m_sub" id="m_sub" method="get" action="" onSubmit="return validarcampos()" >
		<table align="center" width="500" title="Modificar / Eliminar un SUBDIRCCI&Oacute;N/DEPARTAMENTO/PUESTO">
			
			<?php
			$consulta="select idsub as idg, descripcion as desg from cp_subdireccion where estado='1' order by desg";
				
				$datos=ejecutar_sql($consulta);
				
				if(!$datos->rowcount())	'<tr> <td width="10"></td> <td id="non" colspan="4">Datos de Subdireccion Faltantes</td> </tr>';
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dg" id="m_dg" method="get" action="" onSubmit="return validarcampos()" >';
						echo '<tr> <th colspan="5">Subdirecci&oacute;n: '.$datos->fields('desg').' </th>';
						echo '<th> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></th>';
						echo '<input type="hidden" name="idsub" value="'.$datos->fields('idg').'">';
						echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('desg').'">';
					echo '</form>';
					echo '<form name="e_dg" id="e_dg" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos()" >';
						echo '<th> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></th>';	
						echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('idg').'">';
						echo '<input type="hidden" name="accions" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form> <th></th> </tr>';
					
					$consultaps="select idpuesto, descripcion, iddepto from cp_puesto where estado='1' and idsub ='".$datos->fields('idg')."' ";
					$datosps=ejecutar_sql($consultaps);
					if(!$datosps->rowcount())	echo '<tr> <td width="10"></td> <td id="non" colspan="4">Datos de Puestos de Subdireccion Faltantes</td> </tr>';
						else { 
							while(!$datosps->EOF){
								echo '<form name="m_dsg" id="m_dsg" method="get" action="" onSubmit="return validarcampos()" >';
									echo '<td id="non" colspan="5"> Puesto de subdirecci&oacute;n: <strong>'.$datosps->fields('descripcion').'</strong> </td>';
									echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
									echo '<input type="hidden" name="idpuesto" value="'.$datosps->fields('idpuesto').'">';
									echo '<input type="hidden" name="puesto" value="'.$datosps->fields('descripcion').'">';
									echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('idg').'">';
								echo '</form>';
								echo '<form name="e_dsg" id="e_dsg" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos()" >';
									echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';	
									echo '<input type="hidden" name="puesto" value="'.$datosps->fields('idpuesto').'">';
									echo '<input type="hidden" name="accionp" value="delete">';
									echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
								echo '</form>';
								echo '</tr>';
								$datosps->MoveNext();
							}
						}
						$consultasub = "select iddepto, descripcion, idsub from cp_departamento where estado='1' and idsub ='".$datos->fields('idg')."' ";
						$datossub=ejecutar_sql($consultasub);
						if(!$datossub->rowcount())	echo '<tr> <td width="10"></td> <td id="non" colspan="4">Datos de Departamentos Faltantes</td> </tr>';
						else { 
							while(!$datossub->EOF){
								echo '<form name="m_dsg" id="m_dsg" method="get" action="" onSubmit="return validarcampos()" >';
									echo '<tr> <td width="5"></td>';
									echo '<td id="non" colspan="4"> Departamento de <strong>'.$datossub->fields('descripcion').'</strong> </td>';
									echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
									echo '<input type="hidden" name="iddepto" value="'.$datossub->fields('iddepto').'">';
									echo '<input type="hidden" name="departamento" value="'.$datossub->fields('descripcion').'">';
									echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('idg').'">';
								echo '</form>';
								echo '<form name="e_dsg" id="e_dsg" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos()" >';
									echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';	
									echo '<input type="hidden" name="departamento" value="'.$datossub->fields('iddepto').'">';
									echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('idg').'">';
									echo '<input type="hidden" name="acciond" value="delete">';
									echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
								echo '</form>';
								echo '</tr>';
								
										$consultapst = "select idpuesto, descripcion, iddepto from cp_puesto where estado='1' and iddepto ='".$datossub->fields('iddepto')."' ";
										$datospst=ejecutar_sql($consultapst);
										if(!$datospst->rowcount())	echo '<tr> <td width="10"></td> <td colspan="3">Datos de Puestos Faltantes</td> </tr>';
										else { 
											while(!$datospst->EOF){
												echo '<form name="m_dsg" id="m_dsg" method="get" action="" onSubmit="return validarcampos()" >';
													echo '<tr> <td width="5"></td>';
													echo '<td colspan="4"> &nbsp;&nbsp; Puesto: '.$datospst->fields('descripcion').' </td>';
													echo '<td> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
													echo '<input type="hidden" name="idpuesto" value="'.$datospst->fields('idpuesto').'">';
													echo '<input type="hidden" name="puesto" value="'.$datospst->fields('descripcion').'">';
													echo '<input type="hidden" name="departamento" value="'.$datossub->fields('iddepto').'">';
												echo '</form>';
												echo '<form name="e_dsg" id="e_dsg" method="get" action="ime_subdireccion.php" onSubmit="return validarcampos()" >';
													echo '<td> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';	
													echo '<input type="hidden" name="puesto" value="'.$datospst->fields('idpuesto').'">';
													echo '<input type="hidden" name="subdireccion" value="'.$datos->fields('idg').'">';
													echo '<input type="hidden" name="departamento" value="'.$datossub->fields('iddepto').'">';
													echo '<input type="hidden" name="accionp" value="delete">';
													echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
												echo '</form>';
												echo '</tr>';
											$datospst->MoveNext();
											}
										}
							$datossub->MoveNext();
							}
						}
					$datos->MoveNext();
					}
				}
			?>
		</table>
	
	</form>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
