<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";

/*
 * carga_subgrupos_cap.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
 
if ($_GET["rfc"]){
	$rfc = $_GET["rfc"];
	
	//$consulta="select *	from personal where status_empleado = '02' and rfc = '".$rfc."' order by apellidos_empleado, nombre_empleado, rfc";
	//$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.telefono_empleado, p.sexo_empleado, p.correo_electronico, p.estudios
				//from personal as p
				//where p.status_empleado = '02' and p.rfc = '$rfc' order by p.apellidos_empleado, p.nombre_empleado, p.rfc";
	$funcionreload = 'reload_puestoi_per';
} else {
	$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
}

	$accion=$_GET["action"];

	$datos=ejecutar_sql($consulta);
	//echo array_count_values($datos);
	//var_dump($datos->fields);
	//echo $consulta;
	
	
	if(!$datos->rowcount())	echo '<tr> <th>Datos faltantes</th> </tr>';
		else {			
				while(!$datos->EOF){
					
					echo '<tr> <td>';
					echo '<select name="cap0" title="Seleccione una Capacidad de Competencias Generales">';
						$consulta2="select idgrupo as idg, descripcion as desg from cp_grupos_cap where estado='1' order by desg";
						$datos2=ejecutar_sql($consulta2);
						
						if(!$datos2->rowcount())	echo " None ";
						else {
							echo "<option value='-1' selected> - Selecciona una Capacidad - </option>";
							while(!$datos2->EOF){
								echo '<optgroup label="'.utf8_encode($datos2->fields('desg')).'">';
													
									$consulta3="select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos2->fields('idg')."' as integer)";
									$datos3=ejecutar_sql($consulta3);
									if(!$datos3->rowcount())	echo " None ";
									else {
										while(!$datos3->EOF){
											echo '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos3->fields('descripcion')).'">';
																
													$consulta4="select idcapacidad, descripcion from cp_capacidades where estado='1' and idsubgrupo = cast('".$datos3->fields('idsubgrupo')."' as integer)";
													$datos4=ejecutar_sql($consulta4);
													if(!$datos4->rowcount())	echo " None ";
													else {
														while(!$datos4->EOF){
															echo '<option value="'.$datos4->fields('idcapacidad').'">&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos4->fields('descripcion')).' - '.$datos4->fields('idcapacidad').'</option>';
															$datos4->MoveNext();
														}
													}
											
											echo '</optgroup>';
											$datos3->MoveNext();
										}
									}
								
								echo '</optgroup>';
								$datos2->MoveNext();
							}
						}
					echo '</select>';
					echo '</td> </tr>';
				
				$datos->MoveNext();
				}
		}
	
?>
