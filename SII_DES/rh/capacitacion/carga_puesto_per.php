<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";

/*
 * carga_subgrupos_cap.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
 
if (($_GET["depto"]) || ($_GET["depto"] && $_GET["puesto"])){
	$departamento = $_GET["depto"];
	$puesto = $_GET["puesto"];
	$consulta="select idpuesto, descripcion, iddepto from cp_puesto where estado='1' and iddepto = '".$departamento."'";
} else if (($_GET["subid"]) || ($_GET["subid"] && $_GET["puesto"])){
	$subdireccion = $_GET["subid"];
	$puesto = $_GET["puesto"];
	$consulta="select idpuesto, descripcion, iddepto from cp_puesto where estado='1' and idsub = '".$subdireccion."'";
} else {
	$consulta="select idpuesto, descripcion, iddepto from cp_puesto where estado='1'";
}

	$accion=$_GET["action"];

	$datos=ejecutar_sql($consulta);
	echo "<select id='pst1' name='pst1' title='Seleccione Departamento' onchange='".$accion."();'>";
	if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Departamento faltantes </option>";
		else {
			if ($puesto){
				while(!$datos->EOF){
					if ($puesto == $datos->fields('idpuesto'))
						echo '<option value="'.$datos->fields('idpuesto').'" selected>'.utf8_encode($datos->fields('descripcion')).'</option>';
					else
						echo '<option value="'.$datos->fields('idpuesto').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
					$datos->MoveNext();
				}
			} else {
				echo "<option value='-1' selected> - Selecciona Puesto - </option>";
				while(!$datos->EOF){
					echo '<option value="'.$datos->fields('idpuesto').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
					$datos->MoveNext();
				}
			}
		}
	echo "</select>";

?>
