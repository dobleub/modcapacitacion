<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresarInicio = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/capacidades_admin.php'";
	
	if($_GET['capacidad']){
		$and1 = 'and c.idcapacidad = "'.$_GET['capacidad'].'"';
		$and2 = 'and c.idotracap = "'.$_GET['capacidad'].'"';
		$capacidad = $_GET['capacidad'];
		$grupo = $_GET['grupo'];
		$subgrupo = $_GET['subgrupo'];
		$otracap = $_GET['otro'];
		$accion = "update";
	} else {
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		<script type="text/javascript" src="js/jquery-1.10.0.min.js"></script>	
		
		<script type="text/javascript">
			window.onload = function() {
				// code to execute here
				activartextbox();
				<?php if($grupo!="" && $subgrupo!="") { ?>
					cargarSubgrupoID('carga_subgrupos_cap.php','txtHint','gcap',<?php echo $grupo ?>,'sgcap',<?php echo $subgrupo ?>,'reload_grupob_cap');
					reload_grupob_cap();
				<?php } ?>
			}
		</script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
	
<body>

	<h2 align="center">Administraci&oacute;n de Capacidades/Cursos</h2>

<?php
	if ($accion == "update"){
		if($otracap)
			$consulta="select c.idotracap,c.descripcion,s.idsubgrupo,s.descripcion as subgrupo,g.idgrupo,g.descripcion as grupo from cp_otras_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo $and2 ";
		else
			$consulta="select c.idcapacidad,c.descripcion,s.idsubgrupo,s.descripcion as subgrupo,g.idgrupo,g.descripcion as grupo from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo $and1 ";
		$titulo = '<h3 align="center">ACTUALIZAR UNA CAPACIDAD</h3>';
		$form = '<form name="u_capacidad" id="u_capacidad" method="get" action="ime_capacidades.php" onSubmit="return validarcampos_uc()" >';
		$datosgral = ejecutar_sql($consulta);
		while(!$datosgral->EOF){
			$cap_name = $datosgral->fields('descripcion');
			$cap_idsg = $datosgral->fields('idsubgrupo');
			$cap_idg = $datosgral->fields('idgrupo');
		$datosgral->MoveNext();
		}
		
		$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()" disabled>Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar la Capacidad/curso"/>';
		$funcionreload = 'reload_grupob_cap';
		
	} else{
		$titulo = '<h3 align="center">INGRESAR NUEVA CAPACIDAD</h3>';
		$form = '<form name="i_capacidad" id="i_capacidad" method="get" action="ime_capacidades.php" onSubmit="return validarcampos_ic()">';
		$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()">Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la Capacidad/curso"/>';
		$funcionreload = 'reload_grupo_cap';
	}
?>
	<!-- Contenido -->  
<?php
	echo '<br/>';
	echo $titulo;

	echo $form;
?>
		<table align="center" title="Ingresa un nombre descriptivo acerca de la Nueva Capacidad/curso posible a impartir">
			<tr>
				<th width="120"> Ingresar el ID de la Capacidad </th>
				<td id="non" colspan="2">
					<?php
					echo $checkbox;
					echo '&nbsp;&nbsp;';
					echo '<input type="text" id="cap0" name="cap0" size="20" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$capacidad.'" title="Ingrese un ID de Capacitaci&oacute;n/curso. Ejemplo: SCADM02">';
					?>
				</td>
			</tr>
			<tr>
				<th width="120"> Ingrese un nombre de CAPACIDAD/curso: </th>
				<td id="non">

					<input name="cap1" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $cap_name ?>" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">

				</td>
				<td id="non" width="100">
					<select name="gcap" id="gcap" title="Seleccione Grupo a que pertenecer&aacute; la capacidad" onchange="cargarSubgrupos('carga_subgrupos_cap.php','txtHint','gcap',this.value,'<?php echo $funcionreload ?>');'<?php echo $funcionreload ?>()';" >
					<?php
					$consultagrupos="select idgrupo, descripcion from cp_grupos_cap where estado='1'";
					$datosgrupos=ejecutar_sql($consultagrupos);
					
						if(!$datosgrupos->rowcount()) echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else {
							if ($cap_idg){
								while(!$datosgrupos->EOF){
									if ($cap_idg == $datosgrupos->fields('idgrupo'))
										echo '<option value="'.$datosgrupos->fields('idgrupo').'" selected>'.$datosgrupos->fields('descripcion').'</option>';
									else
										echo '<option value="'.$datosgrupos->fields('idgrupo').'">'.$datosgrupos->fields('descripcion').'</option>';
									$datosgrupos->MoveNext();
								}
							} else {
								echo "<option value='-1' selected> - Selecciona Grupo - </option>";
								while(!$datosgrupos->EOF){
									echo '<option value="'.$datosgrupos->fields('idgrupo').'">'.$datosgrupos->fields('descripcion').'</option>';
									$datosgrupos->MoveNext();
								}
							}
						}
					?>
					</select>
					
					<!-- select de Seleccion de Subgrupo -->
					<div id="txtHint"></div>

				</td>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="idcap" value="<?php echo $capacidad ?>">
			<input type="hidden" name="idgc" value="<?php echo $grupo ?>">
			<input type="hidden" name="idsgc" value="<?php echo $subgrupo ?>">
			<input type="hidden" name="accion" value="<?php echo $accion ?>">
			<input type="hidden" name="otro" value="<?php echo $otracap ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar ?>">
			
			<?php
			echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>


	<!-- Modificar Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR CAPACIDADES EXISTENTES</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
		<tr>
			<th width="120"> Buscar CAPACIDAD/curso: </th>
			<td id="non">
				<form name="m_capacidad" id="m_capacidad" method="get" action="" onSubmit="return validarcampos_m()">
					<input name="capm0" type="text" size="40" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">
					<input type="submit" class="boton" name="submit" value="Buscar" tabindex="1" title="Buscar curso"/>
			</td>
			<td id="non">
				<select name="g_cap" id="g_cap" title="Filtrar b&uacute;squeda por Grupos">
					<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
								$datos->MoveNext();
							}
						}
						?>
				</select>
			</td>
			<td id="non">
				<!-- select de Seleccion de Subgrupo -->
				<select id='sg_cap' name='sg_cap' title='Filtrar b&uacute;squeda por Subgrupos'>
					<?php
						$consulta="select idgrupo,descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Subgrupo - </option>";
							while(!$datos->EOF){
								echo '<optgroup label="'.$datos->fields('descripcion').'">';
									$consultacap="select idsubgrupo,descripcion,idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos->fields('idgrupo')."' as integer)";
									$datoscap=ejecutar_sql($consultacap);
									while(!$datoscap->EOF){
										echo '<option value="'.$datoscap->fields('idsubgrupo').'">'.$datoscap->fields('descripcion').'</option>';
										$datoscap->MoveNext();
									}
								echo '</optgroup>';
								$datos->MoveNext();
							}							
						}
					?>
				</select>
			</td>
			<td id="non" colspan="2"></td>
			<input type="hidden" name="idgcap">
			<input type="hidden" name="idsgcap">
		</tr>
			</form>
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
		<tr>
			<th>ID</th>
			<th>Capacidad</th>
			<th>Grupo</th>
			<th>Subgrupo</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
			<?php
		
	if($_GET['g_cap'] and $_GET['g_cap'] != '-1'){
		$idgrupo = "and g.idgrupo = cast('".$_GET['g_cap']."' as integer)";
	}
	if ($_GET['sg_cap'] and $_GET['sg_cap'] != '-1'){
		$idsubgrupo = "and s.idsubgrupo = cast('".$_GET['sg_cap']."' as integer)";
	}
	if ($_GET['capm0']){
		$idcapacidad = "and c.descripcion like '%".$_GET['capm0']."%'";
	}
		
				$consulta="select c.idcapacidad as idc, c.descripcion as des, g.idgrupo as idgrup, g.descripcion as desg, s.idsubgrupo as idsub, s.descripcion as dess from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Capacidades Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('idc').'</td>';
						echo '<td id="non">'.$datos->fields('des').' </td>';
						echo '<td id="non"> '.$datos->fields('desg').' </td>';
						echo '<td id="non"> '.$datos->fields('dess').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('des').'"/> </center></td>';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="grupo" value="'.$datos->fields('idgrup').'">';
						echo '<input type="hidden" name="subgrupo" value="'.$datos->fields('idsub').'">';
					echo '</form>';
					echo '<form name="e_cp" id="e_cp" method="get" action="ime_capacidades.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar: '.$datos->fields('des').'" onclick="delete_confirm(e_cp)"/> </center></td>';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
				
				
				$consulta="select c.idotracap as idc, c.descripcion as des, g.idgrupo as idgrup, g.descripcion as desg, s.idsubgrupo as idsub, s.descripcion as dess from cp_otras_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('idc').'</td>';
						echo '<td id="non">'.$datos->fields('des').' </td>';
						echo '<td id="non"> '.$datos->fields('desg').' </td>';
						echo '<td id="non"> '.$datos->fields('dess').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('des').'"/> </center></td>';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="grupo" value="'.$datos->fields('idgrup').'">';
						echo '<input type="hidden" name="subgrupo" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="otro" value="id">';
					echo '</form>';
					echo '<form name="e_cp" id="e_cp" method="get" action="ime_capacidades.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar: '.$datos->fields('des').'" onclick="delete_confirm(e_cp)"/> </center></td>';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="otro" value="id">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarInicio; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
