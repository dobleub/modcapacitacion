<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	require('fpdf/fpdf.php');
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_instructor_emp.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
//$consulta="alter table cp_capacidad_autorizada add fecha datetime null";


$consulta2 = "create procedure i_capacidad_autorizada @clave varchar(25), @idcap varchar(10) = NULL, @idotra varchar(8) = NULL, @idins varchar(10), @idh varchar(10), @fa datetime = NULL, @lim integer = 50, @itt varchar(20) = '20DIT0004L',
	@echo char(1) output
	as
	declare @faux datetime
	if @fa = NULL begin
		set @faux = (select getdate())
	end else begin
		set @faux = @fa
	end
	
	if (select count(*) from cp_capacidad_autorizada where clavecurso = upper(@clave)) = 0
    begin
    		
		insert into cp_capacidad_autorizada(clavecurso,idcapacidad,idotracap,idinstructor,idhorario,limite_personal,idinstituto,fecha)
			values(@clave,@idcap,@idotra,@idins,@idh,@lim,@itt,@faux)
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_capacidad_autorizada where clavecurso = upper(@clave)) = '0'
		begin
			update cp_capacidad_autorizada set estado = '1' where clavecurso = upper(@clave)
			print 'Capacidad Autorizada dada de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Capacidad Autorizada dada de alta previamente'
			select @echo = cast(0 as char)
		end
    end
";

$consulta2 = "create procedure i_inscripcion_p @clave varchar(25), @rfc char(13),
	@echo char(1) output
	as
	declare @faux datetime
		set @faux = (select getdate())
	
	if (select count(*) from cp_cap_autorizada_personal where clavecurso = upper(@clave) and rfc = upper(@rfc) ) = 0
    begin
    		
		insert into cp_cap_autorizada_personal(clavecurso,rfc,fecha)
			values(upper(@clave),upper(@rfc),@faux)
		print 'Personal inscrito CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_capacidad_autorizada where clavecurso = upper(@clave)) = '0'
		begin
			update cp_cap_autorizada_personal set estado = '1' where clavecurso = upper(@clave)
			print 'Personal inscrito CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Personal inscrito Previamente'
			select @echo = cast(0 as char)
		end
    end
";

$consulta3 = "create procedure i_puesto @des varchar(100), @iddepto varchar(10),
	@echo char(1) output
	as
    declare @idp varchar(10), @idpt varchar(10)
    set @idpt = (select right(@iddepto, 3))
    
        exec cp_crear_clave_10 @des, @idpt, @idp output

	if (select count(*) from cp_puesto where descripcion = upper(@des) and iddepto = upper(@iddepto)) = 0
    begin
		insert into cp_puesto(idpuesto,descripcion,iddepto) values(upper(@idp),upper(@des),upper(@iddepto))
        print 'Puesto dado de alta CORRECTAMENTE'
		select @echo = cast(1 as char)
    end
	else
    begin
		if (select estado from cp_puesto where descripcion = upper(@des) and iddepto = upper(@iddepto)) = '0'
		begin
			update cp_puesto set estado = '1' where descripcion = upper(@des) and iddepto = upper(@iddepto)
            print 'Puesto dado de alta CORRECTAMENTE'
			select @echo = cast(1 as char)
		end
		else
		begin
			print 'Puesto dado de alta previamente'
			select @echo = cast(0 as char)
		end
    end
";

//$consulta = "alter table cp_puesto add idsub varchar(10) null references cp_subdireccion(idsub)";

//$datos=sybase_query($consulta);
	
//$mensaje = sybase_get_last_message();
//echo $mensaje;


?>
