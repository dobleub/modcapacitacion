<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
	<script src="js/mootools-core.js" type="text/javascript"></script>
	<script src="js/mootools-more.js" type="text/javascript"></script>
	<script src="Source/Locale.es-ES-DatePicker.js" type="text/javascript"></script>
	<script src="Source/Picker.js" type="text/javascript"></script>
	<script src="Source/Picker.Attach.js" type="text/javascript"></script>
	<script src="Source/Picker.Date.js" type="text/javascript"></script>
	<link href="Source/datepicker.css" rel="stylesheet">
		
		<title>.:: Seleccionar Horarios ::.</title>
	</head>
<body>
<?php ?>
	<h2 align="center">Administraci&oacute;n de HORARIOS</h2>

  <!-- Contenido -->
	<br/>
	<!--
	<h3 align="center">INGRESAR INSTRUCTOR</h3>
	-->
	
	<form name="e_instructor" id="myForm" method="post" action="" onSubmit="return validarcampos_ei()" >
		<table align="center" title="Por favor rellene los siguientes datos para evaluar un Instructor disponible">
			<tr>
				<th width="150"> Instituto Tecnol&oacute;gico o Centro: </th>
				<td width="350">
					<select name="evali0" title="Seleccione el Tecnol&oacute;gico o Centro">
						<option value="ITT" selected> INSTITUTO TECNOL&Oacute;GICO DE TLAXIACO </option>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Nombre del Instructor: </th>
				<td>
					<select name="evali1" title="Seleccione el Instructor">
						<option value="-1" selected> -Selccione Instructor- </option>
						<option value="Inst1"> Instructor 1 </option>
						<option value="Inst2"> Instructor 2 </option>
						<option value="Inst3"> Instructor 3 </option>
						<option value="Inst4"> Instructor 4 </option>
						<option value="Inst5"> Instructor 5 </option>
					</select>
				</td>
			</tr>
			<tr>
				<th width="100"> Fecha de Evaluaci&oacute;n: </th>
				<td>
					<input name="evali2" type="text" class="open" style="display: block;">
					<style type="text/css"></style>
					<script type="text/javascript">
					window.addEvent('domready', function(){
						myPicker = new Picker.Date($$('a.open, input'), {
							toggle: $$('.toggle'),
							positionOffset: {x: -10, y: 10}
						});
						$('open').addEvent('click', function(e){
							e.stop();
							myPicker.open();
						});
					});
					</script>
				</td>
			</tr>
			<tr> 
				<th width="100"> Nombre del curso: </th>
				<td>
					<select name="evali3" title="Seleccione el Nombre del Curso">
						<option value="-1" selected> -Seleccione curso- </option>
						<option value="C1">Curso A</option>
						<option value="C2">Curso B</option>
						<option value="C3">Curso C</option>
					</select>
				</td>
			</tr>
			<tr>
				<th> Nombre de la Empresa o Plantel: </th>
				<td>
					<select name="evali4" title="Seleccione la empresa de donde proviene el Instructor">
						<option value="-1" selected> -Seleccione una empresa- </option>
						<option value="1">Empresa 1</option>
						<option value="2">Empresa 2</option>
						<option value="3">Empresa 3</option>
					</select>
				</td>
			</tr>
		</table>
		<br/>
		<table align="center" width="400" title="Criterios de Evaluaci&ocute;n">
			<tr>
				<th>Criterio</th>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>Total</th>
			</tr>
			<tr>
				<td>Nombre del Criterio a evaluar</td>
				<td><input name="group1" type="radio" value="1"></td>
				<td><input name="group1" type="radio" value="2"></td>
				<td><input name="group1" type="radio" value="3"></td>
				<td><input name="group1" type="radio" value="4"></td>
				<td><input name="group1" type="radio" value="5"></td>
			</tr>
			<tr>
				<td></td>
				<th colspan="5">Total de Puntaje</th>
				<th>0</th>
			</tr>
		</table>
		
	<br />
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva BARRERA"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">NOTA: Evaluar considerando la siguiente escala.</h3>
	<table align="center">
		<tr>
			<td>&nbsp;<strong>1</strong>-Malo</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>2</strong>-Regular</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>3</strong>-Bien</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>4</strong>-Muy bien</td>
			<td>&nbsp;&nbsp;&nbsp;<strong>5</strong>-Excelente &nbsp;</td>
		</tr>
	</table>
	
  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
