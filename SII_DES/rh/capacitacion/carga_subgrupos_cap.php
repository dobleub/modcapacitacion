<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";

/*
 * carga_subgrupos_cap.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
 
if (($_GET["gcap"]) || ($_GET["gcap"] && $_GET["sgcap"])){
	$idgrupo=$_GET["gcap"];
	$idsubgrupo=$_GET["sgcap"];
	$consulta="select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$idgrupo."' as integer)";
} else {
	$consulta="select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1'";
}

$accion=$_GET["action"];

	$datos=ejecutar_sql($consulta);
	echo "<select id='subcap' name='subcap' title='Seleccione Subgrupo a que pertenecer&aacute; la capacidad' onchange='".$accion."();'>";
	if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Subgrupos faltantes </option>";
		else {
			if ($idsubgrupo){
				while(!$datos->EOF){
					if ($idsubgrupo == $datos->fields('idsubgrupo'))
						echo '<option value="'.$datos->fields('idsubgrupo').'" selected>'.utf8_encode($datos->fields('descripcion')).'</option>';
					else
						echo '<option value="'.$datos->fields('idsubgrupo').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
					$datos->MoveNext();
				}
			} else {
				echo "<option value='-1' selected> - Selecciona Subgrupo - </option>";
				while(!$datos->EOF){
					echo '<option value="'.$datos->fields('idsubgrupo').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
					$datos->MoveNext();
				}
			}
		}
	echo "</select>";

?>
