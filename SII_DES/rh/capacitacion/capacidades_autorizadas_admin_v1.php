<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
<body>
<?php $accion = "ic" ?>
	<h2 align="center">Administraci&oacute;n de Capacidades/Cursos Autorizados </h2>

  <!-- Contenido -->  
	<br/>
	<h3 align="center">INGRESAR NUEVA CAPACIDAD</h3>
	
	<form name="i_capacidad" id="myForm" method="post" action="" onSubmit="return validarcampos_ic()" >
		<table align="center" title="Ingresa un nombre descriptivo acerca de la Nueva Capacidad/curso posible a impartir">
			<tr> 
				<th width="120"> Ingrese un nombre de CAPACIDAD/curso: </th>
				<td id="non">
					<input name="cap0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">
				</td>
				<td id="non">
					<select name="gcap" title="Seleccione Grupo a que pertenecer&aacute; la capacidad">
						<option value="-1" selected> -Seleccione Grupo- </option>
						<option value="1">Grupo 1</option>
						<option value="2">Grupo 2</option>
						<option value="3">Grupo 3</option>
					</select>
					<select name="subcap" title="Seleccione Subgrupo a que pertenecer&aacute la capacidad;">
						<option value="-1" selected> -Seleccione Subgrupo- </option>
						<option value="1">Subgrupo 1</option>
						<option value="2">Subgrupo 2</option>
						<option value="3">Subgrupo 3</option>
					</select>
				</td>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la Capacidad/curso"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>

	<!-- Modificar Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR CAPACIDADES EXISTENTES</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
		<tr>
			<th width="120"> Buscar CAPACIDAD/curso: </th>
			<td id="non">
				<form name="m_capacidad" id="myForm" method="post" action="" onSubmit="return validarcampos_m()" >
					<input name="capm0" type="text" size="40" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">
					<input type="submit" class="boton" name="Submit" value="Buscar" tabindex="1" title="Buscar curso"/>
				</form>
			</td>
			<td id="non">
				<select name="g_cap" title="Filtrar b&uacute;squeda por Grupo">
					<option value=-1 selected> - Filtro por Grupo - </option>
					<option>Grupo 1</option>
					<option>Grupo 2</option>
					<option>Grupo 3</option>
				</select>
			</td>
			<td id="non">
				<select name="sg_cap" title="Filtrar b&uacute;squeda por Subgrupo">
					<option value=-1 selected> - Filtro por Subgrupo - </option>
					<option>Subgrupo 1</option>
					<option>Subgrupo 2</option>
					<option>Subgrupo 3</option>
				</select>
			</td>
			<td id="non" colspan="2"></td>
		</tr>
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
		<tr>
			<th>ID</th>
			<th>Capacidad</th>
			<th>Grupo</th>
			<th>Subgrupo</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
		<tr>
			<td id="non"></td>
			<td id="non"></td>
			<td id="non"></td>
			<td id="non"></td>
			<td id="non"> Modificar </td>
			<td id="non"> Eliminar </td>
		</tr>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
