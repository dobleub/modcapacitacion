<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/capacidades_autorizadas_admin.php'";
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/capacidades_autorizadas_admin.php'";
	$regresarInicio = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	

	if($_GET['idcapacidad']){
		$_idcapacidad = $_GET["idcapacidad"];
		$_capacidad = $_GET["capacidad"];
		$_idgrupo = $_GET["idgrupo"];
		$_grupo = $_GET["grupo"];
		$_idsubgrupo = $_GET["idsubgrupo"];
		$_subgrupo = $_GET["subgrupo"];
		$otro = $_GET["otro"];
		
		$accion = "update";
	} else {
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		
		<link rel="stylesheet" type="text/css" href="css/page.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/calendar-eightysix-v1.1-default.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/calendar-eightysix-v1.1-vista.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/calendar-eightysix-v1.1-osx-dashboard.css" media="screen" />
		<script type="text/javascript" src="js/mootools-1.2.4-core.js"></script>
		<script type="text/javascript" src="js/mootools-1.2.4.4-more.js"></script>
		<script type="text/javascript" src="js/calendar-eightysix-v1.1.js"></script>
		
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<script type="text/javascript">
			window.addEvent('domready', function() {
				// Calendario para la fecha de Registro de la Capacidad
				new CalendarEightysix('registro', { 'toggler': 'exampleIII-picker', 'offsetY': -4 });
				
				// Calendario para fecha de Inicio y Fin de la Capacitacion
				var calendarXIa = new CalendarEightysix('fechainicio', { 'disallowUserInput': true, 'minDate': 'today',  'alignX': 'left', 'alignY': 'bottom', 'offsetX': -4 });
				var calendarXIb = new CalendarEightysix('fechafin', { 'disallowUserInput': true, 'minDate': 'tomorrow', 'alignX': 'left', 'alignY': 'bottom', 'offsetX': -4 });
				calendarXIa.addEvent('change', function(date) {
					date = date.clone().increment(); //At least one day higher; so increment with one day
					calendarXIb.options.minDate = date; //Set the minimal date
					if(calendarXIb.getDate().diff(date) >= 1) calendarXIb.setDate(date); //If the current date is lower change it
					else calendarXIb.render(); //Always re-render
				});
				
				MooTools.lang.set('es-MX', 'Date', {
				months:    ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				days:      ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
				dateOrder: ['date', 'month', 'year', '/']
				});
				
				MooTools.lang.setLanguage('es-MX');
			});	
		</script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
	
<body>

	<h2 align="center">Administraci&oacute;n de Capacidades/Cursos Autorizados</h2>

<?php
	if ($accion == "update"){			
		$titulo = '<h3 align="center">AUTORIZAR UNA CAPACIDAD</h3>';
		$form = '<form name="u_capacidad" id="u_capacidad" method="get" action="ime_capacidades_auth.php" onSubmit="return validarcampos_uc()" >';
		
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()" disabled>Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Autorizar la Capacidad/curso"/>';
		$funcionreload = 'reload_grupob_cap';
		
	} else{
		$titulo = '<h3 align="center">AUTORIZAR UNA CAPACIDAD</h3>';
		$form = '<form name="i_capacidad" id="i_capacidad" method="get" action="ime_capacidades_auth.php" onSubmit="return validarcampos_ic()">';
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()">Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Autorizar la Capacidad/curso"/>';
		$funcionreload = 'reload_grupo_cap';
	}
?>
	<!-- Contenido -->  
<?php
	echo '<br/>';
	echo $titulo;

	echo $form;
?>
		<table align="center" title="Datos de la Nueva Capacidad/curso ya Autorizada, por favor seleccione una capacidad de la lista anexa en la parte de abajo">
			<tr>
				<th width="200"> IT O CENTRO: </th>
				<td>
					<select name="insti0" title="Seleccione el Tecnol&oacute;gico o Centro">
						<?php
						$consulta="select idinstituto,descripcion from cp_institucion";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Instituci&oacute;n faltantes </option>";
						else { 
							//echo "<option value='-1' selected> - Selecciona Instituci&oacute;n - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idinstituto').'">'.$datos->fields('descripcion').' </option>';
								$datos->MoveNext();
							}							
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th> CLAVE DE CAPACIDAD/CURSO AUTORIZADO: </th>
				<td id="non">

					<input name="cap0" type="text" size="25" maxlength="30" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese una CLAVE de Capacitaci&oacute;n/curso. Ejemplo: CP-A-01-119-119-12">

				</td>
			</tr>
			<tr>
				<th> ID de la Capacidad: </th>
				<td id="non" colspan="2">
					<?php
					echo '<input type="text" id="cap1" name="cap0" size="20" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$_idcapacidad.'" title="Muestra el ID de la Capacitaci&oacute;n/curso actual. Ejemplo: SCADM02" disabled>';
					?>
				</td>
			</tr>
			<tr>
				<th> Nombre de CAPACIDAD/curso: </th>
				<td id="non">

					<input name="cap2" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $_capacidad ?>" title="Muestra el nombre de Capacitaci&oacute;n/curso actual. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS" disabled>

				</td>
			</tr>
			<tr>
				<th> Nombre de GRUPO a que pertenece la capacidad/curso: </th>
				<td id="non">

					<input name="cap3" type="text" size="20" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $_grupo ?>" title="Muestra el Grupo a que pertenece la de Capacitaci&oacute;n/curso actual. Ejemplo: ADMINISTRACI&Oacute;N" disabled>

				</td>
			</tr>
			<tr>
				<th> Nombre de SUBGRUPO a que pertenece la capacidad/curso: </th>
				<td id="non">

					<input name="cap4" type="text" size="60" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $_subgrupo ?>" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: CAPACIDAD T&Eacute;CNICA: ADMINISTRACI&Oacute;N DE BASES DE DATOS" disabled>

				</td>
			</tr>
			<tr>
				<th> Nombre del Instructor: </th>
				<td>
					<select name="instr0" title="Seleccione el Instructor">
						<?php
						$consulta="select distinct(i.idinstructor),i.titulo,i.nombre from cp_instructor_cap as i, cp_evaluacion_ins as ei where i.estado='1' and i.idinstructor = ei.idinstructor and ei.aceptado = '1' ";
						//$consulta="select idinstructor,titulo,nombre from cp_instructor_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Intructores faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Instructor - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idinstructor').'">'.$datos->fields('titulo').' '.$datos->fields('nombre').'</option>';
								$datos->MoveNext();
							}							
						}
						echo sybase_get_last_message();				
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th> FECHA de registro de la capacidad/curso: </th>
				<td id="non">
					<input id="registro" name="cap5" type="text" maxlength="10" style="padding-right: 22px;" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013" />
					<div class="picker inElement" id="exampleIII-picker"></div>
				</td>
			</tr>
			<tr>
				<th> Total de horas: </th>
				<td id="non">
					<input id="totalh" name="cap8" type="text" maxlength="10" style="padding-right: 22px;" title="Anote total de horas que tomara completar el Curso, si se omite, se tomar&aacute; como 30 hrs el valor por defecto" />
				</td>
			</tr>
			<tr>
				<th> Cupo l&iacute;mite: </th>
				<td id="non">
					<input id="totalp" name="cap9" type="text" maxlength="10" style="padding-right: 22px;" title="Anote el numero de Personas " />
				</td>
			</tr>
			<tr>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<th> fecha de INICIO: </th>
				<th> fecha de T&Eacute;RMINO de la capacidad/curso: </th>
			</tr>
			<tr>
				<td id="non"> <center><input id="fechainicio" name="cap6" type="text" size="10" maxlength="10" onblur="this.value = this.value.toUpperCase()" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013"/></center> </td>
				<td id="non"><input id="fechafin" name="cap7" type="text" size="10" maxlength="10" title="Fecha de Registro de TERMINO de Curso. Ejemplo: 12/06/2013"/>
				<input type="button" value="Crear horario" class="boton" tabindex="5" onClick="seeline()" title="Crear el horario actual"/>
			</tr>
		</table>
		<table align="center" title="Datos de la Nueva Capacidad/curso ya Autorizada, por favor seleccione una capacidad de la lista anexa en la parte de abajo">
			<tr>
				<th colspan="2" width="250">HORARIO</th>
			</tr>
			<tr>
				<table id="tablahorarios" align="center">
					<tbody id="tablebody">
						<tr id="tablerow">
							<td id="tablecol"></td>
						</tr>
					</tbody>
				</table>
			</tr>
		</table>
		<div align="center">
			<input type="hidden" name="idcap" value="<?php echo $_idcapacidad ?>">
			<input type="hidden" name="capacidad" value="<?php echo $_capacidad ?>">
			<input type="hidden" name="idgc" value="<?php echo $_idgrupo ?>">
			<input type="hidden" name="grupo" value="<?php echo $_grupo ?>">
			<input type="hidden" name="idsgc" value="<?php echo $_idsubgrupo ?>">
			<input type="hidden" name="subgrupo" value="<?php echo $_subgrupo ?>">
			<input type="hidden" name="otro" value="<?php echo $otro ?>">
			<input type="hidden" name="diasdiff" id="diasdiff">
			<input type="hidden" name="horarios" id="horarios">
			
			<input type="hidden" name="accion" value="<?php echo $accion ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar ?>">
			
			<?php
			echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>


	<!-- Modificar Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR CAPACIDADES EXISTENTES</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
		<tr>
			<th width="120"> Buscar CAPACIDAD/curso: </th>
			<td id="non">
				<form name="m_capacidad" id="m_capacidad" method="get" action="" onSubmit="return validarcampos_m()">
					<input name="capm0" type="text" size="40" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">
					<input type="submit" class="boton" name="submit" value="Buscar" tabindex="1" title="Buscar curso"/>
			</td>
			<td id="non">
				<select name="g_cap" id="g_cap" title="Filtrar b&uacute;squeda por Grupos">
					<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
								$datos->MoveNext();
							}
						}
						?>
				</select>
			</td>
			<td id="non">
				<!-- select de Seleccion de Subgrupo -->
				<select id='sg_cap' name='sg_cap' title='Filtrar b&uacute;squeda por Subgrupos'>
					<?php
						$consulta="select idgrupo,descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { 
							echo "<option value='-1' selected> - Selecciona Subgrupo - </option>";
							while(!$datos->EOF){
								echo '<optgroup label="'.$datos->fields('descripcion').'">';
									$consultacap="select idsubgrupo,descripcion,idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos->fields('idgrupo')."' as integer)";
									$datoscap=ejecutar_sql($consultacap);
									while(!$datoscap->EOF){
										echo '<option value="'.$datoscap->fields('idsubgrupo').'">'.$datoscap->fields('descripcion').'</option>';
										$datoscap->MoveNext();
									}
								echo '</optgroup>';
								$datos->MoveNext();
							}							
						}
					?>
				</select>
			</td>
			<td id="non" colspan="2"></td>
			<input type="hidden" name="idgcap">
			<input type="hidden" name="idsgcap">
		</tr>
			</form>
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
		<tr>
			<th>ID</th>
			<th>Capacidad</th>
			<th>Grupo</th>
			<th>Subgrupo</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
			<?php
		
	if($_GET['g_cap'] and $_GET['g_cap'] != '-1'){
		$idgrupo = "and g.idgrupo = cast('".$_GET['g_cap']."' as integer)";
	}
	if ($_GET['sg_cap'] and $_GET['sg_cap'] != '-1'){
		$idsubgrupo = "and s.idsubgrupo = cast('".$_GET['sg_cap']."' as integer)";
	}
	if ($_GET['capm0']){
		$idcapacidad = "and c.descripcion like '%".$_GET['capm0']."%'";
	}
		
				$consulta="select c.idcapacidad as idc, c.descripcion as des, g.idgrupo as idgrup, g.descripcion as desg, s.idsubgrupo as idsub, s.descripcion as dess from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Capacidades Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('idc').'</td>';
						echo '<td id="non">'.$datos->fields('des').' </td>';
						echo '<td id="non"> '.$datos->fields('desg').' </td>';
						echo '<td id="non"> '.$datos->fields('dess').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_chk" value="" title = "Autorizar"/> </center></td>';
						echo '<input type="hidden" name="idcapacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('des').'">';
						echo '<input type="hidden" name="idgrupo" value="'.$datos->fields('idgrup').'">';
						echo '<input type="hidden" name="grupo" value="'.$datos->fields('desg').'">';
						echo '<input type="hidden" name="idsubgrupo" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="subgrupo" value="'.$datos->fields('dess').'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
				
				$consulta="select c.idotracap as idc, c.descripcion as des, g.idgrupo as idgrup, g.descripcion as desg, s.idsubgrupo as idsub, s.descripcion as dess from cp_otras_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('idc').'</td>';
						echo '<td id="non">'.$datos->fields('des').' </td>';
						echo '<td id="non"> '.$datos->fields('desg').' </td>';
						echo '<td id="non"> '.$datos->fields('dess').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_chk" value="" title = "Autorizar"/> </center></td>';
						echo '<input type="hidden" name="idcapacidad" value="'.$datos->fields('idc').'">';
						echo '<input type="hidden" name="capacidad" value="'.$datos->fields('des').'">';
						echo '<input type="hidden" name="idgrupo" value="'.$datos->fields('idgrup').'">';
						echo '<input type="hidden" name="grupo" value="'.$datos->fields('desg').'">';
						echo '<input type="hidden" name="idsubgrupo" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="subgrupo" value="'.$datos->fields('dess').'">';
						echo '<input type="hidden" name="otro" value="id">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarInicio; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
