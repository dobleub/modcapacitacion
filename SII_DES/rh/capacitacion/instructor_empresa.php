<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/instructor_empresa.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	
	if($_GET['empid']){
		$empid = $_GET['empid'];
		$empdes = $_GET['empdes'];
		$emppais = $_GET['emppais'];
		$empedo = $_GET['empedo'];
		$empcid = $_GET['empcid'];
		$empdir = $_GET['empdir'];
		$empmail = $_GET['empmail'];
		$emptel = $_GET['emptel'];
		$accion = "update";
	} else {
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Administrar Empresas ::.</title>
	</head>
<body>
	
	<h2 align="center">Administraci&oacute;n de Empresas de donde proviene el Instructor</h2>
<?php
	if ($accion == "update"){
		$titulo = '<h3 align="center">ACTUALIZAR EMPRESA</h3>';
		$form = '<form name="u_emp" id="u_emp" method="get" action="ime_instructor_emp.php" onSubmit="return validarcampos_uemp()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar la EMPRESA"/>';
		
	} else {
		$titulo = '<h3 align="center">INGRESAR EMPRESA</h3>';
		$form = '<form name="i_emp" id="i_emp" method="get" action="ime_instructor_emp.php" onSubmit="return validarcampos_iemp()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la nueva EMPRESA"/>';
		
	}
?>

  <!-- Contenido -->
<?php
	echo '<br/>';
	echo $titulo;
	
	echo $form;
?>

		<table align="center" title="Por favor rellene los siguientes datos para dar de alta una nueva EMPRESA">
			<tr> 
				<th width="100" colspan="2"> Nombre de la EMPRESA: </th>
				<td id="non">
					<input name="emp0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $empdes ?>" title="Ingrese el NOMBRE de la EMPRESA">
				</td>
			</tr>
			<tr> 
				<th width="150" colspan="2"> Direcci&oacute;n: </th>
			</tr>
			<tr>
				<td width="20"></td>
				<td id="non"> Pais: </td>
				<td id="non">
					<input name="emp1" type="text" size="20" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $emppais ?>" title="">
				</td>
			</tr>
			<tr>
				<td></td>
				<td id="non"> Estado: </td>
				<td id="non">
					<input name="emp2" type="text" size="25" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $empedo ?>" title="">
				</td>
			</tr>
			<tr>
				<td></td>
				<td id="non"> Ciudad: </td>
				<td id="non">
					<input name="emp3" type="text" size="25" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $empcid ?>" title="">
				</td>
			</tr>
			<tr>
				<td></td>
				<td id="non"> Direcci&oacute;n: </td>
				<td id="non">
					<input name="emp4" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $empdir ?>" title="">
				</td>
			</tr>
			<tr>
				<th width="100" colspan="2"> Tel&eacute;fono: </th>
				<td id="non">
					<input name="emp5" type="text" size="20" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $emptel ?>" title="">
				</td>
			</tr>
			<tr>
				<th width="100" colspan="2"> e-mail: </th>
				<td id="non">
					<input name="emp6" type="text" size="25" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $empmail ?>" title="">
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="idemp" value="<?php echo $empid; ?>">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php
				echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	
	<!-- Modificaciones / eliminacion -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR / ELIMINAR EMPRESA</h3>
	<form name="m_emp" id="myForm" method="get" action="" onSubmit="return validarcampos()" >
		<table align="center" title="Modificar / Eliminar una EMPRESA">
			<tr>
				<th>Empresa</th>
				<th>Direcci&oacute;n</th>
				<th>Tel&eacute;fono</th>
				<th>e-mail</th>
				<th colspan="2">Acci&oacute;n</th>
			</tr>
			<?php
			$consulta="select idempresa,descripcion,pais,estadoe,ciudad,direccion,email,telefonos from cp_instructor_emp where estado = '1'";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Grupos/Subgrupos Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_im" id="m_im" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr> <td id="non">'.$datos->fields('descripcion').'</td>';
					echo '<td id="non">'.$datos->fields('ciudad').', '.$datos->fields('estadoe').', '.$datos->fields('direccion').' </td>';
					echo '<td id="non">'.$datos->fields('telefonos').' </td>';
					echo '<td id="non">'.$datos->fields('email').' </td>';
					echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
					echo '<input type="hidden" name="empid" value="'.$datos->fields('idempresa').'">';
					echo '<input type="hidden" name="empdes" value="'.$datos->fields('descripcion').'">';
					echo '<input type="hidden" name="emppais" value="'.$datos->fields('pais').'">';
					echo '<input type="hidden" name="empedo" value="'.$datos->fields('estadoe').'">';
					echo '<input type="hidden" name="empcid" value="'.$datos->fields('ciudad').'">';
					echo '<input type="hidden" name="empdir" value="'.$datos->fields('direccion').'">';
					echo '<input type="hidden" name="empmail" value="'.$datos->fields('email').'">';
					echo '<input type="hidden" name="emptel" value="'.$datos->fields('telefonos').'">';
					echo '</form>';
					echo '<form name="e_im" id="e_im" method="get" action="ime_instructor_emp.php" onSubmit="return validarcampos()" >';
					echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';
					echo '<input type="hidden" name="empresa" value="'.$datos->fields('idempresa').'">';
					echo '<input type="hidden" name="accion" value="delete">';
					echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form> </tr>';
					$datos->MoveNext();
					}
				}
			?>
			
		</table>
	</form>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
