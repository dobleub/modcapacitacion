<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_capacidades_sg.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["acciong"] || $_GET["accions"]){
	$acciong=$_GET["acciong"];
	$accions=$_GET["accions"];
	$regresar=$_GET["regresar"];
	
	$gruponame=$_GET["grp0"];
	$grupoid=$_GET["grp1"];
	$subgruponame=$_GET["subg0"];
	
	$idgrupo=$_GET["idgrupo"];
	$idsubg=$_GET["idsubg"];
	
		if($acciong == "insert" && $accions == "insert"){
			if($gruponame){
				$query = sybase_query("declare @echo char(1) exec i_grupos_cap '$gruponame', @echo output select @echo as echo");
			}
			if($grupoid && $subgruponame){
				$query = sybase_query("declare @echo char(1) exec i_subgrupo_cap '$subgruponame', $grupoid, @echo output");
			}
			$mensaje = sybase_get_last_message();				
		}
		if($acciong == "update" || $accions == "update"){
			if($acciong == "update"){
				if($gruponame!=""){
					$query = sybase_query("update cp_grupos_cap set descripcion='$gruponame' where idgrupo=$idgrupo");
					$mensaje = "Grupo Actualizado CORRECTAMENTE";
				} else {
					$mensaje = "No se pudo Actualizar el Grupo por falta de Informaci&oacute;n";
				}
			}
			if($accions == "update"){
				if($subgruponame!="" && $grupoid!="-1"){
					$query = sybase_query("update cp_subgrupo_cap set descripcion='$subgruponame',idgrupo=$grupoid where idsubgrupo=$idsubg");
					$mensaje = "Subgrupo Actualizado CORRECTAMENTE";
				} else {
					$mensaje = "No se pudo Actualizar el Subgrupo por falta de Informaci&oacute;n";
				}
			}
		}
		if($acciong == "delete" || $accions == "delete"){
			if($acciong == "delete"){
				$query = sybase_query("update cp_grupos_cap set estado='0' where idgrupo=$idgrupo");
				$mensaje = "Grupo Eliminado CORRECTAMENTE";
			}
			if($accions == "delete"){
				$query = sybase_query("update cp_subgrupo_cap set estado='0' where idsubgrupo=$idsubg");
				$mensaje = "Subgrupo Eliminado CORRECTAMENTE";
			}
		}
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
