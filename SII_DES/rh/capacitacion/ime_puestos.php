<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_capacidades.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	$otro=$_GET["otro"];
	$exist = $_GET["exist"];
	$noexist = $_GET["noexist"];
	
	$personal = $_GET["dp0"];
	$departamento = $_GET["dpt1"];
	$puesto = $_GET["pst1"];
	
	
		if($accion == "insert"){
			if($personal!='-1' && $puesto!='-1'){
				$query = sybase_query("declare @echo char(1) exec i_personal_det '$personal', '20DIT0004L', '$puesto', @echo output select @echo as echo");
				$mensaje = sybase_get_last_message();				
			} else {
				$mensaje = "No se pudo Insertar la informacion de Personal";
			}
		}
		if($accion == "update"){
			if($exist=="id"){
				$query = sybase_query("update cp_personal_det set idpuesto='$puesto' where rfc='$personal'");
				$mensaje = "Datos de Personal actualizados CORRECTAMENTE";	
			} else {
				$query = sybase_query("declare @echo char(1) exec i_personal_det '$personal', '20DIT0004L', '$puesto', @echo output select @echo as echo");
				$mensaje = sybase_get_last_message();	
			}
		}
		if($accion == "delete"){
			$query = sybase_query("update cp_personal_det set estado='0' where rfc='$personal' ");
			//$query = sybase_query("alter table cp_personal_det add estado char(1) default '1' ");
			$mensaje = "Datos de personal Eliminado CORRECTAMENTE";
		}
		
		//echo $mensaje;
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
