<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);	
	$regresarInicio = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	

	if($_GET['clave']){		
		$clave = $_GET["clave"];
		$accion = $_GET["accion"];
		$otro = $_GET["otro"];
		$regresar = $_GET["regresar"];
		$cupo = $_GET["cupo"];
	} else {
		$accion = "insert";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<script type="text/javascript">
			//window.onload = function() {
				// code to execute here
				//<?php if($cupo) { ?>
					//seelinepersonal(<?php echo $cupo; ?>);
				//<?php } ?>
			//}
		</script>
		
		<title>.:: Capacidades/cursos ::.</title>
	</head>
	
<body>

	<h2 align="center">Estado de los cursos</h2>

<?php
	if ($accion == "ver"){
		if($otro){
		$consulta = "select ca.clavecurso, ca.idotracap as idcapacidad, c.descripcion, ca.idinstructor, i.titulo, i.nombre, ca.idhorario, ca.limite_personal, ca.idinstituto, ins.descripcion as itt, ca.fecha, hgr.totalhoras, hgr.fechainicio, hgr.fechafin
			from  cp_capacidad_autorizada as ca, cp_otras_capacidades as c, cp_instructor_cap as i, cp_institucion as ins, cp_horario_gral as hgr
			where ca.idotracap = c.idotracap and ca.idinstructor = i.idinstructor and ca.idinstituto = ins.idinstituto 
			and ca.idhorario = hgr.idhorario and ca.clavecurso = '$clave' ";
		$datos = ejecutar_sql($consulta);
		} else {
		$consulta = "select ca.clavecurso, ca.idcapacidad, c.descripcion, ca.idinstructor, i.titulo, i.nombre, ca.idhorario, ca.limite_personal, ca.idinstituto, ins.descripcion as itt, ca.fecha, hgr.totalhoras, hgr.fechainicio, hgr.fechafin
			from  cp_capacidad_autorizada as ca, cp_capacidades as c, cp_instructor_cap as i, cp_institucion as ins, cp_horario_gral as hgr
			where ca.idcapacidad = c.idcapacidad and ca.idinstructor = i.idinstructor and ca.idinstituto = ins.idinstituto 
			and ca.idhorario = hgr.idhorario and ca.clavecurso = '$clave' ";
		$datos = ejecutar_sql($consulta);
		}
			if(!$datos->rowcount()) echo "Datos de Faltantes";
			else {
				while(!$datos->EOF){
					$cap_id = $datos->fields('idcapacidad');
					$cap_des = $datos->fields('descripcion');
					$ins_id = $datos->fields('idinstructor');
					$ins_name = $datos->fields('titulo').' '.$datos->fields('nombre');
					$hr_id = $datos->fields('idhorario');
					$cupo = $datos->fields('limite_personal');
					$it_name = $datos->fields('itt');
					$fecha = $datos->fields('fecha');
					$it_id = $datos->fields('idinstituto');
					$it_name = $datos->fields('itt');
					$hr_total = $datos->fields('totalhoras');
					$fechainicio = $datos->fields('fechainicio');
					$fechafin = $datos->fields('fechafin');
					$horario = $datos->fields('idhorario');
				$datos->MoveNext();
				}
			}
		
		$titulo = '<h3 align="center">ESTADO DEL CURSO</h3>';
		$form = '<form name="u_inscripcion" id="u_inscripcion" method="get" action="" onSubmit="" >';
		
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()" disabled>Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Actualizaci&oacute; de la Inscripcion del personal"/>';
		$funcionreload = 'reload_grupob_cap';
		
	} else{
		$titulo = '<h3 align="center">Inscripci&oacute;n de Personal a Curso ya Autorizado</h3>';
		$form = '<form name="i_inscripcion" id="i_inscripcion" method="get" action="" onSubmit="">';
		//$checkbox = '<input type="checkbox" id="enableid" name="enableid" value="id" title="Marque la casilla si desea anotar el ID de la capacidad" onclick="activartextbox()">Si';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Inscripci&oacute;n de Personal a Curso Autorizado Previamente"/>';
		$funcionreload = 'reload_grupo_cap';
	}
?>
	<!-- Contenido -->  
<?php
	echo '<br/>';
	echo $titulo;

	echo $form;
?>
		<table align="center" title="Inscripci&oacute;n de Personal a Curso Autorizado Previamente">
			<tr>
				<th> IT o CENTRO: </th>
				<td id="non">
					<input name="cap0" type="text" size="20" maxlength="30" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $it_id ?>" title="Clave del IT o CENTRO" disabled>
					<input name="cap1" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $it_name ?>" title="Nombre del IT o CENTRO donde se llevar&aacute; a cabo el Curso." disabled>
				</td>
			</tr>
			<tr>
				<th> CLAVE de capacidad/curso autorizado: </th>
				<td id="non">
					<input name="cap2" type="text" size="20" maxlength="30" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $clave ?>" title="Ingrese una CLAVE de Capacitaci&oacute;n/curso. Ejemplo: CP-A-01-119-119-12" disabled>

					<input name="cap3" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $cap_des ?>" title="Muestra el nombre de Capacitaci&oacute;n/curso actual. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS" disabled>
				</td>
			</tr>
			<tr>
				<th> Instructor: </th>
				<td id="non">

					<input name="cap4" type="text" size="60" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $ins_name ?>" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: CAPACIDAD T&Eacute;CNICA: ADMINISTRACI&Oacute;N DE BASES DE DATOS" disabled>

				</td>
			</tr>
			<tr>
				<th> FECHA de registro de la capacidad/curso: </th>
				<td id="non">
					<input id="registro" name="cap5" type="text" maxlength="30" style="padding-right: 22px;" value="<?php echo $fecha ?>" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013" disabled/>
				</td>
			</tr>
			<tr>
				<th> Duraci&oacute;n del Curso en horas: </th>
				<th> Cupo l&iacute;mite: </th>
			</tr>
			<tr>
				<td id="non">
					<input id="totalh" name="cap8" type="text" maxlength="10" style="padding-right: 22px;" value="<?php echo $hr_total ?>" title="Anote total de horas que tomara completar el Curso" disabled/>
				</td>
				<td id="non">
					<input id="totalp" name="cap9" type="text" maxlength="10" style="padding-right: 22px;" value="<?php echo $cupo ?>" title="Anote total de horas que tomara completar el Curso" disabled/>
				</td>
			</tr>
			<tr>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<th> FECHA de INICIO de la capacidad/curso: </th>
				<th> FECHA de T&Eacute;RMINO de la capacidad/curso: </th>
			</tr>
			<tr>
				<td id="non"> <center><input id="fechainicio" name="cap6" type="text" size="10" maxlength="10" value="<?php echo $fechainicio ?>" title="Fecha de Registro de Curso. Ejemplo: 04/06/2013" disabled/></center> </td>
				<td id="non"><input id="fechafin" name="cap7" type="text" size="10" maxlength="10" value="<?php echo $fechafin ?>" title="Fecha de Registro de TERMINO de Curso. Ejemplo: 12/06/2013" disabled/>
			</tr>
		</table>
		<table align="center" title="Datos de la Nueva Capacidad/curso ya Autorizada, por favor seleccione una capacidad de la lista anexa en la parte de abajo">
			<tr>
				<table id="tablahorarios" align="center">
					<?php
					$consulta="select h.idhorario, h.totalhoras, hd.dia, hd.fecha_dia, hd.horainicio 
					from cp_horario_gral as h, cp_horario_det as hd, cp_horario_gral_det as hgd 
					where hgd.idhorario = h.idhorario and hgd.idhd = hd.idhd ";
					$consulta="select h.idhorario, h.totalhoras from cp_horario_gral as h where h.idhorario = '$horario'";
					
					$datos=ejecutar_sql($consulta);
					
					if(!$datos->rowcount())	echo "<table align='center'> <tr><th>HORARIO</th></tr> <tr><td>Capacidad Autorizada sin Horario</td></tr> </table>";
					else {
						while(!$datos->EOF){
							echo '<tr>';
							echo '<th> HORARIO </th>';
							echo '<td id="non" align="right"> Total de Horas: ';
							echo ' <strong> '.$datos->fields('totalhoras').' </strong> </td>';
							echo '</tr>';
							
								$consulta2 = "select hd.dia, hd.fecha_dia, hd.horainicio 
								from cp_horario_det as hd, cp_horario_gral_det as hgd 
								where hgd.idhorario ='".$datos->fields('idhorario')."' and hgd.idhd = hd.idhd ";
								$datos2=ejecutar_sql($consulta2);
								
								echo '<tr>';
									if(!$datos2->rowcount())	echo "<tr><td>Capacidad Autorizada sin Horario</td></tr>";
									else {
										while(!$datos2->EOF){
											echo '<td>';
												echo '<table>';
												echo '<tr>';
													echo '<td id="non" align="center"> '.$datos2->fields('fecha_dia').' </td>';
												echo '</tr>';
												echo '<tr>';
													echo '<th align="center"> '.$datos2->fields('dia').' </th>';
												echo '</tr>';
												echo '<tr>';
													echo '<td id="non" align="center"> '.$datos2->fields('horainicio').' </td>';
												echo '</tr>';
												echo '</table>';
											echo '</td>';
										$datos2->MoveNext();
										}
									}
								echo '</tr>';
						$datos->MoveNext();
						}
					}
					?>
				</table>
			</tr>
		</table>
		<div align="center">
			<input type="hidden" name="clave" value="<?php echo $clave ?>">
			<input type="hidden" name="otro" value="<?php echo $otro ?>">
			<input type="hidden" name="cupo" value="<?php echo $cupo ?>">
			
			<input type="hidden" name="accion" value="<?php echo $accion ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar ?>">
			
			<?php
			echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>


	<!-- Modificar Capacidades/cursos -->
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">PERSONAL INSCRITO</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
<!--
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
-->
		<tr>
			<th>No.</th>
			<th>RFC</th>
			<th>G&Eacute;NERO</th>
			<th>Nombre</th>
			<th>Puesto</th>
			<th>Departamento </th>
			<th>Subdirecci&oacute;n</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
			
			<?php
			// Muestra todos los empleados que tienen puestos de Subdirectores
			$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.sexo_empleado, pu.descripcion as puesto, su.descripcion as subdir
    from personal as p, cp_cap_autorizada_personal as cap, cp_personal_det as pd, cp_puesto as pu, cp_departamento as dt, cp_subdireccion as su
	where cap.rfc = p.rfc and pd.rfc = cap.rfc and pd.idpuesto = pu.idpuesto and pu.idsub = su.idsub
    and cap.clavecurso = '$clave' and cap.estado = '1' and p.status_empleado = '02' and pd.estado='1' and pu.estado='1' and dt.estado='1' and su.estado='1'
	order by p.apellidos_empleado, p.nombre_empleado, p.rfc  ";

				$datos=ejecutar_sql($consulta);
				$i = 0;
				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
						if($datos->fields('sexo_empleado') == "M")
							$genero = "HOMBRE";
						else 
							$genero = "MUJER";
					echo '<form name="m_dp" id="m_dp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non" align="center">'.($i = $i + 1).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$genero.'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non">'.$datos->fields('puesto').'</td>';
						echo '<td id="non">'.$datos->fields('depto').'</td>';
						echo '<td id="non">'.$datos->fields('subdir').'</td>';
						//echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="idpuesto" value="'.$datos->fields('idpuesto').'">';
						echo '<input type="hidden" name="iddepto" value="'.$datos->fields('iddepto').'">';
						echo '<input type="hidden" name="subdirid" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="exist" value="none">';
					echo '</form>';
					echo '<form name="m_cp" id="m_cp" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Quitar de la lista a '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$clave.'">';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
				
				
			// Muestra todos los empleados que tienen puestos
			$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.sexo_empleado, pu.descripcion as puesto, dt.descripcion as depto, su.descripcion as subdir
    from personal as p, cp_cap_autorizada_personal as cap, cp_personal_det as pd, cp_puesto as pu, cp_departamento as dt, cp_subdireccion as su
	where cap.rfc = p.rfc and pd.rfc = cap.rfc and pd.idpuesto = pu.idpuesto and pu.iddepto = dt.iddepto and dt.idsub = su.idsub
    and cap.clavecurso = '$clave' and cap.estado = '1' and p.status_empleado = '02' and pd.estado='1' and pu.estado='1' and dt.estado='1' and su.estado='1'
	order by p.apellidos_empleado, p.nombre_empleado, p.rfc";

				$datos=ejecutar_sql($consulta);
				if(!$datos->rowcount())	echo " ";
				else { 
					while(!$datos->EOF){
						if($datos->fields('sexo_empleado') == "M")
							$genero = "HOMBRE";
						else 
							$genero = "MUJER";
					echo '<form name="m_dp" id="m_dp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non" align="center">'.($i = $i + 1).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$genero.'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non">'.$datos->fields('puesto').'</td>';
						echo '<td id="non">'.$datos->fields('depto').'</td>';
						echo '<td id="non">'.$datos->fields('subdir').'</td>';
						//echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar: '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="idpuesto" value="'.$datos->fields('idpuesto').'">';
						echo '<input type="hidden" name="iddepto" value="'.$datos->fields('iddepto').'">';
						echo '<input type="hidden" name="idsub" value="'.$datos->fields('idsub').'">';
						echo '<input type="hidden" name="exist" value="id">';
					echo '</form>';
					echo '<form name="m_cp" id="m_cp" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Quitar de la lista a '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$clave.'">';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			
			// Muestra todos los empleados que no estan en la lista de empleados con puestos
				//$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' and rfc not in (select distinct(rfc) from cp_personal_det where estado='1') order by apellidos_empleado, nombre_empleado, rfc";
				$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.sexo_empleado
						from personal as p, cp_cap_autorizada_personal as cap
						where cap.rfc = p.rfc and p.rfc not in (select distinct(rfc) from cp_personal_det where estado='1') 
						and cap.clavecurso = '$clave' and cap.estado = '1' and p.status_empleado = '02' 
						order by p.apellidos_empleado, p.nombre_empleado, p.rfc";
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "<tr> <td> No hay Personal Inscrito en este Curso </td> </tr>";
				else { 
					while(!$datos->EOF){
					$i = $i + 1;
						if($datos->fields('sexo_empleado') == "M")
							$genero = "HOMBRE";
						else 
							$genero = "MUJER";
					echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non" align="center">'.($i).'</td>';
						echo '<td id="non">'.$datos->fields('rfc').'</td>';
						echo '<td id="non">'.$genero.'</td>';
						echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						echo '<td id="non"> - </td>';
						echo '<td id="non"> - </td>';
						echo '<td id="non"> - </td>';
						//echo '<td id="non"> <center> <input type="Submit" class="button_ins" value="" title = "Inscribir"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						echo '<input type="hidden" name="clave" value="'.$datos->fields('rfc').'">';
					echo '</form>';
					echo '<form name="m_cp" id="m_cp" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Quitar de la lista a '.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'"/> </center></td>';
						echo '<input type="hidden" name="clave" value="'.$clave.'">';
						echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar Atr&aacute;s" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
	<form name="Crear Reporte" method="get" action="pdf_informe_curso.php">
	  <br />
		<div align="center">
		  <input type="submit" value="Regresar Atr&aacute;s" class="boton" tabindex="5" title="Generar el Informe Detallado en PDF del Curso actual" />
		  <?php
			echo '<input type="hidden" name="clave" value="'.$clave.'">';
			echo '<input type="hidden" name="it_id" value="'.$it_id.'">';
			echo '<input type="hidden" name="it_name" value="'.$it_name.'">';
			echo '<input type="hidden" name="cap_des" value="'.$cap_des.'">';
			echo '<input type="hidden" name="ins_name" value="'.$ins_name.'">';
			echo '<input type="hidden" name="hr_total" value="'.$hr_total.'">';
			echo '<input type="hidden" name="cupo" value="'.$cupo.'">';
			echo '<input type="hidden" name="fechainicio" value="'.$fechainicio.'">';
			echo '<input type="hidden" name="fechafin" value="'.$fechafin.'">';
		  ?>
	  </div>
	</form>
	<br/>
	<br/>
	<br/>
</body>
</html>

	<?php
				//$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.sexo_empleado
						//from personal as p, cp_cap_autorizada_personal as cap
						//where p.status_empleado = '02' and cap.rfc = p.rfc and cap.clavecurso = '$clave' and cap.estado = '1'
						//order by p.apellidos_empleado, p.nombre_empleado, p.rfc";
				//$datos=ejecutar_sql($consulta);

				//if(!$datos->rowcount())	echo "<tr> <td> No hay Personal Inscrito en este Curso </td> </tr>";
				//else { 
					//$i=0;
					//while(!$datos->EOF){
					//$i = $i + 1;
						//if($datos->fields('sexo_empleado') == "M")
							//$genero = "HOMBRE";
						//else 
							//$genero = "MUJER";
					//echo '<form name="m_cp" id="m_cp" method="get" action="" onSubmit="return validarcampos()" >';
					//echo '<tr>';
						//echo '<td id="non" align="center">'.($i).'</td>';
						//echo '<td id="non">'.$datos->fields('rfc').'</td>';
						//echo '<td id="non">'.$genero.'</td>';
						//echo '<td id="non">'.$datos->fields('apellidos_empleado').' '.$datos->fields('nombre_empleado').'</td>';
						//echo '<td id="non"> - </td>';
						//echo '<td id="non"> - </td>';
						//echo '<td id="non"> - </td>';
						////echo '<td id="non"> <center> <input type="Submit" class="button_ins" value="" title = "Inscribir"/> </center></td>';
						//echo '<input type="hidden" name="clave" value="'.$datos->fields('clavecurso').'">';
						//echo '<input type="hidden" name="clave" value="'.$datos->fields('rfc').'">';
					//echo '</form>';
					//echo '<form name="m_cp" id="m_cp" method="get" action="ime_inscripcion_personal.php" onSubmit="return validarcampos()" >';
						//echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Quitar de la lista"/> </center></td>';
						//echo '<input type="hidden" name="clave" value="'.$clave.'">';
						//echo '<input type="hidden" name="rfc" value="'.$datos->fields('rfc').'">';
						//echo '<input type="hidden" name="accion" value="delete">';
						//echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					//echo '</form>';
					//echo '</tr>';
					//$datos->MoveNext();
					//}
				//}
	?>
