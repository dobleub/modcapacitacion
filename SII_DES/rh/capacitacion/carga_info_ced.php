<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	//$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";

/*
 * carga_subgrupos_cap.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
 
if ($_GET["rfc"]){
	$rfc = $_GET["rfc"];
	
	//$consulta="select *	from personal where status_empleado = '02' and rfc = '".$rfc."' order by apellidos_empleado, nombre_empleado, rfc";
	$consulta="select distinct(p.rfc), p.apellidos_empleado, p.nombre_empleado, p.telefono_empleado, p.sexo_empleado, p.correo_electronico, p.estudios
				from personal as p
				where p.status_empleado = '02' and p.rfc = '$rfc' order by p.apellidos_empleado, p.nombre_empleado, p.rfc";
	$funcionreload = 'reload_puestoi_per';
} else {
	$consulta="select distinct(rfc), apellidos_empleado, nombre_empleado from personal where status_empleado = '02' order by apellidos_empleado, nombre_empleado, rfc";
}

	$accion=$_GET["action"];

	$datos=ejecutar_sql($consulta);
	//echo array_count_values($datos);
	//var_dump($datos->fields);
	//echo $consulta;
	
	
	if(!$datos->rowcount())	echo '<table align="center"> <tr> <th>Datos faltantes</th> </tr> </table>';
		else {			
				while(!$datos->EOF){
					if($datos->fields('sexo_empleado')=="M")
						$genero = "HOMBRE";
					else
						$genero = "MUJER";
					
				echo '<table align="center" title="Por favor rellene los siguientes datos faltantes del Personal">';
					echo '<tr>
						<th colspan="2"> Nombre: </th>
						<td id="non" colspan="2">
							<input name="per0" type="text" size="50" maxlength="100" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('apellidos_empleado')).' '.utf8_encode($datos->fields('nombre_empleado')).'" title="Nombre del Personal seleccionado">
						</td>
					</tr>';
					echo '<tr>
						<th> R.F.C.: </th>
						<td id="non">
							<input name="per1" type="text" size="25" maxlength="25" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('rfc').'" title="R.F.C. del Personal seleccionado" disabled>
						</td>
						<th> G&eacute;nero: </th>
						<td id="non">
							<input name="per2" type="text" size="25" maxlength="25" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$genero.'" title="R.F.C. del Personal seleccionado" disabled>
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Clave Presupuestal: </th>
						<td id="non" colspan="2">
							<input name="per3" type="text" size="20" maxlength="20" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('clave_presupuestal').'" title="Clave Presupuestal">
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Tel&eacute;fono Oficial: </th>
						<td id="non" colspan="2">
							<input name="per4" type="text" size="25" maxlength="20" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('telefono_empleado').'" title="Tel&eacute;fono del Personal" >
						</td>
					</tr>';
					echo '<tr>
						<th colspan="2"> Correo Electr&oacute;nico: </th>
						<td id="non" colspan="2">
							<input name="per5" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.$datos->fields('correo_electronico').'" title="Correo Electr&oacute;nico del Personal" >
						</td>
					</tr>';
				echo '</table>';
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">DATOS ESCOLARES</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
					
					$sql="select idescolaridad, descripcion from cp_escolaridad_p where estado = '1'";
					$datos2=ejecutar_sql($sql);
					//var_dump($datos2-);
					echo '<tr>
						<th>GRADO</th>
						<th>CONCLUIDA</th>
						<th>INCONCLUSA</th>
						<th width="100"></th>
					</tr>';
						while(!$datos2->EOF){
							echo '<tr>
								<td id="non"> '.utf8_encode($datos2->fields('descripcion')).' </td>
								<td id="non"> <input name="escolar" type="radio" value='.$datos2->fields('idescolaridad').'> </td>
								<td id="non"> <input name="escolar" type="radio" value='.$datos2->fields('idescolaridad').''.$datos2->fields('idescolaridad').'> </td>
								<td id="non"></td>
							</tr>';
						$datos2->MoveNext();
						}
					echo '<tr>
						<th>Carrera: </th>
						<td id="non" colspan="3">
							<input name="per6" type="text" size="60" maxlength="60" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('estudios')).'" title="Nombre de la Carrera" >
						</td>
					</tr>';
					
				echo '</table>';
				
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">DATOS LABORALES</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
					
				echo '<tr>
				<th width="200"> IT O CENTRO: </th>
					<td id="non"> <select name="insti0" title="Seleccione el Tecnol&oacute;gico o Centro">';
						$consulta="select idinstituto,descripcion from cp_institucion";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Instituci&oacute;n faltantes </option>";
						else { 
							//echo "<option value='-1' selected> - Selecciona Instituci&oacute;n - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idinstituto').'">'.utf8_encode($datos->fields('descripcion')).' </option>';
								$datos->MoveNext();
							}							
						}
						
					echo '</select>
					</td>
					</tr>';
					
					$consulta="select count(*) as total from cp_personal_det as pd where pd.rfc = '$rfc' and pd.estado = '1'";
					$datos=ejecutar_sql($consulta);
					if(!$datos->rowcount() || $datos->fields('total') == 0){
						echo '<tr>';
						echo '<th width="100"> Subdireccion: </th>
							<td id="non"> <select name="dpt1" id="dpt1" title="Seleccione la Subdirecci&oacute;n" onchange=cargarSubgrupos("carga_puesto_per.php","txtHintpto","subid",this.value,"'.$funcionreload.'"); "'.$funcionreload.'"(); >';
									$consulta="select idsub, descripcion from cp_subdireccion where estado='1'";

									$datos=ejecutar_sql($consulta);

									if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Subdirecci&oacute;n faltantes </option>";
									else { 								
										echo "<option value='-1' selected> - Selecciona Subdirecci&oacute;n - </option>";
										while(!$datos->EOF){
											echo '<option value="'.$datos->fields('idsub').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
											$datos->MoveNext();
										}
									}
							echo '</select> </td>';
						echo '</tr>';
						
						echo '<tr>';
						echo '<th width="100"> Departamento: </th>
							<td id="non"> <select name="dpt1" id="dpt1" title="Seleccione el Departamento" onchange=cargarSubgrupos("carga_puesto_per.php","txtHintpto","depto",this.value,"'.$funcionreload.'"); "'.$funcionreload.'"(); >';
									$sql="select idsub, descripcion from cp_subdireccion where estado='1' ";
									$datosgral=ejecutar_sql($sql);
									
									if(!$datosgral->rowcount())	echo "<option value='-1' selected> Datos de Departamentos faltantes </option>";
									else {
										echo "<option value='-1' selected> - Selecciona Departamento - </option>";
										while(!$datosgral->EOF){
											echo '<optgroup label="SUB: '.utf8_encode($datosgral->fields('descripcion')).'">';
											
												$consulta="select iddepto, descripcion from cp_departamento where estado='1' and idsub='".$datosgral->fields('idsub')."'";
												$datos=ejecutar_sql($consulta);
														while(!$datos->EOF){
															echo '<option value="'.$datos->fields('iddepto').'">'.utf8_encode($datos->fields('descripcion')).'</option>';
															$datos->MoveNext();
														}
											echo '</optgroup>';
											$datosgral->MoveNext();
										}
									}
							echo '</select> </td>';
						echo '</tr>';
						
						echo '<tr>
							<th width="100"> Puesto: </th>
							<td id="non">
								<div id="txtHintpto"></div>
							</td>
						</tr>';
						
					} else {
						$consulta = "select p.idpuesto, p.idsub, p.descripcion as puesto, s.descripcion
							from cp_puesto as p, cp_personal_det as pd, cp_subdireccion as s
							where p.idpuesto = pd.idpuesto and p.idsub = s.idsub and pd.rfc = '$rfc' 
							and p.estado='1' and p.iddepto IS NULL and pd.estado='1' and s.estado = '1'";
							
						$consulta2 = "select p.idpuesto, p.descripcion as puesto, d.descripcion as depto, p.idsub, s.descripcion 
							from cp_puesto as p, cp_personal_det as pd, cp_subdireccion as s, cp_departamento as d
							where p.idpuesto = pd.idpuesto and p.iddepto = d.iddepto and d.idsub = s.idsub and pd.rfc = '$rfc' 
							and p.estado='1' and p.idsub IS NULL and pd.estado='1' and s.estado='1' and d.estado='1'";
						
						$datos=ejecutar_sql($consulta);
						$datos2=ejecutar_sql($consulta2);

						if(!$datos->rowcount()){
							if(!$datos2->rowcount()) echo ' ';
							else {
								while(!$datos2->EOF){
									echo '<tr>
										<th width="100"> Subdireccion: </th>
										<td id="non">
											<input name="per7" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos2->fields('descripcion')).'" title="Subdirecci&oacute;n que esta a cargo del Personal" disabled>
										</td>
									</tr>';
								echo '<tr>
									<th width="100"> Departamento: </th>
									<td id="non">
										<input name="per8" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos2->fields('depto')).'" title="Departamento del que esta a cargo el Personal" disabled>
									</td>
								</tr>';
								echo '<tr>
									<th width="100"> Puesto: </th>
									<td id="non">
										<input name="per9" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos2->fields('puesto')).'" title="Puesto del que esta a cargo el Personal" disabled>
									</td>
								</tr>';
								
								$datos2->MoveNext();
								}
							}
						} else { 								
							while(!$datos->EOF){
								echo '<tr>
									<th width="100"> Subdireccion: </th>
									<td id="non">
										<input name="per7" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('descripcion')).'" title="Subdirecci&oacute;n de la que esta a cargo el Personal" disabled>
									</td>
								</tr>';
								echo '<tr>
									<th width="100"> Departamento: </th>
									<td id="non">
										<input name="per8" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('depto')).'" title="Departamento del que esta a cargo el Personal" disabled>
									</td>
								</tr>';
								echo '<tr>
									<th width="100"> Puesto: </th>
									<td id="non">
										<input name="per9" type="text" size="25" maxlength="50" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('puesto')).'" title="Puesto del que esta a cargo el Personal" disabled>
									</td>
								</tr>';
								
								$datos->MoveNext();
							}
						}
						
					}
				
				echo '<tr>
					<th width="100"> Actividaes que desempe&ntilde;a: </th>
					<td id="non">
						<input name="per7" type="text" size="70" maxlength="250" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('descripcion')).'" title="Describa las Principales actividades que desempe&ntilde;a">
					</td>
				</tr>';
				
				echo '</table>';
				
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">ANTECEDENTES DE CAPACITACI&Oacute;N</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
				
					echo '<tr>
						<th width="50"> Capacidades Cursadas: </th>
						<td id="non">
							<input name="per7" type="text" size="70" maxlength="250" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="'.utf8_encode($datos->fields('descripcion')).'" title="Anote las Capacidades que ha recibido anteriormente">
						</td>
					</tr>';
					
					$consulta2 = "select idbarrera, descripcion from cp_barreras_personal where estado = '1' ";
					
					$datos2=ejecutar_sql($consulta2);
					if(!$datos2->rowcount())	echo " ";
					else { 
						echo '<tr>
							<td colspan="2" width="250"> Considera que dentro de su &aacute;rea de trabajo existe alguna de las siguientes barreras que impiden
							el buen desempe&ntilde;o de sus funciones. Num&eacute;relas en orden de importancia </td>
						</tr>';
						$j = 0;
						while(!$datos2->EOF){
							echo '<tr>
									<td id="non" align="center">
										<input name="bar'.($j = $j + 1).'" type="text" size="4" maxlength="2" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Escriba un numero descriptivo para: '.utf8_encode($datos2->fields('descripcion')).'">
									</td>
									<td id="non">
										'.utf8_encode($datos2->fields('descripcion')).'
									</td>
								</tr>';
							$datos2->MoveNext();
						}
					}
				echo '</table>';
				
				
				echo '<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">REQUERIMIENTOS DE CAPACITACI&Oacute;N</h3>';
				echo'<table align="center" title="Por favor rellene los siguientes datos para dar de alta los detalles del Personal">';
				
				
					echo '<tr>
						<th> I. Competencias Generales </th>
						<th> <input type="button" class="button_add" name="addcap" value="" tabindex="1" title="A;adir una capacidad"/> </th>
					</tr>';
					
					$j = 0;
					while($j<5){
					echo '<tr> <td>';
					echo '<select name="comg'.$j.'" title="Seleccione una Capacidad de Competencias Generales">';
						$consulta2="select idgrupo as idg, descripcion as desg from cp_grupos_cap where estado='1' order by desg";
						$datos2=ejecutar_sql($consulta2);
						
						if(!$datos2->rowcount())	echo " None ";
						else {
							echo "<option value='-1' selected> - Selecciona una Capacidad - </option>";
							while(!$datos2->EOF){
								echo '<optgroup label="'.utf8_encode($datos2->fields('desg')).'">';
													
									$consulta3="select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos2->fields('idg')."' as integer)";
									$datos3=ejecutar_sql($consulta3);
									if(!$datos3->rowcount())	echo " None ";
									else {
										while(!$datos3->EOF){
											echo '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos3->fields('descripcion')).'">';
																
													$consulta4="select idcapacidad, descripcion from cp_capacidades where estado='1' and idsubgrupo = cast('".$datos3->fields('idsubgrupo')."' as integer)";
													$datos4=ejecutar_sql($consulta4);
													if(!$datos4->rowcount())	echo " None ";
													else {
														while(!$datos4->EOF){
															echo '<option value="'.$datos4->fields('idcapacidad').'">&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos4->fields('descripcion')).' - '.$datos4->fields('idcapacidad').'</option>';
															$datos4->MoveNext();
														}
													}
											
											echo '</optgroup>';
											$datos3->MoveNext();
										}
									}
								
								echo '</optgroup>';
								$datos2->MoveNext();
							}
						}
					echo '</select>';
					echo '</td> </tr>';
					$j = $j + 1;
					}
					
					
					echo '<tr>
						<th> II. Competencias Directivas </th>
					</tr>';
					
					$j = 0;
					while($j<5){
					echo '<tr> <td>';
					echo '<select name="comg'.$j.'" title="Seleccione una Capacidad de Competencias Directivas">';
						$consulta2="select idgrupo as idg, descripcion as desg from cp_grupos_cap where estado='1' order by desg";
						$datos2=ejecutar_sql($consulta2);
						
						if(!$datos2->rowcount())	echo " None ";
						else {
							echo "<option value='-1' selected> - Selecciona una Capacidad - </option>";
							while(!$datos2->EOF){
								echo '<optgroup label="'.utf8_encode($datos2->fields('desg')).'">';
													
									$consulta3="select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos2->fields('idg')."' as integer)";
									$datos3=ejecutar_sql($consulta3);
									if(!$datos3->rowcount())	echo " None ";
									else {
										while(!$datos3->EOF){
											echo '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos3->fields('descripcion')).'">';
																
													$consulta4="select idcapacidad, descripcion from cp_capacidades where estado='1' and idsubgrupo = cast('".$datos3->fields('idsubgrupo')."' as integer)";
													$datos4=ejecutar_sql($consulta4);
													if(!$datos4->rowcount())	echo " None ";
													else {
														while(!$datos4->EOF){
															echo '<option value="'.$datos4->fields('idcapacidad').'">&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($datos4->fields('descripcion')).' - '.$datos4->fields('idcapacidad').'</option>';
															$datos4->MoveNext();
														}
													}
											
											echo '</optgroup>';
											$datos3->MoveNext();
										}
									}
								
								echo '</optgroup>';
								$datos2->MoveNext();
							}
						}
					echo '</select>';
					echo '</td> </tr>';
					$j = $j + 1;
					}
					
					
					echo '<tr>
						<th> III. Otros (Cursos fuera del cat&aacute;logo) </th>
					</tr>';
					
					$j = 0;
					while($j<5){
					echo '<tr> <td>';
						echo '<input name="per'.$j.'" type="text" size="70" maxlength="250" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Anote las Capacidades que ha recibido anteriormente">';
					echo '</td></tr>';
					$j = $j + 1;
					}
					
					//echo ' <tr> <td>
									//<select>
									 //<optgroup label="Level One">
									  //<option> A.1 </option>
									  //<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;Level Two">
									   //<option>&nbsp;&nbsp;&nbsp;&nbsp; A.B.1 </option>
									  //</optgroup>
									  //<option> A.2 </option>
									 //</optgroup>
									//</select>';
					//echo '</td> </tr> </table>';
				
				$datos->MoveNext();
				}
		}
	
?>
