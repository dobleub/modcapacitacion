<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";
	
	if($_POST['rfcmod']){
		$accion = 'and rfc = '.$_POST['rfcmod'];
		$rfcmod = $_POST['rfcmod'];
	} else {
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Capacidades/cursos ::.</title>
		
	</head>
<body>

	<h2 align="center">Administraci&oacute;n de Capacidades/Cursos</h2>

  <!-- Contenido -->  
	<br/>
	<h3 align="center">INGRESAR NUEVA CAPACIDAD</h3>
	
	<form name="i_capacidad" id="myForm" method="post" action="" onSubmit="return validarcampos_ic()" >
		<table align="center" title="Ingresa un nombre descriptivo acerca de la Nueva Capacidad/curso posible a impartir">
			<tr> 
				<th width="120"> Ingrese un nombre de CAPACIDAD/curso: </th>
				<td id="non">
					<?php
					echo '<input name="cap0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">';
					?>
				</td>
				<td id="non" width="100">
					<select name="gcap" id="gcap" title="Seleccione Grupo a que pertenecer&aacute; la capacidad" onchange="llenar_subgrupos(this.value,document.i_capacidad.subcap)">
						<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else    { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							  while(!$datos->EOF){
							  echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
							  $datos->MoveNext();
							  }
							}
						?>
					</select>
					<select name="subcap" id="subcap" title="Seleccione Subgrupo a que pertenecer&aacute la capacidad;">
						<option value="-1">- Selecciona Subgrupo -</option>
					</select>
				</td>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar la Capacidad/curso"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>

	<!-- Modificar Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR CAPACIDADES EXISTENTES</h3>
	
	<table align="center" title="Busqueda, Modificaci&oacute;n o Eliminiaci&oacute;n de Capacidades">
		<tr>
			<th width="120"> Buscar CAPACIDAD/curso: </th>
			<td id="non">
				<form name="m_capacidad" id="myForm" method="post" action="" onSubmit="return validarcampos_m()">
					<input name="capm0" type="text" size="40" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de Capacitaci&oacute;n/curso. Ejemplo: ADMINISTRACI&Oacute;N DE PROYECTOS">
					<input type="submit" class="boton" name="submit" value="Buscar" tabindex="1" title="Buscar curso"/>
			</td>
			<td id="non">
				<select name="g_cap" title="Filtrar b&uacute;squeda por Grupo" onChange="submit()">
					<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else    { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							  while(!$datos->EOF){
							  echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
							  $datos->MoveNext();
							  }
							}
					?>
				</select>
			</td>
			<td id="non">
				<select name="sg_cap" title="Filtrar b&uacute;squeda por Subgrupo" onChange="submit()">
					<?php
						$consulta="select idsubgrupo, descripcion from cp_subgrupo_cap where estado='1' ";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Subgrupos faltantes </option>";
						else    { echo "<option value='-1' selected> - Selecciona Subgrupo - </option>";
							  while(!$datos->EOF){
							  echo '<option value="'.$datos->fields('idsubgrupo').'">'.$datos->fields('descripcion').'</option>';
							  $datos->MoveNext();
							  }
							}
					?>
				</select>
			</td>
			<td id="non" colspan="2"></td>
		</tr>
		</form>
		<tr height="5"> 
			<td colspan="4"></td>
		</tr>
		<tr>
			<th>ID</th>
			<th>Capacidad</th>
			<th>Grupo</th>
			<th>Subgrupo</th>
			<th colspan="2">Acci&oacute;n</th>
		</tr>
			<?php
		
	if($_POST['g_cap'] and $_POST['g_cap'] != '-1'){
		$idgrupo = "and g.idgrupo = cast('".$_POST['g_cap']."' as integer)";
	}
	if ($_POST['sg_cap'] and $_POST['sg_cap'] != '-1'){
		$idsubgrupo = "and s.idsubgrupo = cast('".$_POST['sg_cap']."' as integer)";
	}
	if ($_POST['capm0']){
		$idcapacidad = "and c.descripcion like '%".$_POST['capm0']."%'";
	}
		
				$consulta="select c.idcapacidad as idc, c.descripcion as des, g.descripcion as desg, s.descripcion as dess from cp_capacidades as c, cp_subgrupo_cap as s, cp_grupos_cap as g	where c.idsubgrupo = s.idsubgrupo and s.idgrupo = g.idgrupo and c.estado='1' and g.estado='1' and s.estado='1' $idcapacidad $idsubgrupo $idgrupo order by idc";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Capacidades Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dp" id="myForm" method="post" action="" onSubmit="return validarcampos()" >';
					echo '<tr>';
						echo '<td id="non">'.$datos->fields('idc').'</td>';
						echo '<td id="non">'.$datos->fields('des').' </td>';
						echo '<td id="non"> '.$datos->fields('desg').' </td>';
						echo '<td id="non"> '.$datos->fields('dess').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';	
						echo '<input type="hidden" name="rfcmod" value="'.$datos->fields('idc').'">';
					echo '</tr>';
					echo '</form>';
					$datos->MoveNext();
					}
				}
			?>
	</table>
	

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
