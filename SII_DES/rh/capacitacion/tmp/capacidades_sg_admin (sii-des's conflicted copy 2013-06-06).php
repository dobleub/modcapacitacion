<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/index.php'";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Capacidades/cursos - Grupos y Subgrupos ::.</title>
	</head>
<body>
<?php ?>
	<h2 align="center">Administraci&oacute;n de GRUPOS/SUBGRUPOS de Capacidades/Cursos</h2>

  <!-- Contenido -->
	<br/>
	<h3 align="center">INGRESAR NUEVO GRUPO</h3>
	
	<form name="i_grupo" id="myForm" method="post" action="" onSubmit="return validarcampos_ig()" >
		<table align="center" title="Ingresa un nombre descriptivo acerca del nuevo GRUPO de Capacidad/curso">
			<tr> 
				<th width="150"> Ingrese un nombre de GRUPO para agrupar Capacidad/curso: </th>
				<td id="non">
					<input name="grp0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de GRUPO para coleccionar una o m&aacute;s Capacitaciones/cursos. Ejemplo: GRUPO: SERVICIOS">
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="grupo">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el GRUPO Capacidad/curso"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<!-- Insertar nuevo Subgrupo -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">INGRESAR NUEVO SUBGRUPO</h3>
	
	<form name="i_subgrupo" id="myForm" method="post" action="" onSubmit="return validarcampos_isg()" >
		<table align="center" title="Ingresa un nombre descriptivo acerca del nuevo SUBGRUPO de Capacidad/curso">
			<tr> 
				<th width="150"> Ingrese un nombre de SUBGRUPO para agrupar Capacidad/curso: </th>
				<td id="non">
					<input name="subg0" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" title="Ingrese un nombre de SUBGRUPO y desp&uacute;es seleccione un Grupo a donde corresponde. Ejemplo: SUBGRUPO: CAPACIDAD T&Eacute;CNICA ADMINISTRACI&Oacute;n">
					<select name="grp1" title="Seleccione Grupo a que pertenecer&aacute; el nuevo Subgrupo">
					<?php
						$consulta="select idgrupo, descripcion from cp_grupos_cap where estado='1'";

						$datos=ejecutar_sql($consulta);

						if(!$datos->rowcount())	echo "<option value='-1' selected> Datos de Grupos faltantes </option>";
						else { echo "<option value='-1' selected> - Selecciona Grupo - </option>";
							while(!$datos->EOF){
								echo '<option value="'.$datos->fields('idgrupo').'">'.$datos->fields('descripcion').'</option>';
								$datos->MoveNext();
							}
						}
						?>
					</select>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="capacidad">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar el SUBGRUPO Capacidad/curso"/>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>

	<!-- Modificar Grupos/Subgrupos de  Capacidades/cursos -->
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR GRUPOS/SUBGRUPOS EXISTENTES</h3>
	
		<table align="center" width="400" title="Ingresa un nombre descriptivo acerca de la Nueva Capacidad/curso posible a impartir">
			<?php
			$consulta="select g.idgrupo as idg, g.descripcion as desg from cp_grupos_cap as g where g.estado='1' order by desg";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Grupos/Subgrupos Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_dg" id="m_dg" method="get" action="" onSubmit="return validarcampos()" >';
					echo '<tr> <th colspan="2">Grupo: "'.$datos->fields('desg').'"</th>';
					echo '<th> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></th>';
					echo '<th> <center> <input type="button" class="button_del" value="" title = "Eliminar" onclick="delete_cap();submit();"/> </center></th>';	
					echo '<input type="hidden" name="grupo" value="'.$datos->fields('idg').'">';
					echo '</form> </tr>';
						$consultasub = "select idsubgrupo, descripcion, idgrupo from cp_subgrupo_cap where estado='1' and idgrupo = cast('".$datos->fields('idg')."' as integer)";
						$datossub=ejecutar_sql($consultasub);
						while(!$datossub->EOF){
							echo '<form name="m_dsg" id="m_dsg" method="get" action="" onSubmit="return validarcampos()" >';
							echo '<tr> <td width="10"></td>';
							echo '<td id="non">'.$datossub->fields('descripcion').'</td>';
							echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
							echo '<td id="non"> <center> <input type="button" class="button_del" value="" title = "Eliminar" onclick="delete_cap();submit();"/> </center></td>';	
							echo '<input type="hidden" name="grupo" value="'.$datos->fields('idg').'">';
							echo '<input type="hidden" name="subgrupo" value="'.$datossub->fields('idsubgrupo').'">';
							echo '</tr>';
							echo '</form>';
						$datossub->MoveNext();
						}
					$datos->MoveNext();
					}
				}
			?>

		</table>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
