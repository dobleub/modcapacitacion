<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_capacidades_sg.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accions"] || $_GET["acciond"] || $_GET["accionp"]){
	$accions=$_GET["accions"];
	$acciond=$_GET["acciond"];
	$accionp=$_GET["accionp"];
	$regresar=$_GET["regresar"];
	
	$sub_name = $_GET["sub0"];
	$dep_name = $_GET["dep0"];
	$sub_id = $_GET["dep1"];
	$pto_name = $_GET["pto0"];
	$dep_id = $_GET["pto1"];
	$subdir = $_GET["pto2"];
	
	$subid = $_GET["subdireccion"];
	$depid = $_GET["departamento"];
	$ptoid = $_GET["puesto"];
	
		if($accions == "insert" || $acciond == "insert" || $accionp == "insert"){
			if($accions == "insert"){
				if($sub_name!=""){
					$query = sybase_query("declare @echo char(1) exec i_subdireccion '$sub_name', @echo output select @echo as echo");
					$mensaje = sybase_get_last_message();
				} else {
					$mensaje = "No se pudo Insertar la Subdirecci&oacute;n por Falta de Informaci&oacute;n";
				}
			}
			if($acciond == "insert"){
				if($dep_name!="" && $sub_id!='-1'){
					$query = sybase_query("declare @echo char(1) exec i_departamento '$dep_name', '$sub_id', @echo output select @echo as echo");
					$mensaje = sybase_get_last_message();
				} else {
					$mensaje = "No se pudo Insertar el Departamento por Falta de Informaci&oacute;n";
				}
			}
			if($accionp == "insert"){
				if($pto_name!="" && $dep_id!='-1' && $subdir=='-1'){
					$query = sybase_query("declare @echo char(1) exec i_puesto '$pto_name', '$dep_id', @echo output select @echo as echo");
					$mensaje = sybase_get_last_message();
				} else if($pto_name!="" && $dep_id=='-1' && $subdir!='-1'){
					$query = sybase_query("declare @echo char(1) exec i_puesto_sub '$pto_name', '$subdir', @echo output select @echo as echo");
					$mensaje = sybase_get_last_message();
				}else{
					$mensaje = "No se pudo Insertar el Puesto por Falta de Informaci&oacute;n";
				}
			}
		}
		
		if($accions == "update" || $acciond == "update" || $accionp == "update"){
			if($accions == "update"){
				if($sub_name!=""){
					$query = sybase_query("update cp_subdireccion set descripcion='$sub_name' where idsub='$subid'");
					$mensaje = "Subdirección Actualizado CORRECTAMENTE";
				} else {
					$mensaje = "No se pudo Actualizar la Subdirecci&oacute;n por Falta de Informaci&oacute;n";
				}
			}
			if($acciond == "update"){
				if($dep_name!="" && $sub_id!='-1'){
					$query = sybase_query("update cp_departamento set descripcion='$dep_name', idsub='$sub_id' where iddepto='$depid'");
					$mensaje = "Departamento Actualizado CORRECTAMENTE";
				} else {
					$mensaje = "No se pudo Actualizar el Departamento por Falta de Informaci&oacute;n";
				}
			}
			if($accionp == "update"){
				if($pto_name!="" && $dep_id!='-1' && $subdir=='-1'){
					$query = sybase_query("update cp_puesto set descripcion='$pto_name', iddepto='$dep_id' where idpuesto='$ptoid'");
					$mensaje = "Puesto Actualizado CORRECTAMENTE";
				} else if($pto_name!="" && $dep_id=='-1' && $subdir!='-1'){
					$query = sybase_query("update cp_puesto set descripcion='$pto_name', idsub='$subdir' where idpuesto='$ptoid'");
					$mensaje = "Puesto Actualizado CORRECTAMENTE";
				}else{
					$mensaje = "No se pudo Actualizar el Puesto por Falta de Informaci&oacute;n";
				}
				
			}
			
		}
		
		if($accions == "delete" || $acciond == "delete" || $accionp == "delete"){
			if($accions == "delete"){
				$query = sybase_query("update cp_subdireccion set estado='0' where idsub='$subid'");
				$mensaje = "Subdirecci&oacute;n Eliminado CORRECTAMENTE";
			}
			if($acciond == "delete"){
				$query = sybase_query("update cp_departamento set estado='0' where iddepto='$depid'");
				$mensaje = "Departamento Eliminado CORRECTAMENTE";
			}
			if($accionp == "delete"){
				$query = sybase_query("update cp_puesto set estado='0' where idpuesto='$ptoid'");
				$mensaje = "Puesto Eliminado CORRECTAMENTE";
			}
		}
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
