<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);

/*
 * ime_otros.php
 * 
 * Copyright 2013 Sybase Admin <sybase@localhost>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 
if ($_GET["accion"]){
	$accion=$_GET["accion"];
	$regresar=$_GET["regresar"];
	
	$bar_name = $_GET["barr0"];

	$bar_id = $_GET["idbar"];
	
	$barrera = $_GET["barrera"];
	
		if($accion == "insert"){
			if($emp_name!=""){
				$query = sybase_query("declare @echo char(1) i_barreras_gral '$bar_name', @echo output select @echo as echo");
				$mensaje = sybase_get_last_message();				
			} else {
				$mensaje = "No se pudo Insertar datos de Barreras por falta de informaci&oacute;n";
			}
		}
		if($accion == "update"){
			if($emp_id!=""){
				$query = sybase_query("update cp_barreras_personal set descripcion='$bar_name' where idempresa=$bar_id ");	
				$mensaje = "Datos de Barreras Actualizado CORRECTAMENTE";
			} else {
				$mensaje = "No se pudieron Actualizar los datos de Barreras por falta de informaci&oacute;n";
			}
		}
		if($accion == "delete"){
			$query = sybase_query("update cp_barreras_personal set estado='0' where idbarrera=$idbarrera ");
			$mensaje = "Datos de Barreras Eliminado CORRECTAMENTE";
		}
		
		echo '<script type="text/javascript">';
		echo 'alert("'.$mensaje.'");';
		echo $regresar;
		echo '</script>';
}


?>
