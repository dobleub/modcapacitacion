<?php
	/********************************************************
		Administración de las Capacidades/Cursos

		Desarrollado por: Osorio Salinas Edward
		Instituto Tecnológico de Tlaxiaco
		Abril de 2013
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	//MIIIIII__  permiso_sobre_funcion($_SESSION['susr'], 57);
	seguridad('DRH'); 
	$web->Seguridad($_SESSION['susr'],8);
	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."capacitacion/instructor_admin.php'";
	$regresarprev = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	
	if($_GET['capid']){
		$capid = $_GET['capid'];
		$captit = $_GET['captit'];
		$capname = $_GET['capname'];
		$capdir = $_GET['capdir'];
		$capmail = $_GET['capmail'];
		$captel = $_GET['captel'];
		$empid = $_GET['empid'];
		$empdes = $_GET['empdes'];
		
		$accion = "update";
	} else {
		$accion = "insert";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<link rel="stylesheet" type="text/css" href="css/estilo_somebuttons.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<script type="text/javascript" src="js/funciones_capacidades.js"></script>
		
		<title>.:: Administrar Instructor ::.</title>
	</head>

<body>
	
	<h2 align="center">Administraci&oacute;n del Personal Instructor del Curso</h2>
	
<?php
	if($accion == "update"){
		$titulo = '<h3 align="center">ACTUALIZAR INSTRUCTOR</h3>';
		$form = '<form name="u_instructor" id="u_instructor" method="get" action="ime_instructor.php" onSubmit="return validarcampos_ui()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Actualizar" tabindex="1" title="Actualizar datos de INSTRUCTOR"/>';
	} else {
		$titulo = '<h3 align="center">INGRESAR INSTRUCTOR</h3>';
		$form = '<form name="i_instructor" id="i_instructor" method="get" action="ime_instructor.php" onSubmit="return validarcampos_ii()" >';
		$button = '<input type="submit" class="boton" name="Submit" value="Aceptar" tabindex="1" title="Insertar datos de INSTRUCTOR"/>';
	}

?>

  <!-- Contenido -->
 <?php
	echo '<br/>';
	echo $titulo;
	
	echo $form;
 ?>	
		<table align="center" title="Por favor rellene los siguientes datos para dar de alta un nuevo Instructor disponible">
			<tr> 
				<th width="100"> Nombre: </th>
				<td id="non">
					<select name="inst0" title="Seleccione el t&iacute;tulo que ostenta el Instructor">
						<?php
							$consulta="select distinct(titulo) from cp_instructor_cap";
							/*echo $consulta;*/
							$datos=ejecutar_sql($consulta);

							if(!$datos->rowcount())	echo "Titulos Faltantes";
							else { 
								if($captit){
									while(!$datos->EOF){
										if($captit == $datos->fields('titulo'))
											echo '<option value="'.$datos->fields('titulo').'" selected>'.$datos->fields('titulo').'</option>';
										else
											echo '<option value="'.$datos->fields('titulo').'">'.$datos->fields('titulo').'</option>';
										$datos->MoveNext();
									}
								} else {
									echo '<option value="Ing.">Ing.</option>';
									echo '<option value="C." selected>C.</option>';
									echo '<option value="Lic.">Lic.</option>';
									echo '<option value="Arq.">Arq.</option>';
									echo '<option value="Cont.">Cont.</option>';
									echo '<option value="Q.B.">Q.B.</option>';
								}
							}
							
							
						?>
					</select>
					<input name="inst1" type="text" size="50" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $capname ?>" title="Ingrese el NOMBRE COMPLETO de Instructor">
				</td>
			</tr>
			<tr>
				<th> Direcci&oacute;n: </th>
				<td id="non">
				<input name="inst2" type="text" size="90" maxlength="250" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $capdir ?>" title="">
				</td>
			</tr>
			<tr>
				<th> e-mail: </th>
				<td id="non">
				<input name="inst3" type="text" size="30" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $capmail ?>" title="">
				</td>
			</tr>
			<tr>
				<th> Tel&eacute;fono: </th>
				<td id="non">
				<input name="inst4" type="text" size="15" maxlength="150" tabindex="0" onblur="this.value = this.value.toUpperCase()" value="<?php echo $captel ?>" title="">
				</td>
			</tr>
			<tr>
				<th> Empresa: </th>
				<td id="non">
					<select name="inst5" title="Seleccione la empresa de donde proviene el Instructor">
						<?php
							$consulta="select idempresa,descripcion from cp_instructor_emp where estado = '1'";
							/*echo $consulta;*/
							$datos=ejecutar_sql($consulta);

							if(!$datos->rowcount())	echo "Datos de Empresas faltantes";
							else {
								if($empid){
									while(!$datos->EOF){
										if($empid == $datos->fields('idempresa'))
											echo '<option value="'.$datos->fields('idempresa').'" selected>'.$datos->fields('descripcion').'</option>';
										else 
											echo '<option value="'.$datos->fields('idempresa').'">'.$datos->fields('descripcion').'</option>';
										$datos->MoveNext();
									}
								} else {
									echo "<option value='-1' selected> - Selecciona Empresa - </option>";
									while(!$datos->EOF){
										echo '<option value="'.$datos->fields('idempresa').'">'.$datos->fields('descripcion').'</option>';
										$datos->MoveNext();
									}
								}
							}
					?>
					</select>
				</td>
			</tr>
		</table>
	<br />
		<div align="center">
			<input type="hidden" name="ins_id" value="<?php echo $capid; ?>">
			<input type="hidden" name="accion" value="<?php echo $accion; ?>">
			<input type="hidden" name="regresar" value="<?php echo $regresar; ?>">
			<?php
				echo $button;
			?>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Cancelar la operaci&oacute;n"/>
		</div>
	</form>
	
	<br/>
	<br/>
	<br/>
	<h3 align="center" style="background-color: #EFEFEF; padding: 3px 3px 5px">MODIFICAR / ELIMINAR INSTRUCTOR</h3>
	<form name="m_instructor" id="myForm" method="get" action="" onSubmit="return validarcampos()" >
		<table align="center" title="Modificar / Eliminar un Instructor de la lista de Instructores disponibles">
			<tr>
				<th>Nombre del Instructor</th>
				<th>Empresa</th>
				<th colspan="2">Accion</th>
			</tr>
			<?php
			$consulta="select c.idinstructor, c.titulo, c.nombre, c.direccion, c.email, c.telefono, e.idempresa, e.descripcion from cp_instructor_cap as c, cp_instructor_emp as e where c.idempresa = e.idempresa and c.estado='1' and e.estado='1' ";
				/*echo $consulta;*/
				$datos=ejecutar_sql($consulta);

				if(!$datos->rowcount())	echo "Datos de Instructor Faltantes";
				else { 
					while(!$datos->EOF){
					echo '<form name="m_ins" id="m_ins" method="get" action="" onSubmit="return validarcampos()" >';
						echo '<tr> <td id="non">'.$datos->fields('titulo').' '.$datos->fields('nombre').'</td>';
						echo '<td id="non">'.$datos->fields('descripcion').' </td>';
						echo '<td id="non"> <center> <input type="Submit" class="button_mod" value="" title = "Editar"/> </center></td>';
						echo '<input type="hidden" name="capid" value="'.$datos->fields('idinstructor').'">';
						echo '<input type="hidden" name="captit" value="'.$datos->fields('titulo').'">';
						echo '<input type="hidden" name="capname" value="'.$datos->fields('nombre').'">';
						echo '<input type="hidden" name="capdir" value="'.$datos->fields('direccion').'">';
						echo '<input type="hidden" name="capmail" value="'.$datos->fields('email').'">';
						echo '<input type="hidden" name="captel" value="'.$datos->fields('telefono').'">';
						echo '<input type="hidden" name="empid" value="'.$datos->fields('idempresa').'">';
						echo '<input type="hidden" name="empdes" value="'.$datos->fields('descripcion').'">';
					echo '</form>';
					echo '<form name="e_ins" id="e_ins" method="get" action="ime_instructor.php" onSubmit="return validarcampos()" >';
						echo '<td id="non"> <center> <input type="Submit" class="button_del" value="" title = "Eliminar"/> </center></td>';	
						echo '<input type="hidden" name="instructor" value="'.$datos->fields('idinstructor').'">';
						echo '<input type="hidden" name="accion" value="delete">';
						echo '<input type="hidden" name="regresar" value="'.$regresar.'">';
					echo '</form>';
					echo '</tr>';
					$datos->MoveNext();
					}
				}
			?>
	</form>

  <!-- Contenido -->
	<form name="personal" method="post" >
	  <br />
		<div align="center">
		  <input type="button" value="Regresar a Inicio" class="boton" tabindex="5" onClick="<?php echo $regresarprev; ?>" title="Regresar a la pagina de Inicio" />
	  </div>
	</form>
</body>
</html>
