<?php
	/********************************************************
		Procesar Documentos del Empleado para guardarlos
		http://pixelar.me/subir-imagenes-php/
		http://www.peruhardware.net/foros/showthread.php?t=16479
		http://cyberexplorador.wordpress.com/2010/08/26/anadir-imagenes-a-una-base-de-datos-y-cargarlas-usando-php-y-mysql/
		http://www.desarrolloweb.com/articulos/1307.php

		http://php.net/manual/es/function.date.php
		http://www.paraisogeek.com/la-funcion-date-en-php/
		http://www.holamundo.es/lenguaje/php/articulos/fecha-hora-php.html

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2012
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione el documento del Empleado ::.</title>
	</head>
<body>
	<h3 align="center"> Por favor espere a que termine esta operaci&oacute;n </h3>
<?php
	$rfc = $_POST['rfc_personal'];
	$id_doc = $_POST['num_documento'];
	$num_doc1 = $_POST['tipo_de_documento_1'];
	$num_doc2 = $_POST['tipo_de_documento_2'];

	$regresar = "javascript: document.location = 'seleccione_documentacion.php?rfc_personal=$rfc'"; // document.location = 'seleccione_personal.php'

		// COMPROBAR SI EXISTE UN REGISTRO GENERAL PARA ESTE DOCUMENTO

		$query_exp_gral = "select id_expediente_gral from expediente_general where rfc = '$rfc' and id_documento = $id_doc";
		$res_exp_gral = ejecutar_sql($query_exp_gral);
		$id_expediente_gral = $res_exp_gral -> fields('id_expediente_gral');

		// SINO EXISTE SE CREA UN REGISTRO GENERAL

		if ($id_expediente_gral == null){
			$consulta_max= "select count(id_expediente_gral) as id_maximo from expediente_general";
			$sql_consulta_max = ejecutar_sql($consulta_max);
			$id_expediente_gral_max = $sql_consulta_max -> fields('id_maximo') + 1;

			$insert_exp_gral = "insert into expediente_general (id_expediente_gral, id_documento, rfc, total_de_documentos) values ($id_expediente_gral_max, $id_doc, '$rfc', 0)";
			$resp_insert_exp_gral = ejecutar_sql($insert_exp_gral);

			}

		// TENIENDO UN REGISTRO GENERAL

		$query_exp_gral = "select id_expediente_gral, total_de_documentos from expediente_general where rfc = '$rfc' and id_documento = $id_doc";
		$res_exp_gral = ejecutar_sql($query_exp_gral);
		$id_expediente_gral = $res_exp_gral -> fields('id_expediente_gral');
		$total_de_documentos = $res_exp_gral -> fields('total_de_documentos') + 1;
		
		// SE DEBE CALCULAR CUANTOS DOCUMENTOS DE ESTE TIPO HAY PARA ASIGNAR EL PROXIMO Y NOMBRAR EL NUEVO

            if ($id_doc ==  1) $destino = "se";
            if ($id_doc ==  2) $destino = "an";
            if ($id_doc ==  3) $destino = "rpf";
            if ($id_doc ==  4) $destino = "csmn";
            if ($id_doc ==  5) $destino = "cem";
            if ($id_doc ==  6) $destino = "rfc";
            if ($id_doc ==  7) $destino = "curp";
            if ($id_doc ==  8) $destino = "cni";
            if ($id_doc ==  9) $destino = "tocpoce";
            if ($id_doc == 10) $destino = "hus";
            if ($id_doc == 11) $destino = "ardsp";
            if ($id_doc == 12) $destino = "ext";
            if ($id_doc == 13) $destino = "cn";
            if ($id_doc == 14) $destino = "fdb";
            if ($id_doc == 15) $destino = "saroforte";
            if ($id_doc == 16) $destino = "ot";
            if ($id_doc == 17) $destino = "cr";
            if ($id_doc == 18) $destino = "ha";

		$fecha = date('d/m/Y');
 		$hora = date('h:i:s a');

		$ruta = "documentos/$destino/";
		// Calcular el nombre del nuevo documento
		//$query_cant_exp_gral = "select count(id_expediente_gral) as cantidad from expediente_detallado where id_expediente_gral = $id_expediente_gral";
		//$res_cant_exp_gral = ejecutar_sql($query_cant_exp_gral);
		//$cant_expediente_gral = $res_cant_exp_gral -> fields('cantidad') + 1;
		$_1_parte_nombre =(($total_de_documentos < 10)?"0":"");

		$nombre_archivo = $_1_parte_nombre.$total_de_documentos."-".$rfc."-".$destino.".jpg";
		$tipo_archivo = "jpg";

		//INSERTAR EL NUEVO DOCUMENTO
		$consulta_max_detalle= "select max(id_expediente_detalle) as id_maximo from expediente_detallado";
        	$sql_consulta_max_detalle = ejecutar_sql($consulta_max_detalle);
		$id_expediente_detalle_max = $sql_consulta_max_detalle -> fields('id_maximo') + 1;

		//AnalizaR los documentos 9 y 15
            if ($id_doc ==  9 and $num_doc1 > 0) $documento_especifico = $num_doc1;
	    else if ($id_doc == 15 and $num_doc2 > 0) $documento_especifico = $num_doc2;
		else $documento_especifico = 0;

	// SECCIÓN PARA CARGAR LOS DOCUMENTOS
		if (is_uploaded_file($_FILES['doc']['tmp_name']))
			{
	    		// Recibo los datos de la imagen
	    		$nombre = $_FILES['doc']['name'];
	    		$tipo = $_FILES['doc']['type'];
	    		$tamano = $_FILES['doc']['size'];

			$doc_ext = strrchr($_FILES['doc']['name'], '.'); //--//

			//revisar que sea jpg 
			if (($_FILES['doc']['type'] == "image/jpeg" || $_FILES['doc']['type'] == "image/pjpeg" || $doc_ext == ".pdf"  ) &&  ($tamano > 10 and $tamano < 1800000) ) // 340000  3.4 MB 8444= 8.2 KB
				{ 

			// (strpos($tipo, "pdf"))

				if ( $doc_ext == ".pdf" ){  // strpos($tipo, "pdf")
					$nombre_archivo = $_1_parte_nombre.$total_de_documentos."-".$rfc."-".$destino.".pdf";
					$tipo_archivo = "pdf";
					}
				
				//nuevo nombre para la imagen
				$nuevoNombre = $nombre_archivo;
						

	    			// Muevo la imagen desde su ubicación
	    			// temporal al directorio definitivo
				move_uploaded_file($_FILES['doc']['tmp_name'], "documentos/$destino/$nuevoNombre");

				// Despues de cargar el archivo se guarda el nombre en la base
 				$insert_exp_detalle = "insert into expediente_detallado (id_expediente_detalle, id_expediente_gral, ruta, nombre_documento, tipo, documento_especifico, f_actualizacion, hora) values ($id_expediente_detalle_max, $id_expediente_gral, '$ruta', '$nombre_archivo', '$tipo_archivo', $documento_especifico, '$fecha', '$hora')";
				$resp_insert_exp_detalle = ejecutar_sql($insert_exp_detalle);

				//Actualizo el total de documentos de este tipo y para este trabajador
				$update_total_docs="update expediente_general set total_de_documentos = $total_de_documentos where rfc = '$rfc' and id_documento = $id_doc";
				$update_total_documentos=ejecutar_sql($update_total_docs);

				// Ejemplos del formato del nombre de los documentos		 documentos/saroforte/01-CUSM89090122-saroforte.jpg
				} 
			else
				{
				?>
				<script language="javascript" type="text/javascript">
					msg = "<?php echo 'Formato no valido para fichero de imagen'; ?>"
					alert(msg)
					<?php echo $regresar; ?>
				</script>
				<?php
				}
			} 
		else 
			{
			$msg = "Error al cargar archivo: " . $_FILES['doc']['name'];
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo $msg; ?>"
				alert(msg)
				<?php echo $regresar; ?>
			</script>
			<?php
			}

	$msg = "El archivo ha sido cargado correctamente"; 
?>

<script language='javascript' type='text/javascript'>
	msg = "<?php echo $msg; ?>"
	alert(msg)
	<?php echo $regresar; ?>
</script>
 
</body>
</html>
