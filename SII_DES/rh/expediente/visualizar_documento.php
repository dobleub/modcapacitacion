<?php
	/********************************************************
		Visualizar Documento

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Visualizar el documento del Empleado ::.</title>
	</head>
<body>

	<?php   
	$id_expediente_detalle = $_GET['var'];
	// Obtener el ID padre					tipo_archivo
		$select_id_gral="select id_expediente_gral, nombre_documento, documento_especifico, ruta from expediente_detallado where id_expediente_detalle = $id_expediente_detalle";
		$consulta_id_gral=ejecutar_sql($select_id_gral);
		$id_gral = $consulta_id_gral->fields('id_expediente_gral');
		$nombre_documento = $consulta_id_gral->fields('nombre_documento');
		$documento_especifico = $consulta_id_gral->fields('documento_especifico');
		$ruta = $consulta_id_gral->fields('ruta');

		$select_rfc_id="select rfc, id_documento from expediente_general where id_expediente_gral = $id_gral";
		$consulta_rfc_id=ejecutar_sql($select_rfc_id);
		$rfc = $consulta_rfc_id->fields('rfc');
		$id_documento = $consulta_rfc_id->fields('id_documento');

		//Documentos 9 y 15
		if ($id_doc ==  9){
			if ($documento_especifico == 1) $_aux_titulo = 'T&iacute;tulo';
			if ($documento_especifico == 2) $_aux_titulo = 'C&eacute;dula Profesional';
			if ($documento_especifico == 3) $_aux_titulo = 'Certificado de Estudios';
		}
		if ($id_doc ==  15){
			if ($documento_especifico == 1) $_aux_titulo = 'SAR';
			if ($documento_especifico == 2) $_aux_titulo = 'FORTE';
		}

		$select_datos = "select apellidos_empleado, nombre_empleado from personal where rfc ='$rfc'";
		$consulta_datos = ejecutar_sql($select_datos);
		$nombre_empleado = $consulta_datos->fields('apellidos_empleado').' '.$consulta_datos->fields('nombre_empleado');

	//$foto = "<img src='$ruta$nombre_documento' width='800' height='1095' align='center'>"; // FORMATO TAMAÑO CARTA
	$foto = "<embed src='$ruta$nombre_documento' width='800' height='1095' align='center'>"; // FORMATO TAMAÑO CARTA  #toolbar=0&navpanes=0&scrollbar=0  1391   1800

	?>
	<h2 align="center"> Documentaci&oacute;n del Empleado </h2>
	<h2 align="center"> <?php echo $nombre_empleado.' - '.$rfc; ?> </h2>

		<table align="center" border=0>
			<tr align="center"> <th>Ver: </th> </tr>

<?php if ( $id_doc == 9 or $id_doc == 15 ) {?>
			<tr align="center"> <th><?php echo $_aux_titulo; ?> </th> </tr>
<?php } ?>
			<tr align="center"> <td><?php echo $nombre_documento; ?></td> </tr>
			<tr align="center"> <th align="center"> <?php echo $foto; ?> </td></tr>
		</table>
	<br><br />

	<form name="personal" method="post" action='documento_detalle.php'>
		<input type="hidden" name="rfc" value="<? echo $rfc;?>"/>
		<input type="hidden" name="documento_personal" value="<? echo $id_documento;?>"/>
	   <div align="center">
		<input type="submit" name="Cancdelar" class="boton" value="Cancelar" title="Presione este bot&oacute;n para guardar los cambios">
	   </div>
	</form>
</body>
</html>
