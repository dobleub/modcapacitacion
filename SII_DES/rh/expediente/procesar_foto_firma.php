<?php
	/********************************************************
		Procesar Fotografía/Firma del Empleado
		http://pixelar.me/subir-imagenes-php/
		http://www.peruhardware.net/foros/showthread.php?t=16479
		http://cyberexplorador.wordpress.com/2010/08/26/anadir-imagenes-a-una-base-de-datos-y-cargarlas-usando-php-y-mysql/
		http://www.desarrolloweb.com/articulos/1307.php
		http://omarvasquezb.wordpress.com/2009/11/19/funcion-javascript-para-cerrar-la-ventana-actual-para-ie678-google-chrome-menos-firefox/
		http://emilio.aesinformatica.com/2007/05/03/subir-una-imagen-con-php/

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione la imagen del Empleado ::.</title>
	</head>
<body>
	<h2 align="center"> Por favor espere a que termine esta operaci&oacute;n </h2>
<?php
	// Seccion para cargar foto
	if ($_POST['flag'] == 'foto')
		if (is_uploaded_file($_FILES['foto']['tmp_name']))
			{
	    		// Recibo los datos de la imagen
	    		$nombre = $_FILES['foto']['name'];
	    		$tipo = $_FILES['foto']['type'];
	    		$tamano = $_FILES['foto']['size'];

			//revisar que sea jpg 
			if (($_FILES['foto']['type'] == "image/jpeg" || $_FILES['foto']['type'] == "image/pjpeg") &&  ($tamano > 10 and $tamano < 340000) ) // 3.4 MB 8444= 8.2 KB
				{ 
				//nuevo nombre para la imagen
				$nuevoNombre = $_POST['rfc'].".jpg";
	    			// Muevo la imagen desde su ubicación
	    			// temporal al directorio definitivo
				move_uploaded_file($_FILES['foto']['tmp_name'], "documentos/fotos/personal/$nuevoNombre");
				// Actualizar la bandera que indica si hay o no imagen de este trabajador
				$update_status_foto = "update personal set bandera_foto = 1  where rfc = '$rfc'";
				$update_el_status_de_foto = ejecutar_sql($update_status_foto);
				} 
			else	{
				?>
				<script language="javascript" type="text/javascript">
					msg = "<?php echo 'Formato no valido para fichero de imagen'; ?>"
					alert(msg)
					window.close();
				</script>
				<?php
				}
			} 
		else	{
			$msg = "Error al cargar archivo: " . $_FILES['foto']['name'];
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo $msg; ?>"
				alert(msg)
				window.close();	
			</script>
			<?php
			}

	//Sección para cargar firma
	if ($_POST['flag'] == 'firma')
		if (is_uploaded_file($_FILES['firma']['tmp_name']))
			{
	    		// Recibo los datos de la imagen
	    		$nombre = $_FILES['firma']['name'];
	    		$tipo = $_FILES['firma']['type'];
	    		$tamano = $_FILES['firma']['size'];

			//revisar que sea jpg 
			if (($_FILES['firma']['type'] == "image/jpeg" || $_FILES['firma']['type'] == "image/pjpeg") &&  ($tamano > 10 and $tamano < 340000) ) // 3.4 MB 8444= 8.2 KB
				{ 
				//nuevo nombre para la imagen
				$nuevoNombre = $_POST['rfc'].".jpg";
	    			// Muevo la imagen desde su ubicación
	    			// temporal al directorio definitivo
				move_uploaded_file($_FILES['firma']['tmp_name'], "documentos/firmas/personal/$nuevoNombre");
				// Actualizar la bandera que indica si hay o no imagen de este trabajador
				$update_status_firma = "update personal set bandera_firma = 1  where rfc = '$rfc'";
				$update_el_status_de_firma = ejecutar_sql($update_status_firma);
				} 
			else	{
				?>
				<script language="javascript" type="text/javascript">
					msg = "<?php echo 'Formato no valido para fichero de imagen'; ?>"
					alert(msg)
					window.close();
				</script>
				<?php
				}
			} 
		else	{
			$msg = "Error al cargar archivo: " . $_FILES['firma']['name'];
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo $msg; ?>"
				alert(msg)
				window.close();	
			</script>
			<?php
			}

	$msg = "El archivo ha sido cargado correctamente"; 
?>

<script language="javascript" type="text/javascript">
	msg = '<?php echo $msg; ?>'
	alert(msg)
	window.close();
</script>
 
</body>
</html>
