<?php
	/********************************************************
		Seleccione Documentaci+on

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione el documento del Empleado ::.</title>
	</head>
<body>

	<?php  
 		if ($_GET['rfc_personal'] == null)
		$rfc = $_POST['rfc_personal'];
		else
		$rfc = $_GET['rfc_personal'];


		$qry_nombre="select apellidos_empleado, nombre_empleado from personal where rfc='$rfc'";
		$res_nombre=ejecutar_sql($qry_nombre);
		$nombre = $res_nombre->fields('nombre_empleado').' '.$res_nombre->fields('apellidos_empleado');

		$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";
	?>
	<h2 align="center"> Documentaci&oacute;n que puede consultar/Actualizar a: <? echo $nombre; ?></h2>

	<form name="solicitud_ver" method="post" action="documento_detalle.php">
	<h3 align="center"> Seleccione el Documento: </h3>
	<table width="30%" align="center" border=0 >
		<tr align="center"> <th>Ver: </th> </tr>
		<tr align="center">
		  <th>
			<select name="documento_personal" title="Seleccione el documento que le interesa" onChange="javascript:document.solicitud_ver.submit();">
			<?php
			$consulta_documento="select id_documento, descripcion from documentos order by id_documento";
			$datos_documento=ejecutar_sql($consulta_documento);
			if(!$datos_documento->rowcount()) echo "<option value='0' selected> No hay documentos dados de alta </option>"; 
			else
			   { echo "<option value='0' selected> -- Seleccione Documento -- </option>";
			     while(!$datos_documento->EOF){
				echo '<option value="'.$datos_documento->fields('id_documento').'">'.$datos_documento->fields('descripcion').'</option>';
				$datos_documento->MoveNext();
				}
			   }
			?>
			</select>
		   </th>
		</tr>
	</table>
	<br><br><br><br>
		    <div align="center"><i> * Domumentaci&oacute;n Obligatoria <i></div>
	<br>
           <input type="hidden" name="rfc" value="<? echo $rfc;?>"/>
	</form>

	<form name="personal" method="post" >
		<br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
		</div>
	</form>
</body>
</html>
