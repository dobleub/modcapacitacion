<?php
	/********************************************************
		Cargar Firma del Empleado

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Actulizar Firma del Empleado ::.</title>
	</head>
<body>

	<?php   $rfc = $_GET['var'];
		$regresar = "javascript: window.close();";
		$nombre_firma = $rfc.'.jpg';
	//Firma
	$qry_firma = "select bandera_firma from personal where rfc = '$rfc'";
	$res_firma = ejecutar_sql($qry_firma);
	$flag_signature = $res_firma->fields('bandera_firma');

	if ($flag_signature == 0)	$archivo_firma = "documentos/firmas/personal/sin_firma.jpg";
	if ($flag_signature == 1)	$archivo_firma = "documentos/firmas/personal/$rfc.jpg";

		$firma = "<img src='$archivo_firma' width='100' height='125' align='center'>";
	?>

	<h2 align="center"> Firma del Empleado </h2>

	<form action="procesar_foto_firma.php" enctype="multipart/form-data" method="post">

		<table width="30%" align="center" border=1>
			<tr align="center"> <th>Examine el Archivo: </th> </tr>
			<tr align="center"><th><input name="firma" type="file" id="imagen"  title="Examine archivo" size="100" /></th></tr>
		</table>

		<input name="rfc" type="hidden" value="<?php echo $rfc;?>" />
		<input name="flag" type="hidden" value="firma" />

		<div align="center">		
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
			<input name="submit" type="submit" class="boton" value="Guardar" /> 
		</div>
	</form>

	<table width="125px" align="center">
		<tr align="center"> <th>Firma Actual: </th> </tr>
		<tr align="center">
			<th width="9%"> <?php echo $firma; ?> </td>
		</tr>
	</table>

</body>
</html>
