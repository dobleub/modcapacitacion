<?php
	/********************************************************
		Cargar Fotografía del Empleado

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Actulizar Imagen del Empleado ::.</title>
	</head>
<body>

	<?php   $rfc = $_GET['var'];
		$regresar = "javascript: window.close();";
		$nombre_foto = $rfc.'.jpg';
	//Foto
	$qry_foto = "select bandera_foto from personal where rfc = '$rfc'";
	$res_foto = ejecutar_sql($qry_foto);
	$flag_photo = $res_foto->fields('bandera_foto');

	if ($flag_photo == 0)	$archivo_foto = "documentos/fotos/personal/sin_foto.jpg";
	if ($flag_photo == 1)	$archivo_foto = "documentos/fotos/personal/$rfc.jpg";				

	$foto = "<img src='$archivo_foto' width='100' height='125' align='center'>";
	?>

	<h2 align="center"> Fotograf&iacute;a del Empleado </h2>

	<form action="procesar_foto_firma.php" enctype="multipart/form-data" method="post">

		<table width="30%" align="center" border=1>
			<tr align="center"> <th>Examine el Archivo: </th> </tr>
			<tr align="center"><th><input name="foto" type="file" id="imagen"  title="Examine archivo" size="100" /></th></tr>
		</table>

		<input name="rfc" type="hidden" value="<?php echo $rfc;?>" />
		<input name="flag" type="hidden" value="foto" />

		<div align="center">		
			<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
			<input name="submit" type="submit" class="boton" value="Guardar" /> 
		</div>
	</form>

	<table width="125px" align="center">
		<tr align="center"> <th>Fotograf&iacute;a Actual: </th> </tr>
		<tr align="center">
			<th width="9%"> <?php echo $foto; ?> </td>
		</tr>
	</table>

</body>
</html>
