<?php
	/********************************************************
		Documento Detalle

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 

	//Función Fecha
        function formato_de_fecha($fecha)  // Ejemplo: Recibe $fecha = '21/02/2011'
          {     if ($fecha[3]==0 and $fecha[4]==1) $var="Enero";
		else if ($fecha[3]==0 and $fecha[4]==2) $var="Febrero";
		else if ($fecha[3]==0 and $fecha[4]==3) $var="Marzo";
		else if ($fecha[3]==0 and $fecha[4]==4) $var="Abril";
		else if ($fecha[3]==0 and $fecha[4]==5) $var="Mayo";
		else if ($fecha[3]==0 and $fecha[4]==6) $var="Junio";
		else if ($fecha[3]==0 and $fecha[4]==7) $var="Julio";
		else if ($fecha[3]==0 and $fecha[4]==8) $var="Agosto";
		else if ($fecha[3]==0 and $fecha[4]==9) $var="Septiembre";
		else if ($fecha[3]==1 and $fecha[4]==0) $var="Octubre";
		else if ($fecha[3]==1 and $fecha[4]==1) $var="Noviembre";
		else if ($fecha[3]==1 and $fecha[4]==2) $var="Diciembre";
		return $fecha[0].$fecha[1]." de ".$var." de ".$fecha[6].$fecha[7].$fecha[8].$fecha[9];		// Ejemplo:   11 de Marzo del 2011 
          }
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione el documento del Empleado ::.</title>
	</head>
<body>

	<?php   $rfc = $_POST['rfc'];
		$id_doc = $_POST['documento_personal'];

			$select_datos = "select apellidos_empleado, nombre_empleado from personal where rfc ='$rfc'";
			$consulta_datos = ejecutar_sql($select_datos);
			$nombre_empleado = $consulta_datos->fields('apellidos_empleado').' '.$consulta_datos->fields('nombre_empleado');

		$regresar = "javascript: document.location = 'seleccione_documentacion.php?rfc_personal=$rfc'"; // seleccione_personal la
	?>
	<h2 align="center"> Documentaci&oacute;n del Empleado </h2>
	<h2 align="center"> <?php echo $nombre_empleado." - ".$rfc; ?> </h2>

	<form name="solicitud_ver" method="post" action="procesar_documentos.php" enctype="multipart/form-data">
	<h3 align="center"> &Aacute;rea para adjuntar el documento: </h3>
	<table width="30%" align="center" border=0>
		<tr align="center"> <th> Examine el Archivo: </th> </tr>
		<tr align="center"><th><input name="doc" type="file"  title="Examine archivo" size=2 /></th></tr>
	<? if ($id_doc == 9) {?>
		<tr><th> Elija:</td></tr>
		</tr>
			<th> 
				<select name="tipo_de_documento_1">
				  <option value="0">-- Seleccione Documento a adjuntar --</option>
				  <option value="1">T&iacute;tulo</option>
				  <option value="2">C&eacute;dula Profesional</option>
				  <option value="3">Certificado de Estudios</option>
				</select> 
			</td>
		</tr>
	<? } 
	 if ($id_doc == 15) {?>
		<tr><th> Elija:</td></tr>
		</tr>
			<th> 
				<select name="tipo_de_documento_2">
				  <option value="0">-- Seleccione Documento a adjuntar --</option>
				  <option value="1">SAR</option>
				  <option value="2">FORTE</option>
				</select> 
			</td>
		</tr>
	<? } ?>
                <input type="hidden" name="rfc_personal" value="<? echo $rfc;?>"/>
                <input type="hidden" name="num_documento" value="<? echo $id_doc;?>"/>
	</table>
	<br><br>
	<div align="center">
		<input type="submit" name="Guardar" class="boton" value="Guardar" title="Presione este bot&oacute;n para guardar los cambios"/>
		<input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n"/>
	</div>
	</form>

	<br><br>
	<?php
            if ($id_doc ==  1) $destino = "Solicitud de Empleo";
            if ($id_doc ==  2) $destino = "Acta de Nacimiento (Copia Fotost&aacute;tica)";
            if ($id_doc ==  3) $destino = "Registro de Personal Federal";
            if ($id_doc ==  4) $destino = "Cartilla del Servicio Militar Nacional Liberada (Copia Fotost&aacute;tica)";
            if ($id_doc ==  5) $destino = "Certificado de Examen M&eacute;dico";
            if ($id_doc ==  6) $destino = "Registro Federal del Contribuyente";
            if ($id_doc ==  7) $destino = "Clave &Uacute;nica de Registro de Poblaci&oacute;n";
            if ($id_doc ==  8) $destino = "Constancia de no Inhabilitaci&oacute;n";
            if ($id_doc ==  9) $destino = "T&iacute;tulo o C&eacute;dula Profesional o Certificado de Estudios (Copia Fotost&aacute;tica)";
            if ($id_doc == 10) $destino = "Hoja &Uacute;nica de Servicios";
            if ($id_doc == 11) $destino = "Acuse de recibo de la declaraci&oacute;n de situaci&oacute;n Patrimonial (Copia Fotost&aacute;tica)";
            if ($id_doc == 12) $destino = "Copia Certificada de Autorizaci&oacute;n de Residencia (Personas de Nacionalidad extranjera)";
            if ($id_doc == 13) $destino = "Constancia de Nombramiento";
            if ($id_doc == 14) $destino = "Formato de Designaci&oacute;n de Beneficios (Copia Fotost&aacute;tica)";
            if ($id_doc == 15) $destino = "SAR o FORTE (Copia Fotost&aacute;tica)";
            if ($id_doc == 16) $destino = "Oficio de Transferencia";
            if ($id_doc == 17) $destino = "Carta Responsiva";
            if ($id_doc == 18) $destino = "Hoja de Afiliaci&oacute;n";


	$select_id_gral="select id_expediente_gral from expediente_general where id_documento = $id_doc and rfc ='$rfc'";
	$consulta_id_gral=ejecutar_sql($select_id_gral);
	$id_gral = $consulta_id_gral->fields('id_expediente_gral');

	if ($id_gral == null) $id_gral=0;

	$consulta_registros="select id_expediente_detalle, nombre_documento, documento_especifico, f_actualizacion, hora from expediente_detallado where id_expediente_gral = $id_gral order by id_expediente_detalle";
	$registro=ejecutar_sql($consulta_registros);
	
	$id = "non";
	$contador = 0;

	if(!$registro->EOF)
	echo "  <table width='40%' align='center' border=0>
		  <tr> <td align='center' colspan = '5'> <h3> $destino </h3> </td> </tr> <tr><td>&nbsp;</td></tr>
	          <tr>
			<th align='center' > No </th>
			<th align='center' > Documento</th>
			<th align='center' > Fecha Actualizaci&oacute;n </th>
			<th align='center' > Visualizar</th>
			<th align='center' > Eliminar</th>  <tr>";
	else	echo " <table align='center' title='No hay documentos almacenados de este empleado'>
		  <tr align='center'>
			<td> <i> No hay $destino de este empleado </i></th>
		  </tr>";

	$registro->MoveFirst();

	while(!$registro->EOF){
		++$contador;
		$id_expediente_detalle = $registro->fields('id_expediente_detalle');
		$nombre_documento = $registro->fields('nombre_documento');
		$documento_especifico = $registro->fields('documento_especifico');
		$f_actualizacion = $registro->fields('f_actualizacion');
		$hora = $registro->fields('hora');

		echo "<tr id='$id'>
			<td align='center'> $contador </td>
			<td align='center'> $nombre_documento </td>	
			<td align='center'>".formato_de_fecha($f_actualizacion).' a la'.((substr($hora,1,2) < 2)?' ':'s').' '.$hora."</td>		
			<td align='center'>";
	?>
				<a href="visualizar_documento.php?var=<? echo $id_expediente_detalle;?>"> 
					<img border="0" src="../img/ver.png" width='25' height='25' title="Presione este bot&oacute;n para visualizar el documento"/>
				</a>
			</td>
			<td align='center'>
				<a href="borrar_documento.php?var=<? echo $id_expediente_detalle;?>&rfc_=<? echo $rfc;?>" onclick="return confirm('&iquest;Est&aacute; seguro que desea eliminar este documento ?');">
					<img border="0" src="../img/eliminar.gif" title="Presione este bot&oacute;n para eliminar este documento"/>
				</a>
			</td>
	<?php
		echo "</tr>";

		$id = ($id=="non")?"par":"non";
		$registro->MoveNext();
		}
	if ($contador > 0){	
	echo "<tr><th colspan = '4'>Total</th><th>".$contador."</th> <tr><td>&nbsp;</td></tr><tr><td colspan = '5'><i>  ";
		if ($id_doc ==  9)	echo "* Los documentos listados pueden ser T&iacute;tulo, C&eacute;dula Profesional o Certificado de Estudios";
		if ($id_doc == 15)	echo "* Los documentos listados pueden ser SAR o FORTE";
	echo "<i></td></tr>";
	}
	?>
		</table>
</body>
</html>
