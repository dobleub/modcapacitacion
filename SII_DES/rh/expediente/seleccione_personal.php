<?php
	/********************************************************
		Seleccione Personal

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione el documento del Empleado ::.</title>
	</head>
<body>

	<?php	$regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'";	?>

	<h2 align="center"> Secci&oacute;n para consultar/Actualizar el expediente de los Empleados (Activos)</h2>

	<form name="solicitud_ver" method="post" action="seleccione_documentacion.php">
	<h3 align="center"> Seleccione el Empleado: </h3>
	<table width="30%" align="center">
		<tr align="center"> <th>Ver: </th> </tr>
		<tr align="center">
		  <th>
			<select name="rfc_personal" title="Seleccione el nivel de estudios" onChange="javascript:document.solicitud_ver.submit();">
			<?php
			$consulta_rfc="select rfc, apellidos_empleado, nombre_empleado from personal where (status_empleado = '02') order by apellidos_empleado, nombre_empleado, rfc";
			$datos_rfc=ejecutar_sql($consulta_rfc);
			if(!$datos_rfc->rowcount()) echo "<option value='0' selected> No hay personal dado de alta </option>"; 
			else
			   { echo "<option value='0' selected> -- Seleccione Personal -- </option>";
			     while(!$datos_rfc->EOF){
				echo '<option value="'.$datos_rfc->fields('rfc').'">'.$datos_rfc->fields('rfc').' - '.$datos_rfc->fields('apellidos_empleado').' '.$datos_rfc->fields('nombre_empleado').'</option>';
				$datos_rfc->MoveNext();
				}
			   }
			?>
			</select>
		   </th>
		</tr>
	</table>
	<br>
	</form>

	<form name="personal" method="post" >
		<br />
		<div align="center">
		  <input type="button" value="Cancelar" class="boton" tabindex="5" onClick="<?php echo $regresar; ?>" title="Presione este bot&oacute;n para cancelar la operaci&oacute;n" />
		</div>
	</form>
</body>
</html>
