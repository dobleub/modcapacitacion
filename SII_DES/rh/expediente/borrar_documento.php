<?php
	/********************************************************
		Procesar borrado de documentos del expediente del Empleado
		http://pixelar.me/subir-imagenes-php/
		http://www.peruhardware.net/foros/showthread.php?t=16479
		http://cyberexplorador.wordpress.com/2010/08/26/anadir-imagenes-a-una-base-de-datos-y-cargarlas-usando-php-y-mysql/
		http://www.desarrolloweb.com/articulos/1307.php
		http://omarvasquezb.wordpress.com/2009/11/19/funcion-javascript-para-cerrar-la-ventana-actual-para-ie678-google-chrome-menos-firefox/
		http://emilio.aesinformatica.com/2007/05/03/subir-una-imagen-con-php/

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione la imagen del Empleado ::.</title>
	</head>
<body>
	<h3 align="center"> Operaci&oacute;n en proceso ...</h3>
<?php

	$rfc = $_GET['rfc_'];
	$regresar = "javascript: document.location = 'seleccione_documentacion.php?rfc_personal=$rfc_'"; // seleccione_personal

	$id_expediente_detalle = $_GET['var'];
	// Obtener el ID padre
		$select_id_gral="select nombre_documento, ruta from expediente_detallado where id_expediente_detalle = $id_expediente_detalle";
		$consulta_id_gral=ejecutar_sql($select_id_gral);
		$nombre_documento = $consulta_id_gral->fields('nombre_documento');
		$ruta = $consulta_id_gral->fields('ruta');

	$ruta_documento = $ruta.$nombre_documento;

	if (!unlink($ruta_documento)){	//si no puede eliminar el documento se muestra un mensaje 
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo 'Error, intente mas tarde...!'; ?>"
				alert(msg)
				<?php echo $regresar; ?>
			</script>
			<?php
	}
	else{	//si puede eliminarse el documento se elimina el registro de la BD
		$delete_documento = "delete from expediente_detallado where id_expediente_detalle = $id_expediente_detalle";
		$delete_documento_ = ejecutar_sql($delete_documento);
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo 'Archivo removido con exito'; ?>"
				alert(msg)
				<?php echo $regresar; ?>	
			</script>
			<?php
	}
	$msg = "Error, por favor elimine manualmente el registro para $nombre_documento"; 
?>

<script language="javascript" type="text/javascript">
	msg = '<?php echo $msg; ?>'
	alert(msg)
	<?php echo $regresar; ?>
</script>
 
</body>
</html>
