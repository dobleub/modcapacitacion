<?php
	/********************************************************
		Procesar Imagen
		http://pixelar.me/subir-imagenes-php/
		http://www.peruhardware.net/foros/showthread.php?t=16479
		http://cyberexplorador.wordpress.com/2010/08/26/anadir-imagenes-a-una-base-de-datos-y-cargarlas-usando-php-y-mysql/
		http://www.desarrolloweb.com/articulos/1307.php

		Desarrollado por: Miguel Angel Cruz Sandoval
		Instituto Tecnológico de Tlaxiaco
		24 de Noviembre de 2011
	********************************************************/
	require_once("../../../includes/config.inc.php");
	require_once($CFG->funciones_phpDir."/funciones_permisos.php");
	seguridad('DRH');
	//MIIIIII__      permiso_sobre_funcion($_SESSION['susr'], 40);
	//$web->Seguridad($_SESSION['susr'],8); 
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo $CFG->cssDir; ?>/tec_estilo.css" />
		<script type="text/javascript" src="<?php echo $CFG->funciones_jsDir; ?>/funciones.js"></script>
		<title>.:: Seleccione el documento del Empleado ::.</title>
	</head>
<body>
	<?php $regresar = "javascript: document.location = '".$CFG->rootDirServ."/".$_SESSION["pagina_inicio"]."bienvenida.php'"; ?>

	<h2 align="center"> Documentaci&oacute;n del Empleado </h2>
<?php
	if (is_uploaded_file($_FILES['imagen']['tmp_name']))
		{// Ruta donde se guardarán las imágenes
    		$directorio = 'documentos/';
 
    		// Recibo los datos de la imagen
    		$nombre = $_FILES['imagen']['name'];
    		$tipo = $_FILES['imagen']['type'];
    		$tamano = $_FILES['imagen']['size'];

		//revisar que sea jpg 
		if (($_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/pjpeg") &&  ($tamano > 10 and $tamano < 340000) ) // 3.4 MB 8444= 8.2 KB
			{ 
    			// Muevo la imagen desde su ubicación
    			// temporal al directorio definitivo
    			//move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre);

			//nuevo nombre para la imagen
			$nuevoNombre = time().".jpg";

			//mover la imagen
			move_uploaded_file($_FILES['imagen']['tmp_name'], "documentos/$nuevoNombre");

			//echo $destino = "documentos/".$nombre; 
			//copy($_FILES['imagen']['tmp_name'],$destino);
			} 
		else
			{
			?>
			<script language="javascript" type="text/javascript">
				msg = "<?php echo 'Formato no válido para fichero de imagen'; ?>"
				alert(msg)
				<?php echo $regresar; ?>
			</script>
			<?php
			}
		} 
	else 
		{
		$msg = "Error al cargar archivo: " . $_FILES['imagen']['name'];
		?>
		<script language="javascript" type="text/javascript">
			msg = "<?php echo $msg; ?>"
			alert(msg)
			<?php echo $regresar; ?>
		</script>
		<?php
		}

	$msg = "El archivo ha sido cargado correctamente"; 
?>

<script language="javascript" type="text/javascript">
	msg = '<?php echo $msg; ?>'
	alert(msg)
	<?php echo $regresar; ?>
</script>
 
</body>
</html>
